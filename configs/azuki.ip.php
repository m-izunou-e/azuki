<?php

return [
    'allows' => array_map('trim', explode(',', env('AZUKI_ALLOW_IPADDRESS', '127.0.0.1,192.168.0.1/24'))),
];
