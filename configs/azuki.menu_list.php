<?php

return [
    'system' => [
        [
            'url'  => 'directors',
            'name' => 'システム管理者',
            'icon' => 'torso',
        ],
        [
            'url'  => 'managers',
            'name' => '運用管理者',
            'icon' => 'torso-business',
        ],
        [
            'url'  => 'users',
            'name' => 'ユーザー',
            'icon' => 'torsos-all',
        ],
        [
            'url'  => 'system-roles',
            'name' => 'ロール管理',
            'icon' => 'key',
        ],
        [
            'url'  => 'organizations',
            'name' => '組織管理',
            'icon' => 'at-sign',
        ],
        [
            'url'  => 'access-logs',
            'name' => 'アクセスログ',
            'icon' => 'clipboard-notes',
        ],
        [
            'url'  => 'login-logs',
            'name' => 'ログインログ',
            'icon' => 'clipboard-notes',
        ],
        [
            'url'  => 'operation-logs',
            'name' => 'オペレーションログ',
            'icon' => 'clipboard-notes',
        ],
        [
            'url'  => 'system-logs',
            'name' => 'システムログ',
            'icon' => 'clipboard-notes',
        ],
        [
            'url'  => 'execute-queue-status',
            'name' => 'コマンド実行状況',
            'icon' => 'clipboard-notes',
        ],
        [
            'url'  => 'environment',
            'name' => '環境',
            'icon' => 'widget',
        ],
        [
            'url'  => 'structure',
            'name' => 'システム構成',
            'icon' => 'flag',
        ],
    ],
    'manage' => [
        [
            'url'  => 'managers',
            'name' => 'スタッフ',
            'icon' => 'torso-business',
        ],
        [
            'url'  => 'users',
            'name' => 'ユーザー',
            'icon' => 'torsos-all',
        ],
        [
            'url'  => 'execute-queue-status',
            'name' => 'コマンド実行状況',
            'icon' => 'clipboard-notes',
        ],
    ],
];
