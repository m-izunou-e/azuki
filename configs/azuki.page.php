<?php

return [
    'all' => [
        'global' => [
//            'favicon'   => '/img/favicon.ico',
            'favicon'   => '/vendor/azuki/img/favicon.ico',
            'copylight' => '2018-2021 &copy; Azuki Project.',
        ],
        'maxRowPerList' => 50,
    ],
    'system' => [
        'login' => [
            'logoTitle' => 'システム管理',
            'logo'      => '/*logo.png*/',
            'loginIs'   => 'email',
            'reset'     => true,
        ],
    ],
    'manage' => [
        'login' => [
            'logoTitle' => 'Azuki 管理画面',
            'logo'      => '',
            'loginIs'   => 'id',
            'reset'     => true,
        ],
    ],
    'common' => [
        'login' => [
            'logoTitle' => 'ログイン',
            'logo'      => '',
            'loginIs'   => 'id',
            'reset'     => true,
        ],
    ],
];
