<?php

return [
    'userIcon'        => 'fi-torso',
    'infoIcon'        => 'fi-info',
    'menuIcon'        => 'fi-list',
    'homeIcon'        => 'fi-home',
    'userRegistIcon'  => 'fi-pencil',

    'csvFileIcon'     => 'fi-page-csv',
    'uploadIcon'      => 'fi-upload',
    'downloadIcon'    => 'fi-download',

    'trashIcon'       => 'fi-trash',

    'listDetailIcon'   => 'fi-eye',
    'listEditIcon'     => 'fi-pencil',
    'listDeleteIcon'   => 'fi-x',
    'listCopyIcon'     => 'fi-page-copy',
    'listPreviewIcon'  => 'fi-page',
    'listRefreshIcon'  => 'fi-refresh',
    'listDownloadIcon' => 'fi-download',

    'plusIcon'        => 'fi-plus',
    'minusIcon'       => 'fi-minus',

    'arrowUpIcon'     => 'fi-arrow-up',
    'arrowDownIcon'   => 'fi-arrow-down',
];
