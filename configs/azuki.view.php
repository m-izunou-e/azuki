<?php

return [
    'paths' => [
        resource_path('views/vendor/azuki'),
        base_path('vendor/la-cuppe/azuki/resources/views'),
    ],
];
