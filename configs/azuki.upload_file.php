<?php
/**
 *
 *
 *
 */

return [
    'type' => [
        'default' => [
            'class'   => \Azuki\App\Services\Uploader\FileUploader::class,
            'cache'   => true,
            'size'    => 1024,
            'storage' => 'FileManagerDatabaseStorage', // or FileManagerDiskStorage
            'exec'    => false,
        ],
        'text1' => [
            'class'   => \Azuki\App\Services\Uploader\FileUploader::class,
            'cache'   => true,
            'size'    => 1024,
            'storage' => 'FileManagerDatabaseStorage', // or FileManagerDiskStorage
            'exec'    => false,
        ],
        'image1' => [
            'class'     => \Azuki\App\Services\Uploader\ImageUploader::class, // 自動判別は'UploaderFactory'
            'cache'     => false,
            'size'      => 15*1024*1024,
            'width'     => 1024,
            'height'    => 768,
            'storage'   => 'FileManagerDatabaseStorage', // or FileManagerDiskStorage
            'exec'      => false,
            'thumbnail' => [
                'auto'   => true,
                'width'  => 300,
                'height' => 300,
                'ext'    => 'jpeg',
            ],
            'convert'   => true,
            'mimeType'  => [
                //'image/*',
                'image/jpeg',
                'image/gif',
                'image/png',
                'image/webp',
                'image/bmp',
                'image/svg+xml',
                'image/tiff',
                'image/vnd.microsoft.icon',
                'image/x-icon',
                'image/psd',
                'image/vnd.adobe.photoshop',
                'image/photoshop',
                'image/x-photoshop',
            ],
        ],
        'movie1' => [
            'class'     => \Azuki\App\Services\Uploader\MovieUploader::class, // 自動判別は'UploaderFactory'
            'cache'     => false,
            'size'      => 1*1024*1024*1024,
            'partsSize' => 15*1024*1024,
            'storage'   => 'FileManagerDatabaseStorage', // or FileManagerDiskStorage
            'exec'      => false,
            'thumbnail' => [
                'auto'   => true,
                'width'  => 300,
                'height' => 300,
                'ext'    => 'jpeg',
            ],
            'mimeType'  => [
                'video/*',
                //'video/mp4',
            ],
        ],
        'csv1' => [
            'class'   => \Azuki\App\Services\Uploader\CsvUploader::class,
            'cache'   => false,
            'size'    => 1*1024*1024,
            'storage' => 'FileManagerDiskStorage',
            'exec'    => true,
            'execOpt' => [
                'sync' => false, // 同期か非同期か
            ]
        ],
    ],

];
