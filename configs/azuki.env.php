<?php

return [
    'cron' => [
        [
            'schedule' => '20 3 * * *',
            'command'  => 'exec:delete-logs-in-database',
        ],
    ],
    'dusk_test' => env('DUSK_TEST', false),
];
