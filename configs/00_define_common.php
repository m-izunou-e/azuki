<?php
/**
 * デファイン定義を行う
 * デファインをファイルで分けて管理することも考慮して_commonを付けたファイル名にしている
 */

if( !defined('DFINED_DEFINE_COMMON') ) {
    define('DFINED_DEFINE_COMMON', 1);

    /**
     * 基本となるアプリケーションサービス名定義
     */
    define('BASE_APP_ACCESSOR', 'la-cuppe.app');

    /**
     * コントローラー、viewに関するデファイン定義
     */
    // ユーザー種別
    define('USER_KIND_DIRECTORS', 1);
    define('USER_KIND_MANAGERS',  2);
    define('USER_KIND_USERS',     3);
    define('USER_KIND_NONE',     99);

    // フォームのモード。入力か表示か→テンプレートでこの変数を基に表示切替をしている
    define('FORM_MODE_INPUT', 1);
    define('FORM_MODE_VIEW',  2);
    
    // 一覧表示での表示タイプ
    define('CONTROL_TYPE_TEXT',     1);
    define('CONTROL_TYPE_SELECT',   2);
    define('CONTROL_TYPE_DATE',     3);
    define('CONTROL_TYPE_DATETIME', 4);
    define('CONTROL_TYPE_VALID',    5);
    define('CONTROL_TYPE_IMAGE',    6);
    
    define('CONTROL_TYPE_BOOLEAN',       20);
    define('CONTROL_TYPE_CURRENCY',      50);
    define('CONTROL_TYPE_TRUNCATE_TEXT', 51);
    define('CONTROL_TYPE_CONTROLLER',    99);
    
    // 一覧の詳細・編集・削除などのコントロールボタンの種類
    define('CTRL_DETAIL',     1);
    define('CTRL_EDIT',       2);
    define('CTRL_EDITONLIST', 3);
    define('CTRL_DELETE',     4);
    
    define('CTRL_COPY',         10);
    define('CTRL_PREVIEW',      11);
    define('CTRL_CHANGE_ORDER', 12);
    define('CTRL_RESTORE',      13);
    
    // 必須・任意の定義
    define('IS_REQUIRED', 1);
    define('NOT_REQUIRED', 2);

    // 権限の許可・拒否の定義
    define('AUTHRITY_ALLOW', 1);
    define('AUTHRITY_DENY',  2);
    
    
    define('SWITCH_STATUS_IS_RUN',   1);
    define('SWITCH_STATUS_IS_STOP',  2);
    
    // 複数選択肢の確認画面・詳細表示の表示に関する定義
    define('VIEW_SELECT_PREFIX',    '');
    define('VIEW_SELECT_SEPARATOR', ' ');
    
    // 一時ファイル名のプレフィクス
    define('UPLOAD_TEMP_FILE_PREFIX', 'temp_');

    /**
     * データベース定義に関するデファイン
     *
     */
    
    // オペレーションログモード
    define('OPLOG_MODE_CREATE', 1);
    define('OPLOG_MODE_UPDATE', 2);
    define('OPLOG_MODE_DELETE', 3);

    // execute_target_queue
    define('ETQ_STATUS_NEW',       1);
    define('ETQ_STATUS_RUNNING',   2);
    define('ETQ_STATUS_SUCCESS',   3);
    define('ETQ_STATUS_ERROR',     4);
    define('ETQ_STATUS_EXCEPTION', 5);

    
    // system_log_status
    define('SL_STATUS_INFO',           1);
    define('SL_STATUS_ALERT',          2);
    define('SL_STATUS_FAULT',          3);
    define('SL_STATUS_RESTORE',        4);
    define('SL_STATUS_MANUAL_RESTORE', 5);
    define('SL_STATUS_OTHER',          6);
    // system_log_kind
    define('SL_KIND_DATA',        10);
    define('SL_KIND_VALIDATE',    11);
    define('SL_KIND_NETWORK',     30);
    define('SL_KIND_SCHEDULE',    50);
    define('SL_KIND_MAIL',        70);
    define('SL_KIND_DATA_DELETE', 12);
    
    // アップロードデータの種類
    define('UPLOAD_DATA_CATEGORY_ORIGINAL',  1);
    define('UPLOAD_DATA_CATEGORY_THUMBNAIL', 2);
    
    /**
     * アクセス階層の定義
     * エラー画面表示時のアクセス階層の判定に使用する定義
     */
    define('HTTP_ACCESS_LEVEL_SYSTEM', 1);
    define('HTTP_ACCESS_LEVEL_MANAGE', 2);
    define('HTTP_ACCESS_LEVEL_COMMON', 3);

    /**
     * システム切替種類定義
     */
    define('SWITCH_KIND_MAINTENANCE_MODE', 'change-maintenance');
    define('SWITCH_KIND_AUTO_DELETE_LOGS', 'change-auto-delete-logs');
    
    /**
     * SNSプロバイダー種類定義
     */
    define('SNS_PROVIDERS_GOOGLE',   1);
    define('SNS_PROVIDERS_FACEBOOK', 2);
    define('SNS_PROVIDERS_TWITTER',  3);
    define('SNS_PROVIDERS_GITHUB',   4);
    define('SNS_PROVIDERS_APPLE',    5);

}
