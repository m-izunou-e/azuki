<?php

return [

    /**
     * 基本システムの実体置き換えのための設定
     * 基本システムで使用しているコントローラ、モデル、バリデーターをオーバーライドする際に、
     * ここに設定を追加することで設定したクラスを実体として読み込むようになります
     */
    'aliases' => [
        // コントローラの実体を上書きする
        'controllers' => [
            // システム管理画面
            'system' => [
                //'index'          => , // [ダッシュボード]ページ
                //'login'          => , // [ログイン]ページ
                //'profile'        => , // [プロフィール]ページ
                //'directors'      => , // [システム管理者管理]ページ
                //'managers'       => , // [サイト管理者管理]ページ
                //'users'          => , // [ユーザー管理]ページ
                //'systemroles'    => , // [ロール管理]ページ
                //'organizations'  => , // [組織管理]ページ
                //'accesslogs'     => , // [アクセスログ]ページ
                //'loginlogs'      => , // [ログインログ]ページ
                //'operationlogs'  => , // [操作ログ]ページ
                //'systemlogs'     => , // [システムログ]ページ
                //'eqs'            => , // [コマンド実行状況]ページ
                //'resetpassword'  => , // [パスワードリセット]機能を提供するコントローラ
                //'fileupload'     => , // [ファイルアップロード]機能を提供するコントローラ
            ],
            // サイト管理画面
            'manage' => [
                //'index'          => , // [ダッシュボード]ページ
                //'login'          => , // [ログイン]ページ
                //'profile'        => , // [プロフィール]ページ
                //'managers'       => , // [サイト管理者管理]ページ
                //'users'          => , // [ユーザー管理]ページ
                //'eqs'            => , // [コマンド実行状況]ページ
                //'resetpassword'  => , // [パスワードリセット]機能を提供するコントローラ
                //'fileupload'     => , // [ファイルアップロード]機能を提供するコントローラ
            ],
            // フロント画面
            'common' => [
                //'index'          => , // [ダッシュボード]ページ
                //'login'          => , // [ログイン]ページ
                //'mypage'         => , // [マイページ]
                //'regist'         => , // [ユーザー登録]ページ
                //'resetpassword'  => , // [パスワードリセット]機能を提供するコントローラ
                //'uploadfileview' => , // [アップロードファイル表示]機能を提供するコントローラ
            ],
        ],
        // モデルの実体を上書きする
        'models' => [
            //'directors'     => , // システム管理者用モデル
            //'managers'      => , // サイト管理者用モデル
            //'users'         => , // ユーザー用モデル
            //'systemroles'   => , // システムログ用モデル
            //'organizations' => , // 組織用モデル
            //'accesslogs'    => , // アクセスログ用モデル
            //'loginlogs'     => , // ログインログ用モデル
            //'operationlogs' => , // 操作ログ用モデル
            //'systemlogs'    => , // システムログ用モデル
            //'eqs'           => , // コマンド実行状況用モデル
        ],
        // バリデータの実体を上書きする
        'validators' => [
            'system' => [
                //'profile'       => , // プロフィール
                //'directors'     => , // システム管理者
                //'managers'      => , // サイト管理者
                //'users'         => , // ユーザー
                //'systemroles'   => , // ロール
                'organizations' => \Azuki\App\Http\Validation\SharedValidator::class,
            ],
            'manage' => [
                //'managers' => , // サイト管理者
                //'users'    => , // ユーザー
            ],
            'common' => [
                //'mypages' => ,
            ],
        ],
    ],
];
