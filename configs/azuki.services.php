<?php

return [

    // Socialite利用の設定
    'socialList'   => [
        SNS_PROVIDERS_GOOGLE   => 'google',
        SNS_PROVIDERS_FACEBOOK => 'facebook',
        SNS_PROVIDERS_TWITTER  => 'twitter',
    ],
    'snsProviders' => explode(',', env('ENABLES_SNS_PROVIDERS', '')),
    'google' => [
        'client_id'     => env('GOOGLE_CLIENT_ID'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET'),
        'redirect'      => env('APP_URL') . '/login/google/callback',
        'kind'          => SNS_PROVIDERS_GOOGLE,
//        'img_path'      => '/vendor/azuki/img/sns-login/google/vector/btn_google_light_normal_ios.svg',
        'img_path'      => '/vendor/azuki/img/sns-login/google/1x/btn_google_signin_light_normal_web.png',
//        'img_path'      => '/vendor/azuki/img/sns-login/google/2x/btn_google_signin_light_normal_web@2x.png',
    ],
    'facebook' => [
        'client_id'     => env('FACEBOOK_APP_ID'),
        'client_secret' => env('FACEBOOK_APP_SECRET'),
        'redirect'      => env('APP_URL') . '/login/facebook/callback',
        'kind'          => SNS_PROVIDERS_FACEBOOK,
        'img_path'      => '/vendor/azuki/img/sns-login/facebook/login_logo.png',
    ],
    'twitter' => [
        'client_id'     => env('TWITTER_APP_ID'),
        'client_secret' => env('TWITTER_APP_SECRET'),
        'redirect'      => env('APP_URL') . '/login/twitter/callback',
        'kind'          => SNS_PROVIDERS_TWITTER,
        'img_path'      => '/vendor/azuki/img/sns-login/twitter/square/Twitter_social_icons-blue.svg',
    ],

];
