<?php
/**
 * マスター定義ファイル
 * 
 * 定義方法は3種類。
 * １．マスターデータのリストを記載する方法
 * ２．DBで管理されているマスターデータを指定する方法
 * ３．ヘルパに作成したマスターデータ生成用のファンクションを指定する方法
 *
 * １．の場合
 *     '識別子' => [
 *         'type' => 'file',
 *         'list' => [
 *             '1' => ['order' => '1', 'value' => 'xxxxx', 'name' => 'yyyyyy'],
 *             '2' => ['order' => '2', 'value' => 'xxxxx', 'name' => 'yyyyyy'],
 *             ・・・・・・・・
 *         ],
 *     ],
 *
 * ２．の場合
 *     '識別子' => [
 *         'type'  => 'db',
 *         'model' => '対応するモデルクラス名',
 *         'order' => '並び順を決めるカラム名（デフォルトはorder）',
 *         'cond'  => ['対象カラム名', '値'],
 *     ],
 *
 * ３．の場合
 *     '識別子' => [
 *         'type'   => 'method',
 *         'method' => 'ファンクション名'
 *     ],
 * という記載方法で指定する
 */

return [
    // ユーザー種類定義
    'userKindLabel' => [
        'type' => 'file',
        'list' => [
            '1'  => ['order' => '1',  'value'=> USER_KIND_DIRECTORS, 'name' => 'システム管理者'],
            '2'  => ['order' => '2',  'value'=> USER_KIND_MANAGERS,  'name' => '運営管理者'],
            '3'  => ['order' => '3',  'value'=> USER_KIND_USERS,     'name' => '一般ユーザー'],
            '99' => ['order' => '99', 'value'=> USER_KIND_NONE,      'name' => 'その他'],
        ],
    ],
    // 管理ユーザー役職リスト
    'directorRoles' => [
        'type'  => 'db',
        'model' => '\\Azuki\\App\\Models\\SystemRoles',

    ],
    // 管理ユーザー役職リスト
    'managerRoles' => [
        'type'  => 'db',
        'model' => '\\Azuki\\App\\Models\\ManageRoles',
    ],
    // 一般ユーザー種類リスト
    'userRoles' => [
        'type'  => 'db',
        'model' => '\\Azuki\\App\\Models\\CommonRoles',
    ],
    // 定義
    'roleAuthoritiesLabel' => [
        'type' => 'file',
        'list' => [
            '1' => ['order' => '1', 'value'=> AUTHRITY_ALLOW, 'name' => '許可'],
            '2' => ['order' => '2', 'value'=> AUTHRITY_DENY,  'name' => '拒否'],
        ],
    ],
    // ログイン可否表示
    'enableLoginLabel' => [
        'type' => 'file',
        'list' => [
            '1' => ['order' => '1', 'value'=> '1', 'name' => '可'],
            '2' => ['order' => '2', 'value'=> '2', 'name' => '不可'],
        ],
    ],
    // 管理ユーザー有効無効表示
    'validManagerLabel' => [
        'type' => 'file',
        'list' => [
            '1' => ['order' => '1', 'value'=> '1', 'name' => '有効'],
            '2' => ['order' => '2', 'value'=> '2', 'name' => '無効'],
        ],
    ],
    // 所属リスト->組織設定にて定義
    'managerBelongs' => [
        'type'  => 'db',
        'model' => '\\Azuki\\App\\Models\\Organizations',

    ],

    'executeKind' => [
        'type'   => 'method',
        'method' => 'createListFromConfig',
        'conf'   => 'azuki.execute_type',
        'valkey' => 'type_name',
    ],
    'execQueuStatusLabel' => [
        'type' => 'file',
        'list' => [
            '1'  => ['order' => '1',  'value'=> ETQ_STATUS_NEW,       'name' => '新規登録・未実行'],
            '2'  => ['order' => '2',  'value'=> ETQ_STATUS_RUNNING,   'name' => '実行中/途中停止'],
            '3'  => ['order' => '3',  'value'=> ETQ_STATUS_SUCCESS,   'name' => '正常終了'],
            '4'  => ['order' => '4',  'value'=> ETQ_STATUS_ERROR,     'name' => 'エラーあり'],
            '5'  => ['order' => '5',  'value'=> ETQ_STATUS_EXCEPTION, 'name' => '異常終了'],
        ],
    ],

    //----
    // システムログ関係
    //  ------------
    'systemLogStatusLabel' => [
        'type' => 'file',
        'list' => [
            '1'  => ['order' => '1',  'value'=> SL_STATUS_INFO,           'name' => '正常'],
            '2'  => ['order' => '2',  'value'=> SL_STATUS_ALERT,          'name' => '警告'],
            '3'  => ['order' => '3',  'value'=> SL_STATUS_FAULT,          'name' => '障害発生'],
            '4'  => ['order' => '4',  'value'=> SL_STATUS_RESTORE,        'name' => '障害自動復旧'],
            '5'  => ['order' => '5',  'value'=> SL_STATUS_MANUAL_RESTORE, 'name' => '障害手動復旧'],
            '7'  => ['order' => '7',  'value'=> SL_STATUS_OTHER,          'name' => 'その他'],
        ],
    ],
    'systemLogKindLabel' => [
        'type' => 'file',
        'list' => [
            '1'  => ['order' => '51', 'value'=> SL_KIND_DATA,        'name' => 'データ登録・更新'],
            '2'  => ['order' => '52', 'value'=> SL_KIND_VALIDATE,    'name' => 'バリデーション'],
            '3'  => ['order' => '53', 'value'=> SL_KIND_NETWORK,     'name' => '通信'],
            '4'  => ['order' => '54', 'value'=> SL_KIND_SCHEDULE,    'name' => '定期実行'],
            '5'  => ['order' => '55', 'value'=> SL_KIND_MAIL,        'name' => 'メール送信'],
            '6'  => ['order' => '56', 'value'=> SL_KIND_DATA_DELETE, 'name' => 'データ削除'],
        ],
    ],

    // 操作ログ操作種類定義
    'opModeLabel' => [
        'type' => 'file',
        'list' => [
            '1'  => ['order' => '1',  'value'=> OPLOG_MODE_CREATE, 'name' => '新規作成'],
            '2'  => ['order' => '2',  'value'=> OPLOG_MODE_UPDATE, 'name' => '編集'],
            '3'  => ['order' => '3',  'value'=> OPLOG_MODE_DELETE, 'name' => '削除'],
        ],
    ],
 
    // 都道府県
    'pref' => [
        'type' => 'file',
        'list' => [
            '1'  => ['order' => '1',  'value'=> '1',  'name' => '北海道'],
            '2'  => ['order' => '2',  'value'=> '2',  'name' => '青森県'],
            '3'  => ['order' => '3',  'value'=> '3',  'name' => '岩手県'],
            '4'  => ['order' => '4',  'value'=> '4',  'name' => '宮城県'],
            '5'  => ['order' => '5',  'value'=> '5',  'name' => '秋田県'],
            '6'  => ['order' => '6',  'value'=> '6',  'name' => '山形県'],
            '7'  => ['order' => '7',  'value'=> '7',  'name' => '福島県'],
            '8'  => ['order' => '8',  'value'=> '8',  'name' => '茨城県'],
            '9'  => ['order' => '9',  'value'=> '9',  'name' => '栃木県'],
            '10' => ['order' => '10', 'value'=> '10', 'name' => '群馬県'],
            '11' => ['order' => '11', 'value'=> '11', 'name' => '埼玉県'],
            '12' => ['order' => '12', 'value'=> '12', 'name' => '千葉県'],
            '13' => ['order' => '13', 'value'=> '13', 'name' => '東京都'],
            '14' => ['order' => '14', 'value'=> '14', 'name' => '神奈川県'],
            '15' => ['order' => '15', 'value'=> '15', 'name' => '新潟県'],
            '16' => ['order' => '16', 'value'=> '16', 'name' => '富山県'],
            '17' => ['order' => '17', 'value'=> '17', 'name' => '石川県'],
            '18' => ['order' => '18', 'value'=> '18', 'name' => '福井県'],
            '19' => ['order' => '19', 'value'=> '19', 'name' => '山梨県'],
            '20' => ['order' => '20', 'value'=> '20', 'name' => '長野県'],
            '21' => ['order' => '21', 'value'=> '21', 'name' => '岐阜県'],
            '22' => ['order' => '22', 'value'=> '22', 'name' => '静岡県'],
            '23' => ['order' => '23', 'value'=> '23', 'name' => '愛知県'],
            '24' => ['order' => '24', 'value'=> '24', 'name' => '三重県'],
            '25' => ['order' => '25', 'value'=> '25', 'name' => '滋賀県'],
            '26' => ['order' => '26', 'value'=> '26', 'name' => '京都府'],
            '27' => ['order' => '27', 'value'=> '27', 'name' => '大阪府'],
            '28' => ['order' => '28', 'value'=> '28', 'name' => '兵庫県'],
            '29' => ['order' => '29', 'value'=> '29', 'name' => '奈良県'],
            '30' => ['order' => '30', 'value'=> '30', 'name' => '和歌山県'],
            '31' => ['order' => '31', 'value'=> '31', 'name' => '鳥取県'],
            '32' => ['order' => '32', 'value'=> '32', 'name' => '島根県'],
            '33' => ['order' => '33', 'value'=> '33', 'name' => '岡山県'],
            '34' => ['order' => '34', 'value'=> '34', 'name' => '広島県'],
            '35' => ['order' => '35', 'value'=> '35', 'name' => '山口県'],
            '36' => ['order' => '36', 'value'=> '36', 'name' => '徳島県'],
            '37' => ['order' => '37', 'value'=> '37', 'name' => '香川県'],
            '38' => ['order' => '38', 'value'=> '38', 'name' => '愛媛県'],
            '39' => ['order' => '39', 'value'=> '39', 'name' => '高知県'],
            '40' => ['order' => '40', 'value'=> '40', 'name' => '福岡県'],
            '41' => ['order' => '41', 'value'=> '41', 'name' => '佐賀県'],
            '42' => ['order' => '42', 'value'=> '42', 'name' => '長崎県'],
            '43' => ['order' => '43', 'value'=> '43', 'name' => '熊本県'],
            '44' => ['order' => '44', 'value'=> '44', 'name' => '大分県'],
            '45' => ['order' => '45', 'value'=> '45', 'name' => '宮崎県'],
            '46' => ['order' => '46', 'value'=> '46', 'name' => '鹿児島県'],
            '47' => ['order' => '47', 'value'=> '47', 'name' => '沖縄県'],
        ],
    ],
    // 性別
    'gender' => [
        'type' => 'file',
        'list' => [
            '1'  => ['order' => '1',  'value'=> '1',  'name' => '男性'],
            '2'  => ['order' => '2',  'value'=> '2',  'name' => '女性'],
            '3'  => ['order' => '99', 'value'=> '99', 'name' => '未選択'],
        ],
    ],
];
