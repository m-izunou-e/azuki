<?php
/**
 * CSVファイルでのデータの一括登録などの処理に関する設定を記述します
 *
 * 実際に処理を行うクラスの定義が必須です
 * 他設定は処理を実装したクラスの実装に依存して自由に定義してください
 * csvでのデータ登録・ダウンロードについては、columnの設定が必要です。
 *
 */

return [
    'default' => [
        'type_name' => 'モックCSV一括登録',
        'class'     => \Azuki\App\Services\Executer\MocCsvExecuter::class,
    ],
];
