<?php

return [
    /**
     * Azukiパッケージのバージョン、マイナーバージョンまで
     */
    'version'  => '6.0.1',
    
    /**
     * AzukiのExceptions/Handlerクラスを有効にするかどうか
     * 有効にするとHttpエラーなどが発生した際にAzukiのエラーページが表示される
     * ようになり、階層の判別が可能になります
     */
    'enable_azuki_error_handler' => env('AZUKI_ERROR_HANDLER', true),
     
    
    /**
     * azuki.auth.phpでauth.phpを上書き処理するかどうかの設定
     * authの設定はAzukiシステムが動作するにあたり上書きする必要があるため
     * 原則上書き設定としています
     * 認証周りに関して独自に拡張や変更を行った際にauth.php側を有効とする必要が
     * ある場合、このフラグをfalseにすることで上書き処理をしないようになります
     *
     * なお、config/azuki.auth.phpとAzukiパッケージ内のazuki.auth.php両方存在している
     * 場合（publishされている場合）config/azuki.auth.phpを使用します
     * ですので、この設定をtrueのまま、config/azuki.auth.phpを修正することも可能です
     */
    'auth'     => [
        'overwrite' => true,
    ],
    
    /**
     * azukiシステムによるルーティングの追加処理の有効・無効を切り替えます
     * これを無効化するとAzukiシステムのルーティングがすべて無効化されます
     */
    'routing'  => true,
    
    /**
     * 組織機能の有効・無効を切り替えます
     * 組織機能を有効にすると、組織階層にて所属が有効になります
     * 初期設定はfalseです
     */
    'organizations' => env( 'AZUKI_ORGANIZATION_SUPPORT', false),
    
    /**
     * 一般ユーザー機能の有効・無効を切り替えます
     * 一般ユーザー機能を有効にすると、サイトへのユーザー登録・マイページ・ユーザー管理が
     * 下のurlの設定にて有効になります。
     * 初期設定はtrueです
     */
    'user_management' => env( 'AZUKI_CONTENTS_PRIVATE_USER', true),
    
    /**
     * 各コンテンツを無効化するための設定です
     * 表示上ならびにルーティングの無効化であり、機能の無効化ではありません
     * prefixの一致でルーティングやメニューからはじくようになっています
     */
    'contents' => [ // disabled にすると無効に
        'system' => env( 'AZUKI_CONTENTS_SYSTEM',       true)  === true  ? 'enabled' : 'disabled',
        'manage' => env( 'AZUKI_CONTENTS_MANAGE',       true)  === true  ? 'enabled' : 'disabled',
        'users'  => env( 'AZUKI_CONTENTS_PRIVATE_USER', true)  === true  ? 'enabled' : 'disabled',
        
        'organizations'  => env('AZUKI_ORGANIZATION_SUPPORT',  false) === true ? 'enabled' : 'disabled',
        'access-logs'    => env('AZUKI_RECORD_ACCESS_LOGS',    false) === true ? 'enabled' : 'disabled',
        'login-logs'     => env('AZUKI_RECORD_LOGIN_LOGS',     false) === true ? 'enabled' : 'disabled',
        'operation-logs' => env('AZUKI_RECORD_OPERATION_LOGS', false) === true ? 'enabled' : 'disabled',
        'system-logs'    => env('AZUKI_RECORD_SYSTEM_LOGS',    false) === true ? 'enabled' : 'disabled',

        'environment'    => env('AZUKI_ENVIRONMENT_SUPPORT', false) === true ? 'enabled' : 'disabled',
        'structure'      => env('AZUKI_STRUCTURE_SUPPORT',   false) === true ? 'enabled' : 'disabled',
    ],
    
    /**
     * urlレベルでアクセスの有効・無効を切り替える設定です
     * request->is()系での判定になります
     *
     */
    'url' => [ // disabled にすると無効に
        '/regist'         => env('AZUKI_CONTENTS_PRIVATE_USER', true) === true  ? 'enabled' : 'disabled',
        '/login'          => env('AZUKI_CONTENTS_PRIVATE_USER', true) === true  ? 'enabled' : 'disabled',
        '/mypage'         => env('AZUKI_CONTENTS_PRIVATE_USER', true) === true  ? 'enabled' : 'disabled',
        '/reset-password' => env('AZUKI_CONTENTS_PRIVATE_USER', true) === true  ? 'enabled' : 'disabled',
        '/system/users'   => env('AZUKI_CONTENTS_PRIVATE_USER', true) === true  ? 'enabled' : 'disabled',
        '/manage/users'   => env('AZUKI_CONTENTS_PRIVATE_USER', true) === true  ? 'enabled' : 'disabled',
    ],
    
    /**
     * 各種ログ機能の有効・無効を切り替えます
     * これら設定は機能の切り替えだけであり、表示上の切り替えはcontents側で行る必要があります
     */
    // アクセスログの記録をするかどうかの設定
    'record_access_logs' => env('AZUKI_RECORD_ACCESS_LOGS', false),
    // ログインログの記録をするかどうかの設定
    'record_login_logs' => env('AZUKI_RECORD_LOGIN_LOGS', false),
    // DB操作ログの記録をするかどうかのデフォルト値設定
    'record_operation_logs' => env('AZUKI_RECORD_OPERATION_LOGS', false),
    // システムログの記録をするかどうかのデフォルト値設定
    'record_system_logs' => env('AZUKI_RECORD_SYSTEM_LOGS', false),
    
    /**
     * domainごとのprefixに対するサブディレクトリ階層定義を行う
     * domein名 => [
     *     'prefix名' => 'URLサブディレクトリ階層名',
     * ]
     * という形で定義する。defaultは参考として定義しているがコード上必須になっているため削除不可
     *
     */
    'subDir' => [
        'default' => [
            'system' => 'system',
            'manage' => 'manage',
        ],
    ],
    
    /*
     * Laravel８以上の場合に使用するメンテナンスモードのバイパスシークレットトークン
     */
    'maintenance_secret' => env('AZUKI_MAINTENANCE_SECRET_TOKEN', 'access-to-system-bypass-maintenance-mode'),
];
