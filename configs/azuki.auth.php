<?php

return [
    'guards' => [
        'web' => [
            'driver'   => 'session',
            'provider' => 'users',
        ],
        'manage' => [
            'driver'   => 'session',
            'provider' => 'managers',
        ],
        'system' => [
            'driver'   => 'session',
            'provider' => 'directors',
        ],

        'api' => [
            'driver'   => 'token',
            'provider' => 'users',
        ],
    ],
    'providers' => [
        'users' => [
            'driver' => 'eloquent',
            'model'  => Azuki\App\Models\Users::class,
        ],
        'managers' => [
            'driver' => 'eloquent',
            'model'  => Azuki\App\Models\Managers::class,
        ],
        'directors' => [
            'driver' => 'eloquent',
            'model'  => Azuki\App\Models\Directors::class,
        ],

        // 'users' => [
        //     'driver' => 'database',
        //     'table' => 'users',
        // ],
    ],
    'passwords' => [
        'users' => [
            'provider' => 'users',
            'table'    => 'azuki_password_resets',
            'expire'   => 60,
            'throttle' => 60,
        ],
        'managers' => [
            'provider' => 'managers',
            'table'    => 'azuki_password_resets_managers',
            'expire'   => 60,
            'throttle' => 60,
        ],
        'directors' => [
            'provider' => 'directors',
            'table'    => 'azuki_password_resets_directors',
            'expire'   => 60,
            'throttle' => 60,
        ],
    ],
];
