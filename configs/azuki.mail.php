<?php
/**
 * メールに関する追加設定
 * ・メールログの設定
 * ・リターンパスの設定
 * ・各送信メールの設定
 * を記載。
 *
 */
$baseDir = realpath(dirname(__FILE__). '/../../') . '/';

return [
    // メール送信ログの設定。MailLogサービスにて使用
    'log' => [
        'driver' => 'database', // database or file
        'dir'    => $baseDir . 'storage/logs/mail/',
        'model'  => 'Azuki\App\Models\MailLog',
    ],
    
    // リターンパスの設定。AbstractMailにてすべての送信メールに対して設定するデフォルト値
    'returnPath' => env('MAIL_RETURN_PATH', 'test@example.com'),
    
    // 各送信メールの設定
    // 送信メールの種類をキーにして、件名、テンプレートファイル、CC,BCCなどを設定する。
    // 設定項目を増やす場合は、AbstractMailかそれを継承したMailableクラスでロジックを拡張する
    'kind' => [
        'resetPassword' => [
            'subject' => 'パスワードリセットのご案内',
            'view'    => 'auth.reset-password',
            'text'    => 'auth.reset-password',
//            'cc'      => [],
//            'bcc'     => [],
        ],
        'resetPasswordForManage' => [
            'subject' => 'パスワードリセット(管理ユーザー)のご案内',
            'view'    => 'auth.manage.reset-password',
            'text'    => 'auth.manage.reset-password',
        ],
        'resetPasswordForDirector' => [
            'subject' => 'パスワードリセット(管理ユーザー)のご案内',
            'view'    => 'auth.system.reset-password',
            'text'    => 'auth.system.reset-password',
        ],
    ],
];
