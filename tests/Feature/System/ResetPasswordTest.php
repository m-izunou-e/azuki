<?php
namespace Azuki\Tests\Feature\System;

use Azuki\App\Tests\Feature\System\AbstractFeatureTest;
use Azuki\App\Tests\Feature\ResetPasswordTrait;
use Azuki\App\Notifications\ResetPasswordForDirectorNotification as ResetPassword;

class ResetPasswordTest extends AbstractFeatureTest
{
    use ResetPasswordTrait;
    
    /**
     *
     *
     *
     */
    protected function createTestUser()
    {
        $dirctors = $this->getDirectorsFactory(10)->create();
        $dirctor = $dirctors->first();
        
        return $dirctor;
    }
    
    /**
     *
     *
     *
     */
    protected function getResetPasswordClass()
    {
        return ResetPassword::class;
    }
}
