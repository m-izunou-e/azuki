<?php
namespace Azuki\Tests\Feature\System;

use Azuki\App\Tests\Feature\System\AbstractFeatureTest;
use Azuki\App\Tests\Feature\PageAccessTestTrait;

class PageAccessTest extends AbstractFeatureTest
{
    use PageAccessTestTrait;
    
    /**
     *
     *
     */
    protected $accessTestTarget = [
        'index' => [
            'url' => '/',
        ],
        'profile' => [
            'url' => '/profile/detail',
        ],
        'directors' => [
            'url' => '/directors/list',
        ],
        'managers' => [
            'url' => '/managers/list',
        ],
        'users' => [
            'url' => '/users/list',
        ],
        'system-roles' => [
            'url' => '/system-roles/list',
        ],
        'access-logs' => [
            'url' => '/access-logs/list',
        ],
        'operation-logs' => [
            'url' => '/operation-logs/list',
        ],
        'system-logs' => [
            'url' => '/system-logs/list',
        ],
        'execute-queue-status' => [
            'url' => '/execute-queue-status/list',
        ],
    ];
    
    /**
     *
     *
     *
     */
    protected function createTestUser()
    {
        $dirctors = $this->getDirectorsFactory(10)->create();
        $dirctor = $dirctors->first();
        
        return $dirctor;
    }
}
