<?php
namespace Azuki\Tests\Feature\System;

use Azuki\App\Tests\Feature\System\AbstractFeatureTest;

class ManagersTest extends AbstractFeatureTest
{
    /**
     *
     *
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->seedMasterData();
    }

    /**
     *
     *
     */
    public function tearDown(): void
    {
        $this->truncateTables();
        parent::tearDown();
    }

    /**
     * @test
     *
     */
    public function テスト実装のためのテスト()
    {
        // ユーザーを作成
        $loginUser = $this->createTestUser();
        
        // スタッフを登録
        $this->createManagers();
        
        // ページアクセス
        $response = $this->actingAs($loginUser, $this->guard);
        $response = $response->get($this->baseUrl . '/managers/list');
        
        $response->assertStatus(200);
    }

    /**
     *
     *
     *
     */
    protected function createTestUser()
    {
        $dirctors = $this->getDirectorsFactory(1, ['roleIsAdmin', 'userEnable'])->create();
        $dirctor = $dirctors->first();
        
        return $dirctor;
    }
    
    /**
     *
     *
     *
     */
    protected function createManagers()
    {
        $this->getManagersFactory(30)->create();
    }
}
