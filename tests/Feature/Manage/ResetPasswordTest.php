<?php
namespace Azuki\Tests\Feature\Manage;

use Azuki\App\Tests\Feature\Manage\AbstractFeatureTest;
use Azuki\App\Tests\Feature\ResetPasswordTrait;
use Azuki\App\Notifications\ResetPasswordForManageNotification as ResetPassword;

class ResetPasswordTest extends AbstractFeatureTest
{
    use ResetPasswordTrait;

    /**
     *
     *
     *
     */
    protected function createTestUser()
    {
        $managers = $this->getManagersFactory(10)->create();
        $manager = $managers->first();
        
        return $manager;
    }
    
    /**
     *
     *
     *
     */
    protected function getResetPasswordClass()
    {
        return ResetPassword::class;
    }
}
