<?php
namespace Azuki\Tests\Unit\Helpers;

use Azuki\App\Tests\Unit\AbstractClass;

class Common extends AbstractClass
{
    /**
     *
     */
    const OS_LINUX   = 'linux';
    const OS_WINDOWS = 'windows';

    /**
     *
     */
    protected $laravelVersion = 6;
    
    /**
     *
     *
     */
    protected $os = self::OS_LINUX;

    /**
     *
     *
     */
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     *
     *
     */
    public function tearDown(): void
    {
        parent::tearDown();
    }
    
    /**
     * @test
     *
     */
    public function laravelVersionIsSix()
    {
        $ret = laravelVersionIsSix();
        if($this->laravelVersion == 6) {
            $this->assertTrue($ret);
        } else {
            $this->assertFalse($ret);
        }
    }
    
    /**
     * @test
     *
     */
    public function laravelVersionOverEight()
    {
        $ret = laravelVersionOverEight();
        if($this->laravelVersion >= 8) {
            $this->assertTrue($ret);
        } else {
            $this->assertFalse($ret);
        }
    }
    
    /**
     * @test
     *
     */
    public function isWindows()
    {
        $ret = isWindows();
        if($this->os == self::OS_WINDOWS) {
            $this->assertTrue($ret);
        } else {
            $this->assertFalse($ret);
        }
    }
    
    /**
     * @test
     *
     */
    public function getUserKind()
    {
        $this->assertEquals(
            NULL,
            getUserKind(NULL)
        );

        $this->assertEquals(
            USER_KIND_DIRECTORS,
            getUserKind( $this->getSystemUser() )
        );

        $this->assertEquals(
            USER_KIND_MANAGERS,
            getUserKind( $this->getManageUser() )
        );

/*
        $this->assertEquals(
            USER_KIND_USERS,
            getUserKind( $this->getUser() )
        );
*/
    }
    
    /**
     * @test
     *
     */
    public function supportOrganizations()
    {
        $this->assertEquals(
            env( 'AZUKI_ORGANIZATION_SUPPORT', false),
            supportOrganizations()
        );
    }
    
    /**
     * @test
     *
     */
    public function getLoginUser()
    {
        $this->assertEquals(
            NULL,
            getLoginUser()
        );

        $user = $this->getSystemUser();
        $this->actingAs($user, 'system');
        $this->assertEquals(
            $user,
            getLoginUser()
        );

        $user = $this->getManageUser();
        $this->actingAs($user, 'manage');
        $this->assertEquals(
            $user,
            getLoginUser()
        );
    }
    
    /**
     * @test
     *
     */
    public function restrictByBelong()
    {
        config()->set('azuki.standard.organizations', true);
        $this->assertTrue(restrictByBelong());  // ここrestrictByBelongの仕様として要検討箇所！！！
        $this->loginSystem();
        $this->assertFalse(restrictByBelong());
        $this->loginManage();
        $this->assertTrue(restrictByBelong());
//        $this->loginCommon();
//        $this->assertFalse(restrictByBelong());
        
        $this->logout();
        config()->set('azuki.standard.organizations', false);
        $this->assertFalse(restrictByBelong());
        $this->loginSystem();
        $this->assertFalse(restrictByBelong());
        $this->loginManage();
        $this->assertFalse(restrictByBelong());
//        $this->loginCommon();
//        $this->assertFalse(restrictByBelong());
        
    }

/*

    function isActiveMenuCategory( $ctrl, $list ) // 未使用の模様

    function isActiveMenu( $ctrl, $url )          // 未使用の模様

    function getLoginUserBelong($def = 0)

    function getAccessDomainName()

    function getSubDir($domain)

    function getHttpAccessLevel($story)

    function httpAccessLevelIsSystem($story)

    function httpAccessLevelIsManage($story)

    function getDiskSpaceInfo($dir = '/var')

    function getUseDiskSpaceRate($dir = '/var')

    function getSymbolByQuantity($bytes)

    function getAllowIpConfig()

    function getRemoteIpAddr()

    function ifRemoteIpInAllows($return)

    function isMatchIp($ip, $accept)

*/


    
    /**
     *
     *
     */
    protected function getSystemUser()
    {
        $user = $this
            ->getDirectorsFactory(1, ['roleIsAdmin', 'userEnable'])
            ->make()
            ->first();
        
        return $user;
    }
    
    /**
     *
     *
     */
    protected function getManageUser()
    {
        $user = $this
            ->getManagersFactory(1, ['userEnable'])
            ->make()
            ->first();
        
        return $user;
    }
    
    /**
     *
     *
     */
    protected function getUser()
    {
        $user = $this
            ->getUsersFactory(1, ['userEnable'])
            ->make()
            ->first();
        
        return $user;
    }
    
    /**
     *
     *
     */
    protected function loginSystem()
    {
        $this->actingAs($this->getSystemUser(), 'system');
    }
    
    /**
     *
     *
     */
    protected function loginManage()
    {
        $this->actingAs($this->getManageUser(), 'manage');
    }
    
    /**
     *
     *
     */
    protected function loginCommon()
    {
        $this->actingAs($this->getUser());
    }
    
    /**
     *
     *
     */
    protected function logout()
    {
        app('auth')->guard()->logout();
    }

}
