<?php
namespace Azuki\Tests\Unit\Validate;

use Azuki\App\Tests\Unit\AbstractClass;

class Validator extends AbstractClass
{
    /**
     *
     *
     */
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     *
     *
     */
    public function tearDown(): void
    {
        parent::tearDown();
    }
    
    /**
     *
     */
    protected $testDirector = [
        'id'       => 1,
        'email'    => 'hogehoge@example.co.jp',
        'password' => 'hogehoge',
    ];
    
    /**
     *
     */
    protected $testManager = [
        'id'       => 1,
        'email'    => 'hogehoge@example.co.jp',
        'belong'   => 1,
        'password' => 'hogehoge',
    ];
    
    /**
     *
     */
    protected $failpass = 'foofoo';
    
    /**
     *
     */
    protected $failId = 100;
    
    /**
     *
     *
     */
    protected function prepareDirectorData($prepare)
    {
        $this->truncateTable('azuki_directors');
        $this->getDirectorsFactory(1)->create(
            array_map(function($v){
                if($this->testDirector['password'] == $v) {
                    $v = bcrypt($v);
                }
                return $v;
            }, $this->testDirector)
        );
    }
    
    /**
     *
     *
     */
    protected function prepareManagerData($prepare)
    {
        $this->truncateTable('azuki_managers');
        $this->getManagersFactory(1)->create(
            array_map(function($v){
                if($this->testManager['password'] == $v) {
                    $v = bcrypt($v);
                }
                return $v;
            }, $this->testManager)
        );
    }
    
    /**
     *
     *
     */
    protected function prepareProcess($prepare)
    {
        if(is_null($prepare)) {
            return ;
        }
        
        if(method_exists($this, $prepare['method'])) {
            call_user_func([$this, $prepare['method']], $prepare);
        }
    }
    
    /**
     * @test
     * @dataProvider getCustomVelidateTestData
     */
    public function カスタムバリデーター単体試験($except, $rule, $data, $prepare = null)
    {
        $this->prepareProcess($prepare);
        $validator = app('validator')->make($data, $rule);
        $this->assertEquals($except, $validator->passes());
    }
    
    /**
     *
     *
     */
    public function getCustomVelidateTestData()
    {
        $ret = [];
        $testCaseFunctionList = [
            'MatchUserPassword',
            'UniqueOnTable',
            'UniqueOnBelong',
            'MinNL',
            'MaxNL',
            'ZipCode',
            'Tel',
            'Kana',
            'KanaSpace',
            'Alpha',
            'AlphaNum',
            'AlphaNumAtJ',
            'AlphaNumSymbol',
            'ServerString',
            'PathString',
            'Domain',
            'AllowValueWhen',
            'ArrayNoDeplicate',
/*  // 以下の３つはAzukiでは未使用のためSignageにてテストを作成すること
            'DeplicateBetween',
            'IncludeOther',
            'NotChangeReference',
*/
        ];
        
        foreach($testCaseFunctionList as $func) {
            $method = 'generateTestCase'.$func;
            if(method_exists($this, $method)) {
                $ret = array_merge($ret, $this->{$method}());
            }
        }
        
        return $ret;
    }
    
    /**
     *
     *
     */
    protected function generateTestCase($title, $datum, $prepare = null)
    {
        $ret  = [];

        $i = 0;
        foreach($datum as $data) {
            $i++;
            $ti = sprintf('%s-[%02d]', $title, $i);
            $ret[$ti] = [ $data[0], $data[1], $data[2] ];
            if(!is_null($prepare)) {
                $ret[$ti][] = $prepare;
            }
        }
        return $ret;
    }
    
    /**
     *
     *
     */
    protected function generateTestCaseMatchUserPassword()
    {
        $key  = 'test';
        $rule = [$key => sprintf(
            '%s:%s,%s,%s',
            'match_user_password',
            \Azuki\App\Contracts\Models\Directors::class,
            'id',
            $this->testDirector['id']),
        ];
        
        $datum = [
            [ true,  $rule, [$key => $this->testDirector['password']] ],
            [ false, $rule, [$key => $this->failpass] ],
        ];
        
        return $this->generateTestCase(
            'パスワード一致',
            $datum,
            [ 'method' => 'prepareDirectorData', 'data' => [], ]
        );
    }
    
    /**
     *
     *
     */
    protected function generateTestCaseUniqueOnTable()
    {
        $key  = 'test';
        $rule = sprintf(
            '%s:%s,%s,',
            'unique_on_table',
            \Azuki\App\Contracts\Models\Directors::class,
            'email'
        );
        $rule1 = [$key => $rule.$this->testDirector['id']];
        $rule2 = [$key => $rule.$this->failId];
        
        $datum = [
            [ true,  $rule1, [$key => $this->testDirector['email']] ],
            [ false, $rule2, [$key => $this->testDirector['email']] ],
        ];
        
        return $this->generateTestCase(
            'テーブル内に一意のデータ',
            $datum,
            [ 'method' => 'prepareDirectorData', 'data' => [], ]
        );
    }
    
    /**
     *
     *
     */
    protected function generateTestCaseUniqueOnBelong()
    {
        $key  = 'test';
        $rule = sprintf(
            '%s:%s,%s,%s,',
            'unique_on_belong',
            \Azuki\App\Contracts\Models\Managers::class,
            'email',
            $this->testManager['belong']
        );
        $rule1 = [$key => $rule.$this->testManager['id']];
        $rule2 = [$key => $rule.$this->failId];
        
        $datum = [
            [ true,  $rule1, [$key => $this->testManager['email']] ],
            [ false, $rule2, [$key => $this->testManager['email']] ],
        ];
        
        return $this->generateTestCase(
            'テーブル内の同一所属に対して一意のデータ',
            $datum,
            [ 'method' => 'prepareManagerData', 'data' => [] ]
        );
    }
    
    /**
     *
     *
     */
    protected function generateTestCaseMinNL()
    {
        $key  = 'test';
        $rule1 = [$key => 'min_nl:10'];
        $rule2 = [$key => 'numeric|min_nl:10'];
        
        $datum = [
            [ true,  $rule1, [$key => str_repeat('あ', 10),] ],
            [ false, $rule1, [$key => str_repeat('あ', 9),] ],
            [ true,  $rule1, [$key => str_repeat('あ', 5)."\r\n\r\n".str_repeat('あ', 5),] ],
            [ false, $rule1, [$key => str_repeat('あ', 4)."\r\n\r\n".str_repeat('あ', 5),] ],
            [ false, $rule2, [$key => str_repeat('あ', 10),] ],
            [ true,  $rule2, [$key => 10,] ],
            [ false, $rule2, [$key => 9,] ],
        ];
        
        return $this->generateTestCase( '改行コードなしで最小値テスト', $datum );
    }
    
    /**
     *
     *
     */
    protected function generateTestCaseMaxNL()
    {
        $key  = 'test';
        $rule1 = [$key => 'max_nl:10'];
        $rule2 = [$key => 'numeric|max_nl:10'];
        
        $datum = [
            [ true,  $rule1, [$key => str_repeat('あ', 10),] ],
            [ false, $rule1, [$key => str_repeat('あ', 11),] ],
            [ true,  $rule1, [$key => str_repeat('あ', 5)."\r\n\r\n".str_repeat('あ', 5),] ],
            [ false, $rule1, [$key => str_repeat('あ', 6)."\r\n\r\n".str_repeat('あ', 5),] ],
            [ false, $rule2, [$key => str_repeat('あ', 10),] ],
            [ true,  $rule2, [$key => 10,] ],
            [ false, $rule2, [$key => 11,] ],
        ];
        
        return $this->generateTestCase( '改行コードなしで最大値テスト', $datum );
    }
    
    /**
     *
     *
     */
    protected function generateTestCaseZipCode()
    {
        $key  = 'test';
        $rule = [$key => 'zipcode'];
        
        $datum = [
            [ true,  $rule, [$key => '0000000'] ],
            [ true,  $rule, [$key => '9999999'] ],
            [ true,  $rule, [$key => '5890023'] ],
            [ false, $rule, [$key => '111111'] ],
            [ false, $rule, [$key => '11111111'] ],
            [ false, $rule, [$key => 'aaaaaaa'] ],
            [ false, $rule, [$key => 'あいうえお'] ],
        ];
        
        return $this->generateTestCase( '郵便番号形式', $datum );
    }
    
    /**
     *
     *
     */
    protected function generateTestCaseTel()
    {
        $key  = 'test';
        $rule = [$key => 'tel'];
        
        $datum = [
            [ true,  $rule, [$key => '03-1111-2222'] ],
            [ true,  $rule, [$key => '090-1111-1111'] ],
            [ true,  $rule, [$key => '0721-11-1111'] ],
            [ false, $rule, [$key => '03ー1111ー2222'] ],
            [ false, $rule, [$key => '0311112222'] ],
            [ false, $rule, [$key => '09011111111'] ],
            [ false, $rule, [$key => '0721111111'] ],
            [ false, $rule, [$key => '0-11-111'] ],
            [ false, $rule, [$key => '03333-11-111'] ],
            [ false, $rule, [$key => '03-1-111'] ],
            [ false, $rule, [$key => '03-11111-111'] ],
            [ false, $rule, [$key => '03-11-11'] ],
            [ false, $rule, [$key => '03-11-11111'] ],
            [ false, $rule, [$key => 'aaaaaaaa'] ],
            [ false, $rule, [$key => 'aa-aa-aaaa'] ],
            [ false, $rule, [$key => 'あいう-えお-かきくけ'] ],
        ];
        
        return $this->generateTestCase( '電話番号形式', $datum );
    }
    
    /**
     *
     *
     * ミドルウェアの「TrimStrings」によって、入力値はtrimされている
     * バリデーション実行時の入力値はtrimされていない。
     * バリデーション実行時、implicitの検証時に入力値をtrimしており、
     * 半角スペースのみの入力は空として処理される。
     * これは、「空白は許可、半角スペースのみの入力は拒否」というルールが作成できない
     * →追加するルール自体を「implicit」に定義する場合は可能
     * のだが、「ミドルウェアの「TrimStrings」によって、入力値はtrimされている」
     * ため、「半角スペースのみの入力＝空」となり「空白は許可、半角スペースのみの入力は拒否」
     * というルール自体が不要。
     * よって、半角スペースのみの試験は不要（implicitなルールでないならば）。
     *
     */
    protected function generateTestCaseKana()
    {
        $key  = 'test';
        $rule = [$key => 'kana'];
        
        $datum = [
            [ true,  $rule, ['hoge' => 'アイウエオ'] ], // 対象のデータがない場合
            [ true,  $rule, [$key => ''] ],             // データが空の場合は通る
            [ true,  $rule, [$key => 'アイウエオカキクコケサシスセソタチツテトナニヌネノ'] ],
            [ true,  $rule, [$key => 'ハヒフヘホマミムメモヤユヨワヲン'] ],
            [ true,  $rule, [$key => 'ガギグゲゴザジズゼゾダヂヅデドバビブベボパピプペポ'] ],
            [ true,  $rule, [$key => 'ヰヱー'] ],
            [ true,  $rule, [$key => 'ァィゥェォッャュョヮヵヶ'] ],
//            [ true,  $rule, [$key => 'ヴヸヹヺ'] ],
            [ true,  $rule, [$key => 'ヴ'] ],
            [ false, $rule, [$key => 'ア亜'] ],
            [ false, $rule, [$key => 'あア'] ],
            [ false, $rule, [$key => 'ぁァ'] ],
            [ false, $rule, [$key => 'ァィ ゥェ'] ],
        ];
        
        $falseStrings = ''.
            'あんゐゑぁょっ亜　-azAZａｚＡＺｱﾝｶﾞ'.
//            'あいうえおかきくこけさしすせそたちつてとなにぬねのはひふへほまみむめもやゆよわをん'.
//            'ゐゑ'.
//            'ぁぃぅぇぉっゃゅょ'.
//            'ｱｲｳｴｵｶｷｸｺｹｻｼｽｾｿﾀﾁﾂﾃﾄﾅﾆﾇﾈﾉﾊﾋﾌﾍﾎﾏﾐﾑﾒﾓﾔﾕﾖﾜｦﾝ'.
//            '亜　-'.
//            ' '.
//            'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'.
//            'ａｂｃｄｅｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚ'.
//            'ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ'.
            '';
        $falseString  = str_split($falseStrings);
        foreach($falseString as $str) {
            $datum[] = [ false, $rule, [$key => $str] ];
        }
        
        return $this->generateTestCase( 'カナのみ', $datum );
    }
    
    /**
     *
     *
     */
    protected function generateTestCaseKanaSpace()
    {
        $key  = 'test';
        $rule = [$key => 'kana_space'];
        
        $datum = [
            [ true,  $rule, ['hoge' => 'アイウエオ'] ], // 対象のデータがない場合
            [ true,  $rule, [$key => ''] ],             // データが空の場合は通る
            [ true,  $rule, [$key => 'アイウエオカキクコケサシスセソタチツテトナニヌネノ'] ],
            [ true,  $rule, [$key => 'ハヒフヘホマミムメモヤユヨワヲン'] ],
            [ true,  $rule, [$key => 'ヰヱー　'] ],
            [ true,  $rule, [$key => 'ァィゥェォッャュョヶ'] ],
            [ true,  $rule, [$key => 'ァィ ゥェ'] ],
            [ false, $rule, [$key => 'ア亜'] ],
            [ false, $rule, [$key => 'あア'] ],
            [ false, $rule, [$key => 'ぁァ'] ],
        ];
        
        $falseStrings = ''.
            'あんゐゑぁょっ亜-azAZａｚＡＺ'.
//            'あいうえおかきくこけさしすせそたちつてとなにぬねのはひふへほまみむめもやゆよわをん'.
//            'ゐゑ'.
//            'ぁぃぅぇぉっゃゅょ'.
//            '亜-'.
//            'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'.
//            'ａｂｃｄｅｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚ'.
//            'ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ'.
            '';
        $falseString  = str_split($falseStrings);
        foreach($falseString as $str) {
//            $datum[] = [ false, $rule, [$key => $str] ];
        }
        
        return $this->generateTestCase( 'カナ＋空白のみ', $datum );
    }
    
    /**
     *
     *
     */
    protected function generateTestCaseAlpha()
    {
        $key  = 'test';
        $rule = [$key => 'alpha'];
        
        $datum = [
            [ true,  $rule, ['hoge' => 'アイウエオ'] ], // 対象のデータがない場合
            [ true,  $rule, [$key => ''] ],             // データが空の場合は通る
            [ true,  $rule, [$key => 'abcdefghijklmnopqrstuvwxyz'] ],
            [ true,  $rule, [$key => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'] ],
            [ false, $rule, [$key => '0123456789'] ],
            [ false, $rule, [$key => 'あ'] ],
            [ false, $rule, [$key => 'ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ'] ],
        ];
        
        return $this->generateTestCase( '半角英文字のみ', $datum );
    }
    
    /**
     *
     *
     */
    protected function generateTestCaseAlphaNum()
    {
        $key  = 'test';
        $rule = [$key => 'alpha_num'];
        
        $datum = [
            [ true,  $rule, ['hoge' => 'アイウエオ'] ], // 対象のデータがない場合
            [ true,  $rule, [$key => ''] ],             // データが空の場合は通る
            [ true,  $rule, [$key => 'abcdefghijklmnopqrstuvwxyz'] ],
            [ true,  $rule, [$key => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'] ],
            [ true,  $rule, [$key => '0123456789'] ],
            [ false, $rule, [$key => 'あ'] ],
            [ false, $rule, [$key => 'ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ'] ],
        ];
        
        return $this->generateTestCase( '半角英数大小文字のみ', $datum );
    }
    
    /**
     *
     *
     */
    protected function generateTestCaseAlphaNumAtJ()
    {
        $key  = 'test';
        $rule = [$key => 'alpha_num_at_j'];
        
        $datum = [
            [ true,  $rule, ['hoge' => 'アイウエオ'] ], // 対象のデータがない場合
            [ true,  $rule, [$key => ''] ],             // データが空の場合は通る
            [ true,  $rule, [$key => 'abcdefghijklmnopqrstuvwxyz'] ],
            [ true,  $rule, [$key => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'] ],
            [ true,  $rule, [$key => '0123456789'] ],
            [ true,  $rule, [$key => '@.'] ],
            [ false, $rule, [$key => 'あ'] ],
        ];
        
        return $this->generateTestCase( '半角英数大小文字＋＠マーク、ピリオドのみ', $datum );
    }
    
    /**
     *
     *
     */
    protected function generateTestCaseAlphaNumSymbol()
    {
        $key  = 'test';
        $rule = [$key => 'alpha_num_symbol'];
        
        $datum = [
            [ true,  $rule, ['hoge' => 'アイウエオ'] ], // 対象のデータがない場合
            [ true,  $rule, [$key => ''] ],             // データが空の場合は通る
            [ true,  $rule, [$key => 'abcdefghijklmnopqrstuvwxyz'] ],
            [ true,  $rule, [$key => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'] ],
            [ true,  $rule, [$key => '0123456789'] ],
            [ true,  $rule, [$key => '-.,\\'] ],
            [ false, $rule, [$key => 'あ'] ],
            [ true,  [$key => 'alpha_num_symbol:\$'], [$key => 'abAB$09'] ],
            [ false, [$key => 'alpha_num_symbol:\$'], [$key => '-.,'] ],
        ];
        
        return $this->generateTestCase( '半角英数大小文字＋記号のみ', $datum );
    }
    
    /**
     *
     *
     */
    protected function generateTestCaseServerString()
    {
        $key  = 'test';
        $rule = [$key => 'server_string'];
        
        $datum = [
            [ true,  $rule, [$key => 'google.com'] ],
            [ true,  $rule, [$key => 'example.co.jp'] ],
            [ true,  $rule, [$key => '192.168.8.1'] ],
            [ true,  $rule, [$key => '255.255.255.255'] ],
            [ true,  $rule, [$key => '0.0.0.0'] ],
            [ true,  $rule, [$key => '256.256.256.com'] ],
            [ false, $rule, [$key => '192.168.8.1.20'] ],
            [ false, $rule, [$key => '256.256.256.256'] ],
            [ false, $rule, [$key => '192.168'] ],
            [ false, $rule, [$key => '192.168.'] ],
            [ false, $rule, [$key => '192.168.1/24'] ],
            [ false, $rule, [$key => '192.168.1.0/24'] ],
        ];
        
        return $this->generateTestCase( 'サーバー名として適切か', $datum );
    }
    
    /**
     *
     *
     */
    protected function generateTestCasePathString()
    {
        $key  = 'test';
        $rule = [$key => 'path_string'];
        
        $datum = [
            [ true,  $rule, [$key => '192.168.8.1:/var/www/hoge'] ],
            [ true,  $rule, [$key => 'local.test-server.com:/var/www/hoge'] ],
            [ true,  $rule, [$key => '192.168.8.1/hoge'] ],
            [ true,  $rule, [$key => 'local.test-server.com/hoge'] ],
            [ true,  $rule, [$key => 'hoge/foo/bar'] ],
            [ true,  $rule, [$key => 'HOGE/FOO/BAR'] ],
            [ true,  $rule, [$key => 'hoge-foo_bar/hoge.foo.bar'] ],
            [ true,  $rule, [$key => 'h'] ],
            [ true,  $rule, [$key => 'ho'] ],
            [ true,  $rule, [$key => 'h-o'] ],
            [ false, $rule, [$key => '/var/www/hoge'] ],
            [ false, $rule, [$key => 'var/www/hoge/'] ],
            [ false, $rule, [$key => '.example.com'] ],
            [ false, $rule, [$key => 'example.com.'] ],
            [ false, $rule, [$key => '-hoge'] ],
            [ false, $rule, [$key => 'hoge-'] ],
            [ false, $rule, [$key => '_hoge'] ],
            [ false, $rule, [$key => 'hoge_'] ],
            [ false, $rule, [$key => ':hoge'] ],
            [ false, $rule, [$key => 'hoge:'] ],
        ];
        
        return $this->generateTestCase( 'ディレクトリパス名として適切か', $datum );
    }
    
    /**
     *
     *
     */
    protected function generateTestCaseDomain()
    {
        $key  = 'test';
        $rule = [$key => 'domain'];
        $domain255 = '012345678901234567890123456789012345678901234567890123456789.'.
            '012345678901234567890123456789012345678901234567890123456789.'.
            '012345678901234567890123456789012345678901234567890123456789.'.
            '012345678901234567890123456789012345678901234567890123456789.'.
            '12345'.
            '.co.jp';
        
        $datum = [
            [ true,  $rule, [$key => 'google.com'] ],
            [ true,  $rule, [$key => 'example.co.jp'] ],
            [ true,  $rule, [$key => 'EXAMPLE.CO.JP'] ],
            [ true,  $rule, [$key => 'ex-am_ple.co.jp'] ],
            [ true,  $rule, [$key => '012345678901234567890123456789012345678901234567890123456789012.co.jp'] ],
            [ true,  $rule, [$key => 'example.co1.jp'] ],
            [ false, $rule, [$key => '.example.co.jp'] ],
            [ false, $rule, [$key => 'example.co.jp.'] ],
            [ false, $rule, [$key => 'example'] ],
            [ false, $rule, [$key => 'example/co/jp'] ],
            [ false, $rule, [$key => 'example/co/jp'] ],
            [ false, $rule, [$key => 'example.co.j1p'] ],
            [ false, $rule, [$key => '-example.co.jp'] ],
            [ false, $rule, [$key => 'example-.co.jp'] ],
            [ false, $rule, [$key => '_example.co.jp'] ],
            [ false, $rule, [$key => 'example_.co.jp'] ],
            [ false, $rule, [$key => '0123456789012345678901234567890123456789012345678901234567890123.co.jp'] ],
            [ false, $rule, [$key => $domain255] ],
        ];
        
        return $this->generateTestCase( 'ドメイン名として適切か', $datum );
    }
    
    /**
     *
     *
     * Parameter[1]の値がParameter[2]ならばデータはParameter[0]でないといけない　というチェック
     * Parameter[1]の値がParameter[2] => data[Parameter[1]] == Parameter[2]
     */
    protected function generateTestCaseAllowValueWhen()
    {
        $key  = 'test';
        $dep  = 'dep';
        $trueValue = 1;
        $depValue  = 1;
        $rule = [$key => sprintf('allow_value_when:%s,%s,%s', $trueValue, $dep, $depValue)];
        
        $datum = [
            [ true,  $rule, [$key => $trueValue, $dep => $depValue] ],
            [ true,  $rule, [$key => $trueValue, $dep => '0'] ],
            [ true,  $rule, [$key => '0', $dep => '0'] ],
            [ false, $rule, [$key => '0', $dep => $depValue] ],
        ];
        
        return $this->generateTestCase( '項目の値に対して許可値に制限がある場合に制限内の値', $datum );
    }
    
    /**
     *
     *
     */
    protected function generateTestCaseArrayNoDeplicate()
    {
        $key   = 'test';
        $pam   = 'hoge';
        $rule  = [$key => 'array_no_deplicate'];
        $rule2 = [$key => 'array_no_deplicate:'.$pam];
        
        $datum = [
            [ true,  $rule, [$key => [
                'hoge',
                'foo',
                '100',
                'bar',
            ]] ],
            [ true,  $rule2, [$key => [
                [
                    $pam => 'hoge',
                    'foo',
                    '100',
                    'bar',
                ],
                [
                    'hoge',
                    $pam => 'foo',
                    '100',
                    'bar',
                ],
                [
                    'hoge',
                    'foo',
                    $pam => '100',
                    'bar',
                ],
                [
                    'hoge',
                    'foo',
                    '100',
                    $pam => 'bar',
                ],
            ]] ],
            [ false, $rule, [$key => [
                'hoge',
                'foo',
                'hoge',
                '100',
                'bar',
            ]] ],
            [ false, $rule, [$key => [
                'hoge',
                'foo',
                100,
                '100',
                'bar',
            ]] ],
            [ false, $rule2, [$key => [
                [
                    $pam => 'hoge',
                    'foo',
                    '100',
                    'bar',
                ],
                [
                    'hoge',
                    $pam => 'foo',
                    '100',
                    'bar',
                ],
                [
                    $pam => 'hoge',
                    'foo',
                    '100',
                    'bar',
                ],
                [
                    'hoge',
                    'foo',
                    '100',
                    $pam => 'bar',
                ],
            ]] ],
            [ false, $rule2, [$key => [
                [
                    $pam => 'hoge',
                    'foo',
                    '100',
                    'bar',
                ],
                [
                    'hoge',
                    $pam => 'foo',
                    '100',
                    'bar',
                ],
                [
                    'hoge',
                    'foo',
                    '100',
                    'bar',
                ],
                [
                    'hoge',
                    'foo',
                    '100',
                    $pam => 'bar',
                ],
            ]] ],
        ];
        
        return $this->generateTestCase( '配列に重複地がない', $datum );
    }
}
