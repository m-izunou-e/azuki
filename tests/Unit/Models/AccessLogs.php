<?php
namespace Azuki\Tests\Unit\Models;

use Azuki\App\Tests\Unit\Models\BaseClass;
use Azuki\App\Contracts\Models\AccessLogs as Model;
use Azuki\App\Models\OperationLogs;

class AcclessLogs extends BaseClass
{
    /**
     *
     */
    protected $realModelClass = 'Azuki\\App\\Models\\AccessLogs';
    
    /**
     *
     *
     */
    protected $targetModel = null;
    
    /**
     *
     *
     */
    protected $opLogModel = null;

    /**
     *
     *
     */
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     *
     *
     */
    public function tearDown(): void
    {
        parent::tearDown();
    }
    
    /**
     *
     *
     *
     */
    protected function getModel()
    {
        if(is_null($this->targetModel)) {
            $this->targetModel = app(Model::class);
        }
        return $this->targetModel;
    }
    
    /**
     *
     *
     *
     */
    protected function getOpLogModel()
    {
        if(is_null($this->opLogModel)) {
            $this->opLogModel = app(OperationLogs::class);
        }
        return $this->opLogModel;
    }
    
    /**
     * @test
     *
     */
    public function クラスの実体の妥当性をテスト()
    {
        $model = $this->getModel();
        $this->assertEquals($this->realModelClass, get_class($model));
    }
    
    /**
     * @test
     *
     */
    public function データの登録テスト()
    {
        $model = $this->getModel();
        
        $data = [
            'url'         => '/test/test',
            'user_id'     => null,
            'user_kind'   => USER_KIND_NONE,
            'user_info'   => null,
            'access_info' => serialize($_SERVER),
        ];
        $result = $model->_create($data);
        
        $this->assertDatabaseHas($model->getTable(), $data);

        // オペレーションログの記録を確認する
        // →ログインユーザー情報についての確認も必要
        // →アクセスログはオペログ非記載なモデルクラスなのでDBに記録があったらダメ！
        // →持ってる場合のアサーションはassertDatabaseHas
        $opLogModel = $this->getOpLogModel();
        $this->assertDatabaseMissing($opLogModel->getTable(), [
            'table'     => $model->getTable(),
            'target_id' => $result->id,
//            'user_id'   => null,
//            'user_kind' => USER_KIND_NONE,
//            'user_info' => null,
            'op_mode'   => OPLOG_MODE_CREATE,
//            'original_data' => ,
//            'dirty_data'    => ,
        ]);
    }
    
    /**
     * @test
     *
     *
     */
    public function データの削除テスト()
    {
        // データの削除を確認
        // オペレーションログの記録を確認する
        // →ログインユーザー情報についての確認も必要
        
    }
    
    /**
     * @test
     *
     *
     */
    public function データの更新テスト()
    {
        // データの更新を確認
        // オペレーションログの記録を確認する
        // →ログインユーザー情報についての確認も必要
    }
    
    /**
     * @test
     *
     *
     */
    public function １件データ取得テスト()
    {
        // いくつかのデータをファクトリーで登録し、
        // １．１件取得
        
    }
    
    /**
     * @test
     *
     *
     */
    public function 検索データ取得テスト()
    {
        // いくつかのデータをファクトリーで登録し、
        // １．検索して取得を確認する
        //     並び替えもチェック
        
    }
}
