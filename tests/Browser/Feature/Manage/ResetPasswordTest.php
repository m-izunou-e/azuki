<?php
namespace Azuki\Tests\Browser\Feature\Manage;

use Azuki\App\Tests\Browser\Feature\Manage\AbstractFeature;
use Azuki\App\Tests\Browser\Feature\ResetPasswordTrait;
use Azuki\App\Tests\Browser\Pages\Manage\ResetPasswordSendMailPage as SendMailPage;
use Azuki\App\Tests\Browser\Pages\Manage\ResetPasswordSentMailPage as SentMailPage;
use Azuki\App\Tests\Browser\Pages\Manage\ResetPasswordPage;
use Azuki\App\Tests\Browser\Pages\Manage\CompleteResetPasswordPage;

class ResetPasswordTest extends AbstractFeature
{
    use ResetPasswordTrait;

    protected $managersTable = 'azuki_managers';
    
    protected $mailSubject = 'パスワードリセット(管理ユーザー)のご案内';
    
    /**
     *
     *
     *
     */
    protected function createTestUser()
    {
        $managers = $this->getManagersFactory(10)->create();
        $manager = $managers->first();

        $this->loginSet['success']['mail'] = $manager->email;
        
        return $manager;
    }
    
    /**
     *
     *
     *
     */
    protected function getSendMailPage()
    {
        return new SendMailPage;
    }
    
    /**
     *
     *
     *
     */
    protected function getSentMailPage()
    {
        return new SentMailPage;
    }
    
    /**
     *
     *
     *
     */
    protected function getResetPasswordPage()
    {
        return new ResetPasswordPage;
    }
    
    /**
     *
     *
     *
     */
    protected function getCompleteResetPasswordPage()
    {
        return new CompleteResetPasswordPage;
    }
}
