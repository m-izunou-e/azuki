<?php
namespace Azuki\Tests\Browser\Feature\Manage;

use Azuki\App\Tests\Browser\Feature\Manage\AbstractFeature;
use Azuki\App\Tests\Browser\Feature\LoginTrait;
use Azuki\App\Tests\Browser\Pages\Manage\DashboardPage;
use Azuki\App\Database\Factories\ManagersFactory;

class LoginTest extends AbstractFeature
{
    use LoginTrait;

    protected $managersTable = 'azuki_managers';
    
    protected $loginSet = [
        'error' => [
            'screenShotName'  => 'manage.login-page[1]',
            'loginId'         => 'hogehoge@example.co.jp',
            'password'        => 'hogehoge',
            // Azuki\App\Http\Modules\Authenticate->$faildLoginMessage
            'message'         => '入力された内容が登録内容と一致しておりません。',
            'afterScShotName' => 'manage.login-page[2]',
        ],
        'success' => [
            'screenShotName'  => 'manage.login-page[3]',
            'loginId'         => 'azuki@example.com',
            'password'        => 'test1234',
            'pageTitle'       => 'ダッシュボード',
            'afterScShotName' => 'manage.login-page[4]',
        ],
    ];
    
    /**
     *
     *
     *
     */
    protected function createTestUser()
    {
        $managers = $this->getManagersFactory(10, ['roleIsAdmin', 'userEnable'])
            ->create([
                'password' => bcrypt($this->loginSet['success']['password']),
            ]);
        $manager = $managers->first();

        $this->assertDatabaseHas($this->managersTable, [
            'name'     => $manager->name,
            'login_id' => $manager->login_id,
        ]);
        
        $this->loginSet['success']['loginId'] = $manager->login_id;
    }
    
    /**
     *
     *
     *
     */
    protected function getLoginSuccessPage()
    {
        $page = new DashboardPage();
        $page->setLogin();
        return $page;
    }
}
