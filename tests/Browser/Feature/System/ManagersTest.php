<?php
namespace Azuki\Tests\Browser\Feature\System;

use Laravel\Dusk\Browser;
use Azuki\App\Tests\Browser\Feature\System\AbstractFeature;
use Azuki\App\Tests\Browser\Feature\BasicTestTrait;
use Azuki\App\Tests\Browser\Pages\System\ManagersPage as Page;

class ManagersTest extends AbstractFeature
{
    use BasicTestTrait;
    
    /**
     *
     *
     */
    protected function getPage()
    {
        return new Page;
    }
    
    /**
     *
     *
     */
    protected function createTestData()
    {
        $managers = $this->getManagersFactory(10)->create();
        return $managers;
    }
    
    /**
     *
     *
     *
     */
    public function getRegistData()
    {
        $pass = 'test1234';
        $managers = $this->getManagersFactory(1)->make([
            'password' => bcrypt($pass)
        ]);
        $manager = $managers[0];
        $manager->password = $pass;
        return $manager;
    }
    
    /**
     *
     * お名前、ログインID、メールアドレス、役割、有効・無効
     * を任意に組み合わせてデータを作る
     *
     */
    protected function getTestSearchData()
    {
        $managers = $this->getManagersFactory(10, ['roleIsAdmin', 'userEnable'])->create();
        $managers = $managers->concat(
            $this->getManagersFactory(10, ['roleIsAdmin', 'userDisable'])->create()
        );
        $managers = $managers->concat(
            $this->getManagersFactory(10, ['roleIsCommon', 'userEnable'])->create()
        );
        $managers = $managers->concat(
            $this->getManagersFactory(10, ['roleIsCommon', 'userDisable'])->create()
        );
        $managers = $managers->concat(
            $this->getManagersFactory(460/*, ['userDisable']*/)->create()
        );
        return $managers;
    }
    
    /**
     *
     *
     */
    protected function getSearchConditions($data)
    {
        return [
            'case1' => [
                'admin' => [
                    'method' => 'check',
                    'attr'   => 'input[name="search[role][]"][value="2"]',
                    'value'  => '',
                ],
                'enable' => [
                    'method' => 'check',
                    'attr'   => 'input[name="search[enable][]"][value="2"]',
                    'value'  => '',
                ],
            ],
        ];
    }
    
    /**
     *
     *
     */
    protected function getExceptSearchResult($cond, $data)
    {
        return [];
    }
}
