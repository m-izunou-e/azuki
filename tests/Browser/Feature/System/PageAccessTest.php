<?php
namespace Azuki\Tests\Browser\Feature\System;

use Azuki\App\Tests\Browser\Feature\System\AbstractFeature;
use Azuki\App\Tests\Browser\Feature\PageAccessTrait;

class PageAccessTest extends AbstractFeature
{
    use PageAccessTrait;
    
    /**
     *
     *
     */
    protected $accessTestTarget = [
        'login' => [
            'url'    => '/login',
            'public' => true,
            'scName' => 'login',
        ],
        'dashbord' => [
            'url' => '',
            'scName' => 'dashbord',
        ],
        'profile' => [
            'url' => '/profile/detail',
            'scName' => 'profile',
        ],
        'directors' => [
            'url' => '/directors/list',
            'scName' => 'directors',
        ],
        'managers' => [
            'url' => '/managers/list',
            'scName' => 'managers',
        ],
        'users' => [
            'url' => '/users/list',
            'scName' => 'users',
        ],
        'system-roles' => [
            'url' => '/system-roles/list',
            'scName' => 'system-roles',
        ],
        'access-logs' => [
            'url' => '/access-logs/list',
            'scName' => 'access-logs',
        ],
        'operation-logs' => [
            'url' => '/operation-logs/list',
            'scName' => 'operation-logs',
        ],
        'system-logs' => [
            'url' => '/system-logs/list',
            'scName' => 'system-logs',
        ],
        'execute-queue-status' => [
            'url' => '/execute-queue-status/list',
            'scName' => 'execute-status',
        ],
    ];
    
    /**
     *
     *
     *
     */
    protected function createTestData()
    {
        $dirctors = $this->getDirectorsFactory(1, ['roleIsAdmin', 'userEnable'])->create([
            'name' => 'システム管理者',
        ]);
        $ret['loginUser'] = $dirctors->first();
        $dirctors = $this->getDirectorsFactory(19)->create();
        
        return $ret;
    }
}
