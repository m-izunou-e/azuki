<?php
namespace Azuki\Tests\Browser\Feature\System;

use Azuki\App\Tests\Browser\Feature\System\AbstractFeature;
use Azuki\App\Tests\Browser\Feature\ResetPasswordTrait;
use Azuki\App\Tests\Browser\Pages\System\ResetPasswordSendMailPage as SendMailPage;
use Azuki\App\Tests\Browser\Pages\System\ResetPasswordSentMailPage as SentMailPage;
use Azuki\App\Tests\Browser\Pages\System\ResetPasswordPage;
use Azuki\App\Tests\Browser\Pages\System\CompleteResetPasswordPage;

class ResetPasswordTest extends AbstractFeature
{
    use ResetPasswordTrait;

    protected $directorsTable = 'azuki_directors';
    
    protected $mailSubject = 'パスワードリセット(管理ユーザー)のご案内';
    
    /**
     *
     *
     *
     */
    protected function createTestUser()
    {
        $directors = $this->getDirectorsFactory(10)->create();
        $director = $directors->first();

        $this->loginSet['success']['mail'] = $director->email;
        
        return $director;
    }
    
    /**
     *
     *
     *
     */
    protected function getSendMailPage()
    {
        return new SendMailPage;
    }
    
    /**
     *
     *
     *
     */
    protected function getSentMailPage()
    {
        return new SentMailPage;
    }
    
    /**
     *
     *
     *
     */
    protected function getResetPasswordPage()
    {
        return new ResetPasswordPage;
    }
    
    /**
     *
     *
     *
     */
    protected function getCompleteResetPasswordPage()
    {
        return new CompleteResetPasswordPage;
    }
}
