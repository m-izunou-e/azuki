<?php
namespace Azuki\Tests\Browser\Feature\System;

use Azuki\App\Tests\Browser\Feature\System\AbstractFeature;
use Azuki\App\Tests\Browser\Feature\LoginTrait;
use Azuki\App\Tests\Browser\Pages\System\DashboardPage;

class LoginTest extends AbstractFeature
{
    use LoginTrait;

    protected $directorsTable = 'azuki_directors';
    
    protected $loginSet = [
        'error' => [
            'screenShotName'  => 'system.login-page[1]',
            'loginId'         => 'hogehoge@example.co.jp',
            'password'        => 'hogehoge',
            // Azuki\App\Http\Modules\Authenticate->$faildLoginMessage
            'message'         => '入力された内容が登録内容と一致しておりません。',
            'afterScShotName' => 'system.login-page[2]',
        ],
        'success' => [
            'screenShotName'  => 'system.login-page[3]',
            'loginId'         => 'azuki@example.com',
            'password'        => 'secret1234',
            'pageTitle'       => 'ダッシュボード',
            'afterScShotName' => 'system.login-page[4]',
        ],
    ];
    
    /**
     *
     *
     *
     */
    protected function createTestUser()
    {
        $directors = $this->getDirectorsFactory(10, ['roleIsAdmin', 'userEnable'])
            ->create([
                'password' => bcrypt($this->loginSet['success']['password']),
            ]);
        $director = $directors->first();

        $this->assertDatabaseHas($this->directorsTable, [
            'name'  => $director->name,
            'email' => $director->email,
        ]);
        
        $this->loginSet['success']['loginId'] = $director->email;
    }
    
    /**
     *
     *
     *
     */
    protected function getLoginSuccessPage()
    {
        $page = new DashboardPage();
        $page->setLogin();
        return $page;
    }
}
