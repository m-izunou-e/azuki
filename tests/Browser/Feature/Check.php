<?php
namespace Azuki\Tests\Browser\Feature;

use Azuki\App\Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Azuki\App\Tests\Browser\Pages\System\DashboardPage;
use Azuki\App\Tests\Browser\Pages\System\LoginPage;

class Check extends DuskTestCase
{
    /**
     *
     *
     */
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     *
     *
     */
    public function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @  test
     * @return void
     */
    public function redirect()
    {
        $this->browse(function (Browser $browser) {
            $dashboaed = new DashboardPage();
        
            $browser->visit($dashboaed)
//            $browser->visit('/system')
                ->on(new LoginPage);
                
            $user = $this->getDirectorsFactory(1)->first();
            $dashboaed->setLogin();
            $browser->loginAs($user, 'system')
                ->visit($dashboaed);
        });

/*
        $response = $this->get($this->accessUrl);
        $response
            ->assertStatus(302)
            ->assertRedirect($this->loginUrl);
*/
    }
}
