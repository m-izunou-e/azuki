<?php
namespace Azuki;

use Azuki\App\Support\ServiceProvider as AzukiServiceProvider;
use Azuki\App\Services\RouteControll;

/**
 *
 *
 *
 */
class ServiceProvider extends AzukiServiceProvider
{
    /**
     * azukiで使用している設定ファイルのリスト
     *
     */
    protected $configList = [
        'azuki.app'            => ['isPublish' => true, 'merge' => self::CONF_MERGE_TYPE_NRL],
        'azuki.standard'       => ['isPublish' => true, 'merge' => self::CONF_MERGE_TYPE_NRL],
        'azuki.auth'           => ['isPublish' => true, 'merge' => self::CONF_MERGE_TYPE_RVS, 'as' => 'auth'],
        'azuki.execute_type'   => ['isPublish' => true, 'merge' => self::CONF_MERGE_TYPE_NRL],
        'azuki.master'         => ['isPublish' => true, 'merge' => self::CONF_MERGE_TYPE_NRL],
        'azuki.mail'           => ['isPublish' => true, 'merge' => self::CONF_MERGE_TYPE_NRL],
        'azuki.upload_file'    => ['isPublish' => true, 'merge' => self::CONF_MERGE_TYPE_NRL],
        'azuki.menu_list'      => ['isPublish' => true, 'merge' => self::CONF_MERGE_TYPE_NRL],
        'azuki.icon'           => ['isPublish' => true, 'merge' => self::CONF_MERGE_TYPE_NRL],
        'azuki.page'           => ['isPublish' => true, 'merge' => self::CONF_MERGE_TYPE_NRL],
        'azuki.ip'             => ['isPublish' => true, 'merge' => self::CONF_MERGE_TYPE_NRL],
        'azuki.env'            => ['isPublish' => true, 'merge' => self::CONF_MERGE_TYPE_NRL],
        'azuki.analytics_tags' => ['isPublish' => true, 'merge' => self::CONF_MERGE_TYPE_NRL],
        'azuki.services'       => ['isPublish' => true, 'merge' => self::CONF_MERGE_TYPE_NRL, 'as' => 'services'],
        'azuki.view'           => ['isPublish' => false],
    ];
    
    /**
     *
     *
     */
    protected $defineFiles = [
        '00_define_common',
    ];
    
    /**
     *
     */
    protected $publishList = [
        ['src' => '/foundation/assets', 'dst' => ''],
        ['src' => '/img',               'dst' => '/img'],
        ['src' => '/foundation-icons',  'dst' => '/foundation-icons'],
        ['src' => '/css',               'dst' => '/css'],
        ['src' => '/js',                'dst' => '/js'],
    ];
    
    /**
     * azukiで使用しているサービスプロバイダーのリスト
     * register、bootそれぞれの処理時に各プロバイダーのregister、bootを呼び出している
     *
     */
    protected $providers = [
        App\Providers\AzukiAppServiceProvider::class,

        // App\Providerで設定されいているLaravelのサービスプロバイダをAzuki側で
        // 継承したサービスプロバイダに変更
        App\Providers\AuthServiceProvider::class,
        App\Providers\EventServiceProvider::class,

        App\Providers\HelpersServiceProvider::class,
        // Azukiシステムで追加したサービスプロバイダ
        App\Providers\SearchSessionServiceProvider::class,
        App\Providers\OrderSessionServiceProvider::class,
        App\Providers\ResultSessionServiceProvider::class,
        App\Providers\MailLogServiceProvider::class,
        App\Providers\ExtendLogServiceProvider::class,
        App\Providers\FileManagerServiceProvider::class,
        App\Providers\UploderServiceProvider::class,
        App\Providers\ViewerServiceProvider::class,
        App\Providers\ExecuterServiceProvider::class,
        App\Providers\CustomValidatorServiceProvider::class,
    ];
    
    /**
     * azukiで使用するミドルウェアのリスト
     * boot時にregistする
     */
    protected $middleware = [
//        'throttle'   => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        // ログイン済みならリダイレクトさせるミドルウェアの本体を変更
        'guest'        => \Azuki\App\Http\Middleware\RedirectIfAuthenticated::class,
        // 管理画面用にミドルウェアを追加
        'acl'          => \Azuki\App\Http\Middleware\Acl::class,
        'auth.system'  => \Azuki\App\Http\Middleware\AuthSystem::class,
        'guest.system' => \Azuki\App\Http\Middleware\RedirectIfAuthenticatedSystem::class,
        'auth.manage'  => \Azuki\App\Http\Middleware\AuthManage::class,
        'guest.manage' => \Azuki\App\Http\Middleware\RedirectIfAuthenticatedManage::class,
    ];
    
    /**
     * azukiで使用するコマンドのリスト
     *
     *
     */
    protected $registCommands = [
//        \Azuki\App\Console\Commands\AzukiName::class,
        \Azuki\App\Console\Commands\ExecuteUploadedFile::class,
        \Azuki\App\Console\Commands\DeleteLogsInDatabase::class,
    ];
    
    /**
     * azukiの基本システムを構成しているコントローラ、モデル、バリデーター
     * のリスト
     * ここで定義されているキー名に一意なプレフィックスをつけた名前で各クラスを
     * コンテナに登録します
     *
     */
    protected $baseServiceClassList = [
        'controller' => [
            'system.accesslogs'     => 'System\\AccessLogsController',
            'system.directors'      => 'System\\DirectorsController',
            'system.eqs'            => 'System\\ExecuteQueueStatusController',
            'system.fileupload'     => 'System\\FileUploadController',
            'system.index'          => 'System\\IndexController',
            'system.login'          => 'System\\LoginController',
            'system.loginlogs'      => 'System\\LoginLogsController',
            'system.managers'       => 'System\\ManagersController',
            'system.operationlogs'  => 'System\\OperationLogsController',
            'system.organizations'  => 'System\\OrganizationsController',
            'system.profile'        => 'System\\ProfileController',
            'system.resetpassword'  => 'System\\ResetPasswordController',
            'system.systemlogs'     => 'System\\SystemLogsController',
            'system.systemroles'    => 'System\\SystemRolesController',
            'system.manageroles'    => 'System\\ManageRolesController',
            'system.commonroles'    => 'System\\CommonRolesController',
            'system.users'          => 'System\\UsersController',
            'system.environment'    => 'System\\EnvironmentController',
            'system.structure'      => 'System\\StructureController',
            'manage.eqs'            => 'Manage\\ExecuteQueueStatusController',
            'manage.fileupload'     => 'Manage\\FileUploadController',
            'manage.index'          => 'Manage\\IndexController',
            'manage.login'          => 'Manage\\LoginController',
            'manage.managers'       => 'Manage\\ManagersController',
            'manage.profile'        => 'Manage\\ProfileController',
            'manage.resetpassword'  => 'Manage\\ResetPasswordController',
            'manage.users'          => 'Manage\\UsersController',
            'common.index'          => 'Common\\IndexController',
            'common.login'          => 'Common\\LoginController',
            'common.mypage'         => 'Common\\MypageController',
            'common.regist'         => 'Common\\RegistController',
            'common.resetpassword'  => 'Common\\ResetPasswordController',
            'common.uploadfileview' => 'Common\\UploadedFileViewController',
        ],
        'model' => [
            'directors'              => 'Directors',
            'managers'               => 'Managers',
            'users'                  => 'Users',
            'systemroles'            => 'SystemRoles',
            'manageroles'            => 'ManageRoles',
            'commonroles'            => 'CommonRoles',
//            'systemrolesauthorities' => 'SystemRoleAuthorities',
            'organizations'          => 'Organizations',
            'accesslogs'             => 'AccessLogs',
            'loginlogs'              => 'LoginLogs',
            'operationlogs'          => 'OperationLogs',
            'systemlogs'             => 'SystemLogs',
            'systemswitch'           => 'SystemSwitch',
            'eqs'                    => 'ExecuteTargetQueue',
            'socialidentity'         => 'SocialIdentity',
        ],
        'validator' => [
            'system.directors'      => 'System\\DirectorsValidator',
            'system.managers'       => 'System\\ManagersValidator',
            'system.users'          => 'System\\UsersValidator',
            'system.profile'        => 'System\\ProfileValidator',
            'system.systemroles'    => 'System\\SystemRolesValidator',
            'system.manageroles'    => 'System\\ManageRolesValidator',
            'system.commonroles'    => 'System\\CommonRolesValidator',
/*
            'system.organizations'  => 'System\\OrganizationsValidator',
            'system.accesslogs'     => 'System\\AccessLogsValidator',
            'system.loginlogs'      => 'System\\LoginLogsValidator',
            'system.operationlogs'  => 'System\\OperationLogsValidator',
            'system.systemlogs'     => 'System\\SystemLogsValidator',
            'system.eqs'            => 'System\\ExecuteQueueStatusValidator',
*/
            'manage.managers'       => 'Manage\\ManagersValidator',
            'manage.users'          => 'Manage\\UsersValidator',
//            'manage.eqs'            => 'Manage\\ExecuteQueueStatusValidator',
            'common.mypage'         => 'Common\\UsersValidator',
        ],
    ];
    
    /**
     *
     *
     */
    protected function getVendorPath()
    {
        return __DIR__.'/../';
    }
    
    /**
     *
     *
     *
     */
    protected function getRouteControllerInstance($conf)
    {
        return  new RouteControll($conf);
    }
    
    /**
     * function setPublishes
     *
     * vendor:publishでパブリッシュするファイルを設定する
     *
     */
    protected function setPublishes()
    {
        parent::setPublishes();
        $configPath = $this->getConfigPath();

        $this->publishes([
            $configPath . '/azuki.app.php' => config_path('azuki.app.php'),
        ], 'azuki-app');
        $this->publishes([
            $configPath . '/azuki.standard.php' => config_path('azuki.standard.php'),
        ], 'azuki-config');
        
        $this->publishes([
            __DIR__.'/../resources/assets/Foundation6-sites/src/assets/scss/azuki' => resource_path('sass/vendor/azuki'),
        ], 'assets');
        $this->publishes([
            __DIR__.'/../resources/assets/Foundation6-sites/src/assets/js/azuki' => resource_path('js/vendor/azuki'),
        ], 'assets');
    }
    
    /**
     *
     *
     */
    protected function addVendorRouting()
    {
        return $this->app['config']->get('azuki.standard.routing');
    }
    
    /**
     *
     *
     */
    protected function getNewExceptionHandler()
    {
        $handler = null;
        if($this->app['config']->get('azuki.standard.enable_azuki_error_handler')) {
            $handler = '\\Azuki\\App\\Exceptions\\Laravel7\\Handler';
            if(laravelVersionIsSix()) {
                $handler = '\\Azuki\\App\\Exceptions\\Laravel6\\Handler';
            }
        }
        return $handler;
    }
}
