<?php
namespace Azuki\App\Services;
/**
 * 独自のバリデーションルールをここに追加する
 *
 */

use Azuki\App\Services\Abstracts\CustomValidator as Validator;

class CustomValidatorOld extends Validator
{
    /**
     * function validateAlpha
     *
     * 半角英字
     *
     * Laravelのalpahが一部日本語を通すため、
     * オーバーライド
     *
     * @param string   $attribute
     * @param mix      $value
     * @return boolean true or false
     */
    public function validateAlpha($attribute, $value)
    {
        return is_string($value) && preg_match("/^[a-z]+$/iu", $value);
    }

    /**
     * function validateAlphaNum
     *
     * 半角英数字
     *
     * Laravelのalpah_numが一部日本語を通すため、
     * オーバーライド
     *
     * @param string   $attribute
     * @param mix      $value
     * @return boolean true or false
     */
    public function validateAlphaNum($attribute, $value)
    {
        if (! is_string($value) && ! is_numeric($value)) {
            return false;
        }

        return preg_match("/^[a-z0-9]+$/iu", $value);
    }
}
