<?php
namespace Azuki\App\Services;
/**
 * 認可のゲートを構築管理するクラス
 *
 */
use Illuminate\Auth\Access\Response;
use Illuminate\Auth\Access\Gate as GateContract;

class Gate extends GateContract
{
    /**
     * function gateInitialize
     *
     * gateの登録を行う
     * アクセス制限はroleに紐づけてデータベースで管理する方針
     * ここで権限テーブルから情報を取得し動的に判定を行うように構築する
     *
     */
    public function gateInitialize()
    {
        $this->before(function($user, $ability, $arguments){
            return null;
        });

        // beforeでnull以外の場合は処理されない
        $this->define('system', function($user, $request) {
            return $this->checkSystem($user, $request)
                ? Response::allow()
                : Response::deny('リクエストされたページへのアクセス許可がありません。');
        });

        $this->define('manage', function($user, $request) {
            return $this->checkManage($user, $request)
                ? Response::allow()
                : Response::deny('リクエストされたページへのアクセス許可がありません。');
        });

        $this->define('common', function($user, $request) {
            return $this->checkCommon($user, $request)
                ? Response::allow()
                : Response::deny('リクエストされたページへのアクセス許可がありません。');
        });

        // beforeや各チェックでの戻り値に関係なく全ての場合においてafterは処理される
        $this->after(function($user, $ability, $result, $arguments){
            return $result;
        });
    }
    
    /**
     *
     *
     *
     */
    protected function checkSystem($user, $request)
    {
        return $this->untargetUrl($request) || $this->checkSystemRole($user, $request);
    }
    
    /**
     *
     *
     *
     */
    protected function checkManage($user, $request)
    {
        return $this->untargetUrl($request) || $this->checkManageRole($user, $request);
    }
    
    /**
     *
     *
     *
     */
    protected function checkCommon($user, $request)
    {
        return $this->untargetUrl($request) || $this->checkCommonRole($user, $request);
    }
    
    /**
     * function untargetUrl
     *
     */
    protected function untargetUrl($request)
    {
        $ret = false;
        $subDir = getSubDir(getAccessDomainName());
        $untarget = [
            $subDir['system'],
            $subDir['system'].'/profile*',
            $subDir['manage'],
            $subDir['manage'].'/profile*',
            'mypage',
            '*logout',
        ];
        
        foreach( $untarget as $url ) {
            if($request->is($url)) {
                $ret = true;
                break;
            }
        }
        return $ret;
    }
    
    /**
     * function checkSystemRole
     *
     *
     */
    protected function checkSystemRole($user, $request)
    {
        return $this->checkRole('system', $user, $request);
    }
    
    /**
     * function checkManageRole
     *
     *
     */
    protected function checkManageRole($user, $request)
    {
        return $this->checkRole('manage', $user, $request);
    }
    
    /**
     * function checkCommonRole
     *
     *
     */
    protected function checkCommonRole($user, $request)
    {
        return $this->checkRole('common', $user, $request, true);
    }
    
    /**
     *
     *
     *
     */
    protected function checkRole($kind, $user, $request, $defRet = false)
    {
        // authにてブロックされているはずであるが念のためログインユーザーのテーブルチェックをする
        if(!$this->checkUserKind($kind, $user)) {
            return false;
        }

        $ret = null;
        $roleAuth = $this->getRoleAuth($user);
        foreach( $roleAuth as $row ) {
            if( $row->url == '*' ) {
                $defRet = $row->authority == AUTHRITY_ALLOW ? true : false;
            } elseif( $request->is($row->url) ) {
                $ret = $row->authority == AUTHRITY_ALLOW ? true : false;
                break;
            }
        }
        $ret = is_null($ret) ? $defRet : $ret;
        return $ret;
    }
    
    /**
     *
     *
     *
     */
    protected function checkUserKind($kind, $user)
    {
        $list = [
            'system' => USER_KIND_DIRECTORS,
            'manage' => USER_KIND_MANAGERS,
            'common' => USER_KIND_USERS,
        ];
        return $list[$kind] == getUserKind($user);
    }
    
    /**
     *
     *
     *
     */
    protected function getRoleAuth($user)
    {
        $roleModels = [
            USER_KIND_DIRECTORS => app(\Azuki\App\Contracts\Models\SystemRoles::class),
            USER_KIND_MANAGERS  => app(\Azuki\App\Contracts\Models\ManageRoles::class),
            USER_KIND_USERS     => app(\Azuki\App\Contracts\Models\CommonRoles::class),
        ];

        $obj = $roleModels[getUserKind($user)];
        $role = $obj->where('value', $user->role)->first();
        $roleAuth = !is_null($role) ? $role->getAuthorities() : [];
        
        return $roleAuth;
    }
}
