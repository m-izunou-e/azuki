<?php
namespace Azuki\App\Services\Viewer\Abstracts;
/**
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Services\Network\Modules\StreamTools;

abstract class FileViewer
{
    use StreamTools;
    
    /**
     * ファイル管理クラス
     *
     */
    protected $fileManager;
    
    /**
     * アップロードされたファイルに紐づく設定値の配列
     *
     */
    protected $config;
    
    /**
     * 指定ファイルが存在しない場合のデフォルトファイルパス
     *
     */
    protected $noFilePath = '';

    /**
     * function __construct
     *
     */
    public function __construct( $app )
    {
        $this->fileManager = app('UploadedFileManager');
    }
    
    /**
     * function getFile
     *
     * 指定されたファイルの生データを取得する
     *
     * @param  Eloqent $fileInfo FileInjfoモデルのインスタンス
     * @return binary  $data     ファイルの生データ
     *
     */
    public function getFile($fileInfo)
    {
        $data = $this->fileManager->getData($fileInfo);
        
        // cache作る。キャッシュが必要な場合のみ
        $this->createCacheFile($fileInfo);
        
        return $data;
    }
    
    /**
     * function getStreamData
     *
     * 指定されたファイルの生データを取得する
     *
     * @param  Eloqent $fileInfo FileInjfoモデルのインスタンス
     * @return binary  $data     ファイルの生データ
     *
     */
    public function getStreamData($fileInfo, $range)
    {
        $this->outputStreamData($fileInfo, $range);
    }
    
    /**
     * function getDataSize
     *
     * 指定されたファイルの生データを取得する
     *
     * @param  Eloqent $fileInfo FileInjfoモデルのインスタンス
     * @return binary  $data     ファイルの生データ
     *
     */
    public function getDataSize($fileInfo)
    {
        return $this->fileManager->getDataSize($fileInfo);
    }
    
    /**
     * function getChunkData
     *
     * 指定されたファイルの呼び出し回数に応じた生データの部分を取得する
     *
     * @param  Eloqent $fileInfo FileInjfoモデルのインスタンス
     *         Number  $num      呼び出し回数（１から順番に増える）
     * @return binary  $data     ファイルの生データ
     *
     */
    protected function getChunkData($fileInfo, $num)
    {
        return $this->fileManager->getChunkData($fileInfo, $num);
    }
    
    /**
     * function createCacheFile
     *
     * キャッシュ指定があればデータをキャッシュファイルに書き込む
     *
     * @param  Eloqent $fileInfo FileInjfoモデルのインスタンス
     *         Binary  $data     書き出すデータ
     *         Number  $num      呼び出し回数（１から順番に増える）
     *
     */
    protected function createCacheFile($fileInfo)
    {
        $this->fileManager->createCacheFile($fileInfo);
    }
    
    /**
     * function getFileInfoFromUrlParam
     *
     * アップロードファイルへのアクセスURLからFileInfoモデルのインスタンスを取得する
     *
     *
     */
    protected function getFileInfoFromUrlParam($path1, $path2 = null, $path3 = null, $path4 = null)
    {
        $idName   = $path4;
        $category = UPLOAD_DATA_CATEGORY_ORIGINAL;
        if($path1 == 'temp') {
            $idName = $path3 ?? $path2;
            if(!is_null($path3)) {
                $category = intval($path2);
            }
        } else {
            $prefix = explode('-', $path1);
            if(isset($prefix[1])) {
                $category = intval($prefix[1]);
            }
        }
        return $this->fileManager->getInfo($idName, $category);
    }
}
