<?php
namespace Azuki\App\Services\Viewer;
/**
 * アップロードされたファイルを閲覧するための処理を行うクラス
 *
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Services\Viewer\Abstracts\FileViewer;
use Azuki\App\Services\Viewer\Interfaces\UploadedFileInterface;

class UploadedFileViewer extends FileViewer implements UploadedFileInterface
{
    /**
     * function getInfo
     *
     * アップロードファイルへのアクセスURLからFileInfoモデルのインスタンスを取得する
     *
     *
     */
    public function getInfo($path1, $path2 = null, $path3 = null, $path4 = null)
    {
        return $this->getFileInfoFromUrlParam($path1, $path2, $path3, $path4);
    }
}
