<?php
namespace Azuki\App\Services\Viewer\Interfaces;


interface UploadedFileInterface
{
    public function getInfo($path1, $path2 = null, $path3 = null, $path4 = null);
}
