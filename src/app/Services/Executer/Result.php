<?php
namespace Azuki\App\Services\Executer;
/**
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

class Result
{
    /**
     *
     *
     */
    protected $failErrorMessageLimit = 10;
    
    /**
     *
     *
     */
    protected $totalLine = 0;
    
    /**
     *
     *
     */
    protected $totalCount = 0;
    
    /**
     *
     *
     */
    protected $subCount = 0;
    
    /**
     *
     *
     */
    protected $successNum = 0;
    
    /**
     *
     *
     */
    protected $failNum = 0;
    
    /**
     *
     *
     */
    protected $fails = [];
    
    /**
     *
     *
     */
    protected $onLine = 0;
    
    /**
     *
     *
     */
    protected $fieldCount = 0;
    
    /**
     *
     *
     */
    protected $checkData = null;

    /**
     * function __construct
     *
     */
    public function __construct()
    {
    }
    
    /**
     *
     *
     */
    public function nowLine()
    {
        return $this->onLine;
    }
    
    /**
     *
     *
     */
    public function isSuccess()
    {
        $ret = false;
        if( $this->failNum == 0 ) {
            $ret = true;
        }
        
        return $ret;
    }
    
    /**
     *
     *
     */
    public function getTotalLine()
    {
        return $this->totalLine;
    }
    
    /**
     *
     *
     */
    public function getTotalCount()
    {
        return $this->totalCount;
    }
    
    /**
     *
     *
     */
    public function getSubCount()
    {
        return $this->subCount;
    }
    
    /**
     *
     *
     */
    public function getSuccessNum()
    {
        return $this->successNum;
    }
    
    /**
     *
     *
     */
    public function getFailNum()
    {
        return $this->failNum;
    }
    
    /**
     *
     *
     */
    public function getMessage()
    {
        $failCount  = count($this->fails);
        $limitCount = $this->failErrorMessageLimit;
    
        $message = '';
        if( $failCount > 0 ) {
            $message = sprintf(
                "登録失敗発生件数： [%d] 件 \n最新の%d件までを記録しています。\n",
                $failCount,
                $limitCount
            );
        }
        
        $num = 0;
        foreach( $this->fails as $fail ) {
            $message .= var_export($fail, true) . "\n";
            $num++;
            if( $num >= $limitCount ) {
                $message .= '・・・・・' . "\n";
                break;
            }
        }
        
        // TODO messageがTEXT型の最大長（65535Byte）を超える場合は丸める処理が必要
        
        return $message;
    }
    
    /**
     *
     *
     */
    public function getFieldCount()
    {
        return $this->fieldCount;
    }
    
    /**
     *
     *
     */
    public function getCheckData()
    {
        return $this->checkData;
    }
    
    /**
     *
     *
     */
    public function setTotalLine($num)
    {
        $this->totalLine = $num;
    }
    
    /**
     *
     *
     */
    public function setFieldCount($num)
    {
        $this->fieldCount = $num;
    }
    
    /**
     *
     *
     */
    public function setCheckData($data)
    {
        $this->checkData = $data;
    }
    
    /**
     *
     *
     */
    public function totalCountUp()
    {
        $this->totalCount++;
    }
    
    /**
     *
     *
     */
    public function subCountUp()
    {
        $this->subCount++;
    }
    
    /**
     *
     *
     */
    public function successCountUp()
    {
        $this->successNum++;
    }
    
    /**
     *
     *
     */
    public function failCountUp()
    {
        $this->failNum++;
    }
    
    
    /**
     *
     *
     */
    public function putFails($fail)
    {
        $this->fails[] = $fail;
    }
}
