<?php
namespace Azuki\App\Services\Executer;
/**
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

class Config
{
    /**
     * function __construct
     *
     */
    public function __construct($app)
    {
    }
    
    /**
     * function getConfig
     *
     *
     *
     * @param  string   $type  実行type
     *
     */
    public function getConfig($type)
    {
        $config = app(BASE_APP_ACCESSOR)->get('execute_type.'.$type);
        if( empty($config) ) {
            $type = 'default';
            $config = app(BASE_APP_ACCESSOR)->get('execute_type.'.$type);
        }
        return $config;
    }
}
