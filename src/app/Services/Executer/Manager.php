<?php
namespace Azuki\App\Services\Executer;
/**
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Models\ExecuteTargetQueue;

class Manager
{
    /**
     *
     *
     */
    private $exModel;
    
    /**
     *
     *
     */
    protected $fileManager;

    /**
     * function __construct
     *
     */
    public function __construct($app)
    {
        $this->exModel = new ExecuteTargetQueue();
        $this->fileManager = app('UploadedFileManager');
    }
    
    /**
     * function put
     *
     *
     *
     *
     */
    public function put( $data, $sync = false )
    {
        $status = $sync ? ETQ_STATUS_RUNNING : ETQ_STATUS_NEW;
        return $this->exModel->put($data, $status);
    }
    
    /**
     *
     *
     */
    public function getFilePath($exObj)
    {
        $fileInfo = $this->fileManager->getInfoById($exObj->file_info_id);
        return $fileInfo->path;
    }
}
