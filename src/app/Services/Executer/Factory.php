<?php
namespace Azuki\App\Services\Executer;
/**
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

class Factory
{
    /**
     * $executer
     *
     * 一度生成したインスタンスを保持しておく配列
     */
    private $executer;

    /**
     * function __construct
     *
     */
    public function __construct($app)
    {
    }
    
    /**
     * function getExecuter
     *
     *
     *
     * @param  string   $type  実行type
     * @return Executer -      Executerサービスのインスタンス
     *
     */
    public function getExecuter($type)
    {
        return $this->getExecuterInstance($type);
    }
    
    /**
     * function getExecuterInstance
     *
     *
     * @param  string   $type Executeの内部定義されたタイプ
     * @return executer -     エグゼキュータクラス
     */
    private function getExecuterInstance($type)
    {
        if(isset($this->executer[$type])) {
            return $this->executer[$type];
        }
        
        $config = app('ExecuterConfig')->getConfig($type);
        $class  = $config['class'];
        
        $this->executer[$type] = app($class);
        return $this->executer[$type];
    }
}
