<?php
namespace Azuki\App\Services\Executer;
/**
 * 通常ファイルのアップロード処理を行うクラス
 *
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Services\Executer\Abstracts\CsvExecuter;
use Azuki\App\Services\ExtendLog;

class MocCsvExecuter extends CsvExecuter
{
    
    /**
     *
     */
    protected function skipLine($res, $file, $line)
    {
        return true;
    }
    
    /**
     * function getValidator
     *
     * バリデーションオブジェクトを返す
     */
    protected function getValidator($data)
    {
        return null;
    }
    
    /**
     *
     *
     */
    protected function convertLineToData($data)
    {
        return $data;
    }
    
    /**
     *
     *
     */
    protected function individualProcess($res, $data)
    {
        return $res;
    }
}
