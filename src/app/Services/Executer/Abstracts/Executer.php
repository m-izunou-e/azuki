<?php
namespace Azuki\App\Services\Executer\Abstracts;
/**
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Services\ExtendLog;
use Azuki\App\Services\Executer\Result;

abstract class Executer
{
    /**
     *
     *
     */
    protected $inputCharset;
    protected $internalCharset = 'UTF-8';

    /**
     * $exLog
     *
     * Log出力用のインスタンス
     */
    private static $exLog;

    /**
     *
     */
    protected $manager;
    
    /**
     *
     *
     */
    protected $belong = null;

    /**
     * function __construct
     *
     */
    public function __construct($inputChar = 'UTF-8')
    {
        self::$exLog = app('ExtendLog');
        $this->manager = app('ExecuteManager');
        $this->config = app('ExecuterConfig');
        $this->inputCharset = $inputChar;
    }
    
    /**
     *
     *
     */
    public function setInputCharset($char)
    {
        $this->inputCharset = $char;
        return $this;
    }
    
    /**
     *
     *
     */
    protected function getConfig($kind)
    {
        return $this->config->getConfig($kind);
    }
    
    /**
     *
     *
     *
     * TODO return の構成をフィックスさせる必要がある
     */
    public function exec($obj, $cond = [])
    {
        $resObj = new Result();
        $this->setBelongsFromObj($obj);
    
        setlocale(LC_ALL, 'ja_JP.UTF-8');
        $fileList = $this->getTargetFileList($obj, $cond);

        foreach( $fileList as $path ) {
            $file = $this->getSplFileObject($path);

            while ($file->valid() || $file->key() == 0) {
                set_time_limit(30);
                $line = $file->current();
                $file->next();
                
                $resObj->setTotalLine($file->key());
                if($this->skipLine($resObj, $file, $line)) {
                    continue;
                }
                $resObj = $this->executeLine($resObj, $file, $line);
                // 進行状況をある程度把握できるようにするためのログの更新処理
                $this->updateProcess($obj, $resObj);
            }
        }
        // ステータスの更新、エラー詳細などのデータの保存を行う
        $this->fixResultData($obj, $resObj, $cond);

        return $resObj;
    }
    
    /**
     * function setBelongs
     *
     * 所属情報をセットする
     *
     */
    protected function setBelongsFromObj($obj)
    {
        if(isset($obj->belong)) {
            $this->setBelongs($obj->belong);
        }
    }
    
    /**
     * function setBelongs
     *
     * 所属情報をセットする
     *
     */
    protected function setBelongs($belong = null)
    {
        $this->belong = $belong;
    }
    
    /**
     *
     *
     */
    protected function getTargetFileList($obj, $cond)
    {
        $ret = [
            $this->getTargetFilePath($obj),
        ];
        return $ret;
    }
    
    /**
     *
     *
     */
    protected function getTargetFilePath($obj)
    {
        return $this->manager->getFilePath($obj);
    }
    
    /**
     *
     *
     */
    protected function getSplFileObject($path)
    {
        $filterInput = $path;
        if($this->inputCharset != $this->internalCharset) {
            $filterInput = sprintf(
                "php://filter/read=convert.iconv.%s%%2F%s/resource=%s",
                $this->inputCharset,
                $this->internalCharset,
                $path
            );
        }
        $file = new \SplFileObject($filterInput);
        
        return $file;
    }
    
    /**
     *
     *
     */
    protected function updateProcess($obj, $res)
    {
        if( $res->nowLine() % 100 != 0 ) { // TODO 進行状態記録の条件なので別メソッド化など調整
            return ;
        }
    
        $data = [
            'success_count' => $res->getSuccessNum(),
            'failure_count' => $res->getFailNum(),
        ];
        $obj->updateData($data);
    }
    
    /**
     *
     * TODO 異常終了はどこで補足するのか。。
     */
    protected function fixResultData($obj, $res, $cond = [])
    {
        if( $res->isSuccess() ) {
            $obj->close();
        } else {
            $obj->errorClose();
        }

        $data = [
            'total_line'    => $res->getTotalLine(),
            'total_count'   => $res->getTotalCount(),
            'success_count' => $res->getSuccessNum(),
            'failure_count' => $res->getFailNum(),
            'message'       => $res->getMessage(),
        ];

        try {
            $obj->updateData($data);
        } catch(\Exception $e) {
            $this->exceptionLog($e);
        }
    }


    /**
     * function exceptionLog
     *
     * 主にトランザクション時のException発生時にログを記録するためのメソッド
     *
     */
    protected function exceptionLog($e)
    {
        self::$exLog->exception($e);
    }

    /**
     * function writeLog
     *
     * Exception以外で処理は続けるもののエラーが発生している際にログ記録するためのメソッド。
     *
     */
    protected function logWrite($level, $msg)
    {
        self::$exLog->write($level, $msg);
    }
    
    /**
     * function systemLog
     *
     * systemLogを記録する
     * 通常のファイルのログもExtendLogクラスにて記録される
     *
     */
    protected function systemLog($kind, $level, $msg)
    {
        self::$exLog->systemLogWrite($kind, $level, $msg);
    }
    
    /**
     * function restoreNetwork
     *
     * network復旧ログを記録する
     *
     */
    protected function restoreNetwork()
    {
        if( $this->existsNetworkFaultLog() ) {
            $msg = 'ネットワーク障害から自動復旧しました。';
            self::$exLog->systemRestoreLog(SL_KIND_NETWORK, $msg);
        }
    }
    
    /**
     *
     *
     */
    protected function existsNetworkFaultLog()
    {
        return self::$exLog->existsFaultSystemLog(SL_KIND_NETWORK);
    }
    
    
    /**
     *
     *
     *
     */
    abstract protected function skipLine($res, $file, $line);
    
    /**
     *
     *
     *
     */
    abstract protected function executeLine($res, $file, $line);
}
