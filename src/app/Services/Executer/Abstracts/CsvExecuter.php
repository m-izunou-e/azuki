<?php
namespace Azuki\App\Services\Executer\Abstracts;
/**
 * 通常ファイルのアップロード処理を行うクラス
 *
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Services\Executer\Abstracts\Executer;
use Azuki\App\Services\ExtendLog;
use Azuki\App\Http\Controllers\ManagementTrait;

abstract class CsvExecuter extends Executer
{
    /**
     * $outputDebug
     *
     * デバッグログの記録をするかどうか
     */
    protected $outputDebug = false;
    
    /**
     * 構成要素
     *
     * array $elements
     */
    protected $elements = [];

    /**
     * バリデーション初期化に必要な設定を生成するメソッドなどが
     * 利用できるようにする
     */
    use ManagementTrait;

    /**
     * function __construct
     *
     */
    public function __construct($inputChar = 'sjis-win')
    {
        parent::__construct($inputChar);
    }

    /**
     * function getSplFileObject
     *
     * CSVファイルを処理するためのSplFileObjectクラスを作成するメソッド
     *
     * param $path 対象ファイルのパス
     *
     * return $file SplFileObjectインスタンス
     */
    protected function getSplFileObject($path)
    {
        $file = parent::getSplFileObject($path);
        $file->setFlags(
            \SplFileObject::READ_CSV |
            \SplFileObject::SKIP_EMPTY |
            \SplFileObject::READ_AHEAD //|
//            \SplFileObject::DROP_NEW_LINE
        );
        
        return $file;
    }
    
    /**
     * function skipLine
     *
     * 無関係なデータ行をスキップするための判定をするメソッド
     * ヘッダ行があることが前提の処理となっている
     * ヘッダ行がないCSVの場合や、その他スキップ条件があるCSVはこのメソッドをオーバーライドする
     *
     * param $res  結果オブジェクト
     * param $file fileオブジェク
     * param $line 1行の文字列
     *
     * return boolean $ret スキップ対象はtrue、処理が必要な場合はfalse
     */
    protected function skipLine($res, $file, $line)
    {
        $ret = false;
        if( $file->key() == 1 ) { // nextしているので、先頭行処理時にkeyは0ではなく、1
            $res->setFieldCount(count($line));
            $this->logWrite(
                ExtendLog::INFO,
                'CSV-UP: フィールド数：'. $res->getFieldCount() . ':フィールド:' . var_export($line, true)
            );
            $ret = true;
        }
        if( count($line) < $res->getFieldCount() ) {
            $this->logWrite(
                ExtendLog::WARNING,
                'CSV-UP: フィールド数不足によるスキップ：'. count($line) . ':フィールド:' . var_export($line, true)
            );
            $ret = true;
        }
        return $ret;
    }
    
    /**
     * function executeLine
     *
     * CSV１行づつ処理しながら１処理分のデータの処理を実行するメソッド
     * 複数行で１処理となるデータの場合、本メソッドで１処理分の行数が処理される
     * 成功時は結果オブジェクトの成功カウントをインクリメントするがこのカウントは成功した処理データ分
     * となるので、CSVファイルの行数とは異なる場合がある
     *
     * param $res  結果オブジェクト
     * param $file fileオブジェク
     * param $line 1行の文字列
     *
     * return $res 結果オブジェクト　処理状況に応じて内容を更新した結果オブジェクトを返す
     */
    protected function executeLine($res, $file, $line)
    {
        try {
            $data[] = $line;
            $data = $this->getDataset($data, $file);
    
            $data = $this->convertLineToData($data);
            if(empty($data)) {
                $this->logWrite(
                    ExtendLog::WARNING,
                    'CSV-UP: データがありません：line-'. $file->key() . ':フィールド:' . var_export($line, true)
                );
                $res->failCountUp();
                $res->putFails([$file->key(), $line]);
                return $res;
            }

           if( $this->outputDebug ) {
                $this->logWrite(
                    ExtendLog::DEBUG,
                    'CSVデータ：' . var_export($data, true)
                );
            }
            
            $res->totalCountUp();
            $validator = $this->validation($data);
            if($validator->passes()) {
                $res = $this->individualProcess($res, $data);
            } else {
                $res = $this->createFailResult($res, $data, $validator);
            }
            $data = [];

        } catch(\Exception $e) {
            $res->failCountUp();
            $res->putFails([$data, $e->getMessage()]);
            $this->exceptionLog($e);
        }
        
        return $res;
    }
    
    /**
     * function getDataset
     *
     * 行データを処理単位のデータにまとめる
     * $data[0]には1行分のデータがすでに登録されていて、このメソッドでは
     * 追加行の判定を行い、必要であれば$dataに追加行分のデータを登録する
     *
     * param $data 対象データ
     * param $file fileオブジェクト
     *
     * return $data 処理単位分の行データをまとめた配列
     */
    protected function getDataset($data, $file)
    {
        if($file->valid()) {
            $line = $file->current();
            if($this->isAppendData($line)) {
                $file->next();
                $data[] = $line;
                $data = $this->getDataset($data, $file);
            }
        }
        
        return $data;
    }
    
    /**
     * function isAppendData
     *
     * １データセットを作成する際に、次の行もデータに追加する必要があるかどうかを判断する
     *
     * param $line チェックする行の文字列
     *
     * return boolean 追加の必要があればtrue、なければfalse
     * 原則的なCSVデータは１行に１データなので、ほとんどの場合falseで問題がない
     *
     */
    protected function isAppendData($line)
    {
        return false;
    }

    /**
     * function validation
     *
     * バリデーションオブジェクトを返す
     *
     */
    public function validation($data)
    {
        return $this->getValidator($data);
    }
    
    /**
     * function createFailResult
     *
     * 失敗結果を記録する
     * 結果オブジェクトの失敗カウントをインクリメントし、失敗理由をプッシュする
     *
     * paran $res       結果オブジェクト
     * param $data      データ
     * param $validator バリデーションオブジェクト
     *
     * return $res 結果オブジェクト　処理状況に応じて内容を更新した結果オブジェクトを返す
     */
    protected function createFailResult($res, $data, $validator)
    {
        $res->failCountUp();
        $res->putFails([$data, $validator->messages()]);
        
        return $res;
    }
    
    /**
     * function getValidator
     *
     * バリデーションオブジェクトを返す
     */
    abstract protected function getValidator($data);
    
    /**
     *
     * function convertLineToData
     *
     * 文字列をデータ配列に変換する処理
     * 原則は一行分。ものによっては複数行で１データセットとなる場合がある。
     * その場合１データセット分の複数行を１データセットに変換する
     *
     * １行分のデータしかない場合も$dataは配列となる
     * 
     */
    abstract protected function convertLineToData($data);
    
    /**
     * function individualProcess
     *
     * １データセット分の処理を実装する
     *
     * param $res  結果オブジェクト
     * param $data 処理するデータ配列
     *
     * return $res 結果オブジェクト　処理状況に応じて内容を更新した結果オブジェクトを返す
     * 引数で与えられた結果オブジェクトに処理状況を登録していくので、戻さなくても
     * よいのだけど、この形の実装の方が副作用がなく誤解がないと思う
     *
     */
    abstract protected function individualProcess($res, $data);
}
