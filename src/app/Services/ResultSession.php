<?php
namespace Azuki\App\Services;
/**
 *
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Services\Abstracts\SomethingSession;

/**
 *
 *
 */
class ResultSession extends SomethingSession
{
    /**
     * $sessionNamePrefix
     *
     * セッション名の接頭辞
     */
    protected $sessionNamePrefix = 'resultData';
}
