<?php
namespace Azuki\App\Services;
/**
 * 独自のバリデーションルールをここに追加する
 *
 */

use Azuki\App\Services\Abstracts\CustomValidator as Validator;

class CustomValidator extends Validator
{
    /**
     * function validateAlpha
     *
     * 半角英字
     *
     * Laravelのalpahが一部日本語を通すため、
     * オーバーライド
     *
     * @param string   $attribute
     * @param mix      $value
     * @return boolean true or false
     */
    public function validateAlpha($attribute, $value, $parameters)
    {
        return parent::validateAlpha($attribute, $value, ['ascii']);
    }

    /**
     * function validateAlphaNum
     *
     * 半角英数字
     *
     * Laravelのalpah_numが一部日本語を通すため、
     * オーバーライド
     *
     * @param string   $attribute
     * @param mix      $value
     * @return boolean true or false
     */
    public function validateAlphaNum($attribute, $value, $parameters)
    {
        return parent::validateAlphaNum($attribute, $value, ['ascii']);
    }
}
