<?php
namespace Azuki\App\Services;
/**
 *
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Services\Abstracts\SomethingSession;

/**
 *
 *
 */
class SearchSession extends SomethingSession
{
    /**
     * $sessionNamePrefix
     *
     * セッション名の接頭辞
     */
    protected $sessionNamePrefix = 'search';

    /**
     * function get 設定された検索情報を取得する
     * 
     * 指定された名前に紐づいたkeyで指定された検索情報を返す
     * 
     * @param string $sName
     * @param string $key
     */
    public function get( $key = null, $name = null, $sessionName = null )
    {
        $name = is_null($name) ? 'default' : $name;
        return parent::get($key, $name, $sessionName);
    }
}
