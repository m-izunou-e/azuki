<?php
namespace Azuki\App\Services;

/**
 * Logを独自拡張するためのクラス
 * CustomLogServiceProviderを使っているので、そちらでLogの拡張自体はできるのだけど、
 * それとは別で個別の実行IDをログに追記したり、改行を含むログの整形を行う。
 * また、明示的に使用する必要があることで、このクラスを通った想定しているメッセージなのか
 * 予期せぬ部分やシステムのより深い部分で吐き出されているログなのか判断することも可能。
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Log; //Illuminate\Support\Facades\Log;
use Azuki\App\Contracts\Models\SystemLogs;

/**
 * class ExtendLog
 *
 * ログを拡張し、自動的により情報を追加・成形するクラス
 */
class ExtendLog
{
    /**
     * ログレベル定義
     *
     */
    const EMERGENCY = 1;
    const ALERT     = 2;
    const CRITICAL  = 3;
    const ERROR     = 4;
    const WARNING   = 5;
    const NOTICE    = 6;
    const INFO      = 7;
    const DEBUG     = 8;

    /**
     * シーケンス番号
     * String runId
     */
    private static $runId;
    
    /**
     * ログ出力整形用のスペーサー長
     * Integer $specerMaxNum
     */
    private static $specerMaxNum;

    /**
     * ログレベルに対するログ記載メソッドの定義
     * Array $logMethods
     */
    private static $logMethods = [
        self::EMERGENCY => 'emergency',
        self::ALERT     => 'alert',
        self::CRITICAL  => 'critical',
        self::ERROR     => 'error',
        self::WARNING   => 'warning',
        self::NOTICE    => 'notice',
        self::INFO      => 'info',
        self::DEBUG     => 'debug',
    ];
    
    /**
     *
     * $lfSpacer
     */
    private static $lfSpacer;
    
    /**
     *
     * $systemLog
     */
    private $systemLog;
    
    /**
     *
     * $recordSystemLogs
     */
    private $recordSystemLogs;

    /**
     * ログレベルに対するシステムログ記載メソッドの定義
     * Array $logMethods
     */
    private static $systemLogMethods = [
        self::EMERGENCY => 'insertExceptionLog',
        self::ALERT     => 'insertExceptionLog',
        self::CRITICAL  => 'insertExceptionLog',
        self::ERROR     => 'insertExceptionLog',
        self::WARNING   => 'insertWarningLog',
        self::NOTICE    => 'insertInfoLog',
        self::INFO      => 'insertInfoLog',
        self::DEBUG     => 'insertInfoLog',
    ];

    /**
     * function __construct
     *
     * コンストラクタ。サービスプロバイダーによって、シングルトンで生成される
     *
     * @param array $config
     */
    public function __construct($config = null)
    {
         $this->recordSystemLogs = app(BASE_APP_ACCESSOR)->get('standard.record_system_logs');
    
        if( !isset(self::$runId) ) {
            self::$runId = md5(uniqid(rand(),1));
        }
        if( !isset(self::$specerMaxNum) ) {
            $spacer = 0;
            foreach( self::$logMethods as $method ) {
                $mLen = strlen($method);
                if( $mLen >= $spacer ) {
                    $spacer = $mLen;
                }
            }
            self::$specerMaxNum = $spacer;
        }
//        self::$lfSpacer = "\n" . str_repeat(' ', strlen('[YYYY-MM-DD HH:ii:SS] local.EMERGENCY: ') );
        self::$lfSpacer = "\n\t";
    }
    
    /**
     * function convertToLogMsgStringFromArray
     *
     * @params Mix    $msg
     * @ret    String $msg
     */
    protected function convertToLogMsgStringFromArray($msg, $nest = 0)
    {
        if(is_array($msg)) {
            $tmp = '';
            foreach( $msg as $key => $val ) {
                if(is_array($val)) {
                    $tmp .= sprintf("%s%s => [\n%s%s\n],",
                        str_repeat(' ', $nest*4),
                        $key,
                        str_repeat(' ', $nest*4+1),
                        $this->convertToLogMsgStringFromArray($val, $nest+1)
                    );
                } else {
                    $tmp .= sprintf('%s%s => %s,',
                        str_repeat(' ', $nest*4),
                        $key,
                        $val
                    );
                }
            }
            $msg = $tmp;
        }
        
        return $msg;
    }
    
    /**
     * fucntion write
     *
     * ログを記録する
     *
     * @parans Int    $level
     * @params String $msg
     */
    public function write($level, $msg)
    {
        if(empty($msg)) {
            return;
        }
        $msg = $this->convertToLogMsgStringFromArray($msg);
        
        if( !is_numeric($level) || !in_array($level, array_keys(self::$logMethods)) ) {
            $level = 1;
        }
        $method = self::$logMethods[$level];
        
        $spacer = str_repeat(' ', self::$specerMaxNum - strlen($method));
        $msg = sprintf('%sRID [ %s ] : %s', $spacer, self::$runId, $msg);
        $msg = str_replace("\n", self::$lfSpacer, $msg);
        Log::{$method}($msg);
    }
    
    /**
     * fucntion exception
     *
     * Exceptionログを記録する
     *
     * @parans Exception $e
     */
    public function exception($e)
    {
        if(empty($e)) {
            return;
        }

        $msg = sprintf(
            "□□Exception□□ Code:%s [%s:%s]\n%s",
            $e->getCode(),
            $e->getFile(),
            $e->getLine(),
            $e->getMessage()
        );

        $this->write(self::CRITICAL, $msg);
        $this->write(self::CRITICAL, $e->getTraceAsString());
    }
    
    /**
     * function systemLogWrite
     *
     * ログファイルの記録と共にsystem_logsテーブルへのログの記録を行う
     *
     */
    public function systemLogWrite($kind, $level, $msg)
    {
        $this->write($level, $msg);
        $this->systemLogWriteDb($kind, $level, $msg);
    }
    
    /**
     * function systemLogWriteDb
     *
     * system_logsテーブルへのログの記録を行う
     *
     */
    public function systemLogWriteDb($kind, $level, $msg)
    {
        if(empty($msg)) {
            return;
        }
        $msg = $this->convertToLogMsgStringFromArray($msg);
        if( !is_numeric($level) || !in_array($level, array_keys(self::$systemLogMethods)) ) {
            $level = 1;
        }
        $method = self::$systemLogMethods[$level];
        if($this->recordSystemLogs) {
            $systemLog = $this->getSystemLogs();
            $systemLog->{$method}($kind, $msg);
        }
    }
    
    /**
     * function systemRestoreLog
     *
     * リストアログを記録する。
     * INFOのログとsystem_logの該当レコードのrestoreを記録する
     *
     */
    public function systemRestoreLog($kind, $msg)
    {
        $this->systemLogWrite($kind, self::INFO, $msg);
        if($this->recordSystemLogs) {
            $systemLog = $this->getSystemLogs();
            $systemLog->restoreFault($kind);
        }
    }
    
    /**
     * function existsFaultSystemLog
     *
     * 指定種類の障害発生中のログが存在するかチェックする
     *
     */
    public function existsFaultSystemLog($kind = null)
    {
        $systemLog = $this->getSystemLogs();
        return $systemLog->existsFault($kind);
    }
    
    /**
     * function getSystemLogs
     *
     */
    protected function getSystemLogs()
    {
        if(is_null($this->systemLog)) {
            $this->systemLog = app(SystemLogs::class);
        }
        return $this->systemLog;
    }
}
