<?php
namespace Azuki\App\Services;

/**
 * Azukiの基本機能を提供するサービス
 *
 * @copyright Copyright 2020 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Support\Application;

/**
 * class AzukiApplication
 *
 *
 */
class AzukiApplication extends Application
{
}
