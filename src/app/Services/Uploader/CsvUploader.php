<?php
namespace Azuki\App\Services\Uploader;
/**
 * 通常ファイルのアップロード処理を行うクラス
 *
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Services\Uploader\Abstracts\Uploader;

class CsvUploader extends Uploader
{
    /**
     * このアップローダーで許可されるmime-type
     *
     */
    protected $allowMimeType = [
        'text/csv', 
        'application/vnd\.ms-excel', 
    ];

    /**
     * function validate
     *
     * 追加のバリデーション処理を実装する
     *
     * @param  array   $file   ファイル情報の配列
     *         pointer $errors エラーを保持する配列の参照
     * @return boolean true or false
     *
     */
    protected function validate(Array $file, &$errors = null)
    {
        $ret = parent::validate($file, $errors);
        
        // 親のバリデーションでOKだった場合のみ、追加のバリデーションを行う
        if( $ret ) {
            // mimetypeが合致していない場合があるのでチェックする
            $ret = parent::checkMimeType($file['mimeType'], $this->allowMimeType);
            if( !$ret ) {
                $errors['message'] = '許可されていないMime-Typeです。';
            }
        }
        
        return $ret;
    }
    
    /**
     * function prepare
     *
     * アップロード処理の事前準備があればそれを実装する
     *
     * @param  array $file ファイル情報の配列
     * @return array $file ファイル情報の配列
     *
     */
    protected function prepare(Array $file)
    {
        return $file;
    }
    
    /**
     * function postProcess
     *
     * アップロード処理の事後処理があればそれを実装する
     *
     * @param  array $ret  返信情報の配列
     * @param  array $file ファイル情報の配列
     * @return array $ret  返信情報の配列
     *
     */
    protected function postProcess(Array $ret, Array $file)
    {
        if( !$ret['result'] ) {
            return $ret;
        }

        $config = $this->fileManager->getFileConfig($file['ufType']);
        // 実行処理ありの設定だったら実行処理を呼び出す
        if( $config['exec'] ) {
            $ret['executable']  = true;
            $result = $this->registExecute($file);
            // resultに応じて結果を変更処理する
            $ret['result']  = $result['result'];
            $ret['sync']    = $result['sync'];
            $ret['message'] = $this->getExecuterMessage($result['resObj']);
        }

        return $ret;
    }
    
    /**
     *
     *
     */
    protected function getExecuterMessage($result)
    {
        return '';
    }
}
