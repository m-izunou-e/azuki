<?php
namespace Azuki\App\Services\Uploader;
/**
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

class Factory
{
    /**
     * $uploader
     *
     * 一度生成したインスタンスを保持しておく配列
     */
    private $uploader;

    /**
     * function __construct
     *
     */
    public function __construct($app)
    {
    }
    
    /**
     * function getUploader
     *
     * アップロードされたファイルに応じたアップローダーを取得する
     *
     * @param  string   $ufType ファイルのtype
     * @return Uploader -      Uploaderサービスのインスタンス
     *
     */
    public function getUploader($ufType)
    {
        return $this->getUploaderInstance($ufType);
    }
    
    /**
     * function getSharedUploader
     *
     * 共通処理用のアップローダーを取得する
     *
     * @param  string   $ufType ファイルのtype
     * @return Uploader -      Uploaderサービスのインスタンス
     *
     */
    public function getSharedUploader()
    {
        // 現状はデフォルトのアップローダーとしている
        return $this->getUploaderInstance('default');
    }
    
    /**
     * function getUploaderInstance
     *
     * ufTypeから設定ファイルの内容を取得し、そこで指定されているアップローダークラスの
     * インスタンスを生成して返す。
     * ※ufTypeによって使用するstorageなどの違いが発生するため、サービスではなく都度インスタンス生成
     * 　を行っている
     *
     * @param  string   $ufType アップロードされたファイルの内部定義されたタイプ
     * @return Uploader -       アップローダークラス
     */
    private function getUploaderInstance($ufType)
    {
        if(isset($this->uploader[$ufType])) {
            return $this->uploader[$ufType];
        }
        
        $setting = app('UploadedFileManager')->getFileConfig($ufType);
        $class   = $setting['class'];
        
        $this->uploader[$ufType] = app($class);
        return $this->uploader[$ufType];
    }
    
    /**
     * function upload
     *
     * アップローダーを対象ファイルに応じて適宜判断し、アップロード処理を実行する
     *
     * @param  Array $fileObject アップロードファイル情報の配列
     * @return Array $ret 成否・ファイル名・アクセスURLなどの情報を返す
     *
     */
    public function upload( $fileObject )
    {
        $uploader = $this->detectUploader($fileObject);
        return $uploader->upload($fileObject);
    }
    
    /**
     *
     *
     */
    protected function detectUploader($fileObject)
    {
        $uploader = \Azuki\App\Services\Uploader\FileUploader::class;
        if($this->isImageFile($fileObject['type'])) {
            $uploader = \Azuki\App\Services\Uploader\ImageUploader::class;
        } else if($this->isMovieFile($fileObject['type'])) {
            $uploader = \Azuki\App\Services\Uploader\MovieUploader::class;
        } else if($this->isCsvFile($fileObject['type'])) {
            $uploader = \Azuki\App\Services\Uploader\CsvUploader::class;
        }
        
        return app($uploader);
    }
    
    /**
     *
     *
     */
    protected function isImageFile($mime)
    {
        return preg_match('/^image\/.*$/', $mime);
    }
    
    /**
     *
     *
     */
    protected function isMovieFile($mime)
    {
        return preg_match('/^video\/.*$/', $mime);
    }
    
    /**
     *
     *
     */
    protected function isCsvFile($mime)
    {
        return in_array($mime, [
            'text/csv', 
            'application/vnd\.ms-excel', 
        ]);
    }
}
