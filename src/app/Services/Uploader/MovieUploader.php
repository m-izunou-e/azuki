<?php
namespace Azuki\App\Services\Uploader;
/**
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Services\Uploader\Abstracts\MediaUploader;
use FFMpeg;

class MovieUploader extends MediaUploader
{
    /**
     * 指定ファイルが存在しない場合のデフォルトファイルパス
     *
     */
    protected $noFilePath = '';
    
    /**
     * このアップローダーで許可されるmime-type
     *
     */
    protected $allowMimeType = [
        'video/*', 
    ];
    
    /**
     * function prepare
     *
     * アップロード処理の事前準備があればそれを実装する
     *
     * @param  array $file ファイル情報の配列
     * @return array $file ファイル情報の配列
     *
     */
    protected function prepare(Array $file)
    {
        $config = $this->fileManager->getFileConfig($file['ufType']);
        $saveFile = $this->getSaveFilePath($file);
        if( !move_uploaded_file($file['tmpPath'], $saveFile) ) {
            throw new \Exception('アップロードファイルを保存できませんでした');
        }
        $file['tmpPath'] = $saveFile;
        
        if($this->requireCreateThumbnail($config, $file)) {
            $file = $this->createThumbnail($config, $file);
        }
        
        return $file;
    }
    
    /**
     *
     *
     *
     */
    protected function createThumbnail($config, $file)
    {
        $srcFile       = $file['tmpPath'];
        $thumbConfig   = $config['thumbnail'] ?? [];
        $thumbnailFile = $this->getThumbnailFilePath($thumbConfig, $srcFile);
        
        $media = FFMpeg::fromDisk('local')
            ->open(str_replace(storage_path('app'), '', $srcFile));
        $second = $media->getDurationInSeconds();
        $media->getFrameFromSeconds(floor($second/2))
            ->export()
            ->toDisk('local')
            ->save(str_replace(storage_path('app'), '', $thumbnailFile));
        
        if($this->canConvertImageMime(mime_content_type($thumbnailFile))) {
            $this->convertImage($thumbConfig, $thumbnailFile, $thumbnailFile);
        }
        $file['thumbnail'] = $this->createThumbnailInfo($file, $thumbnailFile);
        
        return $file;
    }
    
    /**
     * function postProcess
     *
     * アップロード処理の事後処理があればそれを実装する
     *
     * @param  array $ret  返信情報の配列
     * @param  array $file ファイル情報の配列
     * @return array $ret  返信情報の配列
     *
     */
    protected function postProcess(Array $ret, Array $file)
    {
        if( file_exists($file['tmpPath']) ) {
            @unlink($file['tmpPath']);
        }
        if( isset($file['thumbnail']) && file_exists($file['thumbnail']['tmpPath']) ) {
            @unlink($file['thumbnail']['tmpPath']);
        }
        
        return $ret;
    }
}
