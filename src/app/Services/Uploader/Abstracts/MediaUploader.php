<?php
namespace Azuki\App\Services\Uploader\Abstracts;
/**
 *
 * @copyright Copyright 2022 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Services\Uploader\Abstracts\Uploader;
use Image;

abstract class MediaUploader extends Uploader
{
    /**
     * function getUploadSuccessResult
     *
     * アップロード成功時結果の配列を生成し、返す
     *
     * @param  Array $file アップロードファイル情報
     * @return Array $ret 
     */
    protected function getUploadSuccessResult($file)
    {
        $ret = parent::getUploadSuccessResult($file);
        if(existsUploadFileThumbnail($file['idName'])) {
            $ret['thumbnailUrl'] = getUploadedFileThumbnailUrl($file['idName'], $file['ufType']);
        }
        
        return $ret;
    }
    
    /**
     *
     *
     */
    protected function convertImage($config, $filePath, $saveFile)
    {
        // 画像の回転、必要に応じてリサイズを行う
        $img = Image::make($filePath);
        $img->orientate();
        
        $width  = isset($config['width']) ? $config['width'] : null ;
        $height = isset($config['height']) ? $config['height'] : null ;
        
        if(
            ( !is_null($width) || !is_null($height) )
            && ($width < $img->width() || $height < $img->height())
        ) {
            $img->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            });
        }
        
        if(isset($config['ext'])) {
            $img = $img->encode($config['ext']);
        }
        
        $img->save($saveFile);
        $img->destroy();
    }
    
    /**
     *
     *
     */
    protected function getThumbnailFilePath($config, $srcFile)
    {
        $path = storage_path('app/uploadedFile');
        if(!file_exists($path)) {
            createDirIfNotExists($path);
        }
        $fileName = 'thumbnail_'.basename($srcFile);
        if(isset($config['ext'])) {
            //if(($ext = $this->getFileExtension($fileName)) != '') {
            //    $fileName = str_replace($ext, $config['ext'], $fileName);
            //}
            $fileName = $fileName . '.' . $config['ext'];
        }
        return $path . '/' . $fileName;
    }
    
    /**
     *
     *
     */
    protected function requireConvert($config, $file)
    {
        return $config['convert'] ?? false;
    }
    
    /**
     *
     *
     *
     */
    protected function requireCreateThumbnail($config, $file)
    {
        return $config['thumbnail']['auto'] ?? false;
    }
    
    /**
     * function canConvertImageMime
     *
     * mimeタイプがImageクラスでコンバート可能かをチェックするメソッド
     * GDが対応しているmimeタイプの場合にtrueを返すように設定
     *
     */
    protected function canConvertImageMime($mime)
    {
        $can = [
            'image/jpeg',
            //'image/jpg',
            'image/gif',
            'image/png',
            'image/webp',
            'image/bmp',
        ];
        if($this->imageDriverIsImageMagick()) {
            $can = array_merge($can, [
                'image/tiff',
                'image/vnd.microsoft.icon',
                'image/x-icon',
                'image/psd',
                'image/vnd.adobe.photoshop',
                'image/photoshop',
                'image/x-photoshop',
            ]);
        }
        
        return in_array($mime, $can);
    }
    
    /**
     *
     *
     */
    protected function imageDriverIsImageMagick()
    {
        $driver = config('image.driver');
        if($driver instanceof \Intervention\Image\Imagick\Driver) {
            $driver = 'imagick';
        }
        if(is_string($driver) && ucfirst($driver) == 'Imagick') {
            return true;
        }
        
        return false;
    }
    
    /**
     *
     *
     */
    protected function createThumbnailInfo($file, $thumbnail)
    {
        $fileName = $file['name'];
        if(
            ($orgExt = $this->getFileExtension($file['name'])) != 
            ($ext = $this->getFileExtension($thumbnail))
        ) {
            $fileName = str_replace($orgExt, $ext, $fileName);
        }
        return [
            'idName'   => $file['idName'],
            'name'     => $fileName,
            'mimeType' => mime_content_type($thumbnail),
            'ufType'   => $file['ufType'],
            'tmpPath'  => $thumbnail,
            'ext'      => $ext,
            'category' => UPLOAD_DATA_CATEGORY_THUMBNAIL,
        ];
    }
}
