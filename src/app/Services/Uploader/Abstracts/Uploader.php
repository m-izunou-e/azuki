<?php
namespace Azuki\App\Services\Uploader\Abstracts;
/**
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Str;

abstract class Uploader
{
    /**
     * ファイルデータを記録するストレージクラス
     *
     */
    protected $fileManager;
    
    /**
     * 指定ファイルが存在しない場合のデフォルトファイルパス
     *
     */
    protected $noFilePath = '';
    
    /**
     * アップロードエラー時のエラーメッセージ定義
     * 共通エラーに関しての設定。
     * https://www.php.net/manual/ja/features.file-upload.errors.php
     *
     */
    private static $uploadErrorMessage = [
        0                     => 'アップロードが正常に完了していません。',      // デフォルトのエラーメッセージ
        UPLOAD_ERR_INI_SIZE   => 'ファイルサイズが許容値を超えています。（1）', // php.iniのupload_max_filesizeのサイズを超えている
        UPLOAD_ERR_FORM_SIZE  => 'ファイルサイズが許容値を超えています。（2）', // HTMLフォームで指定された MAX_FILE_SIZE を超えている
        UPLOAD_ERR_PARTIAL    => 'アップロードが正常に完了していません。（3）', // 一部のみしかアップロードされていない
        UPLOAD_ERR_NO_FILE    => 'アップロードが正常に完了していません。（4）', // アップロードされなかった
        UPLOAD_ERR_NO_TMP_DIR => 'アップロードが正常に完了していません。（6）', // サーバー一時保管フォルダがない
        UPLOAD_ERR_CANT_WRITE => 'アップロードが正常に完了していません。（7）', // ディスクへの書き込みに失敗
        UPLOAD_ERR_EXTENSION  => 'アップロードが正常に完了していません。（8）', // PHPの拡張モジュールがアップロードを中止した
    ];

    /**
     * function __construct
     *
     */
    public function __construct()
    {
        $this->fileManager = app('UploadedFileManager');
    }
    
    /**
     * function upload
     *
     * アップロード処理を実行する
     *
     * @param  Array $fileObject アップロードファイル情報の配列
     *                           $_FILE[xxxxx] の中身
     * @return Array $ret 成否・ファイル名・アクセスURLなどの情報を返す
     *
     */
    public function upload( $fileObject )
    {
        $ret = $this->getDefaultResult($fileObject);
    
        // ファイル情報の取得
        $file = $this->getFileInfoObject($fileObject);
        
        // バリデーション
        if( !$this->validate($file, $errors) ) {
            $ret['message'] = $errors['message'] ?: '';
            return $ret;
        }
        
        // 事前処理
        $file = $this->prepare($file);
        
        // ファイル情報/データの保存
        if( $this->fileManager->save($file, $errors) ) {
            // 結果を戻す
            $ret = $this->getUploadSuccessResult($file);
        } else {
            $ret['message'] = $errors['message'] ?: '';
        }
        // 事後処理を行う
        // 実行処理が必要な場合の実行処理のトリガーもここで行う
        $ret = $this->postProcess($ret, $file);
        
        return $ret;
    }
    
    /**
     * function getDefaultResult
     *
     * アップロード結果の初期配列を返す
     * アップロード前なので結果はfalse固定
     *
     * @param  Array $fileObject アップロードファイル情報の配列
     * @return Array $ret 内容は固定
     */
    protected function getDefaultResult($fileObject)
    {
        $ret = [
            'result'  => false,
            'name'    => $fileObject['name'],
            'idName'  => '',
            'message' => '',
        ];
        
        return $ret;
    }
    
    /**
     * function getUploadSuccessResult
     *
     * アップロード成功時結果の配列を生成し、返す
     *
     * @param  Array $file アップロードファイル情報
     * @return Array $ret 
     */
    protected function getUploadSuccessResult($file)
    {
        $ret = [
            'result' => true,
            'name'   => $file['name'],
            'idName' => $file['idName'],
            'url'    => $file['url'],
            'type'   => $file['mimeType'],
        ];
        
        return $ret;
    }
    
    /**
     * function getFileInfoObject
     *
     * アップロードされたファイル情報から必要な情報を取り出して
     * システム内部で利用する形に生成・情報の追加を行う
     *
     * @param  Array $fileObject アップロードファイル情報の配列
     * @return Array $file       この後の処理に必要な情報をまとめた配列
     *
     */
    protected function getFileInfoObject($fileObject)
    {
        $file = [
            'uploadErr' => $fileObject['error'],
            'mimeType'  => $fileObject['type'],
            'ufType'    => isset($fileObject['ufType']) ? $fileObject['ufType'] : 'default',
            'exType'    => isset($fileObject['exType']) ? $fileObject['exType'] : '',
            'name'      => $fileObject['name'],
            'tmpPath'   => $fileObject['tmp_name'],
            'size'      => $fileObject['size'],
            'ext'       => $this->getFileExtension($fileObject['name']),
        ];
        // 一時ファイル名の取得
        $file['idName'] = $this->createTempFileName();
        // ファイル保存成功時にそのファイルへアクセスするためのURL
        $file['url']     = $this->getUploadedFileUrl($file['idName'].'.'.$file['ext'], $file['ufType']);
        
        return $file;
    }
    
    /**
     *
     *
     */
    protected function getSaveFilePath($file)
    {
        $path = storage_path('app/uploadedFile');
        if(!file_exists($path)) {
            createDirIfNotExists($path);
        }
        $fileName = basename($file['tmpPath']);
        // Windowsで、tmpPathに格納されるファイル名の拡張子がtmpになってしまうことに対する対応
        $fileName = preg_replace('/(.*\.)tmp$/', '${1}'.$file['ext'], $fileName);
        return $path . '/' . $fileName;
    }
    
    /**
     * function createTempFileName
     *
     * 一時ファイル名を生成する
     * 生成ロジックはユニークであれば何でもよいが、ここではuniqidと固定のプレフィックスに
     * タイムスタンプで作成している
     *
     * @param  string $ext  拡張子
     * @return string $name 一時ファイル名
     *
     */
    private function createTempFileName()
    {
        return UPLOAD_TEMP_FILE_PREFIX.Str::uuid();
    }
    
    /**
     * function getFileExtension
     *
     * ファイル名から拡張子を取得する
     * ファイル名の最後のピリオド以下を取得する方法を採用している
     * →もっと正確にするには一旦ファイルを保存の上file情報取得系のメソッドを使う方法がある
     *
     * @param  string $fileName ファイルの名前
     * @return string $ext      ファイルの拡張子
     *
     */
    protected function getFileExtension($fileName)
    {
        $ext = strrpos($fileName, '.') === false ?
            '' : 
            substr($fileName, strrpos($fileName, '.') + 1);
        return $ext;

    }
    
    /**
     * function validate
     *
     * ファイルアップロードにおける共通のバリデーション処理
     * アップロードにてアップロードエラーが発生していた場合に
     * falseを返し、エラーの詳細に応じたメッセージを与えられたエラーオブジェクトにセットする
     *
     * @param  array   $file   ファイル情報の配列
     *         pointer $errors エラーを保持する配列の参照
     * @return boolean true or false
     *
     */
    protected function validate(Array $file, &$errors)
    {
        if(! ($ret = $this->basicValidate($file, $errors)) ) {
            return $ret;
        }
        
        // mimetypeが合致していない場合があるのでチェックする
        $ufType = $file['ufType'];
        $ret = $this->checkMimeType($file['mimeType'], $this->getAllowMimeType($ufType));
        if( !$ret ) {
            $errors['message'] = '許可されていないMime-Typeです。';
        } else {
            $config = $this->fileManager->getFileConfig($ufType);
            $filesize = filesize($file['tmpPath']);
            if( isset($config['size']) && $config['size'] < $filesize ) {
                $ret = false;
                $errors['message'] = 'ファイルサイズが大きすぎます。';
            }
        }
        
        return $ret;
    }
    
    /**
     * function basicValidate
     *
     * ファイルアップロードにおける共通のバリデーション処理
     * アップロードにてアップロードエラーが発生していた場合に
     * falseを返し、エラーの詳細に応じたメッセージを与えられたエラーオブジェクトにセットする
     *
     * @param  array   $file   ファイル情報の配列
     *         pointer $errors エラーを保持する配列の参照
     * @return boolean true or false
     *
     */
    protected function basicValidate(Array $file, &$errors)
    {
        $errorStatus = $file['uploadErr'];
        if( $errorStatus == UPLOAD_ERR_OK ) {
            return true;
        }
        
        $ret = false;
        $errMsg = self::$uploadErrorMessage;
        $errors['message'] = $errMsg[0];
        if( isset($errMsg[$errorStatus]) ) {
            $errors['message'] = $errMsg[$errorStatus];
        }
        
        return $ret;
    }
    
    /**
     * function checkMimeType
     *
     * mime-typeのチェックを行う
     *
     * @param  string  $mime      マイムタイプ
     * @param  array   $allowList 許可されるマイムタイプのリスト。ワイルドカード指定あり
     * @return boolean $ret       true or false 許可されていればtrue
     */
    protected function checkMimeType($mime, $allowList)
    {
        $ret = false;
        
        foreach( $allowList as $allow ) {
            if( $allow == '*' || preg_match( '$'.$allow.'$', $mime, $matches ) ) {
                $ret = true;
                break;
            }
        }
        
        return $ret;
    }
    
    /**
     * function getAllowMimeType
     *
     * 許可するmime-typeのリストを取得する
     *
     */
    protected function getAllowMimeType($ufType)
    {
        $config = $this->fileManager->getFileConfig($ufType);
        if(isset($config['mimeType'])) {
            return $config['mimeType'];
        }
        return $this->allowMimeType ?? ['*'];
    }
    
    /**
     * function getUploadedFileUrl
     *
     * アップロードされたファイルのURLを取得する
     *
     * @param  string $fileName  ファイル名
     * @param  string $defReturn 指定ファイルがなかった場合のデフォルト値
     * @return string $ret       ファイルのURL
     *
     */
    public function getUploadedFileUrl( $fileName, $uftype, $defReturn = null, $category = UPLOAD_DATA_CATEGORY_ORIGINAL )
    {
        $defReturn = is_null($defReturn) ? $this->noFilePath : $defReturn;
        $ret = $fileName;
        
        $uploadFileInfo = $this->fileManager->getUploadedFileInfo($fileName, $uftype, $category);
        $path = $this->fileManager->getUploadedFilePath($uploadFileInfo);
        if( $path === false ) {
            return $defReturn;
        }
        if(!is_null($fileInfo = $this->fileManager->getInfo($fileName, $category))) {
            $fileName = !empty($fileInfo->ext) ? $fileName . '.' . $fileInfo->ext : $fileName;
        }
        $ret = sprintf('%s/%s', $path, $fileName);

        return $ret;
    }
    
    /**
     * function registExecute
     *
     * 実行処理が必要なアップロードファイルの実行処理を行う
     *
     * @param  array   $file   ファイル情報の配列
     *         pointer $errors エラーを保持する配列の参照
     * @return boolean true or false
     */
    protected function registExecute($file)
    {
        $ret = [
            'result' => true,
            'sync'   => false,
            'resObj' => null,
        ];

        $fileInfo = $this->fileManager->getInfo($file['idName']);
        if( is_null($fileInfo) ) {
            $errors['message'] = 'fileInfo Object not exists idName is ' . $file['idName'];
            return false;
        }
        
        $config = $this->fileManager->getFileConfig($file['ufType']);
        $sync = $config['execOpt']['sync'];
        
        $put = [
            'fileInfoId' => $fileInfo->id,
            'kind'       => $file['exType'],
        ];
        
        // 所属情報を追加
        if(supportOrganizations()) {
            $put['belong'] = getLoginUserBelong(null);
        }
        
        
        $executeManager = app('ExecuteManager');
        $obj = $executeManager->put($put, $sync);
        
        $this->fileManager->registFileInfo($file['ufType'], $obj->id, $fileInfo->id_name);
        
        if($sync) {
            // 実行処理を行う
            $executer = app('ExecuterFactory')->getExecuter($obj->kind);
            $result = $executer->exec($obj);
            
            $ret = [
                'result' => $result->isSuccess(),
                'sync'   => true,
                'resObj' => $result,
            ];
        } else {
            // バックグラウンド実行を呼び出す
            exec('nohup  php '.base_path().'/artisan exec:upload-file > /dev/null &');
        }

        return $ret;
    }
    
    /**
     * protected function prepare
     *
     */
    abstract protected function prepare(Array $file);
    
    /**
     * protected function postProcess
     *
     */
    abstract protected function postProcess(Array $ret, Array $file);
}
