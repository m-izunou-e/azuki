<?php
namespace Azuki\App\Services\Uploader;
/**
 * 通常ファイルのアップロード処理を行うクラス
 *
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Services\Uploader\Abstracts\Uploader;

class FileUploader extends Uploader
{
    /**
     * function validate
     *
     * 追加のバリデーション処理を実装する
     *
     * @param  array   $file   ファイル情報の配列
     *         pointer $errors エラーを保持する配列の参照
     * @return boolean true or false
     *
     */
    protected function validate(Array $file, &$errors = null)
    {
        $ret = parent::validate($file, $errors);
        
        // 親のバリデーションでOKだった場合のみ、追加のバリデーションを行う
        if( $ret ) {
        }
        
        return $ret;
    }
    
    /**
     * function prepare
     *
     * アップロード処理の事前準備があればそれを実装する
     *
     * @param  array $file ファイル情報の配列
     * @return array $file ファイル情報の配列
     *
     */
    protected function prepare(Array $file)
    {
        $config = $this->fileManager->getFileConfig($file['ufType']);
        $saveFile = $this->getSaveFilePath($file);
        if( !move_uploaded_file($file['tmpPath'], $saveFile) ) {
            throw new \Exception('アップロードファイルを保存できませんでした');
        }
        $file['tmpPath'] = $saveFile;
        
        return $file;
    }
    
    /**
     * function postProcess
     *
     * アップロード処理の事後処理があればそれを実装する
     *
     * @param  array $ret  返信情報の配列
     * @param  array $file ファイル情報の配列
     * @return array $ret  返信情報の配列
     *
     */
    protected function postProcess(Array $ret, Array $file)
    {
        return $ret;
    }
}
