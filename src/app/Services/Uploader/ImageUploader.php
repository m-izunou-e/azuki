<?php
namespace Azuki\App\Services\Uploader;
/**
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Services\Uploader\Abstracts\MediaUploader;

class ImageUploader extends MediaUploader
{
    /**
     * 指定ファイルが存在しない場合のデフォルトファイルパス
     *
     */
    protected $noFilePath = '/azuki/img/no_img.jpg';
    
    /**
     * このアップローダーで許可されるmime-type
     *
     */
    protected $allowMimeType = [
        'image/*', 
    ];
    
    /**
     * function prepare
     *
     * アップロード処理をする際に必要となる事前処理があれば行う
     * →画像であれば許可されるサイズにリサイズしたり、方向を変更するなど
     *
     * @param  array $file ファイル情報をセットした配列
     * @return array $file 必要に応じて情報変更をしたファイル情報配列
     */
    protected function prepare(Array $file)
    {
        $config = $this->fileManager->getFileConfig($file['ufType']);
        $saveFile = $this->getSaveFilePath($file);
        if($this->requireConvert($config, $file) && $this->canConvertImageMime($file['mimeType'])) {
            $this->convertImage($config, $file['tmpPath'], $saveFile);
        } else {
            if( !move_uploaded_file($file['tmpPath'], $saveFile) ) {
                throw new \Exception('アップロードファイルを保存できませんでした');
            }
        }
        $file['tmpPath'] = $saveFile;
        
        if($this->requireCreateThumbnail($config, $file)) {
            $file = $this->createThumbnail($config, $file);
        }
        
        return $file;
    }
    
    /**
     *
     *
     *
     */
    protected function createThumbnail($config, $file)
    {
        $srcFile       = $file['tmpPath'];
        $thumbConfig   = $config['thumbnail'] ?? [];
        $thumbnailFile = $this->getThumbnailFilePath($thumbConfig, $srcFile);
        if($this->canConvertImageMime($file['mimeType'])) {
            $this->convertImage($thumbConfig, $srcFile, $thumbnailFile);
            $file['thumbnail'] = $this->createThumbnailInfo($file, $thumbnailFile);
        } else {
            // 変換できない場合元データとサムネイルデータが同じになってしまうので、ログだけ残す
            warningExLog('サムネイル画像作成を画像変換不可のためスルー');
        }
        
        return $file;
    }
    
    /**
     * function postProcess
     *
     * アップロード処理をした際に必要となる事後処理があれば行う
     * →一時ファイルを作成していた場合にその削除処理など
     *
     * @param  array $ret  返信情報の配列
     * @param  array $file ファイル情報の配列
     * @return array $ret  返信情報の配列
     *
     */
    protected function postProcess(Array $ret, Array $file)
    {
        if( file_exists($file['tmpPath']) ) {
            @unlink($file['tmpPath']);
        }
        if( isset($file['thumbnail']) && file_exists($file['thumbnail']['tmpPath']) ) {
            @unlink($file['thumbnail']['tmpPath']);
        }
        
        return $ret;
    }
}
