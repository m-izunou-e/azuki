<?php
namespace Azuki\App\Services\Abstracts;
/**
 * 一覧の検索に関する情報をセッションを使って管理し、提供するサービス
 * の抽象クラス。イレギュラーな検索条件管理が必要な場合はこれを継承してオーバーライドし
 * サービスプロバイダでの登録を変更するか使用する場所で呼び出しを変更する
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

abstract class SomethingSession
{
    /**
     * $session
     *
     * セッションオブジェクト
     * メンバ変数に持つことでオーバーヘッドを削減
     */
    protected $session;

    /**
     * $sessionName
     *
     */
    protected $sessionName;
    
    /**
     * $sessionNamePrefix
     *
     * セッション名の接頭辞
     */
    protected $sessionNamePrefix = '';

    /**
     * function __construct
     *
     */
    public function __construct($app)
    {
        $this->session = $app->session;
    }
    
    /**
     * function setSessionData
     *
     */
    protected function setSessionData($data, $sessionName = null)
    {
        $sessionName = $this->getSessionName($sessionName);
        $this->session->put($this->sessionNamePrefix.$sessionName, $data);
    }
    
    /**
     * function forgetSessionData
     *
     */
    protected function forgetSessionData($sessionName = null)
    {
        $sessionName = $this->getSessionName($sessionName);
        $this->session->forget($this->sessionNamePrefix.$sessionName);
    }
    
    /**
     * function getSessionData
     *
     */
    protected function getSessionData($sessionName = null)
    {
        $sessionName = $this->getSessionName($sessionName);
        return empty($sessionName) ? [] : $this->session->get($this->sessionNamePrefix.$sessionName);
    }
    
    /**
     * function setSessoinName
     *
     */
    public function setSessoinName($name)
    {
        $this->sessionName = $name;
    }
    
    /**
     * function getSessionName
     *
     *
     */
    public function getSessionName($sessionName = null)
    {
        $sessionName = is_null($sessionName) ? $this->sessionName : $sessionName;
        return $sessionName;
    }

    /**
     * function reset 情報をリセットする
     * 
     * 指定された名前に該当する情報をセッションから削除する
     * 
     * @param string $sName
     */
    public function reset( $sessionName = null )
    {
        $this->forgetSessionData($sessionName);
    }
    
    /**
     * function set 情報をセットする
     * 
     * 指定された名前と情報をセッションにセットする
     * 
     * @param string $sName
     * @param Array  $search
     */
    public function set( $data, $sessionName = null )
    {
        $this->setSessionData($data, $sessionName);
    }
    
    /**
     * function get 設定された情報を取得する
     * 
     * 指定された名前に紐づいたkeyで指定された情報を返す
     * 
     * @param string $sName
     * @param string $key
     */
    public function get( $key = null, $name = null, $sessionName = null )
    {
        $data = $this->getSessionData($sessionName);
        
        if( !is_null($name) ) {
            $data = isset($data[$name]) ? $data[$name] : [];
        }
        $ret = $data;

        if( !is_null($key) ) {
            $ret = isset($data[$key]) ? $data[$key] : "";
        }

        return $ret;
    }
    
    /**
     * function getAll 設定された検索情報すべてを取得する
     * 
     * @param string $sName
     */
    public function all( $sessionName = null )
    {
        $data = $this->getSessionData($sessionName);
        return isset($data) ? $data : [];
    }
}
