<?php
namespace Azuki\App\Services\Abstracts;
/**
 * 独自のバリデーションルールをここに追加する
 *
 */

use Illuminate\Validation\Validator;

abstract class CustomValidator extends Validator
{
    /**
     * funciotn validateMatchUserPassword
     *
     * ユーザーIDに対してパスワードが一致するかを検証する
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return int
     */
    public function validateMatchUserPassword($attribute, $value, $parameters)
    {
        $this->requireParameterCount(1, $parameters, 'matchUserPassword');

        $model  = $parameters[0];

        $column = isset($parameters[1]) ? $parameters[1] : $attribute;
        $val    = isset($parameters[2]) ? $parameters[2] : $value;

        $query = $this->createModel($model)->prepare();
        $row = $query->where($column, '=', $val)->first();

        // そもそも指定条件のデータがなければ失敗
        if( is_null($row) ) {
            return false;
        }

        $hasher = $this->container['hash'];
        return $hasher->check($value, $row->getAuthPassword());
    }

    /**
     * funciotn validateUniqueOnBelong
     *
     * 所属に対してデータが一意であるかを検証する
     * データ更新時は自身を除いて一意であることを確認する必要がある
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return int
     */
    public function validateUniqueOnTable($attribute, $value, $parameters)
    {
        $ret = true;
        $this->requireParameterCount(2, $parameters, 'uniqueOnTable');

        $model  = $parameters[0];
        $column = $parameters[1];
        $id     = isset($parameters[2]) && !empty($parameters[2]) ? $parameters[2] : null;

        $model = $this->createModel($model);
        $table = $model->getTable();
        $query = $model->prepare(null, false);
        $query = $query->where($column, '=', $value);
        if(!is_null($id)) {
            $query = $query->where($table.'.id', '!=', $id);
        }

        $row = $query->first();
        
        if( !is_null($row) ) {
            $ret = false;
        }
        
        return $ret;
    }

    /**
     * funciotn validateUniqueOnBelong
     *
     * 所属に対してデータが一意であるかを検証する
     * データ更新時は自身を除いて一意であることを確認する必要がある
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return int
     */
    public function validateUniqueOnBelong($attribute, $value, $parameters)
    {
        $ret = true;
        $this->requireParameterCount(3, $parameters, 'uniqueOnBelong');

        $model  = $parameters[0];
        $column = $parameters[1];
        $belong = $parameters[2];
        $id     = isset($parameters[3]) && !empty($parameters[3]) ? $parameters[3] : null;

        $model = $this->createModel($model);
        $table = $model->getTable();
        if(!is_null($belong)) {
            if(method_exists($model, 'setBelongValue')) {
                $model->setBelongValue($belong);
            }
        }
        $query = $model->prepare();
        $query = $query->where($column, '=', $value);
        if(!is_null($belong)) {
            $query = $query->where('belong','=', $belong);
        }
        if(!is_null($id)) {
            $query = $query->where($table.'.id', '!=', $id);
        }

        $row = $query->first();
        
        if( !is_null($row) ) {
            $ret = false;
        }
        
        return $ret;
    }

    /**
     * 改行コードなしでのカウントValidate the size of an attribute is greater than a minimum value.
     *
     * @param  string  $attribute
     * @param  mixed   $value
     * @param  array   $parameters
     * @return bool
     */
    protected function validateMinNL($attribute, $value, $parameters)
    {
        $valueNoLineFeedCode = preg_replace("/\r|\n/", "", $value);
        return parent::validateMin($attribute, $valueNoLineFeedCode, $parameters);
    }

    /**
     * 改行コードなしでのカウントValidate the size of an attribute is less than a maximum value.
     *
     * @param  string  $attribute
     * @param  mixed   $value
     * @param  array   $parameters
     * @return bool
     */
    protected function validateMaxNL($attribute, $value, $parameters)
    {
        $valueNoLineFeedCode = preg_replace("/\r|\n/", "", $value);
        return parent::validateMax($attribute, $valueNoLineFeedCode, $parameters);
    }

    /**
     * minNLのルールに設定した数字に[:minNL]を置換する処理です。
     * このメソッドがあるため、バリデーションメッセージで「:minNL」を指定することが出来ます。
     *
     * @param  string  $message
     * @param  string  $attribute
     * @param  string  $rule
     * @param  array   $parameters
     * @return string
     */
    protected function replaceMinNL($message, $attribute, $rule, $parameters)
    {
        return str_replace(':minNL', $parameters[0], $message);
    }

    /**
     * manNLのルールに設定した数字に[:manNL]を置換する処理です。
     * このメソッドがあるため、バリデーションメッセージで「:manNL」を指定することが出来ます。
     *
     * @param  string  $message
     * @param  string  $attribute
     * @param  string  $rule
     * @param  array   $parameters
     * @return string
     */
    protected function replaceMaxNL($message, $attribute, $rule, $parameters)
    {
        return str_replace(':maxNL', $parameters[0], $message);
    }

    /**
     * funciotn validateZipCode
     *
     * 郵便番号の検証
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return int
     */
    public function validateZipCode($attribute, $value, $parameters)
    {
        return preg_match("/^[0-9]{7}+$/u", $value);
    }

    /**
     * funciotn validateTel
     *
     * 電話番号の検証
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return int
     */
    public function validateTel($attribute, $value, $parameters)
    {
        return preg_match("/^[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}$/u", $value);
    }

    /**
     * funciotn validateKana
     *
     * カタカナの検証
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return int
     */
    public function validateKana($attribute, $value, $parameters)
    {
        return preg_match("/^[ァ-ヶー]+$/u", $value);
    }

    /**
     * funciotn validateKanahSpace
     *
     * カタカナの検証（空白許可）
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return int
     */
    public function validateKanaSpace($attribute, $value, $parameters)
    {
        return preg_match("/^([ァ-ヶー　 ])+$/u", $value);
    }

    /**
     * Create a new instance of the model.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function createModel( $model )
    {
        return app($model);
    }

    /**
     * ※非推奨・・alpha_num　を使用すること
     */
    public function validateAlphaNumJ($attribute, $value, $parameters)
    {
        return $this->validateAlphaNum($attribute, $value, null);
    }

    /**
     * function validateAlphaNumAtJ
     *
     * 半角英数字+@マーク
     * ※非推奨・・alpha_num_symbol　を使用すること
     *
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return true
     */
    public function validateAlphaNumAtJ($attribute, $value)
    {
        return $this->validateAlphaNumSymbol($attribute, $value, ['@.']);
    }

    /**
     * function validateAlphaNumSymbol
     *
     * 半角英数字+記号
     *
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return true
     */
    public function validateAlphaNumSymbol($attribute, $value, $parameters)
    {
        if (! is_string($value) && ! is_numeric($value)) {
            return false;
        }
        $marks = isset($parameters[0]) && is_string($parameters[0]) && !empty($parameters[0]) ?
            $parameters[0] : "!-\/:-@¥[-`{-~";
        $regrex = sprintf('/^[a-z0-9%s]+$/iu', $marks);

        return preg_match($regrex, $value);
    }
    /**
     * alpha_num_symbol_j は非推奨
     */
    public function validateAlphaNumSymbolJ($attribute, $value, $parameters)
    {
        return $this->validateAlphaNumSymbol($attribute, $value, $parameters);
    }

    /**
     * function validateServerString
     *
     * ipアドレス形式あるいはドメイン形式
     *
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return true
     */
    public function validateServerString($attribute, $value, $parameters)
    {
        return $this->validateIp($attribute, $value) || $this->validateDomain($attribute, $value, $parameters);
//        return preg_match("/^[a-z0-9]+[\.a-z0-9]+[a-z0-9]+$/iu", $value);
    }

    /**
     * function validatePathString
     *
     * [IPアドレスorサーバー名:共有パス]、[IPアドレスorサーバー名/共有名]
     * とディレクトリパスとして有効なものを許可する
     * 現状のルールとしては厳格性は低いものになっている
     *
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return true
     */
    public function validatePathString($attribute, $value, $parameters)
    {
        return preg_match("/^[a-z0-9]$/iu", $value) || preg_match("/^[a-z0-9][:a-z0-9_\-\.\/]*[a-z0-9]$/iu", $value);
    }

    /**
     * function validateDomain
     *
     * ドメイン形式
     *
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return true
     */
    public function validateDomain($attribute, $value, $parameters)
    {
        return preg_match("/^([a-z0-9][a-z0-9\-\_]{0,61}[a-z0-9]\.)+[a-z]+$/iu", $value) && strlen($value) < 255;
    }

    /**
     * funciotn validateDeplicateBetween
     *
     * 期間が他の期間と重複していないか（他の期間に含まれていないか）をチェックする
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return int
     */
    public function validateDeplicateBetween($attribute, $value, $parameters)
    {
        $this->requireParameterCount(2, $parameters, 'deplicateBetween');
        $model = $parameters[0];
        $id    = $parameters[1];

        $model = $this->createModel($model);

        $query = $model
            ->where('start', '<', $value)
            ->where('end',   '>', $value);
        if(!is_null($id)) {
            $query = $query->where('id', '!=', $id);
        }

        $record = $query->get();

        return count($record) > 0 ? false : true;
    }

    /**
     * funciotn validateIncludeOther
     *
     * 期間が他の期間を含んでいないかをチェックする
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return int
     */
    public function validateIncludeOther($attribute, $value, $parameters)
    {
        $this->requireParameterCount(4, $parameters, 'includeOther');
        $model = $parameters[0];
        $id    = $parameters[1];
        $start = $parameters[2];
        $end   = $parameters[3];

        $model = $this->createModel($model);

        $query = $model
            ->where('start', '>=', $start)
            ->where('start', '<',  $end)
            ->where('end',   '>',  $start)
            ->where('end',   '<=', $end);

        if(!is_null($id)) {
            $query = $query->where('id', '!=', $id);
        }

        $record = $query->get();

        return count($record) > 0 ? false : true;
    }
    
    /**
     * funcion validateAllowValueWhen
     *
     * ある項目の値に対して許可される値に制限がある場合にその制限された値であることを判定する
     *
     * $parameters[0] 許可される値
     * $parameters[1] 条件になる項目のキー名
     * $parameters[2] 条件になる項目の制限が発動する値
     *
     */
    public function validateAllowValueWhen($attribute, $value, $parameters)
    {
        $this->requireParameterCount(3, $parameters, 'allowValueWhen');
        $allow = $parameters[0];
        $key = $parameters[1];
        $val = $parameters[2];

        $data = $this->getData();
        $compare = $data[$key];
        
        $ret = true;
        if( $value != $allow && $compare == $val ) {
            $ret = false;
        }
        
        return $ret;
    }
    
    /**
     * funcion validateArrayNoDeplicate
     *
     * 配列の中に重複要素が存在するかをチェックする
     *
     */
    public function validateArrayNoDeplicate($attribute, $value, $parameters)
    {
        $ret = true;
        
        $key = isset($parameters[0]) ? $parameters[0] : null;
        $array = $value;
        if(!is_null($key)) {
            $array = [];
            foreach($value as $val) {
                if(!isset($val[$key])) {
                    return false;
                }
                $array[] = $val[$key];
            }
        }

        if( count(array_unique($array)) != count($array) ) {
            $ret = false;
        }
        
        return $ret;
    }

    /**
     * funciotn validateNotChangeReference
     *
     * 指定モデルの指定IDのデータの指定カラムが指定モデルの指定カラムにて参照されていないか
     * を確認する
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return int
     */
    public function validateNotChangeReference($attribute, $value, $parameters)
    {
        $ret = true;
        $this->requireParameterCount(5, $parameters, 'notChangeReference');

        $model     = $parameters[0];
        $id        = $parameters[1];
        $column    = $parameters[2];
        $refModel  = $parameters[3];
        $refColumn = $parameters[4];
        
        if( is_null($id) ) {
            return $ret;
        }

        $model = $this->createModel($model);
        $obj = $model->find($id);
        if(is_null($obj) || $obj->{$column} == $value) {
            return $ret;
        }

        $refModel = $this->createModel($refModel);
        $refObj = $refModel->where($refColumn, '=', $obj->{$column})->first();
        if( !is_null($refObj) ) {
            $ret = false;
        }
        
        return $ret;
    }
}
