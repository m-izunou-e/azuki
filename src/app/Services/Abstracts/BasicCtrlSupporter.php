<?php
namespace Azuki\App\Services\Abstracts;

/**
 * コントローラ—をサポートするサービス
 *
 * コントローラ—にて本オブジェクトのインスタンスを取得し、設定をinitして、必要なコールバックがあれば
 * bind　することで、一連の処理が実装される。
 *
 * 各アクショントレイトは処理の大きな流れを制御し、本インスタンスを適宜呼び出すことで処理が実行される
 * 本インスタンスはコントローラで設定された内容に応じて、動的に処理が変更される。
 *
 * □本クラスの現状
 *   本クラスは管理画面の生成の流れで作成されており、このままではフロント画面の生成には適合していない。
 *   アクショントレイトも同様に管理画面用になっているので、フロント側はフロント側ように作成が必要となる。
 * □本クラスの拡張
 *   本クラスにて、アクショントレイトやコントローラーから呼び出しのあるメソッドについてはインターフェースを
 *   別途作成し、そのインターフェースを実装する形にするのがよい。
 *   また、管理画面、フロント画面問わず共通な部分を切り出して、抽象クラス化し、それを継承するようにする
 *   そうすることで、各所に応じたコントローラーサポートのクラスを生成することが容易になる
 *   その上で、それぞれサービス登録し、コントローラーでインスタンス取得時に、取り出す名前を変えることで各所に応じた
 *   クラスのインスタンスを取得できるようになる。
 *
 */

use Azuki\App\Services\Abstracts\CtrlSupporter;
use Azuki\App\Services\Interfaces\CtrlSupportInterfaceForBaseAction;
use Azuki\App\Services\ExtendLog;

class BasicCtrlSupporter extends CtrlSupporter implements CtrlSupportInterfaceForBaseAction
{
    /**
     * モデル
     *
     * @var Illuminate\Database\Eloquent\Model model
     * 使用するモデルクラスをメンバに保持します。
     * 複数持てるように拡張予定です。
     */
    protected $model;
    
    /**
     * バリデーター
     *
     * @var App\Http\Validation\Validator validator
     * 使用するバリデーターをメンバに保持します。
     * 複数持てるように拡張予定です。
     */
    protected $validator;
    
    /**
     * フォーム設定
     *
     * @var Array forms
     * フォームの設定をメンバに保持します。
     * この変数に設定された内容でデータの管理が行われるため、必須となります。
     */
    protected $forms = [];
    
    /**
     * 一覧の設定
     *
     * @var Array list
     * 一覧に表示する内容などの設定をメンバに保持します。
     * これを設定しないとテンプレートでがりがり一覧のHTMLを記載する必要があります。
     */
    protected $list = [];
    
    /**
     * 並び順の設定
     *
     * @var Array order
     * 並び順の設定の初期条件をメンバに保持します。
     */
    protected $initializeOrder = ['order' => 'id', 'condition' => ['id' => 'asc']];
    
    /**
     * 検索フォームの設定
     *
     * @var Array searchForm
     * 検索フォームの設定をメンバに保持します。
     * この設定を元に検索フィールドを生成します。
     */
    protected $searchForm = [];
    
    /**
     * function init
     *
     * 引数で与えられた設定値を元にオブジェクトの初期化を行う
     *
     * @param Array $config 設定値の配列
     */
    protected function additionalInit( $config )
    {
        $this->model     = $this->generateModel($config);
        $this->validator = $this->generateValidator($config);
//        $this->setListSetting($config);
        $this->setOrderSetting($config);
//        $this->setSearchForm($config);
        
        app('SearchSession')->setSessoinName($this->getCanonicalUrlName());
        app('OrderSession')->setSessoinName($this->getCanonicalUrlName());
        
        // 変数追加による暫定対応。本当は各コントローラー側に設定追加する方が統一されているのだけど、
        // 未設定時にテンプレート側で処理するのが面倒なのでここで一括でデフォルト設定する。
        $ctrlCondition = [];
        if(isset($config['views']['ctrlCondition'])) {
            $ctrlCondition = $config['views']['ctrlCondition'];
        }
        view()->share('ctrlCondition', $ctrlCondition);
    }
    
    /**
     * function initBind
     *
     * コールバックメソッドの初期登録を行う
     *
     */
    protected function initBind()
    {
        // インデックスアクセス時のリダイレクト先をルート名を返す処理
        $this->bind('getRouteForIndexRedirect', function(){
            return $this->getNameListRoute();
        });

        // 一覧表示時のアサインの追加処理
        $this->bind('shareAsaignList', function(){
            view()->share('order',      $this->getOrderCondition());
        });

        // 登録編集時のアサインの追加処理
        $this->bind('shareAsaignForm', function(){
            view()->share('forms', $this->forms);
        });

        // 確認時のアサインの追加処理
        $this->bind('shareAsaignConfirm', function(){
            view()->share('forms', $this->forms);
        });

        // 詳細時のアサインの追加処理
        $this->bind('shareAsaignDetail', function(){
            view()->share('forms', $this->forms);
        });
        
        // 登録・更新時の戻りURLを返す処理
        $this->bind('getRedirectNameFromComplete', function(){
            $ret = $this->getNameListRoute();
            return $ret;
        });
        
        // 一覧画面のURLを返す処理
        $this->bind('getNameListRoute', function(){
            return str_replace( strrchr( $this->urlName, '.' ) , '.list', $this->urlName );
        });
        
        // post、getでURL名が違うがそれをget側に合わせた名前として返す処理
        $this->bind('getCanonicalUrlName', function(){
            return str_replace( ['.p-', '.prev'], ['.', ''], $this->urlName );
        });
        
        $this->bind('beforeDisplayEditForm', function($id, $post){
        });

        $this->bind('beforeDisplayConfirm', function($id, $post){
        });

        $this->bind('beforeRegistData', function($data, $flow){
            return $data;
        });
        
    }
    
    /**
     * function generateModel
     *
     * 引数で与えられた設定からモデルクラスを生成して返す
     *
     * @param Array $config $config['model']にモデルオブジェクトかモデルクラス名が設定されている必要がある。
     * @return Illuminate\Database\Eloquent\Model
     *
     */
    protected function generateModel($config)
    {
        return $this->generateClass( $config, 'model', 'Illuminate\Database\Eloquent\Model' );
    }
    
    /**
     * function generateValidator
     *
     * 引数で与えられた設定からバリデートクラスを生成して返す
     *
     * @param Array $config $config['validator']にバリデートオブジェクトかバリデートクラス名が設定されている必要がある。
     * @return App\Http\Validation\Validator
     *
     */
    protected function generateValidator($config)
    {
        return $this->generateClass( $config, 'validator', 'Azuki\App\Http\Validation\Validator' );
    }
    
    /**
     * function generateClass
     *
     * 引数で与えられたクラスのインスタンスを生成して、インスタンス型のチェックをしたうえで、
     * そのインスタンスを返す
     *
     * @param Array  $config    すべての設定を持つ配列
     * @param string $name      対象の名前
     * @param string $instance  対象オブジェクトのインスタンス型
     *
     * @return $instance型のオブジェクト
     */
    protected function generateClass( $config, $name, $instance )
    {
        if( !isset($config[$name]) || empty($config[$name]) ) {
            abort(500, $name.'が設定されていません。');
        }
        
        $obj = $config[$name];
        if( is_string($obj) ) {
            $obj = app($obj);
        }
        if( $obj instanceof $instance ) {
            return $obj;
        }
        
        abort(500, $name.'が正しく設定されていません。');
    }
    
    /**
     * function setDetailForm
     *
     * フォームの設定をメンバ変数に設定する。
     * フォームの設定をしないと、原則的には機能しないので、設定がない場合はWarningを発行する
     *
     * @param Array $forms
     *
     */
    public function setDetailForm( $forms )
    {
        $this->setForm($forms, '詳細表示');
    }
    
    /**
     * function setForm
     *
     * フォームの設定をメンバ変数に設定する。
     * フォームの設定をしないと、原則的には機能しないので、設定がない場合はWarningを発行する
     *
     * @param Array $forms
     *
     */
    public function setForm( $forms, $place = '新規登録・編集' )
    {
        if( empty($forms) ) {
            $msg = sprintf('-[%s]:%sのフォームの設定がされていません。', $this->urlName, $place);
            $this->logWrite(ExtendLog::WARNING, $msg);
            return false;
        }

        $this->forms = $forms;
    }
    
    /**
     * function setListSetting
     *
     * 一覧の設定があれば、メンバ変数に設定する
     *
     * @param Array $config 使用するのは　$config['list']
     *
     */
    protected function setListSetting($config)
    {
        $this->list = isset($config['list']) ? $config['list'] : [];
    }
    
    /**
     * function setOrderSetting
     *
     * 並び替えの設定があれば、メンバ変数に設定する
     *
     * @param Array $config 使用するのは　$config['list'],$config['order']
     *
     */
    protected function setOrderSetting($config)
    {
/*
        $list  = isset($config['list']) ? $config['list'] : [];
        $order = [];
        foreach( $list as $row ) {
            if(isset($row['isOrder']) && $row['isOrder'] == true) {
                $order['condition'][$row['column']] = isset($row['order']) ? $row['order'] : 'none';
            }
        }
        $order['order'] = isset($config['order']) ? $config['order'] : '';
*/
        $order = isset($config['order']) && !empty($config['order']) ? $config['order'] : ['order' => ''];
        $this->initializeOrder = $order;
    }
    
    /**
     * function setSearchForm
     *
     * 検索フォームの設定があれば、メンバ変数に設定しますします
     *
     * @param Array $config 使用するのは　$config['searchForm']
     *
     */
    protected function setSearchForm($config)
    {
        $this->searchForm = isset($config['searchForm']) ? $config['searchForm'] : [];
    }
    
    /**
     * function getForm
     *
     * フォームの設定を返す
     *
     */
    public function getForm()
    {
        return $this->forms;
    }
    
    /**
     * function getModel
     *
     * モデルオブジェクトを返す
     * 将来的にモデルを複数管理できるように拡張したいので、その時にはここで
     * どのモデルを返すかという部分を吸収させる
     *
     */
    protected function getModel()
    {
        return $this->model;
    }
    
    /**
     * function getValidator
     *
     * バリデーションオブジェクトを返す
     * 将来的にバリデーションを複数管理できるように拡張したいので、その時にはここで
     * どのバリデーションを返すかという部分を吸収させる
     *
     */
    protected function getValidator()
    {
        return $this->validator;
    }
    
    /**
     * function initializeValidator
     *
     * バリデーションオブジェクトを初期化する
     *
     */
    public function initializeValidator($option)
    {
        $this->validator->setValidInitialize($option);
    }
    
    /**
     * function getSubstituteIndexRoute
     *
     * インデックスアクセス時にリダイレクト先のルートを返すメソッド
     * 引数でリダイレクト先の指定がない場合はgetRouteForIndexRedirectメソッドで
     * 取得する。これはbindにて設定しており、デフォルトは一覧画面とする
     *
     * @param $to string リダイレクト先のルーティング名
     * @return Route 
     */
    public function getSubstituteIndexRoute($to = null, $param = [])
    {
        $redirectTo = $to == null ? $this->getRouteForIndexRedirect() : $to;
        return route($redirectTo, $param);
    }
    
    /**
     * function resetCondition
     *
     * 検索条件(オーダー条件含む)をリセットするメソッド
     * POSTもGETも同じセッションで管理する。そのために$this->urlNameではなく$this->getCanonicalUrlName()　をキーに指定している
     * 並び替え（order）は全てリセット後に、初期値を設定する必要がある。
     *
     */
    public function resetCondition()
    {
        $page = $this->request->input('page');
        if( !isset($page) || empty($page) || !is_numeric($page) ) {
            app('SearchSession')->reset();
            app('OrderSession')->reset();
            app('OrderSession')->set( $this->initializeOrder );
        }
    }

    /**
     * function setCondition
     *
     * 検索条件(オーダー条件含む)をセットするメソッド
     *
     * オーダー条件 name属性がorder[]　でポストされてきたデータ
     * 検索条件     name属性がsearch[] でポストされてきたデータ
     * それぞれが存在すれば、それを設定するメソッドの呼び出しを行う
     *
     */
    public function setCondition($condition = null)
    {
        $order = isset($condition['order']) ? $condition['order'] : $this->request->get('order');
        if( isset($order) && is_array($order) ) {
            $this->setOrder($order);
        }
        
        $search = isset($condition['search']) ? $condition['search'] : $this->request->get('search');
        if( isset($search) && is_array($search) ) {
            $this->setSearch($search);
        } elseif($this->request->get('list_search_btn')) {
            $this->setSearch([]);
        }
        
    }
    
    /**
     * function getSearchBtnKind
     *
     * 検索フィールドが簡易検索か詳細検索かどちらで検索されたのかを
     * 判定してその種類を返す
     *
     */
    public function getSearchBtnKind()
    {
        $kind = '';
        if( $this->request->has('list_search_short_btn') ) {
            $kind = 'short';
        } elseif( $this->request->has('list_search_btn') ) {
            $kind = 'normal';
        }
        
        return $kind;
    }

    /**
     * function setOrder
     *
     * オーダー条件を整形して、セッションに設定する
     * 複数のおーだ—条件を管理できるように、条件に名前を付けて管理できるようにしている
     * order[name] の name 属性を設定すると、そこに設定された名前でオーダー条件を管理する
     * 名前の指定がない場合は「default」という名前で管理する
     *
     */
    protected function setOrder( $order )
    {
        $condition = [];
        $name = isset($order['name']) && !empty($order['name']) ? $order['name'] : 'default';
        $condition[$name] = $order;
        
        // orderのorderを設定する
        if( isset($order['click']) && !empty($order['click']) ) {
            $order['order'] = str_replace($order['click'], '', $order['order']);
            if( isset($order['condition'][$order['click']]) && $order['condition'][$order['click']] != 'none' ) {
                $condition[$name]['order'] = $order['click'] . '|' . $order['order'];
            }
        }
        
        app('OrderSession')->set( $condition );
    }

    /**
     * function setSearch
     *
     * 検索条件を整形して、セッションに設定する
     * 複数の検索条件を管理できるように、条件に名前を付けて管理できるようにしている
     * order[name] の name 属性を設定すると、そこに設定された名前で検索条件を管理する
     * 名前の指定がない場合は「default」という名前で管理する
     *
     */
    protected function setSearch( $search )
    {
        $condition = [];
        $name = isset($search['search-name']) && !empty($search['search-name']) ? $search['search-name'] : 'default';
        $condition[$name] = $search;
        
        app('SearchSession')->set($condition);
    }

    /**
     * function paginate
     *
     * 一覧に表示するデータをページャー情報を含めて取得する
     * モデルからクエリを取得して、そのクエリに対して
     * １．初期設定　select やjoin　など
     * ２．検索条件を設定
     * ３．オーダー条件を設定
     * して、paginateを実行し、条件に合致するデータの取得を行う
     *
     * @param  numeric $num １ページに表示する件数 未設定だと$this->perPageを使用する
     * @return Object
     *
     */
    public function paginate($num = null)
    {
        $model = $this->getModel();
    
        $num = is_numeric($num) ? $num : $this->perPage;

        $query = $model->query();

        // 事前準備
        $query = $model->prepare($query);

        // 検索条件をセット
        $query = $model->setCondition($query, $this->getSearchCondition());
        
        // オーダーをセット
        $query = $model->setOrder($query, $this->getOrderCondition());

        // データの取得
        if($num == 0) {
            return $query->get();
        } else {
            return $query->paginate($num);
        }
    }
    
    /**
     * function getSearchConditionPost
     *
     * 検索条件をセッションから取得して検索ポスト配列にして返す
     *
     * @return Array
     */
    public function getSearchConditionPost()
    {
        $ret = [];
        $cond = $this->getSearchCondition();
        
        foreach($cond as $key => $row) {
            $ret[sprintf('search[%s]', $key)] = $row;
        }
        
        return $ret;
    }
    
    /**
     * function getSearchCondition
     *
     * 検索条件をセッションから取得して返す
     * defaultが設定されている場合は、defaultの中身だけを返す
     * 名前を付けて管理されている場合は、モデルクラス側でもその名前に応じて実装をしておく必要がある
     *
     * @return Array
     */
    protected function getSearchCondition()
    {
        $ret = [];
        $search = app('SearchSession')->all();

        $ret = $search;
        if( isset($search['default']) && !empty($search['default']) ) {
            $ret = $search['default'];
        }
        
        return $ret;
    }
    
    /**
     * function getOrderCondition
     *
     * オーダー条件をセッションから取得して返す
     * defaultが設定されている場合は、defaultの中身だけを返す
     * 名前を付けて管理されている場合は、モデルクラス側でもその名前に応じて実装をしておく必要がある
     *
     * @return Array
     */
    protected function getOrderCondition()
    {
        $ret = [];
        $order = app('OrderSession')->all();

        $ret = $order;
        if( isset($order['default']) && !empty($order['default']) ) {
            $ret = $order['default'];
        }
        
        return $ret;
    }


    /**
     * function getDefaultPost
     *
     * フォーム設定からデフォルト値の指定があるものを抜き出して返す
     * デフォルト値の指定は、フォームに新規にアクセスした際に使用される
     * →get formアクション時に呼ばれ、この戻り値がpostとしてviewに設定される
     */
    public function getDefaultPost()
    {
        $ret = [];

        $forms = $this->getForm();
        foreach( $forms as $form ) {
            if( isset($form['default']) && $this->isFormType($form['type'])) {
                $ret[$form['name']] = $form['default'];
            }
        }

        return $ret;
    }

    /**
     * function getPost
     *
     * post form、post edit　の際に呼び出される。
     * ポストされたデータから$thi->formsに設定されたデータを抜き出して返す
     * flowとidについてはここで、viewに設定をしている。
     * ↑１メソッドが2つの役割を持ってしまっているのでリファクタリング対象　TODO
     *
     */
    public function getPost()
    {
        $ret = [];
        $request = $this->request;

        $forms = $this->getForm();
        foreach( $forms as $form ) {
            if( !$this->isFormType($form['type']) ) {
                continue;
            }
            $ret[$form['name']] = $request->get($form['name']);
        }

        $flow = $request->get('flow');
        $id   = $request->get('id');

        view()->share('flow', $flow);
        view()->share('id',   $id);
        
        return $ret;

    }
    
    /**
     * function getPrev
     *
     * postされたprev情報を返す
     * postされたprevがない場合は「list」を返す
     * →これは戻りを指定する情報。詳細から編集に入ったらdetailになるなどで管理している
     *
     */
    public function getPrev()
    {
        $request = $this->request;
        $prev = $request->get('prev');
        return isset($prev) ? $prev : "list";
    }


    /**
     * function validation
     *
     * バリデーションを行う。ここで引数でとっている　$request　はFormRequestクラスのインスタンス
     * バリデーションは通常のリクエストクラスでは実装出来ないため引数でもらう必要がある。
     * なお、呼び出し元のアクションでは、タイプヒントによって、自動的に注入されている。
     *
     * ポストデータを$this->formsの設定に応じてデータの取り出しを行い、バリデーションを掛ける
     * flowとidをここでviewに設定している
     * ↑１つのメソッドで２つの処理なので、リファクタんリグ対象 TODO
     *
     * ※バリデーション対象のデータを加工してバリデーションをかけるという仕組み
     *   例）電話番号の場合、ハイフンを抜いて、半角にしたものを数字〇桁のバリデーションにかけるなど
     *   を以前はしていたが、バリデーション側でtelephoneなどのカスタムバリデーションを作成して対応する形に変更する
     *
     */
    public function validation($request)
    {
        $forms = $this->getForm();

        $valid = [];
        foreach( $forms as $form ) {
            if( !$this->isFormType($form['type']) || !isset($form['column']) ) {
                continue;
            }
            $val = $request->get($form['name']);
            if(is_null($val)) {
                continue;
            }
            if(($val = $this->convertPostData($form, $val)) === false) {
                continue;
            }
            
            $valid[$form['column']] = $val;
            $post[$form['name']]    = $val;
        }

        $flow = $request->get('flow');
        $id   = $request->get('id');

        if(empty($valid)) {
            // TODO 重大なシステムエラー扱いでログを残して終了する必要あり
            abort(500);
        }
        $validator = $this->getValidator();
        $validator->validation($request, $valid, $flow, $id);
        
        view()->share('flow', $flow);
        view()->share('id',   $id);
        
        return $post;
    }
    
    /**
     *
     */
    protected function convertPostData($form, $val)
    {
        $ret = false;
        if( !$this->isFormType($form['type']) || !isset($form['column']) ) {
            return $ret;
        }
        if( isset($form['unset']) && $form['unset'] == true ) {
            $ret = false;
        } elseif( $this->checkFormSet($form, 'unsetIfEmpty', $val) ) {
            $ret = false;
        } elseif( isset($form['ifEmptyIs']) && $this->customEmpty($val)) {
            $ret = $form['ifEmptyIs'];
        } elseif( $this->checkFormSet($form, 'isNullIfEmpty', $val) ) {
            $ret = NULL;
        } elseif( $this->checkFormSet($form, 'ignoreIfEmpty', $val) ) {
            $ret = false;
        } else {
            $ret = $val;
        }
        return $ret;
    }
    
    /**
     *
     *
     */
    protected function checkFormSet($form, $key, $val)
    {
        return isset($form[$key]) && $form[$key] == true && $this->customEmpty($val);
    }
    
    /**
     *
     *
     */
    protected function customEmpty($val)
    {
        if(is_numeric($val) && intval($val) === 0) {
            return false;
        }
        return empty($val);
    }

    /**
     * function find
     *
     * 引数で指定されたデータ１件を取り出す
     * 取り出したデータは$this->formsの設定に基づいてデータの持ち方をview側にあうように変更する
     *
     * なお、ここでidについてはviewにセットしている。
     * １つのメソッドで２つの処理ではあるがidのviewへのセットはここがしっくりくる。
     *
     * @param numeric $id
     */
    public function find($id, $setId = true)
    {
        $model   = $this->getModel();
        $orgData = $model->_findOrFail($id);
        $data    = $orgData->toArray();
        
        $forms = $this->getForm();
        $ret = [];
        foreach( $forms as $form ) {
            if( !$this->isFormType($form['type']) || !isset($form['column']) ) {
                continue;
            }
            if( isset($data[$form['column']]) && $data[$form['column']] !== '' ) {
                $ret[$form['name']] = $data[$form['column']];
            }
        }
        
        if($setId) {
            view()->share('id', $id);
        }

        view()->share('orgData', $orgData);
        return $ret;
    }

    /**
     * function getRecord
     *
     * 引数で指定されたデータ１件を取り出す
     *
     * @param numeric $id
     */
    public function getRecord($id)
    {
        $model = $this->getModel();
        return $model->find($id);
    }
    
    /**
     * function getLastRecordId
     *
     * 最新レコードのIDを返す。
     *
     * @param numeric $id レコードが存在しなかった場合に変身するID
     */
    public function getLastRecordId($id = 1)
    {
        $ret = $id;
        $record = $this->getLastRecord();
        if( !is_null($record) ) {
            $ret = $record->id;
        }
        return $ret;
    }
    
    /**
     * function getLastRecord
     *
     * 最新レコードを返す。
     * latestで並び替えてfirstで１件取り出している。
     * orderで並べ替えてfirstにした方が取得されるデータがわかりやすいか。
     *
     */
    public function getLastRecord()
    {
        return $this->getModel()->latest()->first();
    }

    /**
     * function createOrUpdate
     *
     * データの登録・更新を行う
     * $this->formsに設定された内容に応じて、データを登録できる状態に変更する
     * →DBへの登録が不要なデータのunset
     *
     * ※unsetとかここで行うようにしてみたものの、これってモデル側でやった方がいいんじゃね？と再考中 TODO
     *
     * @param Array $post
     */
    public function createOrUpdate($post)
    {
        $request = $this->request;

        $data = $post;
        /*
        $data = [];
        $forms = $this->getForm();
        foreach( $forms as $form ) {
            if( !$this->isFormType($form['type']) || !isset($form['column']) ) {
                continue;
            }
            if( isset($form['unset']) && $form['unset'] == true ) {
//                unset($post[$form['column']]);
            } elseif( isset($form['unsetIfEmpty']) && $form['unsetIfEmpty'] == true && empty($post[$form['name']]) ) {
//                unset($post[$form['column']]);
            } elseif( isset($form['ifEmptyIs']) && empty($post[$form['name']]) ) {
                $data[$form['column']] = $form['ifEmptyIs'];
            } elseif( isset($form['isNullIfEmpty']) && $form['isNullIfEmpty'] == true && empty($post[$form['name']]) ) {
                $data[$form['column']] = NULL;
            } elseif( isset($form['ignoreIfEmpty']) && $form['ignoreIfEmpty'] == true && empty($post[$form['name']]) ) {
                continue;
            } else {
                $data[$form['column']] = $post[$form['name']];
            }
        }
        */

        $flow = $request->get('flow');
        $id   = $request->get('id');
        $data = $this->beforeRegistData($data, $flow);
        
        if( $flow == 'edit' ) {
            if( empty($id) || !is_numeric($id) ) {
                abort(500, '更新対象のIDが不正です');
            }
            $this->update($id, $data);
        } else {
            $this->create($data);
        }
    }

    /**
     * function create
     *
     * データをDBに登録します。
     *
     * @param Array $post 登録データ
     *
     */
    public function create($post)
    {
        $model = $this->getModel();
        $result = $model->_create($post);
        $resultId = 0;
        if($result) {
            $resultId = $result->id;
        }
        $this->_setInsertId( $resultId );
    }

    /**
     * function update
     *
     * DBのデータを更新します
     *
     * @param numeric $id    更新対象のID
     * @param Array   $post  更新データ
     *
     */
    public function update($id, $post)
    {
        $model = $this->getModel();
        $result = $model->_update($id, $post);
        $this->_setUpdateSuccess( $result );
    }


    /**
     * function toggleValidDone
     *
     * 有効無効を切り替える
     *
     * ※リクエストからポストされた値を取得するが、name属性'open_id'は関連を制御できていないので、
     *   関連づくように修正した方がよいかもしれない
     */
    public function toggleValidDone()
    {
        $request = $this->request;
        $id = $request->get('open_id');
        if( !isset($id) || !is_numeric($id) ) {
            abort( 500, 'invalid id');
        }

        $model = $this->getModel();
        $this->_setOpenSuccess( $model->toggleValidDone($id) );
    }

    /**
     * function deleteDone
     *
     * 対象データを削除する
     *
     * ※リクエストからポストされた値を取得するが、name属性'delete_id'は関連を制御できていないので、
     *   関連づくように修正した方がよいかもしれない
     */
    public function deleteDone()
    {
        $request = $this->request;
        $id = $request->get('delete_id');
        $model  = $this->getModel();
        $record = $model->find($id);

        if( is_null($record) ) {
            abort( 500, 'data not exists in id');
        }

        \DB::beginTransaction();
        try {
            $ret = $record->delete();
            \DB::commit();
        } catch(\Exception $e) {
//            Log::error( $e->getMessage() );
//            Log::error( $e->getTraceAsString() );
            $ret = false;
            \DB::rollback();
        }
        $this->_setDeleteSuccess( $ret ? 1 : 0 );
//        $this->_setDeleteSuccess( $model->where('id', '=', $id)->delete() );

    }

    /**
     * function getOldInput
     *
     * flushされたデータを取得するメソッド
     * エラーで戻ってきた際に入力していた値を取り出すなどで使用します
     *
     */
    public function getOldInput()
    {
        return $this->request->old();
    }

    /**
     * function changeOrder
     *
     * 引数で指定されたデータとその移動先のデータのオーダーを変更する
     *
     * @param numeric $id
     */
    public function changeOrder($id, $orderKind)
    {
        $model = $this->getModel();
        
        if($orderKind == 'up') {
            $obj = $model->changeOrderUp($id);
        } else {
            $obj = $model->changeOrderDown($id);
        }
        
        return $obj;
    }

    /**
     * function setResultStatus
     *
     * 登録や更新、削除などの結果をセットします
     *
     */
    public function setResultStatus()
    {
        return $this->setListBackData();
    }

    /**
     * function setListBackData 一覧戻り時のデータセットを行うメソッド
     * 
     * 一覧戻り時のデータセットを行うメソッド
     *
     * 一覧画面では、処理結果を表示することが必要であるため、
     * 各処理結果の内容を一覧に表示するためのアサインを行う
     * @return void
     */
    protected function setListBackData()
    {
        $resultData = app('ResultSession')->all('temp');
    
        // 登録完了時
        if ( isset($resultData["insertId"]) ) {
            $set = $resultData["insertId"];
            $set = is_numeric($set) && $set > 0 ? true : false;
            // 成否をセット
            view()->share('insertSuccess', $set);
        }
        // 編集完了時
        else if ( isset($resultData["updateSuccess"]) ) {
            $set = $resultData["updateSuccess"];
//            $set = is_numeric($set) && $set > 0 ? true : false;
            // 成否をセット
            view()->share('updateSuccess', $set);
        }
        // 削除完了時
        else if ( isset($resultData["deleteSuccess"]) ) {
            $set = $resultData["deleteSuccess"];
            $set = is_numeric($set) && $set > 0 ? true : false;
            // 成否をセット
            view()->share('deleteSuccess', $set);
        }
        // 公開切替時
        else if ( isset($resultData["openSuccess"]) ) {
            $set = $resultData["openSuccess"];
            // 成否をセット
            view()->share('openSuccess', $set);
        }
        else if ( isset($resultData["changeOrderSuccess"]) ) {
            $set = $resultData["changeOrderSuccess"];
            // 成否をセット
            view()->share('changeOrderSuccess', $set);
        }
        else if ( isset($resultData["csvUploadResult"]) ) {
            view()->share('csvUploadResult', $resultData["csvUploadResult"]);
        }

        app('ResultSession')->reset('temp');
        return;
    }
    
    /**
     *
     *
     */
    protected function setResultSession( $key, $val )
    {
        app('ResultSession')->set([$key => $val], 'temp');
    }

    /**
     * function _setInsertId データ登録状態をセッションにセットする
     * 
     * データ登録状態をセッションのセットする
     * @return void
     */
    public function _setInsertId( $result )
    {
        $key = 'insertId';
        $this->setResultSession( $key, $result );
    }

    /**
     * function _setUpdateSuccess 更新成功状態をセッションにセットする
     * 
     * 更新成功状態をセッションにセットする
     * @return void
     */
    public function _setUpdateSuccess( $result )
    {
        $key = 'updateSuccess';
        $this->setResultSession( $key, $result );
    }

    /**
     * function _setDeleteSuccess 削除処理結果をセッションにセットする
     * 
     * データ登録状態をセッションのセットする
     * @return void
     */
    public function _setDeleteSuccess( $result )
    {
        $key = 'deleteSuccess';
        $this->setResultSession( $key, $result );
    }

    /**
     * function _setOpenSuccess 公開非公開の切替結果をセッションにセットする
     * 
     * データ登録状態をセッションのセットする
     * @return void
     */
    public function _setOpenSuccess( $result )
    {
        $key = 'openSuccess';
        $this->setResultSession( $key, $result );
    }

    /**
     * function _setChangeOrderSuccess 並び順の変更結果をセッションにセットする
     * 
     * データ登録状態をセッションのセットする
     * @return void
     */
    public function _setChangeOrderSuccess( $result )
    {
        $key = 'changeOrderSuccess';
        $this->setResultSession( $key, $result );
    }
    
    /**
     * function getRedirectPathFromComplete 登録・編集完了時にリダイレクトする先を指定するメソッド
     *
     * 登録・更新時にリダイレクトする先を決定する
     *
     * 基本はリスト画面に戻す。
     *
     * @return void
     */
    public function getRedirectPathFromComplete()
    {
        return route($this->getRedirectNameFromComplete());
    }
    
    /**
     * function setPerPage
     *
     * 一覧の表示レコード数をセットするメソッド
     *
     */
    public function setPerPage($num)
    {
        $this->perPage = $num;
    }
    
    /**
     * function isFormType
     *
     *
     */
    protected function isFormType($type)
    {
        $ret = true;
        if( in_array($type, ['title', 'label', 'button']) ) {
            $ret = false;
        }
        
        return $ret;
    }
}
