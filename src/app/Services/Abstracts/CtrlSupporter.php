<?php
namespace Azuki\App\Services\Abstracts;

/**
 * コントローラ―をサポートするサービスの抽象クラス
 *
 * コントローラ―にて本オブジェクトのインスタンスを取得し、設定をinitして、必要なコールバックがあれば
 * bind　することで、一連の処理が実装される。
 *
 * 各アクショントレイトは処理の大きな流れを制御し、本インスタンスを適宜呼び出すことで処理が実行される
 * 本インスタンスはコントローラで設定された内容に応じて、動的に処理が変更される。
 *
 * □本クラスの現状
 *   本クラスは管理画面の生成の流れで作成されており、このままではフロント画面の生成には適合していない。
 *   アクショントレイトも同様に管理画面用になっているので、フロント側はフロント側ように作成が必要となる。
 * □本クラスの拡張
 *   本クラスにて、アクショントレイトやコントローラーから呼び出しのあるメソッドについてはインターフェースを
 *   別途作成し、そのインターフェースを実装する形にするのがよい。
 *   また、管理画面、フロント画面問わず共通な部分を切り出して、抽象クラス化し、それを継承するようにする
 *   そうすることで、各所に応じたコントローラーサポートのクラスを生成することが容易になる
 *   その上で、それぞれサービス登録し、コントローラーでインスタンス取得時に、取り出す名前を変えることで各所に応じた
 *   クラスのインスタンスを取得できるようになる。
 *
 */
use Azuki\App\Services\ExtendLog;

abstract class CtrlSupporter
{
    /**
     * $exLog
     *
     * Log出力用のインスタンス
     */
    private static $exLog;

    /**
     * URL名
     *
     * @var string urlName
     * master.account.list　など。ポストの場合はp-listのように「p-」がつきます。
     * config/route.phpにて設定される値になります。
     */
    protected $urlName;
    
    /**
     * リクエストオブジェクト
     *
     * @var Request request
     * リクエストオブジェクトをinit時にメンバに保持します
     */
    protected $request;
    
    /**
     * テンプレート設定
     *
     * @var Array templates
     * 各アクションにて呼び出されるテンプレートをメンバに保持します
     * 指定がない場合はデフォルトのルールに従ったテンプレートが使用されます
     * →デフォルトはURL名と同じ名前のテンプレートです。
     */
    protected $templates = [];
    
    /*:
     * $templatePrefix
     *
     * テンプレートプレフィックス文字列
     * これをセットしている場合デフォルトのテンプレート名が $templatePrefix.$type になる。
     * →設定されていなければ、デフォルトテンプレート名はgetCanonicalUrlNameメソッドで決まる
     */
    protected $templatePrefix;
    
    /**
     * $className
     * コマンドのクラス名
     */
    private $className;
    
    /**
     * 一覧の１ページあたりの表示件数の設定
     *
     * @var Int perPage
     */
    protected $perPage = 50;
    
    /**
     * function __construct
     *
     * コンストラクタ。サービスプロバイダーによって、シングルトンで生成される
     *
     * @param object $app
     */
    public function __construct($app = null)
    {
        self::$exLog = app('ExtendLog');
        $this->className = get_class($this);

        $request = app('request');
        $this->request = $request;
        $route = $request->route();
        $this->urlName = isset($route) ? $route->getName() : '';
        $this->perPage = getMaxRowPerList();
    }
    
    /**
     * function init
     *
     * 引数で与えられた設定値を元にオブジェクトの初期化を行う
     *
     * @param Array $config 設定値の配列
     */
    public function init( $config )
    {
        $this->setTemplates($config);
        $this->setShareView($config);

        $this->initBind();

        $this->additionalInit($config);
        
        // getCanonicalUrlNameメソッドはbindで定義されるのでbind後にアサイン
        view()->share('cUrlName', $this->getCanonicalUrlName());
    }
    
    /**
     * function additionalInit
     *
     * 追加の初期化処理を行う
     *
     */
    abstract protected function additionalInit($config);
    
    /**
     * function initBind
     *
     * コールバックメソッドの初期登録を行う
     *
     */
    abstract protected function initBind();

    /**
     * private setShareView
     *
     * 機能に共通して使用する情報のアサインを行う
     * $config['views']があれば、その情報をviewにアサインする
     *
     * @param array $config 設定 $config['views']を使用
     *
     */
    protected function setShareView($config)
    {
        if( isset($config['views']) && is_array($config['views']) ) {
            foreach($config['views'] as $name => $val) {
                view()->share($name, $val);
            }
        }
    }
    
    /**
     * function setTemplates
     *
     * テンプレートの設定があれば、メンバ変数に設定する
     *
     * @param Array $config
     */
    protected function setTemplates($config)
    {
        $this->templatePrefix = isset($config['templatePrefix']) ? $config['templatePrefix'] : '';
        $templates = isset($config['templates']) && is_array($config['templates']) ? $config['templates'] : [];
        
        foreach( $templates as $type => $template ) {
            if(!empty($type) && !empty($template)) {
                $this->setTemplate( $type, $template );
            }
        }
    }
    
    /**
     * function setTemplate
     *
     * テンプレートの設定をメンバ変数に設定する
     *
     * @param string $type     設定名称 form list など
     * @param string $template テンプレート名称 master.account.list など
     *
     */
    public function setTemplate( $type, $template )
    {
        $this->templates[$type] = $template;
    }

    /**
     * function view
     *
     * viewにテンプレートと値のアサインをして返す
     *
     * viewの処理前に追加でアサインしたいデータがあったりする場合に、
     * コールバックメソッドにてそれが出来るようにしている。
     * ※リストにて使用はしているものの、viewの前処理というよりもアサイン処理という形のメソッド名で縛っている
     *   点踏まえ、拡張の仕方はもう少し考慮してもよいかもしれない。
     *
     */
    public function view( $type, $assign )
    {
        $method = 'shareAsaign'.ucfirst($type);
        $this->{$method}();

        $assign['pageType'] = $type;

        return view( $this->getTemplate($type), $assign );
    }
    
    /**
     * function getTemplate
     *
     * テンプレートファイル名を取得します
     * urlNameをデフォルトとして、設定で登録があればそちらを返すようになっています。
     *
     * @param string $type アクションにて指定されます。現在のトレイトでは list form confirm detal があります。
     *                     complete は完了時一覧に戻る挙動になっているため現状呼ばれることはありません。
     *
     */
    protected function getTemplate($type)
    {
        // デフォルトのテンプレート名を設定
        $template = $this->getDefaultTemplateName($type);
        
        // テンプレート名の指定があればそれで上書きする
        if( isset($this->templates[$type]) && !empty($tpl = $this->resolveTemplate($this->templates[$type])) ) {
            $template = $tpl;
        }
        
        return $template;
    }
    
    /**
     * function getDefaultTemplateName
     *
     *
     */
    protected function getDefaultTemplateName($type)
    {
        $template = $this->getCanonicalUrlName();
        if( isset($this->templatePrefix) && !empty($this->templatePrefix) ) {
            $template = $this->templatePrefix . $type;
        }
        
        return $template;
    }
    
    /**
     * function resolveTemplate
     *
     * テンプレート名を解決します
     * テンプレート名の設定は名称そのもの以外に、コールバックメソッドを登録することもできるようになっています。
     * コールバックメソッドで登録されている場合は、そのコールバックを呼び出して戻り値を返します。
     *
     * @param Object $setting
     *
     */
    protected function resolveTemplate( $setting )
    {
        $tpl = '';
        if( is_string($setting) ) {
            $tpl = $setting;
        } elseif( is_callable($setting) ) {
            $tpl = call_user_func($setting);
        }
        
        return $tpl;
    }

    /**
     * function bind
     *
     * メソッドを登録するメソッド
     * $this->closure　にメソッド名と実行内容がセットされます。
     *
     * @param string    $name    登録名（メソッド名）
     * @param callable? $closure 実行するメソッド内容。もしくはcall_user_funcで実行できる配列
     *
     */
    public function bind( $name, $closure )
    {
        $ret = false;
    
        if( is_callable($closure) ) {
            if( isset($this->closure[$name]) ) {
                // 上書きになるのでログを記録する
                $msg = sprintf('メソッド [ %s ] のbindを上書きします。', $name);
                $this->logWrite(ExtendLog::INFO, $msg);
            }
            $this->closure[$name] = $closure;
            $ret = true;
        }
        return $ret;
    }
    
    /**
     * function __call
     *
     * マジックメソッド
     * 定義されていないメソッド呼び出しがあった際に呼ばれる
     * $this->closureにメソッド定義が存在すればそれを実行し、ない場合はnullを返す
     *
     */
    public function __call( $name, $argments )
    {
        if( isset($this->closure[$name]) ) {
            $closure = $this->closure[$name];
            if( is_callable($closure) ) {
                return call_user_func_array($closure, $argments);
            }
        }

        // ログを残す
        $msg = sprintf('定義されていないメソッド [ %s ] がコールされました。', $name);
        $this->logWrite(ExtendLog::INFO, $msg);

        return null;
    }
    
    /**
     * function logWrite
     *
     */
    protected function logWrite($level, $msg)
    {
        self::$exLog->write($level, $this->messageFormat($msg));
    }
    
    /**
     * function messageFormat
     *
     * ログメッセージにクラス名を付加してフォーマットする
     *
     */
    protected function messageFormat($msg)
    {
        return sprintf("url [ %s ] CtrlSuppoter Class [ %s ]\n%s", $this->urlName, $this->className, $msg);
    }
}
