<?php
namespace Azuki\App\Services;

/**
 * Azuki\routesディレクトリ以下に配置したルーティング設定ファイル
 * に登録された設定内容をベースにしてルーティングテーブルを生成するためのサービス
 *
 * default.phpが基本のルーティング設定ファイルとなり、
 * ドメイン名.phpという名前でドメイン毎のルーティングを設定することができる
 * なお、LaravelのRouterクラスではdomainメソッドでドメインに関するルーティングを
 * 追加する設定方法があるが、domain外で設定のあるパスをdomainで上書きするような挙動では
 * なく、指定ドメインでのみ有効な設定を追加する挙動となっている。
 * 本ファイルで設定されるdomainのルーティングはそのドメインのルーティングのみ読み込む仕様
 * とし、追加設定ではない。
 * ※RouteControllServiceProviderクラスの特定メソッドをオーバーライドあるいは書き換えることで
 * この仕様は柔軟に変更が可能。
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

/**
 * class RouteControll
 *
 * ルーティングテーブルを生成するクラス
 */
class RouteControll
{
    /**
     * ルーティング設定の配列をこの変数に保持
     * Array route
     */
    private static $route;
    
    /**
     *
     *
     */
    private static $namespace    = '\Azuki\App\Contracts\Http\Controllers';
//    private static $namespace    = '\Azuki\App\Http\Controllers';

    /**
     *
     *
     */
    protected $defaultWebDomain = [
        'azuki.web',
        'web',
    ];

    /**
     *
     *
     */
    protected $defaultApiDomain = [
        'azuki.api',
        'api',
    ];

    /**
     * function __construct
     *
     * コンストラクタ。サービスプロバイダーによって、シングルトンで生成される
     *
     * @param array $config
     */
    public function __construct($config)
    {
        if( !isset($route) ) {
            self::$route = $config;
//            self::$route = $this->addRoutingFromDb($config);
        }
    }
    
    /**
     * function addRoutingFromDb
     *
     * DBで設定されているルーティング情報をマージする
     *
     * @param  array $route
     * @return array $ret
     */
    protected function addRoutingFromDb($route)
    {
        //
    }
    

    /**
     * function getRoutingList
     *
     * ルーティングを生成するための大元の設定の中身を返す
     */
    public function getRoutingList()
    {
        return self::$route;
    }
    
    /**
     * function makeRouting
     *
     * ルーティングを作成する
     * 現状のRouteControllServiceProviderの仕様ではdomainが混じったルーティング
     * の設定が渡されることはないが、ここでdomainに対応したルーティングを生成する
     * ロジックにしておくことで拡張性を上げている
     *
     * @param Route $router
     */
    public function makeRouting($router)
    {
        $routings = $this->getRoutingList();
        
        foreach( $routings as $domain => $routing ) {
            $namespace = isset($routing['namespace']) && !empty($routing['namespace']) ? $routing['namespace'] : self::$namespace;
            
            // ドメインがazuki.webではない場合はdomainに紐づけたルーティングにする
            if( in_array( $domain, $this->defaultWebDomain ) ) {
                $router
                    ->middleware('web')
                    ->namespace($namespace)
                    ->group(function ($router) use($routing, $domain) {
                        $this->makeDetailRouting($router, $routing, $domain);
                    });
            } elseif( in_array( $domain, $this->defaultApiDomain ) ) {
                $router
                    ->middleware('api')
                    ->namespace($namespace)
                    ->group(function ($router) use($routing, $domain) {
                        $this->makeDetailRouting($router, $routing, $domain);
                    });
            } else {
                $router
                    ->middleware('web')
                    ->namespace($namespace)
                    ->domain($domain)->group(function($router) use($routing, $domain) {
                        $this->makeDetailRouting($router, $routing, $domain);
                    });
            }
        }
    }
    
    /**
     * function makeDetailRouting
     *
     * グループ、リストの設定を展開してルーティングを登録する
     *
     * @param Route $router  Routerオブジェクト
     * @param array $routing ルーティング設定、詳細はroutes/default.phpを参照
     * @param String $domain ルーティング対象のドメイン名
     *
     */
    protected function makeDetailRouting($router, $routing, $domain)
    {
        // グループ設定があればグループ設定のルーティングを作成する
        if( !empty($routing['group']) ) {
            foreach( $routing['group'] as $group ) {
                $this->makeGroupRouting($router, $group, $domain);
            }
        }

        // グループ以外のルーティングを作成する
        if( !empty($routing['list']) ) {
            $routes = $routing['list'];
            foreach( $routes as $route ) {
                $this->makeRoute($router, $route);
            }
        }
    }
    
    /**
     * function makeGroupRouting
     *
     * グループ設定一つ一つのルーティングを作成する
     *
     * @param Route $router
     * @param array $group
     * @param String $domain ルーティング対象のドメイン名
     */
    protected function makeGroupRouting($router, $group, $domain)
    {
        $prefix     = $group['prefix'];
        $middleWare = $group['middleware'];
        $routes     = $group['list'];
        if( !empty($routes) && $this->enableContents($prefix) ) {
            $groupSettings = [
                'prefix'    => $this->getGroupUrlPrefix($prefix, $domain)
            ];
            if( !empty($middleWare) ) {
                $groupSettings['middleware'] = $middleWare;
            }
            $router->group( $groupSettings, function() use($prefix, $routes, $router) {
                foreach( $routes as $route ) {
                    $this->makeRoute($router, $route, $prefix);
                }
            });
        }
    }
    
    /**
     * function enableContents
     *
     * prefixでコンテンツの有効・無効を判断する
     * 無効なコンテンツの場合ルーティングセットが行われない。
     *
     */
    protected function enableContents($prefix)
    {
        return $this->checkEnable('contents.'.$prefix);
    }
    
    /**
     * function enableUrl
     *
     * urlでコンテンツの有効・無効を判断する
     * 無効なコンテンツの場合ルーティングセットが行われない。
     *
     */
    protected function enableUrl($route)
    {
        $ret = true;
        $keyList = $this->getUrlKeyForCheck($route);
        foreach($keyList as $url) {
            $ret = $this->checkEnable('url.'.$url);
            if(!$ret) {
                break;
            }
        }
        
        return $ret;
    }
    
    /**
     * function getUrlKeyForCheck
     *
     */
    protected function getUrlKeyForCheck($route)
    {
        $group = isset($route['gPrefix']) ? $route['gPrefix'] : '';
        $url = ltrim($route['url'], '/');
        
        $list = [$url];
        if(preg_match('/(.*)\/[^\/]*/', $url, $matches)) {
            $list = [
                $matches[1],
                $matches[0],
            ];
        }
        $list = array_map(function($url) use($group){
            return !empty($group) ? sprintf('/%s/%s', $group, $url) : '/'.$url;
        }, $list);

        return $list;
    }
    
    /**
     * function checkEnable
     *
     * prefixでコンテンツの有効・無効を判断する
     * 無効なコンテンツの場合ルーティングセットが行われない。
     *
     */
    protected function checkEnable($configKey)
    {
        $ret = true;
        if(app(BASE_APP_ACCESSOR)->get('standard.'.$configKey) == 'disabled') {
            $ret = false;
        }
        
        return $ret;
    }
    
    /**
     * function makeRoute
     *
     * 1セット当たりのルーティングを生成する
     * setで定義されているものはここで展開して１ルーティングずつ作成を行う
     * $prefixがあるものはグループからの呼び出しの場合で、この場合各ルーティングの設定に
     * グループのプレフィックスを足した設定になるように調整を行う
     *
     * @param Route  $router
     * @param array  $route
     * @param string $prefix
     */
    protected function makeRoute($router, $route, $prefix = null)
    {
        if( isset($route['set']) && is_array($route['set']) && !empty($route['set']) ) {
            if( !$this->enableContents($route['prefix']) ) {
                return;
            }
            // セットで設定されているルーティングを展開して処理する
            foreach( $route['set'] as $set ) {
                $exRoute = $this->expandRoute($route, $set);
                $exRoute = !empty($prefix) ? $this->addPrefix($prefix, $exRoute) : $exRoute;
                $this->setRoute( $router, $exRoute );
            }
        } else {
            $route = !empty($prefix) ? $this->addPrefix($prefix, $route) : $route;
            $this->setRoute( $router, $route );
        }
    }
    
    /**
     * function expandRoute
     *
     * setで定義されたルーティングをルーティングが作成できる設定に展開する
     *
     * @param array $route ルーティング設定
     * @param array $set   setに定義された配列のひとつのペア
     */
    protected function expandRoute($route, $set)
    {
        $url        = $set['url'] == 'index' ? $route['prefix'] : sprintf( '/%s/%s', $route['prefix'], $set['url'] );
        $method     = $set['method'];
        $nameFormat = $method == 'post' ? '%s.p-%s' : '%s.%s';
        $bUrl       = ($tmp = strstr($set['url'], '/', true)) !== FALSE ? $tmp : $set['url'];
        $name       = isset($set['name']) && !empty($set['name']) ? 
            $set['name'] : sprintf( $nameFormat, $route['prefix'], $bUrl );
        $uses       = sprintf( '%s@%s%s', $route['controller'], $method, str_replace( '-', '', ucwords($bUrl, '-') ) );
        $where      = isset($set['where']) && !empty($set['where']) ? $set['where'] : '';

        // editにてprevありのURLの場合のname処理
        if( preg_match('/.*\/edit\/\{id\}\/prev\/.*/', $url, $matches) ) {
            $name = $name . '.prev';
        }

        return [
            'url'        => $url,
            'method'     => $method,
            'name'       => $name,
            'middleware' => isset($set['middleware']) ? $set['middleware'] : [],
            'uses'       => $uses,
            'where'      => $where,
        ];
    }
    
    /**
     * function addPrefix
     *
     * nameとusesにプレフィックスを足したルーティング設定を返す
     *
     * @param string $prefix
     * @param array  $route
     *
     * @return array $route
     */
    protected function addPrefix( $prefix, $route )
    {
        $route['gPrefix'] = $prefix;
        $route['name'] = !empty($route['name']) ? sprintf( '%s.%s', $prefix, $route['name'] ) : $prefix;
        $route['uses'] = sprintf( '%s\%s', ucfirst($prefix), $route['uses'] );
        
        return $route;
    }
    
    /**
     * function setRoute
     *
     * １つのルーティングを作成する
     * Route::get( 'xxxxx', ['as' => 'xxxxx', 'middleware' => 'xxxxx', 'uses' => 'xxxxxx@xxxxx', 'where' => 'xxxxxx'] );
     * Route::post( 'xxxxx', ['as' => 'xxxxx', 'middleware' => 'xxxxx', 'uses' => 'xxxxxx@xxxxx', 'where' => 'xxxxxx'] );
     * ↑などを実行するのと同義の処理を行う
     * 
     * @param Route $routeObj
     * @param array $route
     */
    protected function setRoute( $routeObj, $route )
    {
        if(!$this->enableUrl($route)) {
            return ;
        }
        
        $settings = [];
        $rel = [
            'name'       => 'as',
            'middleware' => 'middleware',
            'uses'       => 'uses',
            'where'      => 'where',
        ];
        foreach( $rel as $key => $name ) {
            if( !empty($route[$key]) ) {
                $settings[$name] = $route[$key];
            }
        }
        
        $method     = $route['method'];
        $url        = $route['url'];
        $routeObj->{$method}( $url, $settings );
    }
    
    /**
     * function getGroupUrlPrefix
     *
     * グループのURLプレフィックスを返す
     * systemやmanageなど決まったプレフィックスを変更したい場合
     * ここで処理をすることで、システムの挙動をそのままにURLを変更できる
     *
     */
    protected function getGroupUrlPrefix($prefix, $domain)
    {
        // 設定を取得し、設定に応じてprefixを変更する
        $subDir = $this->getSubDir($domain);
        
        return isset($subDir[$prefix]) && !empty($subDir[$prefix]) ? $subDir[$prefix] : $prefix;
    }
    
    /**
     * function getSubDir
     *
     * ドメインに対してprefixの置換定義があればその設定を返す
     * なければデフォルト値を返す
     * デフォルト値はprefix=system、manage　を定義しているが、
     * 他のprefixについてもルーティングでグループ定義しているものはここで置換処理が出来る
     * ただし、置換の結果URLの重複が発生しないようにする必要があるが、それはシステム側ではサポートされない
     */
    protected function getSubDir($domain)
    {
        return getSubDir($domain);
    }
}
