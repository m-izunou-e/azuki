<?php
namespace Azuki\App\Services\Interfaces;


interface CtrlSupportInterfaceForBaseAction
{
    public function getSubstituteIndexRoute();

    public function view($type, $assign);
    public function find($id);
    public function paginate($num);

    public function resetCondition();
    public function setCondition();
    public function setResultStatus();

    public function getDefaultPost();
    public function getPost();
    public function getPrev();
    public function getOldInput();
    public function validation($request);
    public function createOrUpdate($post);

    public function getRedirectPathFromComplete();

    public function toggleValidDone();
    public function deleteDone();
}
