<?php
namespace Azuki\App\Services\FileManager\Abstracts;
/**
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Models\FileInfo;

abstract class Storage
{
    /**
     * function __construct
     *
     */
    public function __construct($app)
    {
    }
    
    /**
     * function getRawData
     *
     * 引数で与えられたファイルの生データを取得する
     * 引数で指定されたファイルが存在しない場合などはfalseが返る
     * 
     * @param  string $filePath ファイルのフルパス
     * @return binary -         ファイルの生データ
     */
    protected function getRawData($filePath)
    {
        return file_get_contents($filePath);
    }

    /**
     * function getFileData
     *
     */
    abstract public function getFileData( $fileInfo );

    /**
     * function getFileDataSize
     *
     */
    abstract public function getFileDataSize( $fileInfo );

    /**
     * function getFileDataSize
     *
     */
    abstract public function getFileChunkData( $fileInfo, $num, $partsSize );
    
    /**
     * function save
     *
     */
    abstract public function save( $data, $fileInfo );
    
    /**
     * function delete
     *
     */
    abstract public function delete( $fileInfo );
}
