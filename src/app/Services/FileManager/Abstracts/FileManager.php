<?php
namespace Azuki\App\Services\FileManager\Abstracts;
/**
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Models\FileInfo;

abstract class FileManager
{
    /**
     * FileInfoモデル $infoModel
     *
     */
    private $infoModel;
    
    /**
     * typeに紐づくstorageサービスをキャッシュする
     *
     */
    private $storages = [];
    
    /**
     *
     */
    protected $cacheFileDataSize;
    
    /**
     * 一時ファイルを削除対象とするまでの時間設定
     *
     */
    private $tempFileLimitHour = 5;
    
    /**
     *
     */
    protected $defaultPartsSize = 15*1024*1024;
    
    /**
     * function __construct
     *
     */
    public function __construct( $app )
    {
        $this->infoModel = new FileInfo();
    }
    
    /**
     * function deleteFile
     *
     * ファイル削除を行う
     *
     * 与えられたFileInfoオブジェクトのdeleteを実行することで、
     * FileInfo.delete->deletedを経由し、FileManager.deleteUploadedFileを通り
     * FileManager.deleteDataが実行される。
     * 結果、FileInfoの削除、Fileの実体データの削除、キャッシュがあればキャッシュの削除
     * が実行される。
     *
     * @param Eloquent $fileInfo FileInfoモデルのインスタンス
     *
     */
    public function deleteFile($idName)
    {
        $ret = false;
        
        $idNames = [$idName, preg_replace('/^(.*)\.[^\.]*$/', '${1}', $idName)];
        $info = $this->infoModel
            ->whereIn('id_name', $idNames)
            ->get();
        
        foreach($info as $fileInfo) {
            $ret = $fileInfo->delete();
            if(!$ret) {
                break;
            }
        }
        
        return $ret;
    }
    
    /**
     * function deleteData
     *
     * 指定されたファイルを削除する
     * キャッシュファイル削除は上位処理で行う。
     *
     * @param Eloquent $fileInfo FileInfoモデルのインスタンス
     *
     */
    protected function deleteData( $fileInfo )
    {
        $storage = $this->getStorage($fileInfo->type, $fileInfo);
        $storage->delete($fileInfo);
    }
    
    /**
     * function saveInfo
     *
     * FileInfoデータを保存する
     * 引数で渡された情報をFileInfoモデルを通してDBに登録し、結果のインスタンスを返す
     *
     * @param  array   $file     ファイル情報を設定した配列
     * @return Eloqent $fileInfo FileInfoインスタンス
     *
     */
    protected function saveInfo( $data )
    {
        $info = $this->createSaveInfoData($data);
        $fileInfo = $this->infoModel
            ->where('id_name',  '=', $info['id_name'])
            ->where('category', '=', $info['category'])
            ->first();
        
        if(is_null($fileInfo)) {
            $fileInfo = $this->infoModel->_create($info);
        } else {
            $this->infoModel->_update($fileInfo->id, $info);
        }
        return $fileInfo;
    }
    
    /**
     *
     *
     */
    protected function getStorage($type, $fileInfo = null)
    {
        $isRegistStorage = !is_null($fileInfo) && !is_null($fileInfo->storage);
        $type = $isRegistStorage ? sprintf('id-%04d', $fileInfo->id) : $type;
        if( !isset($this->storages[$type]) ) {
            if($isRegistStorage) {
                $storage = $fileInfo->storage;
            } else {
                $config = $this->getFileConfig($type);
                $storage = $config['storage'];
            }
            $this->storages[$type] = app($storage);
        }
        
        return $this->storages[$type];
    }
    
    /**
     * function getInfo
     *
     * ファイル名から紐づくFileInfo情報を取得する
     *
     * @param  string  $idName ファイル名（id_name）
     * @return Eloqent $info   FileInfoモデルのインスタンス
     *
     */
    public function getInfo( $idName, $category = UPLOAD_DATA_CATEGORY_ORIGINAL )
    {
        $idNames = [$idName, preg_replace('/^(.*)\.[^\.]*$/', '${1}', $idName)];
        $info = $this->infoModel
            ->whereIn('id_name', $idNames)
            ->where('category', '=', $category)
            ->first();
        return $info;
    }

    /**
     * function getInfoById
     *
     * ファイル名から紐づくFileInfo情報を取得する
     *
     * @param  integer $id     id
     * @return Eloqent $info   FileInfoモデルのインスタンス
     *
     */
    public function getInfoById( $id )
    {
        $info = $this->infoModel->find($id);
        return $info;
    }

    /**
     * function getData
     *
     * 指定されたFileInfoから紐づくファイルの生データを取得する
     * getFileDataメソッドは本クラスを継承した各storageクラスで適切に実装する
     *
     */
    public function getData( $fileInfo )
    {
        if(is_null($fileInfo)) {
            return null;
        }
        $storage = $this->getStorage($fileInfo->type, $fileInfo);
        return $storage->getFileData($fileInfo);
    }

    /**
     * function getDataSize
     *
     * 指定されたファイルのデータサイズを取得するメソッド
     *
     * @param  eloqent $fileInfo FileInfoモデルのインスタンス
     * @return binary  ファイルの生データを読み込んで返す
     */
    public function getDataSize( $fileInfo )
    {
        if(is_null($fileInfo)) {
            return null;
        }
        if(!isset($this->cacheDataSize[$fileInfo->id])) {
            $storage = $this->getStorage($fileInfo->type, $fileInfo);
            $this->cacheDataSize[$fileInfo->id] = $storage->getFileDataSize($fileInfo);
        }
        
        return $this->cacheDataSize[$fileInfo->id];
    }

    /**
     * function getChunkData
     *
     * 指定されたファイルのデータサイズを取得するメソッド
     *
     * @param  eloqent $fileInfo FileInfoモデルのインスタンス
     * @return binary  ファイルの生データを読み込んで返す
     */
    public function getChunkData( $fileInfo, $num )
    {
        if(is_null($fileInfo)) {
            return null;
        }
        $storage = $this->getStorage($fileInfo->type, $fileInfo);
        return $storage->getFileChunkData($fileInfo, $num, $this->getPartsSize($fileInfo->type));
    }
    
    /**
     * function deleteTempFile
     *
     * 一時ファイルを削除する
     * FileInfoから一時ファイルの命名規則のファイル名になっているもの
     * で一定時間以上前に登録されたものは不要な一時ファイルと判断し削除する
     * 対象のFileInfoインスタンスそれぞれに対してdeleteを実行することで、
     * FileInfoモデルのbootで追加処理している削除時の関連削除処理が実行されるようにしている
     *
     */
    public function deleteTempFile()
    {
        $tempName = UPLOAD_TEMP_FILE_PREFIX.'%';
        $infoModel = $this->infoModel
            ->where('id_name', 'like', $tempName)
            ->where('created_at', '<', date('Y-m-d H:i:s', strtotime('-'.$this->tempFileLimitHour.' hours') ))
            ->get();

        foreach( $infoModel as $info ) {
            $info->delete();
        }
    }
    
    /**
     * function registFileInfo
     *
     * アップロード時に一時ファイルではなくフィックスさせる際に
     * 正式名にid_nameを更新するためのメソッド
     *
     * @param  string $prefix   正式なファイル名につけるプレフィックス
     * @param  number $belongId アップロードファイルが紐づけられているレコードのID
     * @param  string $name     現行のファイル名
     *
     */
    public function registFileInfo($prefix, $belongId, $name)
    {
        $this->infoModel->updateFileNameFromTemp($prefix, $belongId, $name);
    }
    
    /**
     *
     *
     */
    protected function getPartsSize($type)
    {
        $config = $this->getFileConfig($type);
        return $config['partsSize'] ?? $this->defaultPartsSize;
    }
    
    /**
     * function save
     *
     */
    abstract public function save( $data, &$errors );
    
    /**
     * function createSaveInfoData
     *
     */
    abstract protected function createSaveInfoData( $data );
    
    /**
     * function getFileConfig
     *
     */
    abstract public function getFileConfig( $type );

}
