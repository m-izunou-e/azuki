<?php
namespace Azuki\App\Services\FileManager;
/**
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Services\FileManager\Abstracts\Storage;
use Azuki\App\Models\FileData;

class DatabaseStorage extends Storage
{
    /**
     * ファイルデータを保存するモデル
     *
     */
    private $dataModel;
    
    /**
     *
     */
    protected $partsSize = 15*1024*1024;

    /**
     * function __construct
     *
     */
    public function __construct($app)
    {
        parent::__construct($app);
        $this->dataModel = new FileData();
    }

    /**
     * function getFileData
     *
     * 指定されたファイルのバイナリデータを取得するメソッド
     *
     * @param  eloqent $fileInfo FileInfoモデルのインスタンス
     * @return binary  データベースから取り出したデータをエンコードしたファイルのバイナリデータ
     */
    public function getFileData( $fileInfo )
    {
        $obj = $this->dataModel
            ->where('file_info_id', '=', $fileInfo->id)
            ->orderBy('number', 'asc')
            ->get();
        if( count($obj) <= 0 ) {
            return null;
        }
        $data = '';
        foreach($obj as $row) {
            $data .= $row->data;
        }
        unset($obj);
        pMem(9, true);
        return $data;
    }
    
    /**
     * function getFileDataSize
     *
     */
    public function getFileDataSize( $fileInfo )
    {
        $obj = $this->dataModel
            ->select(['id',
                'file_info_id',
                \DB::raw('LENGTH(`data`) as dataSize')
            ])
            ->where('file_info_id', '=', $fileInfo->id)->get();
        if( count($obj) <= 0 ) {
            return null;
        }
        $size = 0;
        foreach($obj as $row) {
            $size += $row->dataSize ?? 0;
        }
        return $size;
    }
    
    /**
     * function getFileChunkData
     *
     */
    public function getFileChunkData( $fileInfo, $num, $partsSize = null )
    {
        debugExLog('getFileChunkData：'.$num);
        $obj = $this->dataModel
            ->select(['id',
                'file_info_id',
                'data',
                'number',
            ])
            ->where('file_info_id', '=', $fileInfo->id)
            ->orderBy('number', 'asc')
            ->skip($num - 1)->take(1)->first();
        if( is_null($obj) ) {
            return null;
        }
        pMem(9, true);
        return $obj->data;
    }
    
    /**
     * function save
     *
     * ファイルデータを保存する
     *
     * @param array $file 保存するファイル情報をセットした配列
     */
    public function save( $file, $fileInfo )
    {
        $this->saveData($fileInfo->id, $file);
        
        pMem('saveEnd', true);
    }
    
    /**
     * function saveData
     *
     *
     */
    protected function saveData($fileInfoId, $file)
    {
        $readSize = $file['partsSize'] ?? $this->partsSize;
        
        pMem(0);
        $dataTable = $this->dataModel->getTable();
        $insSql = 'INSERT INTO ' . $dataTable .
            ' (`file_info_id`, `number`, `data`, `created_at`, `updated_at`) ' .
            ' VALUES (?,?,?,?,?)';
        $this->dataModel
            ->select(['id', 'file_info_id'])
            ->where('file_info_id', '=', $fileInfoId)
            ->delete();
        
        try {
            $fp = fopen($file['tmpPath'], 'rb');
            $dataNum = 0;
            while(!feof($fp)) {
                $row = fread($fp, $readSize);
                pMem(1);
                $dataNum++;
                $this->execSaveSql($insSql, [
                    $fileInfoId,
                    $dataNum,
                    $row,
                    date('Y-m-d H:i:s'),
                    date('Y-m-d H:i:s'),
                ]);
                pMem(3);
            }
        } catch(\Exception $e) {
            fclose($fp);
            $msg = substr($e->getMessage(), 0, 150);
            errorExlog($msg);
            throw new \Exception($msg);
        }
        fclose($fp);
        unset($row);
    }
    
    /**
     * function execSaveSql
     *
     */
    protected function execSaveSql($sql, $bind)
    {
        $pdo = \DB::connection()->getPdo();
        $stmt = $pdo->prepare($sql);
//        $row = \DB::connection()->getPdo()->quote($row, \PDO::PARAM_LOB);
        pMem(2);
        $stmt->execute($bind);
        unset($stmt);
    }
    
    /**
     * function delete
     *
     * 指定されたファイルを削除する
     * キャッシュファイル削除は上位処理で行う。
     *
     * @param Eloquent $fileInfo FileInfoモデルのインスタンス
     *
     */
    public function delete( $fileInfo )
    {
        $dataObj = $this->dataModel
            ->select(['id', 'file_info_id'])
            ->where('file_info_id', '=', $fileInfo->id);
        if( !is_null($dataObj) ) {
            $dataObj->delete();
        }
    }
}
