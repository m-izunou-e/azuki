<?php
namespace Azuki\App\Services\FileManager;
/**
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Services\FileManager\Abstracts\Storage;

class DiskStorage extends Storage
{
    /**
     * アップロードされたファイルのオリジナルファイルを保存するベースディレクトリ
     *
     */
    protected $fileSaveParentDir;

    /**
     * function __construct
     *
     */
    public function __construct($app)
    {
        parent::__construct($app);
        $this->fileSaveParentDir = storage_path('app/uploadedFile');
    }
    
    /**
     * function createOrgFileSavePath
     *
     * アップロードファイルのオリジナルファイルを保存するパス
     *
     * @param Eloqent $fileInfo FileInfoモデルのインスタンス
     * preturn -                パス
     */
    protected function createOrgFileSavePath($fileInfo)
    {
        $id = sprintf('%09d', $fileInfo->id);
        $path1 = sprintf('%02d', ($id / 1000000) % 100);
        $path2 = sprintf('%03d', $id / 1000 % 1000);
        return sprintf('%s/%s', $path1, $path2);
    }
    
    /**
     * function getOrgSavePath
     *
     * アップロードファイルの保存ファイル名をフルパスで返す
     * 保存ディレクトリが存在しない場合は作成も行う
     *
     * @param  array   $file     ファイル情報を格納した配列
     * @param  Eloqent $fileInfo FileInfoモデルのインスタンス
     * @return string  $filePath 保存ファイル名（フルパス）
     */
    protected function getOrgSavePath($file, $fileInfo)
    {
        $path = sprintf( '%s/%s', $this->fileSaveParentDir, $this->createOrgFileSavePath($fileInfo));
        if(!file_exists($path)) {
            createDirIfNotExists($path);
        }
        
        $fileName = sprintf('org_%09d', $fileInfo->id);
        $filePath = sprintf( '%s/%s.%s', $path, $fileName, $file['ext']);
        return $filePath;
    }

    /**
     * function getFileData
     *
     * 指定されたファイルのバイナリデータを取得するメソッド
     *
     * @param  eloqent $fileInfo FileInfoモデルのインスタンス
     * @return binary  ファイルの生データを読み込んで返す
     */
    public function getFileData( $fileInfo )
    {
        return $this->getRawData($this->getFilePathOrNoImagePath($fileInfo));
    }
    
    /**
     * function getFileDataSize
     *
     */
    public function getFileDataSize( $fileInfo )
    {
        return filesize($this->getFilePathOrNoImagePath($fileInfo));
    }
    
    /**
     * function getFileChunkData
     *
     */
    public function getFileChunkData( $fileInfo, $num, $partsSize )
    {
        debugExLog('getFileChunkData：'.$num.':'.$partsSize);
        $seek = $partsSize * ($num - 1);
        $data = null;
        $file = $this->getFilePathOrNoImagePath($fileInfo);
        $fp = fopen($file, 'r');
        if($fp) {
            fseek($fp, $seek, SEEK_SET);
            $data = fread($fp, $partsSize);
            fclose($fp);
        }
        
        return $data;
    }
    
    /**
     *
     *
     */
    protected function getFilePathOrNoImagePath($fileInfo)
    {
        return file_exists($fileInfo->path) ? $fileInfo->path : public_path('vendor/azuki/img/no_img.jpg');
    }
    
    /**
     * function save
     *
     * ファイルデータを保存する
     *
     * @param array $file 保存するファイル情報をセットした配列
     */
    public function save( $file, $fileInfo )
    {
        $orgFilePath = $this->getOrgSavePath($file, $fileInfo);
        
        if( !rename($file['tmpPath'], $orgFilePath) ) {
            throw new \Exception('アップロードファイルを保存できませんでした');
        }
        @chmod($orgFilePath, 0664);
        
        $fileInfo->path = $orgFilePath;
        $fileInfo->save();
    }
    
    /**
     * function delete
     *
     * 指定されたファイルを削除する
     * キャッシュファイル削除は上位処理で行う。
     *
     * @param Eloquent $fileInfo FileInfoモデルのインスタンス
     *
     */
    public function delete( $fileInfo )
    {
        @unlink($fileInfo->path);
    }
}
