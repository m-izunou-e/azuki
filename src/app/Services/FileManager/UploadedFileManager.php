<?php
namespace Azuki\App\Services\FileManager;
/**
 *
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Services\FileManager\Abstracts\FileManager;

class UploadedFileManager extends FileManager
{
    
    /**
     * アップロードしたファイルへのアクセスURLのベース部分
     *
     */
    protected $baseUploadedUrl = '/uploaded-file-view/';
    
    /**
     * function save
     *
     * ファイルデータを保存する
     *
     * @param array $file 保存するファイル情報をセットした配列
     */
    public function save( $file, &$errors )
    {
        $type = $file['ufType'];
        $storage = $this->getStorage($type);

        $ret = true;

        \DB::beginTransaction();
        try {
            $fileInfo = $this->saveInfo( $file );
            $storage->save($file, $fileInfo);
            if(isset($file['thumbnail'])) {
                $fileInfo = $this->saveInfo( $file['thumbnail'] );
                $storage->save($file['thumbnail'], $fileInfo);
            }
            \DB::commit();
        } catch( \Exception $e ) {
            \DB::rollback();
            $ret = false;
            $errors['message'] = $e->getMessage();
        }
        
        return $ret;
    }
    
    /**
     *
     * 
     */
    protected function createSaveInfoData($data)
    {
        $info = [
            'id_name'   => $data['idName'],
            'ext'       => $data['ext'],
            'name'      => $data['name'],
            'mime_type' => $data['mimeType'],
            'type'      => $data['ufType'],
            'path'      => '',
            'category'  => $data['category'] ?? UPLOAD_DATA_CATEGORY_ORIGINAL,
//            'storage'   => get_class($this->getStorage($data['ufType'])),
            'storage'   => ($this->getFileConfig($data['ufType']))['storage'],
        ];
        
        return $info;
    }
    
    /**
     *
     *
     * 
     */
    public function getFileConfig($type)
    {
        $config = app(BASE_APP_ACCESSOR)->get('upload_file.type.'.$type);
        if( empty($config) ) {
            $type = 'default';
            $config = app(BASE_APP_ACCESSOR)->get('upload_file.type.'.$type);
        }
        return $config;
    }
    
    /**
     * function getUploadedFilePath
     *
     * アップロードされたファイルへのアクセスパス
     * →URLベースなのでpublicディレクトリ以下のパス
     *
     * @param  array  $uploadFileInfo 対象のアップロードファイル情報をまとめた配列
     * @return string $ret            ファイルパス（ファイル名含まず）
     *
     */
    public function getUploadedFilePath($uploadFileInfo)
    {
        $urlPath = $this->baseUploadedUrl . $uploadFileInfo['ufType'];
        $prefix   = $uploadFileInfo['prefix'];
        $belongId = $uploadFileInfo['belongId'];

        $ret = false;
        $category = $uploadFileInfo['category'];
        if( $prefix == 'temp' ) {
            // 一時ファイルのURLを生成
            $ret = sprintf('%s/%s/%03d', $urlPath, 'temp', $category);
        } elseif( !empty($prefix) && !empty($belongId) ) {
            $prefix = sprintf('%s-%03d', $prefix, $category);
            if(!empty($uploadFileInfo['fileId'])) {
                $path1 = sprintf('%02d', ($belongId / 1000000) % 100);
                $path2 = sprintf('%03d', $belongId / 1000 % 1000);
            } else {
                list($path1, $path2, $other) = explode('-', $belongId);
            }
            $ret = sprintf('%s/%s/%s/%s', $urlPath, $prefix, $path1, $path2);
        }
        
        return $ret;
    }
    
    /**
     * function getUploadedFileInfo
     *
     * ファイル名からアップロードされたファイルのファイル情報を取得する
     *
     * @param  string $fileName ファイル名（idName）
     * @return array  $ret      ファイル情報をまとめた配列
     *
     */
    public function getUploadedFileInfo($fileName, $ufType, $category = UPLOAD_DATA_CATEGORY_ORIGINAL)
    {
        $ret = [
            'fileName' => $fileName,
            'ufType'   => $ufType,
            'category' => $category,
            'prefix'   => '',
            'belongId' => '',
            'fileId'   => '',
            'ext'      => '',
        ];

        $name = $fileName;
        if(strpos($fileName, '.')) {
            list($name, $ret['ext']) = explode('.', $fileName);
        }
        $parts = explode('_', $name);
        $ret['prefix']   = isset($parts[0]) ? $parts[0] : '';
        $ret['belongId'] = isset($parts[1]) ? $parts[1] : '';
        $ret['fileId']   = isset($parts[2]) ? $parts[2] : '';
        
        return $ret;
    }
    
    /**
     * function deleteUploadedFile
     *
     * アップロードされたファイルを削除する
     * FileInfoモデルで削除対象を指定。キャッシュがあればキャッシュも削除する
     *
     * @param Eloqent $fileInfo FileInfoモデルのインスタンス
     *
     */
    public function deleteUploadedFile($fileInfo)
    {
        if(is_null($fileInfo)) {
            return false;
        }
    
        $fileName = $fileInfo->id_name;
        $this->deleteData($fileInfo);
        
        $file = $this->getCacheFile($fileName, $fileInfo->type);
        $this->clearCacheFile($file);
    }
    
    /**
     * function createCacheFile
     *
     * キャッシュファイルを作成する
     * キャッシュが必要ない設定のものはキャッシュ生成はしない。かつ
     * キャッシュが存在していれば削除する
     * キャッシュファイルを置くディレクトリが存在しない場合は再帰的に生成する
     *
     * @param Eloqent $fileInfo FileInfoモデル
     *
     */
    public function createCacheFile($fileInfo)
    {
        $fileName = $fileInfo->id_name . '.'. $fileInfo->ext;
        if( !$this->needCache($fileInfo) ) {
            // キャッシュ必要なしなのにキャッシュがあれば削除処理
            $file = $this->getCacheFile($fileName, $fileInfo->type, $fileInfo->category);
            $this->clearCacheFile($file);
            return ;
        }
        $path = $this->getCacheFilePath($fileName, $fileInfo->type, $fileInfo->category);
        $file = sprintf('%s/%s', $path, $fileName);

        if(!file_exists($path)) {
            createDirIfNotExists($path);
        }
        
        $num = 0;
        $fp = fopen($file, 'wb');
        while($fp) {
            $num++;
            $data = $this->getChunkData($fileInfo, $num);
            if(is_null($data) || strlen($data) <= 0) {
                break;
            }
            fwrite($fp, $data, strlen($data));
        }
        fclose($fp);
        @chmod($file, 0664);
    }
    
    /**
     * function clearCacheFile
     *
     * キャッシュファイルを削除する
     * キャッシュファイルが存在しなければ何もしない
     *
     * @param string $file ファイル名（フルパス指定）
     */
    public function clearCacheFile($file)
    {
        if( $this->cacheExists($file) ) {
            @unlink($file);
        }
    }
    
    /**
     * function cacheExists
     *
     * キャッシュファイルの存在有無をチェックする
     * 現状はfile_existsしているだけ。
     *
     * @param  string $file ファイル名（フルパス指定）
     * @return boolean true or false
     *
     */
    protected function cacheExists($file)
    {
        return file_exists($file);
    }
    
    /**
     * function getCacheFile
     *
     * 指定されたファイル名からキャッシュファイルのフルパスを取得する
     *
     * @param  string $fileName ファイル名（idName）
     * @return string $file     キャッシュファイルのフルパス名
     *
     */
    protected function getCacheFile($fileName, $type)
    {
        $path = $this->getCacheFilePath($fileName, $type);
        $file = sprintf('%s/%s', $path, $fileName);

        return $file;
    }
    
    /**
     * function getCacheFilePath
     *
     * キャッシュファイルのディレクトリパスを取得する
     * ファイル名含まない部分
     *
     * @param  string $fileName ファイル名（idName）
     * @return string $file     キャッシュファイルのディレクトリパス
     *
     */
    protected function getCacheFilePath($fileName, $type, $category = UPLOAD_DATA_CATEGORY_ORIGINAL)
    {
        $publicPath = public_path();
        
        $uploadFileInfo = $this->getUploadedFileInfo($fileName, $type, $category);
        $path = $this->getUploadedFilePath($uploadFileInfo);
        $ret = sprintf('%s%s', $publicPath, $path);
        
        return $ret;
    }
    
    /**
     * function needCache
     *
     * キャッシュファイルの必要有無を判定する
     * 一時ファイルはキャッシュ不要と判定する
     * それ以外はファイルのtypeで判断する
     * →typeはコントローラでufTypeで指定されfile_info.typeへ保存される。
     *   このtype情報に紐づく詳細をconfig/upload_fileから取得して判断する
     *
     * @param  Eloqent $fileInfo FileInfoモデルのインスタンス
     * @return boolean $ret true or false
     *
     */
    protected function needCache($fileInfo)
    {
        $config = $this->getFileConfig($fileInfo->type);
        $ret = $config['cache'];
        
        $fileName = $fileInfo->id_name;
        if( strpos($fileName, UPLOAD_TEMP_FILE_PREFIX) !== FALSE ) {
            $ret = false;
        }
        
        return $ret;
    }
}
