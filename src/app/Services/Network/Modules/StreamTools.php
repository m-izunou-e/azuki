<?php
namespace Azuki\App\Services\Network\Modules;

/**
 * ファイルストリーム再生のための共通メソッドをまとめたトレイト
 *
 *
 */

trait StreamTools
{
    /**
     * function getResponseFileDataCode
     *
     * ファイルデータ取得時のレスポンスコードを取得する
     * range対応していない場合は200、対応している場合は206
     * rangeのリクエストヘッダに応じて切り替える
     * 不正時はExceptionで対応
     *
     */
    public function getResponseFileDataCode()
    {
        return is_null($this->getRequestRange()) ? 200 : 206;
    }
    
    /**
     * function getResponseFileDataHeader
     *
     * ヘッダのrangeを踏まえてレスポンスヘッダを作成する
     *
     */
    public function getResponseFileDataHeader($fileInfo)
    {
        $size = $this->getDataSize($fileInfo);
        $header = [
            'Accept-Ranges'  => 'bytes',
            'Content-Type'   => $fileInfo->mime_type,
            'Content-Length' => $size,
        ];
        $range = $this->parseRequestRange($this->getRequestRange(), $size);
        
        if(!is_null($range)) {
            infoExLog($range);
            $header['Content-Length'] = $range['end'] - $range['start'] + 1;
            $header['Content-Range']  = sprintf('bytes %d-%d/%d', $range['start'], $range['end'], $size);
        }
        return $header;
    }
    
    /**
     * function parseRequestRange
     *
     * ヘッダのrange指定をパースしてスタートとエンドを決定する
     * エンドの決定にはコンテンツのサイズが必要なので、コンテンツサイズも引数で渡す
     *
     */
    public function parseRequestRange($range, $size)
    {
        if(is_null($range)) {
            return $range;
        }
        if(preg_match('/bytes=([0-9]*)\-([0-9]*)/', $range, $matches)) {
            $start = isset($matches[1]) && is_numeric($matches[1]) ? intval($matches[1]) : 0;
            $end   = isset($matches[2]) && is_numeric($matches[2]) ? intval($matches[2]) : $size - 1;
            $end   = $end > $size -1 ? $size -1 : $end;
            if($start > $end || $start > $size - 1 || $end >= $size) {
                throw new \Excption('416 Requested Range Not Satisfiable');
            }
            $range = [
                'start'  => $start,
                'end'    => $end,
            ];
        } else {
            // ヘッダのrange指定のフォーマットが未対応な書式だったら無視する
            warningExLog($range);
            $range = null;
        }
        
        return $range;
    }
    
    /**
     * function getRequestRange
     *
     * リクエストのrangeヘッダを取得する
     * rangeヘッダが設定されていなければNULLになる
     *
     */
    public function getRequestRange()
    {
        return app('request')->header('Range');
    }
    
    /**
     * function outputStreamData
     *
     * 指定されたファイルをストリームに出力する
     *
     * @param  Eloqent $fileInfo FileInjfoモデルのインスタンス
     *
     */
    protected function outputStreamData($fileInfo, $range)
    {
        $num = 1;
        $readByte = 0;
        $stream = fopen('php://output', 'wb');
        $reading = true;
        while($reading) {
            $data = $this->getChunkData($fileInfo, $num);
            if(is_null($data) || strlen($data) <= 0) {
                break;
            }
            $num++;
            if(!is_null($range)) {
                $readByte += strlen($data);
                if($readByte <= $range['start']) {
                    continue;
                }
                if($readByte > $range['end']+1) {
                    $reading = false;
                }
                $data = $this->getSubData($data, $range, $readByte);
            }
            fwrite($stream, $data, strlen($data));
        }
        // cache作る。キャッシュが必要な場合のみ
        $this->createCacheFile($fileInfo);
        pMem('outputStreamEnd', true);
        unset($data);
        fclose($stream);
    }
    
    /**
     * function getSubData
     *
     * データの指定部分を切り出す処理
     *
     */
    protected function getSubData($data, $range, $readByte)
    {
        $dataLen = strlen($data);
        $offset = $range['start'] - ($readByte - $dataLen);
        $offset = $offset < 0 ? 0 : $offset;
        $length = $dataLen - $offset;
        if($readByte > $range['end']+1) {
            $length = $range['end'] - ($readByte - $dataLen) - $offset + 1;
        }
        debugExLog([
            'range'   => $range,
            'offset'  => $offset,
            'length'  => $length,
            'readByte'=> $readByte,
            'dataLen' => $dataLen,
        ]);
        $data = substr($data, $offset, $length);
        
        return $data;
    }
    
    /**
     * function getDataSize
     *
     */
    abstract public function getDataSize( $fileInfo );
    
    /**
     * function getChunkData
     *
     */
    abstract protected function getChunkData( $fileInfo, $num );
    
    /**
     * function createCacheFile
     *
     */
    abstract protected function createCacheFile( $fileInfo );
}
