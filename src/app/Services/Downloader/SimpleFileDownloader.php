<?php
namespace Azuki\App\Services\Downloader;

/**
 * 通常ファイルのアップロード処理を行うクラス
 *
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Services\Downloader\Abstracts\Downloader;
use Azuki\App\Services\ExtendLog;

class SimpleFileDownloader extends Downloader
{
    /**
     *
     *
     */
    protected $outputContentType = 'application/octet-stream';
    
    /**
     *
     *
     *
     */
    protected $target;
    
    /**
     *
     *
     *
     */
    public function setTarget($file)
    {
        if(file_exists($file)) {
            $this->outputContentType = mime_content_type($file);
            $this->target = $file;
        }
    }

    /**
     * function outputStream
     *
     *
     */
    protected function outputStream($output)
    {
        if(empty($this->target)) {
            return ;
        }
        $fp = fopen($this->target, 'rb');
        if($fp === false) {
            return ;
        }
        while(!feof($fp)) {
            $this->flushStream($output, fread($fp, 1024));
        }
    }
    
    /**
     *
     *
     */
    protected function convertOutputData($row, $before)
    {
        return $row;
    }
}
