<?php
namespace Azuki\App\Services\Downloader\Abstracts;

/**
 *
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Services\Downloader\Abstracts\Downloader;
use Azuki\App\Services\ExtendLog;

abstract class CsvDownloader extends Downloader
{
    /**
     *
     *
     */
    protected $outputCharset   = 'sjis-win';
    protected $internalCharset = 'UTF-8';

    /**
     *
     *
     */
    protected $outputContentType = 'text/csv';
    
    /**
     *
     *
     */
    protected $chunkNumber = 10000;
    
    /**
     * function outputStream
     *
     *
     */
    protected function outputStream($output)
    {
        $header = $this->getHeaderLine();
        $header = $this->convertEncodingOnMemory($header);
        $this->flushStream($output, $header);

        $before = null;
        $sql = $this->getSqlForOutputCsvData();
        $sql->chunk($this->chunkNumber, function($list) use($output, $before){
            $this->logWrite( ExtendLog::INFO, sprintf('CSV-DL: mem:%s', memory_get_usage()) );

            foreach ($list as $row) {
                $outputData = $this->convertOutputData($row, $before);
                $before = $row;

                $csv = $this->convertEncodingOnMemory($outputData);
                $this->flushStream($output, $csv);
            }
        });
    }
    
    /**
     *
     * TODO 個別部分を切り出して一般化できそうなメソッドなので考えてみる必要あり。
     *      ただ、CSVに特有の処理部分がstreamに関する処理と混ざっているので無理かも。
     */
    protected function convertEncodingOnMemory(Array $data)
    {
        $stream = fopen('php://memory', 'rb+');

        $keys = array_keys($data);
        $data = is_array($data[$keys[0]]) ? $data : [$data];
        foreach($data as $row) {
            if (count($row) > 0) {
                fputcsv($stream, $row);
            }
        }
        rewind($stream);
        $csv = str_replace(PHP_EOL, "\r\n", stream_get_contents($stream));
        $csv = mb_convert_encoding($csv, $this->outputCharset, $this->internalCharset);
        fclose($stream);
        
        return $csv;
    }
    
    /**
     * function getCsvSearchCondition
     *
     * CSVDL字の対象絞り込み条件を取得する
     *
     */
    protected function getCsvSearchCondition()
    {
        $ret = [];
        $request = app('request');
        $search = $request->input('search', []);
        foreach($search as $key => $val) {
            if(!is_null($val)) {
                $ret[$key] = $val;
            }
        }
        
        return $ret;
    }
    
    /**
     *
     *
     */
    abstract protected function getHeaderLine();
    
    /**
     *
     *
     */
    abstract protected function getSqlForOutputCsvData();
    
    /**
     *
     *
     */
    abstract protected function convertOutputData($row, $before);

}
