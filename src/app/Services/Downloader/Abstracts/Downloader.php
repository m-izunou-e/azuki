<?php
namespace Azuki\App\Services\Downloader\Abstracts;
/**
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Services\ExtendLog;

abstract class Downloader
{
    /**
     *
     *
     */
    protected $outputCharset   = 'UTF-8';
    protected $internalCharset = 'UTF-8';

    /**
     *
     *
     */
    protected $outputContentType = 'text/plain';

    /**
     * $exLog
     *
     * Log出力用のインスタンス
     */
    private static $exLog;

    /**
     *
     *
     */
    public function __construct()
    {
        self::$exLog = app('ExtendLog');
    }
    
    /**
     * function outputCsvData
     *
     */
    public function outputData( $filename )
    {
        return $this->responceStream($filename);
    }
    
    /**
     * function responceLump
     * TODO　仮
     *
     */
    public function responceLump($filename, $data)
    {
        $stream = fopen('php://temp', 'rb+');

        $before = null;
        foreach ($data as $row) {
            $outputData = $this->convertOutputData($row, $before);
            $before = $row;
            foreach( $outputData as $row2 ) {
                fputcsv($stream, $row2);
            }
        }
        rewind($stream);
        $output = str_replace(PHP_EOL, "\r\n", stream_get_contents($stream));
        $output = mb_convert_encoding($csv, $this->outputCharset, $this->internalCharset);

        $headers = $this->getOutputHeaders($filename);
        return \Response::make($output, 200, $headers);
    }
    
    /**
     * function responceStream
     *
     */
    public function responceStream($filename)
    {
        $headers = $this->getOutputHeaders($filename);

        return response()->stream(function() {
            $this->processOutputStream();
        }, 200, $headers);
    }
    
    /**
     *
     *
     *
     */
    protected function getOutputHeaders($filename)
    {
        $headers = array(
            'Content-Type'        => $this->outputContentType,
            'Content-Disposition' => "attachment; filename=$filename",
            'Pragma'              => 'no-cache',
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Expires'             => '0',
        );
        
        return $headers;
    }
    
    /**
     * function processOutputStream
     *
     *
     */
    protected function processOutputStream()
    {
        $output = fopen('php://output', 'wb');
        
        if( $output !== FALSE ) {
            $this->outputStream($output);
            fclose($output);
        }
    }
    
    /**
     *
     *
     *
     */
    protected function flushStream($output, $data)
    {
        fwrite($output, $data, strlen($data));
        ob_flush();
        flush();
    }


    /**
     * function exceptionLog
     *
     * 主にトランザクション時のException発生時にログを記録するためのメソッド
     *
     */
    protected function exceptionLog($e)
    {
        self::$exLog->exception($e);
    }

    /**
     * function writeLog
     *
     * Exception以外で処理は続けるもののエラーが発生している際にログ記録するためのメソッド。
     *
     */
    protected function logWrite($level, $msg)
    {
        self::$exLog->write($level, $msg);
    }

    
    /**
     *
     *
     */
    abstract protected function outputStream($output);
    
    /**
     *
     *
     */
    abstract protected function convertOutputData($row, $before);
}
