<?php
namespace Azuki\App\Services;
/**
 *
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Services\Abstracts\SomethingSession;

/**
 *
 *
 */
class OrderSession extends SomethingSession
{
    /**
     * $sessionNamePrefix
     *
     * セッション名の接頭辞
     */
    protected $sessionNamePrefix = 'order';
    
    /**
     * function getType 並び順情報を取得する
     * 
     * 指定された名前に該当する並び順のオーダー情報を返す
     * DESC or ASC
     * 
     * @param string $sName
     */
    public function getType( $sessionName = null )
    {
        $type = $this->get('type', null, $sessionName);
        return empty($type) || $type !== 'DESC' ? 'ASC' : 'DESC';
    }
}
