<?php
namespace Azuki\App\Services;

/**
 * 設定に応じたメールログ記録のクラス実体を取得し、メールログの記録を
 * 提供するサービス
 *
 * config/mail.php　にてメールログのドライバやモデル、記録ディレクトリの設定を行う
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Illuminate\Log\Writer;
use Monolog\Logger as Monolog;

/**
 * class MailLog
 *
 * メールの送信ログを記録する。
 * ファイル記録時のメッセージのフォーマットメソッドを別途作成した方が
 * 柔軟になるので、メッセージフォーマットを変更したい場合はそこで切り出しする
 *
 */
class MailLog
{
    /**
     * DBに記録する場合のmodelを保持する
     * Eloquent model
     */
    private $model;

    /**
     * function __construct
     *
     * コンストラクタ
     * 設定に応じてログ記録に必要な事前処理を行う
     *
     * @param Application $app
     */
    public function __construct($app)
    {
        $config = $app['azuki']->get('mail.log');
        $this->driver = $config['driver'];
        if( $this->driver == 'database' ) {
            $this->model  = $this->createModel( $config['model'] );
        } else {
            $logDir = $config['dir'];
            if( !file_exists($logDir) ) {
                createDirIfNotExists($logDir);
            }
            $logFile = sprintf(
                $logDir.'mail-%s.log',
                date('Y-m-d')
            );
            $this->fLog = new Writer(
                new Monolog($app->environment()), $app['events']
            );
            $this->fLog->useFiles( $logFile );
        }
    }
    
    /**
     * function save
     *
     * 送信前ログを記録する
     * 引数はそのままDBに登録できる形の配列を想定。
     * ファイルに記録する場合はvar_exportで配列を文字列化して保存
     *
     * @param array $log
     */
    public function save($log)
    {
        if( $this->driver == 'database' ) {
            $this->model->create($log);
        } else {
            $msg  = date('Y/m/d H:i:s ') . "--------------------------------------------\n";
            $msg .= var_export($log, true) . "\n";
            $this->fLog->info($msg);
        }
    }
    
    /**
     * function saveResult
     *
     * 送信結果ログを記録する
     * メッセージIDで送信ログと紐づける
     *
     * @param string $mId
     * @param array  $result
     */
    public function saveResult($mId, $result)
    {
        if( $this->driver == 'database' ) {
            $this->model->saveResult($mId, $result);
        } else {
            $msg  = date('Y/m/d H:i:s ') . "=========================\n";
            $msg .= var_export($result, true) . "\n";
            $this->fLog->info($msg);
        }
    }

    /**
     * Create a new instance of the model.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function createModel( $model )
    {
        $class = '\\'.ltrim($model, '\\');
        return app($class);
    }
}
