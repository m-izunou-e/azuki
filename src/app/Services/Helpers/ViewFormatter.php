<?php
namespace Azuki\App\Services\Helpers;

/**
 *
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

class ViewFormatter
{
    /**
     *
     */
    protected $detailViewMethods = [
        'select'          => 'getSelectFormatDataForList',
        'radio'           => 'getSelectFormatDataForList',
        'checkbox'        => 'getSelectFormatDataForList',
        'month'           => 'getMonthFormat',
        'date'            => 'getDateFormatDataForList',
        'datetime'        => 'getDateTimeFormatDataForList',
        'password'        => 'getFormatPasswordView',
        'serialize-array' => 'getFormatSerializeArrayView',
        'currency-number' => 'getFormatNumberCurrency',
        'truncate-text'   => 'getTruncateText',
    ];
    
    /**
     *
     *
     */
    protected $listViewMethods = [
        CONTROL_TYPE_SELECT        => 'getSelectFormatDataForList',
        CONTROL_TYPE_DATE          => 'getDateFormatDataForList',
        CONTROL_TYPE_DATETIME      => 'getDateTimeFormatDataForList',
        CONTROL_TYPE_BOOLEAN       => 'getBooleanFormatDataForList',
        CONTROL_TYPE_CURRENCY      => 'getFormatNumberCurrency',
        CONTROL_TYPE_TRUNCATE_TEXT => 'getTruncateText',
    ];

    /**
     *
     *
     */
    public function getFormatView( $post, $set, $area = '' )
    {
        $methods = $this->detailViewMethods;
    
        $val = '';
//        if( isset($set['name']) && isset($post[$set['name']]) && !empty($post[$set['name']])) {
        if( isset($set['name']) && isset($post[$set['name']]) && $post[$set['name']] !== '' && $post[$set['name']] !== NULL) {
            $val = $post[$set['name']];
            
            if(isset($set['type']) && in_array($set['type'], array_keys($methods))) {
                if(is_callable([$this, $methods[$set['type']]])) {
                    $val = $this->{$methods[$set['type']]}($set, $val);
                }
            }
            if(isset($set['vType']) && in_array($set['vType'], array_keys($methods))) {
                if(is_callable([$this, $methods[$set['vType']]])) {
                    $val = $this->{$methods[$set['vType']]}($set, $val);
                }
            }
        } else {
            // 値がセットされていないものについて、未設定時の指定があればそれを表示する
            if( isset($set['defString']) ) {
                $val = is_array($set['defString']) ? '' : $set['defString'];
                if(!empty($area) && isset($set['defString'][$area])) {
                    $val = $set['defString'][$area];
                }
            }
        }
        
        return is_numeric($val) ? strval($val) : $val;
    }
    
    /**
     *
     *
     */
    public function getFormatDataForList( $setting, $values )
    {
        $methods = $this->listViewMethods;
        
        $val = isset($setting['defVal']) ? $setting['defVal'] : '';
        if( isset($setting['column']) && isset($values->{$setting['column']}) ) {
            $val = $values->{$setting['column']};
            
            if(isset($setting['type']) && in_array($setting['type'], array_keys($methods))) {
                if(is_callable([$this, $methods[$setting['type']]])) {
                    $val = $this->{$methods[$setting['type']]}($setting, $val);
                }
            }
        }
        
        return $val;
    }
    
    /**
     *
     *
     */
    public function getFormatPasswordView( $setting, $val )
    {
        $cnt = strlen($val);
        $cnt = $cnt > 16 ? 16 : $cnt;
        $ret = str_repeat('*', $cnt);
        
        return $ret;
    }
    
    /**
     *
     *
     */
    public function getFormatSerializeArrayView( $setting, $val )
    {
        $ret = '';
        
        $unserialize = unserialize($val);
        foreach( $unserialize as $key => $value ) {
            if(is_array($value)) {
                // とりあえず第二階層以上の配列部分はvar_exportで文字列化
                $value = var_export($value, true);
            } elseif(is_null($value)) {
                $value = 'NULL';
            }
            $ret .= sprintf("%s => %s \n", $key, $value);
        }
        
        return $ret;
    }
    
    /**
     *
     */
    public function getSelectFormatDataForList( $setting, $val )
    {
        $key = isset($setting['select']) ? $setting['select'] : null;
        if(is_null($key)) {
            // ここはシステム的にはエラー。ログを残すか。
            return '';
        }
        
        $ret = getSelectName($key, $val);
        if(is_array($val)) {
            $prefix = isset($setting['select_prefix']) ? $setting['select_prefix'] : VIEW_SELECT_PREFIX;
            foreach($val as $v) {
                $ret[] = $prefix.getSelectName($key, $v);
            }
            $separator = isset($setting['select_separator']) ? $setting['select_separator'] : VIEW_SELECT_SEPARATOR;
            $ret = implode($separator, $ret);
        }
        return $ret;
    }
    
    /**
     *
     *
     */
    public function getDateFormatDataForList( $setting, $val )
    {
        $format = isset($setting['format']) ? $setting['format'] : 'Y/m/d';
        return date($format, strtotime($val));
    }
    
    /**
     *
     *
     */
    public function getDateTimeFormatDataForList( $setting, $val )
    {
        $format = isset($setting['format']) ? $setting['format'] : 'Y/m/d H:i:s';
        return date($format, strtotime($val));
    }
    
    /**
     *
     *
     */
    public function getBooleanFormatDataForList( $setting, $val )
    {
        $allowValue = isset($setting['allowValue']) ? $setting['allowValue'] : null;
        return $allowValue != null && $allowValue == $val ? '〇' : '×';
    }
    
    /**
     *
     *
     */
    public function getFormatNumberCurrency($setting, $val)
    {
        return $this->currencyFormatter->format($val);
    }
    
    /**
     *
     *
     */
    public function currencyFormat($number)
    {
        return $this->currencyFormatter->format($number);
    }
    
    /**
     *
     *
     */
    public function getMonthFormat($setting, $val)
    {
        $format = isset($setting['format']) ? $setting['format'] : 'Y/m';
        return date($format, strtotime($val));
    }
    
    /**
     *
     *
     */
    public function getTruncateText($setting, $val)
    {
        $width = $setting['t_width'] ?? 80;
        $marker = $setting['marker'] ?? '…';
        $encoding = $setting['encoding'] ?? null;
        return mb_strimwidth($val, 0, $width, $marker, $encoding);
    }
}
