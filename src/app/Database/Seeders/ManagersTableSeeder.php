<?php
namespace Azuki\App\Database\Seeders;

use Azuki\App\Database\Seeder;
use Azuki\App\Contracts\Models\Managers;

class ManagersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createData(app(Managers::class), $this->managers);
    }
}
