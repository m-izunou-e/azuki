<?php
namespace Azuki\App\Database\Seeders;

use Azuki\App\Database\Seeder;
use Azuki\App\Contracts\Models\SystemRoles;
use Azuki\App\Models\SystemRoleAuthorities;
use Azuki\App\Contracts\Models\ManageRoles;
use Azuki\App\Models\ManageRoleAuthorities;
use Azuki\App\Contracts\Models\CommonRoles;
use Azuki\App\Models\CommonRoleAuthorities;
use Azuki\App\Contracts\Models\SystemSwitch;

class MasterDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->registRoles();
        $this->registSystemSwitch();
    }
    
    /**
     *
     *
     */
    protected function registRoles()
    {
        $this->forceTruncate(app(SystemRoleAuthorities::class));
        $this->createData(app(SystemRoles::class), $this->systemRole);

        $this->forceTruncate(app(ManageRoleAuthorities::class));
        $this->createData(app(ManageRoles::class), $this->manageRole);

        $this->forceTruncate(app(CommonRoleAuthorities::class));
        $this->createData(app(CommonRoles::class), $this->commonRole);
    }
    
    /**
     *
     *
     */
    protected function registSystemSwitch()
    {
        $this->createData(app(SystemSwitch::class), $this->switch);
    }
}
