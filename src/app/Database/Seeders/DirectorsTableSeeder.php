<?php
namespace Azuki\App\Database\Seeders;

use Azuki\App\Database\Seeder;
use Azuki\App\Contracts\Models\Directors;

class DirectorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createData(app(Directors::class), $this->directors);
    }
}
