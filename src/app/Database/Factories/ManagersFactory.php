<?php
namespace Azuki\App\Database\Factories;

use Azuki\App\Models\Managers;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ManagersFactory extends Factory
{
    /**
     * モデルに対応したファクトリの名前
     *
     * @var string
     */
    protected $model = Managers::class;

    /**
     * Get a new factory instance for the model.
     *
     * @param  mixed  $parameters
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public static function factory(...$parameters)
    {
        $factory = self::new();

        return $factory
                    ->count(is_numeric($parameters[0] ?? null) ? $parameters[0] : null)
                    ->state(is_array($parameters[0] ?? null) ? $parameters[0] : ($parameters[1] ?? []));
    }

    /**
     * モデルのデフォルト状態
     *
     * @return array
     */
    public function definition()
    {
        $loginId  = $this->faker->unique()->userName;
        $email    = $this->faker->unique()->safeEmail;
        $password = $this->faker->password;
        $role     = $this->faker->numberBetween(1, 2);
        $enable   = $this->faker->numberBetween(1, 2);
        return [
            'name'     => $this->faker->name,
            'login_id' => $loginId,
            'email'    => $email,
//            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
            'password' => bcrypt($password),
            'role'     => $role,
            'enable'   => $enable,
//            'belong'   => ,
            'remember_token' => str_random(10),
        ];
    }
    
    /**
     *
     *
     *
     */
    public function roleIsAdmin()
    {
        return $this->state([
            'role' => '1',
        ]);
    }
    
    /**
     *
     *
     *
     */
    public function roleIsCommon()
    {
        return $this->state([
            'role' => '2',
        ]);
    }
    
    /**
     *
     *
     *
     */
    public function userEnable()
    {
        return $this->state([
            'enable' => '1',
        ]);
    }
    
    /**
     *
     *
     *
     */
    public function userDisable()
    {
        return $this->state([
            'enable' => '2',
        ]);
    }
}
