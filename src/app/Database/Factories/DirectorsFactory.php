<?php
namespace Azuki\App\Database\Factories;

use Azuki\App\Models\Directors;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class DirectorsFactory extends Factory
{
    /**
     * モデルに対応したファクトリの名前
     *
     * @var string
     */
    protected $model = Directors::class;

    /**
     * Get a new factory instance for the model.
     *
     * @param  mixed  $parameters
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public static function factory(...$parameters)
    {
        $factory = self::new();

        return $factory
                    ->count(is_numeric($parameters[0] ?? null) ? $parameters[0] : null)
                    ->state(is_array($parameters[0] ?? null) ? $parameters[0] : ($parameters[1] ?? []));
    }

    /**
     * モデルのデフォルト状態
     *
     * @return array
     */
    public function definition()
    {
        $email    = $this->faker->unique()->safeEmail;
        $password = $this->faker->password;
        $role     = $this->faker->numberBetween(1, 2);
        $enable   = $this->faker->numberBetween(1, 2);
        return [
            'name'     => $this->faker->name,
            'email'    => $email,
            'password' => bcrypt($password),
            'role'     => $role,
            'enable'   => $enable,
            'remember_token' => str_random(10),
        ];
    }
    
    /**
     *
     *
     *
     */
    public function roleIsAdmin()
    {
        return $this->state([
            'role' => '1',
        ]);
    }
    
    /**
     *
     *
     *
     */
    public function roleIsCommon()
    {
        return $this->state([
            'role' => '2',
        ]);
    }
    
    /**
     *
     *
     *
     */
    public function userEnable()
    {
        return $this->state([
            'enable' => '1',
        ]);
    }
    
    /**
     *
     *
     *
     */
    public function userDisable()
    {
        return $this->state([
            'enable' => '2',
        ]);
    }
}
