<?php
namespace Azuki\App\Database;

use Illuminate\Database\Seeder as BaseSeeder;
use Illuminate\Support\Facades\DB;

abstract class Seeder extends BaseSeeder
{
    /**
     * シードデータはログを記録する必要はなく、
     * また原則としては一度テーブルを空にして登録しなおしなので
     * その部分をメソッド化しておく
     *
     */
    protected function createData($obj, $data, $isTruncate = true)
    {
        $obj->setNoOperationLogs();
        if($isTruncate) {
            $this->forceTruncate($obj);
        }
        $obj->reguard();
        foreach($data as $datam) {
            $obj->_create($datam);
        }
    }

    /**
     * テーブルを強制truncateする処理
     *
     */
    protected function forceTruncate($obj)
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $obj->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
