<?php
/**
 * 主にビューで使用する見た目に関するヘルパー関数を管理するファイル
 */

/**
 * faviconを取得
 *
 *
 */
if ( ! function_exists('getFavicon'))
{
    function getFavicon()
    {
        return app(BASE_APP_ACCESSOR)->getFavicon();
    }
}

/**
 * copylghtを取得
 *
 *
 */
if ( ! function_exists('getCopyLight'))
{
    function getCopyLight()
    {
        return app(BASE_APP_ACCESSOR)->getCopyLight();
    }
}

/**
 * 編集可能なレコードかどうかを判定。
 *
 * 編集不可に設定されいてるレコードの編集ができないように
 * コンディションでコントロールできるようにする
 *
 */
if ( ! function_exists('editableRecord'))
{
    function editableRecord($record, $condition)
    {
        return isEnableRecord('edit', $record, $condition);
    }
}

/**
 * 削除可能なレコードかどうかを判定。
 *
 * システム管理者のid=1や、削除不可に設定されいてるレコードの削除ができないように
 * コンディションでコントロールできるようにする
 *
 */
if ( ! function_exists('deletableRecord'))
{
    function deletableRecord($record, $condition)
    {
        return isEnableRecord('delete', $record, $condition);
    }
}

/**
 * 復旧可能なレコードかどうかを判定。
 *
 * 復旧不可に設定されいてるレコードの復旧ができないように
 * コンディションでコントロールできるようにする
 *
 */
if ( ! function_exists('restorebleRecord'))
{
    function restorebleRecord($record, $condition)
    {
        return isEnableRecord('restore', $record, $condition);
    }
}

if ( ! function_exists('isEnableRecord'))
{
    function isEnableRecord($kind, $record, $condition)
    {
        $ret = true;
    
        if( isset($condition[$kind]) && !empty($condition[$kind]) ) {
            $cond = $condition[$kind];
            $ret  = $cond['default'];
            
            if(isset($cond['enable'])) {
                foreach( $cond['enable'] as $col => $val ) {
                    if( isset($record[$col]) && $record[$col] == $val ) {
                        $ret = true;
                        break;
                    }
                }
            }
            if(isset($cond['disable'])) {
                foreach( $cond['disable'] as $col => $val ) {
                    if( isset($record[$col]) && $record[$col] == $val ) {
                        $ret = false;
                        break;
                    }
                }
            }
        }
        return $ret;
    }
}

/**
 * 表示データをフォーマットして返す
 *
 */
if ( ! function_exists('getFormatView'))
{
    function getFormatView( $post, $set, $area = '' )
    {
        return app('ViewFormatter')->getFormatView( $post, $set, $area = '' );
    }
}

/**
 * データフォーマットを行うメソッド
 *
 */
if ( ! function_exists('getFormatDataForList'))
{
    function getFormatDataForList( $setting, $values )
    {
        return app('ViewFormatter')->getFormatDataForList( $setting, $values );
    }
}

/**
 * パスワードを伏字にして返す
 *
 */
if ( ! function_exists('getFormatPasswordView'))
{
    function getFormatPasswordView( $setting, $val )
    {
        return app('ViewFormatter')->getFormatPasswordView( $setting, $val );
    }
}

/**
 * シリアライズされた配列データを加工して返す
 *
 */
if ( ! function_exists('getFormatSerializeArrayView'))
{
    function getFormatSerializeArrayView( $setting, $val )
    {
        return app('ViewFormatter')->getFormatSerializeArrayView( $setting, $val );
    }
}

/**
 * マスターデータから値に紐づく選択肢の表示名を取得する
 * 複数選択可能な選択肢の場合にも対応
 * →チェックボックスやマルチセレクトを想定
 *
 */
if ( ! function_exists('getSelectFormatDataForList'))
{
    function getSelectFormatDataForList( $setting, $val )
    {
        return app('ViewFormatter')->getSelectFormatDataForList( $setting, $val );
    }
}

/**
 * 日付型にフォーマットして返す。
 * formatという名前の連想配列名でフォーマット形式が定義されていればそれに従う
 * デフォルトのフォーマットは「Y/m/d」
 *
 */
if ( ! function_exists('getDateFormatDataForList'))
{
    function getDateFormatDataForList( $setting, $val )
    {
        return app('ViewFormatter')->getDateFormatDataForList( $setting, $val );
    }
}

/**
 * 日時型にフォーマットして返す。
 * formatという名前の連想配列名でフォーマット形式が定義されていればそれに従う
 * デフォルトのフォーマットは「Y/m/d H:i:s」
 *
 */
if ( ! function_exists('getDateTimeFormatDataForList'))
{
    function getDateTimeFormatDataForList( $setting, $val )
    {
        return app('ViewFormatter')->getDateTimeFormatDataForList( $setting, $val );
    }
}

/**
 * 〇か×を返す。
 * データがallowValueと同じなら〇。それ以外は×とする
 *
 */
if ( ! function_exists('getBooleanFormatDataForList'))
{
    function getBooleanFormatDataForList( $setting, $val )
    {
        return app('ViewFormatter')->getBooleanFormatDataForList( $setting, $val );
    }
}

/**
 *
 *
 */
if ( ! function_exists('getCurrencyNumber'))
{
    function getCurrencyNumber($number)
    {
        return app('ViewFormatter')->currencyFormat($number);
    }
}

/**
 * フォームの要素セットがグループインプットかどうかを返す。
 *
 */
if ( ! function_exists('isGroupInput'))
{
    function isGroupInput( $key )
    {
        $ret = false;
        
        if( isset($key) && !empty($key) ) {
            if( strpos($key, 'GroupInput') !== FALSE ) {
                $ret = true;
            }
        }
        
        return $ret;
    }
}

/**
 * フォームセルの横幅を決めるクラスを返す。
 *
 * TODO larage small もつける　size取得のpreg_replaceに使っている命名について規則を見直す
 */
if ( ! function_exists('getSizeClassForFormCell'))
{
    function getSizeClassFormCell( $key, $defSize = 8 )
    {
        $ret = '';
        
        $size  = $defSize;
        $sSize = null;
        if( is_array($key) ) {
            if( isset($key['size']) ) {
                $size = $key['size'];
            }
            if( isset($key['smallSize']) ) {
                $sSize = $key['smallSize'];
            }
        } else {
            if(preg_match('/.*_size_([0-9]*)/u', $key, $matches)) {
                if( is_numeric($matches[1]) ) {
                    $size = $matches[1];
                }
            }
            if(preg_match('/.*_smallSize_([0-9]*)/u', $key, $matches)) {
                if( is_numeric($matches[1]) ) {
                    $sSize = $matches[1];
                }
            }
        }
        if(is_null($sSize)) {
            $ret = sprintf('medium-%d', $size);
        } else {
            $ret = sprintf('medium-%d small-%d', $size, $sSize);
        }
        return $ret;
    }
}

/**
 * フォームタイプに応じたパーツのテンプレートパスを返す。
 *
 */
if ( ! function_exists('getFormPartsTemplate'))
{
    function getFormPartsTemplate( $type )
    {
        $partsPath = app(BASE_APP_ACCESSOR)->getFieldTemplatePath();
        $ret = $partsPath . '.field-text';
        $fieldPartsPaths = app(BASE_APP_ACCESSOR)->getFieldPartsPaths();
        
        foreach($fieldPartsPaths as $path) {
            if(isset($type)) {
                $file = sprintf('%s/field-%s.blade.php', $path, $type);
                if(file_exists($file)) {
                    $ret = $partsPath . '.field-' . $type;
                    break;
                }
            }
        }
        
        return $ret;
    }
}

/**
 * フォームについて１行ことに表示するかどうかのチェックを行う
 *
 * $flow は　'list', 'form', 'confirm' のいづれか？
 */
if ( ! function_exists('isViewRow'))
{
    function isViewRow( $key = '', $pageType = 'list' )
    {
        $ret = true;
        
        if( !empty($key) && !is_numeric($key) ) {
            if( !preg_match('/.*('.$pageType.').*/u', $key, $matches) ) {
                $ret = false;
            }
        }
        
        return $ret;
    }
}

/**
 * input-groupボタンに設定されたイベント属性を返す。
 *
 */
if ( ! function_exists('addEventAttribute'))
{
    function addEventAttribute( $set )
    {
        $ret = '';
        
        if(isset($set['event']) && is_array($set['event'])) {
            foreach($set['event'] as $event => $prog) {
                $ret .= sprintf(' %s="%s"', $event, $prog);
            }
        }
        
        return $ret;
    }
}

/**
 * アップロード画像、動画などがmultipleかどかを返す。
 *
 */
if ( ! function_exists('isMultiple'))
{
    function isMultiple($set)
    {
        return isset($set['isMulti']) && $set['isMulti'] ? true : false;
    }
}

/**
 * ソート選択されているかどうかを返す。
 *
 */
if ( ! function_exists('isSelectedSort'))
{
    function isSelectedSort( $order, $column, $type )
    {
        $ret = false;
        
        if( isset($order['condition'][$column]) && $order['condition'][$column] == $type ) {
            $ret = true;
        }
        
        return $ret;
    }
}

/**
 * formの項目、入力フィールドについて必須かどうかを返す。
 *
 */
if ( ! function_exists('requiredFieldType'))
{
    function requiredFieldType( $parts, $flow )
    {
        $ret = false;
        
        if( isset($parts['required']) ) {
            $ret = $parts['required'];
        }
        if( $flow == 'regist' && isset($parts['required_if_regist']) ) {
            $ret = $parts['required_if_regist'];
        }
        if( $flow == 'edit' && isset($parts['required_if_edit']) ) {
            $ret = $parts['required_if_edit'];
        }
        
        return $ret;
    }
}

/**
 * ショート検索フィールドが有効かどうかを判定する
 *
 */
if ( ! function_exists('isShortcutSearch'))
{
    function isShortcutSearch($search_shortcut = false)
    {
        return $search_shortcut;
    }
}

/**
 * ショート検索フィールドの検索ボタンが押下されたのかを判定する
 *
 */
if ( ! function_exists('searchShortcut'))
{
    function searchShortcut($searchBtn)
    {
        $ret = $searchBtn == 'short' ? true : false;
        return $ret;
    }
}

/**
 * グローバルナビゲーションの非表示判定を行う。
 *
 *
 */
if ( ! function_exists('disableGlobalNavi'))
{
    function disableGlobalNavi($globalConfig = null)
    {
        $ret = false;
        if(!is_null($globalConfig) && isset($globalConfig['globalMenu']) && $globalConfig['globalMenu'] == false) {
            $ret = true;
        }
        return $ret;
    }
}

/**
 * ヘッダインフォメーションの非表示判定を行う。
 *
 */
if ( ! function_exists('disableHeadInfo'))
{
    function disableHeadInfo($globalConfig = null)
    {
        $ret = false;
        if(!is_null($globalConfig) && isset($globalConfig['headInfo']) && $globalConfig['headInfo'] == false) {
            $ret = true;
        }
        return $ret;
    }
}

/**
 * ヘッダインフォメーションのログアウトリンクの表示判定を行う。
 *
 */
if ( ! function_exists('enableLogout'))
{
    function enableLogout($globalConfig = null)
    {
        $ret = false;
        if(!is_null($globalConfig) && isset($globalConfig['enableLogout']) && $globalConfig['enableLogout'] == true) {
            $ret = true;
        }
        return $ret;
    }
}



/**
 * 曜日付き日付を返す　フォーマットは固定で
 *
 */
if ( ! function_exists('formatDateWithDateOfWeek'))
{
    function formatDateWithDateOfWeek($time)
    {
        $dowList = ['日', '月', '火', '水', '木', '金', '土'];
        $date = date('Y年m月d日', $time);
        $dow  = $dowList[date('w', $time)];
        
        return sprintf('%s（%s）', $date, $dow);
    }
}

/**
 * アップロードファイルに関しての補足情報を取得する
 *
 */
if ( ! function_exists('getUploadFileInfomation'))
{
    function getUploadFileInfomation($ufType = 'default')
    {
        $ret = '';
        $units = ['byte', 'KB', 'MB', 'GB'];
        $config = app('UploadedFileManager')->getFileConfig($ufType);
        
        if(isset($config['size'])) {
            $size = $config['size'];
            for( $i = 0; $i < 3; $i++ ) {
                if( floor($size / 1024) <= 0 ) {
                    break;
                }
                $size = $size / 1024;
            }
            $ret = sprintf('最大容量は[%d%s]です。', floor($size), $units[$i]);
        }
        if(isset($config['width']) && isset($config['height'])) {
            $ret .= sprintf('推奨サイズは[%dｘ%d]です。', $config['width'], $config['height']);
        }

        return $ret;
    }
}


/**
 * 
 *
 */
if ( ! function_exists('getPageTitle'))
{
    function getPageTitle($domain, $page, $default = '')
    {
        return app(BASE_APP_ACCESSOR)->getPageTitle($domain, $page, $default);
    }
}

/**
 * 
 *
 */
if ( ! function_exists('getLogoTitle'))
{
    function getLogoTitle($domain, $page, $default = '')
    {
        return app(BASE_APP_ACCESSOR)->getLogoTitle($domain, $page, $default);
    }
}

/**
 * 
 *
 */
if ( ! function_exists('isLogoImage'))
{
    function isLogoImage($domain, $page)
    {
        return app(BASE_APP_ACCESSOR)->isLogoImage($domain, $page);
    }
}

/**
 * 
 *
 */
if ( ! function_exists('getLogoImagePath'))
{
    function getLogoImagePath($domain, $page, $default = '')
    {
        return app(BASE_APP_ACCESSOR)->getLogoImagePath($domain, $page, $default);
    }
}

/**
 * 
 *
 */
if ( ! function_exists('loginIs'))
{
    function loginIs($domain)
    {
        return app(BASE_APP_ACCESSOR)->loginIs($domain);
    }
}

/**
 * 
 *
 */
if ( ! function_exists('enableResetPassword'))
{
    function enableResetPassword($domain)
    {
        return app(BASE_APP_ACCESSOR)->enableResetPassword($domain);
    }
}

/**
 *
 *
 */
if ( ! function_exists('getMenuName'))
{
    function getMenuName($prefix, $menu)
    {
        return app(BASE_APP_ACCESSOR)->getMenuName($prefix, $menu);
    }
}

/**
 *
 *
 */
if ( ! function_exists('getTransPageTitle'))
{
    function getTransPageTitle($key, $def = '')
    {
        return app(BASE_APP_ACCESSOR)->getTransPageTitle($key, $def);
    }
}

/**
 *
 *
 */
if ( ! function_exists('getTransPageName'))
{
    function getTransPageName($key, $def = '')
    {
        return app(BASE_APP_ACCESSOR)->getTransPageName($key, $def);
    }
}

/**
 *
 *
 */
if ( ! function_exists('getTransLinkName'))
{
    function getTransLinkName($key, $def = '')
    {
        return app(BASE_APP_ACCESSOR)->getTransLinkName($key, $def);
    }
}

/**
 *
 *
 */
if ( ! function_exists('getRoleSubMenuList'))
{
    function getRoleSubMenuList()
    {
        $list = [
            'system-roles',
            'manage-roles',
        ];
        
        if(app(BASE_APP_ACCESSOR)->get('standard.user_management')) {
            $list[] = 'common-roles';
        }
        return $list;
    }
}

/**
 *
 *
 */
if ( ! function_exists('getTextareaRows'))
{
    function getTextareaRows($data, $set)
    {
        $minDef = 6;
        $maxDef = 40;
        
        $rows = isset($set['rows']) ? $set['rows'] : $minDef;
        $num = count(explode("\n", $data));
        
        return $num > $rows ? ($num > $maxDef ? $maxDef : $num) : $rows;
    }
}

/**
 *
 *
 */
if ( ! function_exists('getFormatTextareaString'))
{
    function getFormatTextareaString($data)
    {
        $data = str_replace(['array (', '),', "\n", ' '], ['[', '],', "<br>\n", '&nbsp;&nbsp;'], e($data));
        return $data;
    }
}

/**
 *
 *
 */
if ( ! function_exists('enableSnsLogin'))
{
    function enableSnsLogin()
    {
        $providers = snsProviderList();
        return !empty($providers) ? true : false;
    }
}

/**
 *
 *
 */
if ( ! function_exists('snsProviderList'))
{
    function snsProviderList()
    {
        $providers = Config()->get('services.snsProviders');
        return $providers;
    }
}

/**
 *
 *
 */
if ( ! function_exists('getSnsUrlFrom'))
{
    function getSnsUrlFrom($provider)
    {
        return url('login/'.$provider);
    }
}

/**
 *
 *
 */
if ( ! function_exists('getSnsImgSrc'))
{
    function getSnsImgSrc($provider)
    {
        $key = sprintf('services.%s.img_path', $provider);
        return Config()->get($key);
    }
}

/**
 *
 *
 */
if ( ! function_exists('getSnsProviderName'))
{
    function getSnsProviderName($provider)
    {
        $key = sprintf('services.%s.kind', $provider);
        $kind = Config()->get($key);
        $nameList = Config()->get('services.socialList');
        
        return $nameList[$kind] ?? null;
    }
}

/**
 * 一覧テーブルの１ページあたりの最大行数（デフォルト値）を取得する
 *
 */
if ( ! function_exists('getMaxRowPerList'))
{
    function getMaxRowPerList()
    {
        return app(BASE_APP_ACCESSOR)->config('page.all.maxRowPerList');
    }
}

/**
 * function showRegistButton
 * 
 * 一覧での新規登録ボタンの表示を行うかどうか
 */
if ( ! function_exists('showRegistButton'))
{
    function showRegistButton($prefix, $controller)
    {
        return existsRegistRoute($prefix, $controller);
    }
}

