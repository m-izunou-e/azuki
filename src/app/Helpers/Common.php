<?php
/**
 * 一般的なヘルパー関数を管理するファイル
 * グローバルな関数としてアクセスが可能。一般的な機能を提供するヘルパについて記載をする
 */

/**
 * laravelバージョンチェック
 *
 */
if ( ! function_exists('laravelVersionIsSix'))
{
    function laravelVersionIsSix()
    {
        $v = (float)app()->version();
        return $v >= 6 && $v < 7;
    }
}

/**
 * laravelバージョンチェック
 *
 */
if ( ! function_exists('laravelVersionOverEight'))
{
    function laravelVersionOverEight()
    {
        $v = (float)app()->version();
        return $v >= 8;
    }
}

/**
 * サーバーOSの判定。Windowsかどうかでチェック
 *
 */
if ( ! function_exists('isWindows'))
{
    function isWindows()
    {
        return strpos( php_uname('s'), 'Windows' ) !== false;
    }
}

/**
 * グローバルメニューのカテゴリがアクティブかどうかを返す
 *
 */
if ( ! function_exists('isActiveMenuCategory'))
{
    function isActiveMenuCategory( $ctrl, $list )
    {
        $ret = false;

        foreach( $list as $row ) {
            $url = $row['url'];
            $ret = isActiveMenu( $ctrl, $url );
            if( $ret ) {
                break;
            }
        }

        return $ret;
    }
}

/**
 * グローバルメニューがアクティブかどうかを返す
 *
 */
if ( ! function_exists('isActiveMenu'))
{
    function isActiveMenu( $ctrl, $url )
    {
        $ret = false;

        $parse = explode('/', $url);
        if( isset($parse[0]) && $parse[0] == $ctrl ) {
            $ret = true;
        }

        return $ret;
    }
}

/**
 * ユーザーの種類を返す
 *
 */
if ( ! function_exists('getUserKind'))
{
    function getUserKind( $user )
    {
        return app(BASE_APP_ACCESSOR)->getUserKind($user);
    }
}

/**
 * 組織機能のサポート状態を返す
 *
 */
if ( ! function_exists('supportOrganizations'))
{
    function supportOrganizations()
    {
        return app(BASE_APP_ACCESSOR)->get('standard.organizations');
    }
}



/**
 * ログインユーザー情報を返す
 *
 */
if ( ! function_exists('getLoginUser'))
{
    function getLoginUser()
    {
        $auth = app('auth');
        return isset($auth) ? $auth->user() : null;
    }
}

/**
 * 組織機能による制限が必要かどうかを返す
 *
 */
if ( ! function_exists('restrictByBelong'))
{
    function restrictByBelong()
    {
        $kind = getUserKind(getLoginUser());
        return !( !supportOrganizations() || $kind == USER_KIND_DIRECTORS || $kind == USER_KIND_USERS );
    }
}

/**
 * ログインユーザーの所属情報を返す
 *
 */
if ( ! function_exists('getLoginUserBelong'))
{
    function getLoginUserBelong($def = 0)
    {
        $ret = $def;
        $user = getLoginUser();
        if(isset($user->belong) && !is_null($user->belong)) {
            $ret = $user->belong;
        }

        return $ret;
    }
}

/**
 * アクセスドメイン名を取得する
 *
 */
if ( ! function_exists('getAccessDomainName'))
{
    function getAccessDomainName()
    {
        return isset($_SERVER['HTTP_X_FORWARDED_HOST']) && !empty($_SERVER['HTTP_X_FORWARDED_HOST']) ? 
            $_SERVER['HTTP_X_FORWARDED_HOST'] : ( isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : null );
    }
}

/**
 * function getSubDir
 *
 * ドメインに対してprefixの置換定義があればその設定を返す
 * なければデフォルト値を返す
 * デフォルト値はprefix=system、manage　を定義しているが、
 * 他のprefixについてもルーティングでグループ定義しているものはここで置換処理が出来る
 * ただし、置換の結果URLの重複が発生しないようにする必要があるが、それはシステム側ではサポートされない
 */
if ( ! function_exists('getSubDir'))
{
    function getSubDir($domain)
    {
        $subDirs = app(BASE_APP_ACCESSOR)->get('standard.subDir');

        $subDir = $subDirs['default'];
        if( !empty($domain) && isset($subDirs[$domain]) ) {
            $subDir = $subDirs[$domain];
        }
        
        return $subDir;
    }
}

/**
 * function getHttpAccessLevel
 *
 */
if ( ! function_exists('getHttpAccessLevel'))
{
    function getHttpAccessLevel($story)
    {
        $story = trim($story, '/');
        $subDir = getSubDir(getAccessDomainName());
        return $subDir['system'] == $story ? 
            HTTP_ACCESS_LEVEL_SYSTEM : 
            ($subDir['manage'] == $story ? HTTP_ACCESS_LEVEL_MANAGE : HTTP_ACCESS_LEVEL_COMMON);
    }
}

/**
 * function httpAccessLevelIsSystem
 *
 */
if ( ! function_exists('httpAccessLevelIsSystem'))
{
    function httpAccessLevelIsSystem($story)
    {
        return getHttpAccessLevel($story) == HTTP_ACCESS_LEVEL_SYSTEM;
    }
}

/**
 * function httpAccessLevelIsManage
 *
 */
if ( ! function_exists('httpAccessLevelIsManage'))
{
    function httpAccessLevelIsManage($story)
    {
        return getHttpAccessLevel($story) == HTTP_ACCESS_LEVEL_MANAGE;
    }
}

/**
 * function getDiskSpaceInfo
 *
 * 指定のディレク容量の情報を取得する。
 *
 */
if ( ! function_exists('getDiskSpaceInfo'))
{
    function getDiskSpaceInfo($dir = '/var')
    {
        $total = disk_total_space($dir);
        $free  = disk_free_space($dir);
        
        return [$total, $free];
    }
}

/**
 * function getUseDiskSpaceRate
 *
 * ディスク使用量のパーセントを取得する
 *
 */
if ( ! function_exists('getUseDiskSpaceRate'))
{
    function getUseDiskSpaceRate($dir = '/var')
    {
        list($total, $free) = getDiskSpaceInfo($dir);
        
        return sprintf('%.2f', (floor($total - $free)/$total)*100);
    }
}

/**
 * function getSymbolByQuantity
 *
 * 指定のbyteサイズを適当な単位に変換して、単位付きの文字列にする。
 *
 */
if ( ! function_exists('getSymbolByQuantity'))
{
    function getSymbolByQuantity($bytes)
    {
        $symbols = array('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB');
        $exp = floor(log($bytes)/log(1024));
    
        return sprintf('%.2f '.$symbols[$exp], ($bytes/pow(1024, floor($exp))));
    }
}

/**
 * function getAllowIpConfig
 *
 * 許可設定されているIPを取得する
 *
 */
if ( ! function_exists('getAllowIpConfig'))
{
    function getAllowIpConfig()
    {
        return app(BASE_APP_ACCESSOR)->get('ip.allows');
    }
}

/**
 * function getRemoteIpAddr
 *
 * アクセス元IPを取得する
 *
 */
if ( ! function_exists('getRemoteIpAddr'))
{
    function getRemoteIpAddr()
    {
        $server = app('request')->server;
        $remote = $server->get('HTTP_X_REAL_IP');
        return empty($remote) ? $server->get('REMOTE_ADDR') : $remote;
    }
}

/**
 * function ifRemoteIpInAllows
 *
 * アクセス元IPを取得する
 *
 */
if ( ! function_exists('ifRemoteIpInAllows'))
{
    function ifRemoteIpInAllows($return)
    {
        $ret = $return ? false : true;
        $allow  = getAllowIpConfig();
        $remote = getRemoteIpAddr();

        foreach($allow as $ac) {
            if(isMatchIp($remote, $ac)) {
                $ret = $return;
                break;
            }
        }
        
        return $ret;
    }
}

/**
 * function isMatchIp
 *
 * IPのマッチングを確認する
 *
 */
if ( ! function_exists('isMatchIp'))
{
    function isMatchIp($ip, $accept)
    {
        $ac   = $accept;
        $mask = 32;
        if(strpos($accept, '/') !== false) {
            list($ac, $mask) = explode('/', $accept);
        }
        if( filter_var( $ip, FILTER_VALIDATE_IP ) === false || filter_var( $ac, FILTER_VALIDATE_IP ) === false ) {
            return false;
        }

        $ip_long = ip2long($ip) >> (32 - $mask);
        $ac_long = ip2long($ac) >> (32 - $mask);
        
        return $ip_long === $ac_long;
    }
}

/**
 * function getCronTextSettings
 *
 * アプリケーションで設定しているcronの設定内容を取得する
 *
 */
if ( ! function_exists('getCronTextSettings'))
{
    function getCronTextSettings()
    {
        $cronSettings = app(BASE_APP_ACCESSOR)->getCronSettings();
        $crons = [];
        $path = base_path();
        foreach($cronSettings as $cron) {
            $crons[] = sprintf('%s /usr/bin/php %s/artisan %s', $cron['schedule'], $path, $cron['command']);
        }
        return $crons;
    }
}

/**
 * function getCronInformation
 *
 * OSに設定されているcronの設定内容を取得する
 *
 */
if ( ! function_exists('getCronInformation'))
{
    function getCronInformation()
    {
        $ret = exec('crontab -l 2>&1', $output, $retVal);
        if($retVal !== 0) {
            $msg = empty($output) ? '' : (is_array($output) ? var_export($output, true) : $output);
            warningExLog(sprintf( '戻り値[%s],出力内容[%s]', $retVal, $msg ));
            return [];
        }
        return $output;
    }
}

/**
 * function pMem
 *
 * メモリ使用量をログに記録する
 *
 */
if ( ! function_exists('pMem'))
{
    function pMem($mark, $checkPeakMem = false)
    {
        $mem = memory_get_usage()/1024/1024;
        infoExLog(sprintf('メモリ使用量(%s)：%.2f', $mark, $mem));
        
        if($checkPeakMem) {
            $mem = memory_get_peak_usage()/1024/1024;
            infoExLog(sprintf('ピークメモリ使用量(%s)：%.2f', $mark, $mem));
        }
    }
}

/**
 * function existsRegistRoute
 * 
 * 新規登録を実行可能なルートが設定されているかどうかを判定
 * 
 * @prefix     
 * @controller 
 */
if ( ! function_exists('existsRegistRoute'))
{
    function existsRegistRoute($prefix, $controller)
    {
        return Route::has(sprintf('%s.%s.regist', $prefix, $controller));
    }
}
