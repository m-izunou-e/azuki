<?php
/**
 * ファイルに関するヘルパー関数を管理するファイル
 */

/**
 * function createDirIfNotExists
 *
 * 引数で与えらたディレクトリが存在しなければ再帰的に生成する
 *
 */
if ( ! function_exists('createDirIfNotExists'))
{
    function createDirIfNotExists($path)
    {
        if(!file_exists($path)) {
            createDirIfNotExists(dirname($path));
            @mkdir($path, 02775);
            @chmod($path, 02775);
        }
    }
}

/**
 * ファイルのURLを取得するメソッド
 *
 */
if ( ! function_exists('getUploadedFileUrl'))
{
    function getUploadedFileUrl( $fileName, $ufType = 'default', $defReturn = null )
    {
        $service = app('UploaderFactory')->getSharedUploader();
        return $service->getUploadedFileUrl( $fileName, $ufType, $defReturn );
    }
}

/**
 * ファイルのURLを取得するメソッド
 *
 */
if ( ! function_exists('getUploadedFileThumbnailUrl'))
{
    function getUploadedFileThumbnailUrl( $fileName, $ufType = 'default', $defReturn = null )
    {
        $service = app('UploaderFactory')->getSharedUploader();
        return $service->getUploadedFileUrl( $fileName, $ufType, $defReturn, UPLOAD_DATA_CATEGORY_THUMBNAIL );
    }
}

/**
 * ファイルのサムネイル登録の有無を判定するメソッド
 *
 */
if ( ! function_exists('existsUploadFileThumbnail'))
{
    function existsUploadFileThumbnail( $fileName )
    {
        $service = app('UploadedFileManager');
        return $service->getInfo( $fileName, UPLOAD_DATA_CATEGORY_THUMBNAIL );
    }
}

/**
 * 指定された名前の元ファイルとキャッシュファイルを削除する
 *
 */
if ( ! function_exists('deleteUploadedFile'))
{
    function deleteUploadedFile($idName)
    {
        $service = app('UploadedFileManager');
        $ret = $service->deleteFile( $idName );

        return $ret;
    }
}
