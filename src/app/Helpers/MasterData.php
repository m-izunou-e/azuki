<?php
/**
 * マスターデータに関するヘルパー関数を管理するファイル
 */


/**
 * キーに紐づく選択肢配列を返す
 * DB関係のデータも処理できるように拡張したためファットになってきている
 * これ以上膨らんでくるようだとサービス化しロジックを別クラスに切り出すことも考慮する
 *
 */
if ( ! function_exists('getSelectList'))
{
    function getSelectList( $key )
    {
        $list = [];
    
        $conf = app(BASE_APP_ACCESSOR)->get('master.'.$key);
        if( isset($conf['type']) && $conf['type'] == 'db' ) {
            $obj = app($conf['model']);
            $list = $obj->formatForSelectList($obj->getMasterList($conf));

        } else if( isset($conf['type']) && $conf['type'] == 'method' ) {
            if(is_callable($conf['method'])) {
                $list = $conf['method']($conf);
            }
        } else {
            $list = is_array($conf['list']) ? $conf['list'] : [];
            // orderで並び替え
            usort($list, function($a, $b){
                return $a['order'] >= $b['order'];
            });
        }

        return $list;
    }
}

/**
 * キーに紐づく選択肢配列の値に相当する表示名を返す
 *
 */
if ( ! function_exists('getSelectName'))
{
    function getSelectName( $key, $val )
    {
        $ret = '';
        $list = getSelectList( $key );

        foreach($list as $row) {
            if( $row['value'] == $val ) {
                $ret = $row['name'];
            }
        }

        return $ret;
    }
}

/**
 * キーに紐づくマスターデータの値のみの配列を返す
 * バリデーションで使用している
 */
if ( ! function_exists('getSelectValues'))
{
    function getSelectValues( $key )
    {
        $ret = [];
        $list = getSelectList( $key );

        foreach($list as $row) {
            $ret[] = $row['value'];
        }

        return $ret;
    }
}

/**
 */
if ( ! function_exists('createListFromConfig'))
{
    function createListFromConfig( $conf )
    {
        $ret = [];
        $list = config()->get($conf['conf']);

        foreach($list as $key => $row) {
            if( isset($row[$conf['valkey']]) ) {
                $ret[] = [
                    'value' => $key,
                    'name'  => $row[$conf['valkey']]
                ];
            }
        }

        return $ret;
    }
}
