<?php
/**
 * ログ記録に関するヘルパー関数を管理するファイル
 * ExtendLogクラスを利用したログの記録を簡単に出来るようにするためのメソッド定義を実装する
 */

/**
 * ログ記録用メソッド
 *
 */
if ( ! function_exists('writeExLog'))
{
    function writeExLog($level, $msg)
    {
        app('ExtendLog')->write($level, $msg);
    }
}

/**
 * デバッグログ記録用メソッド
 *
 */
if ( ! function_exists('debugExLog'))
{
    function debugExLog($msg)
    {
        writeExLog(app('ExtendLog')::DEBUG, $msg);
    }
}

/**
 * インフォメーションログ記録用メソッド
 *
 */
if ( ! function_exists('infoExLog'))
{
    function infoExLog($msg)
    {
        writeExLog(app('ExtendLog')::INFO, $msg);
    }
}

/**
 * ワーニングログ記録用メソッド
 *
 */
if ( ! function_exists('warningExLog'))
{
    function warningExLog($msg)
    {
        writeExLog(app('ExtendLog')::WARNING, $msg);
    }
}

/**
 * エラーログ記録用メソッド
 *
 */
if ( ! function_exists('errorExLog'))
{
    function errorExLog($msg)
    {
        writeExLog(app('ExtendLog')::ERROR, $msg);
    }
}

