<?php
namespace Azuki\App\Notifications;

/**
 * パスワードリマインダーメールを送信するためのNotificationファイル
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Illuminate\Auth\Notifications\ResetPassword as Notification;
use Azuki\App\Mail\ResetPassword;

/**
 * class ResetPasswordNotification
 *
 * Laravel標準のパスワードリマインダーメールは英語でかつメール本文が
 * プログラム内(toMailメソッド)に埋め込まれているので、その部分を
 * 日本語化するとともに他メール送信のロジックと合わせこめるように
 * 実装したクラス
 */
class ResetPasswordNotification extends Notification
{
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        parent::__construct($token);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable ←Usersモデル
     * @return ResetPassword
     */
    public function toMail($notifiable)
    {
        return (new ResetPassword($this->token, $notifiable));
    }
}
