<?php
namespace Azuki\App\Notifications;

/**
 * パスワードリマインダーメール(管理画面用)を送信するためのNotificationファイル
 * ResetPasswordNotificationクラスとの違いはtoMailメソッドで生成している
 * メール送信クラスのみ。
 * 少し工夫すれば統合は可能。ただし、統合した方がよいか、別の方がよいかが
 * 微妙なクラスなのでこのままとしておく。
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Illuminate\Auth\Notifications\ResetPassword as Notification;
use Azuki\App\Mail\ResetPasswordForManage;

/**
 * class ResetPasswordForManageNotification
 *
 */
class ResetPasswordForManageNotification extends Notification
{
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        parent::__construct($token);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable ←Managersモデル
     * @return ResetPasswordForManage
     */
    public function toMail($notifiable)
    {
        return (new ResetPasswordForManage($this->token, $notifiable));
    }
}
