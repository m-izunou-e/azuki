<?php
namespace Azuki\App\Exceptions\Laravel6;

/**
 * Exceptionハンドラー
 * Exception時の処理を記述する際に使用する
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Exception;
use App\Exceptions\Handler as ExceptionHandler;

/**
 * class
 *
 */
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        // ここでエラーの階層をテンプレートに出力する処理を追加する
        // テンプレート側でその変数に応じて表示を切替することで各階層に応じたエラー画面を作成する
        $domain = getAccessDomainName();
        
        $subDir = getSubDir($domain);
        
        $level = HTTP_ACCESS_LEVEL_COMMON;
        $story = '/';
        if( $request->is($subDir['system']) || $request->is($subDir['system'].'/*') ) {
            $level = HTTP_ACCESS_LEVEL_SYSTEM;
            $story = '/'.$subDir['system'];
        } elseif( $request->is($subDir['manage']) || $request->is($subDir['manage'].'/*') ) {
            $level = HTTP_ACCESS_LEVEL_MANAGE;
            $story = '/'.$subDir['manage'];
        }
        
        $code = method_exists($exception, 'getStatusCode') ? $exception->getStatusCode() : 500;
        $msg  = method_exists($exception, 'getMessage') ?    $exception->getMessage()    : '';

        view()->share('accessLevel', $level);
        view()->share('story', $story);
        view()->share('code',    $code);
        view()->share('message', $msg);
        return parent::render($request, $exception);
    }

    /**
     * Register the error template hint paths.
     * errorsのviewパスにazukiのエラーディレクトリを追加する
     *
     * @return void
     */
    protected function registerErrorViewPaths()
    {
        parent::registerErrorViewPaths();

        $newHints = [];
        $hints  = view()->getFinder()->getHints();
        $errors = $hints['errors'];

       $paths = app(BASE_APP_ACCESSOR)->get('view.paths');

        foreach($errors as $hint) {
            if(preg_match('/.*(\/|\\\\)Illuminate(\/|\\\\)Foundation(\/|\\\\)Exceptions.*/', $hint, $matches)) {
                foreach($paths as $path) {
                    $newHints[] = $path.'/errors';
                }
            }
            $newHints[] = $hint;
        }
        view()->replaceNamespace('errors', $newHints);
    }

}
