<?php
namespace Azuki\App\Contracts\Http\Validation\System;

/**
 * 
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Contracts\Http\Validation\Validator;

/**
 * class User
 *
 */
interface ManagersValidator extends Validator
{
}
