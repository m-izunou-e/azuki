<?php
namespace Azuki\App\Contracts\Http\Controllers\System;

interface StructureController
{
    public function getMiddleware();
}
