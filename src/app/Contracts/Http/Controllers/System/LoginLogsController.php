<?php
namespace Azuki\App\Contracts\Http\Controllers\System;

interface LoginLogsController
{
    public function getMiddleware();
}
