<?php
namespace Azuki\App\Contracts\Http\Controllers\System;

interface AccessLogsController
{
    public function getMiddleware();
}
