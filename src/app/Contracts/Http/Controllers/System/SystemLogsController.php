<?php
namespace Azuki\App\Contracts\Http\Controllers\System;

interface SystemLogsController
{
    public function getMiddleware();
}
