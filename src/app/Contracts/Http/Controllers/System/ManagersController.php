<?php
namespace Azuki\App\Contracts\Http\Controllers\System;

interface ManagersController
{
    public function getMiddleware();
}
