<?php
namespace Azuki\App\Contracts\Http\Controllers\System;

interface IndexController
{
    public function getMiddleware();
}
