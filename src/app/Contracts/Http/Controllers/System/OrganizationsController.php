<?php
namespace Azuki\App\Contracts\Http\Controllers\System;

interface OrganizationsController
{
    public function getMiddleware();
}
