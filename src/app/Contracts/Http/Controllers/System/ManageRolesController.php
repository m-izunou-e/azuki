<?php
namespace Azuki\App\Contracts\Http\Controllers\System;

interface ManageRolesController
{
    public function getMiddleware();
}
