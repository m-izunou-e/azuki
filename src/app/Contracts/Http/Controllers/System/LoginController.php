<?php
namespace Azuki\App\Contracts\Http\Controllers\System;

interface LoginController
{
    public function getMiddleware();
}
