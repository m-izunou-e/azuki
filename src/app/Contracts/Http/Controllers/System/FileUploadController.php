<?php
namespace Azuki\App\Contracts\Http\Controllers\System;

interface FileUploadController
{
    public function getMiddleware();
}
