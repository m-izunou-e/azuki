<?php
namespace Azuki\App\Contracts\Http\Controllers\System;

interface ResetPasswordController
{
    public function getMiddleware();
}
