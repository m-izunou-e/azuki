<?php
namespace Azuki\App\Contracts\Http\Controllers\System;

interface DirectorsController
{
    public function getMiddleware();
}
