<?php
namespace Azuki\App\Contracts\Http\Controllers\System;

interface CommonRolesController
{
    public function getMiddleware();
}
