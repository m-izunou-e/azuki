<?php
namespace Azuki\App\Contracts\Http\Controllers\System;

interface ExecuteQueueStatusController
{
    public function getMiddleware();
}
