<?php
namespace Azuki\App\Contracts\Http\Controllers\System;

interface SystemRolesController
{
    public function getMiddleware();
}
