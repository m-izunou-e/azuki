<?php
namespace Azuki\App\Contracts\Http\Controllers\System;

interface EnvironmentController
{
    public function getMiddleware();
}
