<?php
namespace Azuki\App\Contracts\Http\Controllers\System;

interface OperationLogsController
{
    public function getMiddleware();
}
