<?php
namespace Azuki\App\Contracts\Http\Controllers\Manage;

interface FileUploadController
{
    public function getMiddleware();
}
