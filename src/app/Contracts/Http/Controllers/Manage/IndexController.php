<?php
namespace Azuki\App\Contracts\Http\Controllers\Manage;

interface IndexController
{
    public function getMiddleware();
}
