<?php
namespace Azuki\App\Contracts\Http\Controllers\Manage;

interface LoginController
{
    public function getMiddleware();
}
