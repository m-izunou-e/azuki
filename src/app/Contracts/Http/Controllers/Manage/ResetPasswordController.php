<?php
namespace Azuki\App\Contracts\Http\Controllers\Manage;

interface ResetPasswordController
{
    public function getMiddleware();
}
