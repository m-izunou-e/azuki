<?php
namespace Azuki\App\Contracts\Http\Controllers\Manage;

interface ExecuteQueueStatusController
{
    public function getMiddleware();
}
