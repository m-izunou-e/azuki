<?php
namespace Azuki\App\Contracts\Http\Controllers\Manage;

interface ManagersController
{
    public function getMiddleware();
}
