<?php
namespace Azuki\App\Contracts\Http\Controllers\Common;

interface IndexController
{
    public function getMiddleware();
}
