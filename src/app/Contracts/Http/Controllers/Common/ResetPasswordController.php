<?php
namespace Azuki\App\Contracts\Http\Controllers\Common;

interface ResetPasswordController
{
    public function getMiddleware();
}
