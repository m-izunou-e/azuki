<?php
namespace Azuki\App\Contracts\Http\Controllers\Common;

interface UploadedFileViewController
{
    public function getMiddleware();
}
