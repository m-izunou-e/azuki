<?php
namespace Azuki\App\Contracts\Http\Controllers\Common;

interface RegistController
{
    public function getMiddleware();
}
