<?php
namespace Azuki\App\Contracts\Http\Controllers\Common;

interface LoginController
{
    public function getMiddleware();
}
