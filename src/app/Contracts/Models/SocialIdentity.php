<?php
namespace Azuki\App\Contracts\Models;

/**
 * 
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

/**
 * class SocialIdentity
 *
 */
interface SocialIdentity extends BaseModel
{
}
