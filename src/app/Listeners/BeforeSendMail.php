<?php
namespace Azuki\App\Listeners;

/**
 * メール送信前にトリガされるイベントに登録しているリスナ
 * メールログを記録している
 *
 * Laravel9にてSwiftメールからSymfonyメールに変更されたため
 * それに伴う修正を行っている
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Illuminate\Mail\Events\MessageSending;
use Symfony\Component\Mime\Email;

/**
 * class BeforeSendMail
 *
 */
class BeforeSendMail extends AbstractSendmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Handle the event.
     *
     * @param  MessageSending  $event
     * @return void
     */
    public function handle(MessageSending $event)
    {
        // メッセージIDが設定されていなければ追加する
        $this->checkAndSetMessageId($event->message);
        // メールの送信ログを記録する
        $this->saveMailLog($event->message);
    }
    
    /**
     *
     *
     */
    protected function checkAndSetMessageId(Email $message)
    {
        $messgeId = $message->generateMessageId();
        $headers  = $message->getHeaders();
        if (!$headers->has('Message-ID')) {
            $headers->addIdHeader('Message-ID', $messgeId);
        }
    }
    
    /**
     * function saveMailLog
     *
     * メッセージから保存する配列データを作ってMailLogクラスを経由して保存する
     *
     * @param Email $message
     */
    protected function saveMailLog(Email $message)
    {
        $log = $this->createSaveMailLogData($message);
        $this->logObj->save($log);
    }
    
    /**
     * function createSaveMailLogData
     *
     * メッセージから保存する配列を生成するメソッド
     * mail_logsテーブルの形式に合わせて生成
     *
     * @param  Email $message
     * @return array
     */
    protected function createSaveMailLogData(Email $message)
    {
        $headers = $message->getPreparedHeaders();
        return [
            'messageId'   => $headers->get('Message-Id')->getId(),
            'headers'     => $headers->toString(),
            'date'        => $headers->getHeaderBody('Date')->format('Y-m-d H:i:s'),
            'from'        => $this->formatAddress($headers->get('From')),
            'reply_to'    => $this->formatAddress($headers->get('Reply-To')),
            'to'          => $this->formatAddress($headers->get('To')),
            'cc'          => $this->formatAddress($headers->get('Cc')),
            'bcc'         => $this->formatAddress($headers->get('Bcc')),
            'sender'      => $this->formatAddress($headers->get('Sender')),
            'return_path' => $this->formatAddress($headers->get('Return-Path')),
            'subject'     => $message->getSubject(),
            'body'        => $this->getBody($message),
        ];
    }
    
    /**
     * function formatAddress
     * fromやccなど表示名＋メールアドレスを配列で持っている可能性のあるデータが存在する
     * そういったデータを文字列にフォーマットするメソッド
     *
     * @param  mix   $addressObj 文字列形式のメールアドレスか配列形式のメールアドレス
     * @return array $ret        名前<メールアドレス>,・・・　という形式にフォーマットしたメールアドレス
     */
    protected function formatAddress($addressesObj)
    {
        if(is_null($addressesObj)) {
            return '';
        }
        
        $addresses = method_exists($addressesObj, 'getAddress') ? $addressesObj->getAddress() : 
            ( method_exists($addressesObj, 'getAddresses') ? $addressesObj->getAddresses() : null );
        if(!is_array($addresses)) {
            $addresses = is_null($addresses) ? [] : [$addresses];
        }
        
        // $addresses->getBodyAsString() だと名前部分がエンコードされるが、DBには未エンコード状態のものを
        // 記録しておく方が調査などの際にわかりやすいため以下を独自実装している
        $tmp = [];
        foreach($addresses as $addr) {
            $mail = $addr->getEncodedAddress();
            $name = $addr->getName();
            $tmp[] = sprintf('%s<%s>', $name, $mail);
        }
        
        return implode(',', $tmp);
    }
    
    /**
     * function getBody
     * メッセージオブジェクトからメール本文をDBに記録する形で取得するメソッド
     * 記録するデータはメール送信内容を人が確認するためのログのため、エンコードなどが
     * 行われていない状態のデータで記録する
     *
     * @param  Email $message メッセージオブジェクト
     * @return string         DBに記録するメール本文の文字列
     *
     */
    protected function getBody(Email $message)
    {
        $text = $message->getTextBody();
        $html = $message->getHtmlBody();
        
        return sprintf("text:[\n%s\n]\nhtml:[\n%s\n]\n", $text, $html);
    }
}
