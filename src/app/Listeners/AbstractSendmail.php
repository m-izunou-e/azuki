<?php
namespace Azuki\App\Listeners;

/**
 * メール送信イベントに関係するリスナの抽象クラス
 * コンストラクタにてログ記録オブジェクトを設定
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */


/**
 * class AbstractSendmail
 *
 */
abstract class AbstractSendmail
{
    /**
     *
     *
     */
    protected $logObj;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        // ログオブジェクトをメンバに設定する
        $this->logObj = app('mailLog');
    }

}
