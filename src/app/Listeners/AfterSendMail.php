<?php
namespace Azuki\App\Listeners;

/**
 * メール送信後にトリガされるイベントに登録しているリスナ
 * メールログを記録している
 *
 * Laravel9にてSwiftメールからSymfonyメールに変更されたため
 * それに伴う修正を行っている
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Illuminate\Mail\Events\MessageSent;
use Symfony\Component\Mime\Email;

/**
 * class AfterSendMail
 *
 */
class AfterSendMail extends AbstractSendmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Handle the event.
     *
     * @param  MessageSent  $event
     * @return void
     */
    public function handle(MessageSent $event)
    {
        // メール送信結果を記録する
        $this->saveResultLog($event->message);
    }
    
    /**
     * function saveResultLog
     *
     * メッセージから保存する配列データを作ってMailLogクラスを経由して保存する
     *
     * @param Email $message
     */
    protected function saveResultLog(Email $message)
    {
        $messageId = $this->getMessageId($message);
        $result    = $this->getResult($message);
        
        $this->logObj->saveResult($messageId, $result);
    }
    
    /**
     * function getMessageId
     *
     * メッセージIDを取得する
     *
     * @param Email $message
     */
    protected function getMessageId(Email $message)
    {
        $ids = $message->getHeaders()->getHeaderBody('Message-Id');
        return $ids[0] ?? null;
    }
    
    /**
     * function getResult
     *
     * 送信結果配列を生成する
     *
     * SwiftMail->SmfonyMailになったことで、ResultとFailedRecipientsの取得が出来なくなっている
     * 送信エラーが発生した場合はExceptionが発生する模様。よって、ここでは一旦終了を記録する
     * ということで、オール：16をDBに残す←Swift＿Eventの成功の場合の番号
     *
     * @param  Email $message
     * @return array $result result値の詳細はmail_logsのマイグレーションファイル参照
     */
    protected function getResult(Email $message)
    {
//        $result   = $evt->getResult();
//        $failures = array_flip($evt->getFailedRecipients());
        
        $result = [
            'result'   => 16, // $result,
            'failures' => null, // serialize($failures),
        ];
        
        return $result;
    }
}
