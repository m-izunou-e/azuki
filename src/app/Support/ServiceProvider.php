<?php
namespace Azuki\App\Support;

use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

/**
 *
 *
 *
 */
abstract class ServiceProvider extends LaravelServiceProvider
{
    /**
     *
     */
    const CONF_MERGE_TYPE_NRL = 'normal';
    const CONF_MERGE_TYPE_RVS = 'reverse';

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;
    
    /**
     * 設定ファイルのリスト
     *
     */
    protected $configList = [];
    
    /**
     *
     *
     */
    protected $defineFiles = [];
    
    /**
     * サービスプロバイダーのリスト
     * register、bootそれぞれの処理時に各プロバイダーのregister、bootを呼び出している
     *
     */
    protected $providers = [];
    
    /**
     * ミドルウェアのリスト
     * boot時にregistする
     */
    protected $middleware = [];
    
    /**
     * コマンドのリスト
     *
     *
     */
    protected $registCommands = [];
    
    /**
     * 基本システムを構成しているコントローラ、モデル、バリデーター
     * のリスト
     * ここで定義されているキー名に一意なプレフィックスをつけた名前で各クラスを
     * コンテナに登録します
     *
     */
    protected $baseServiceClassList = [];
    
    /**
     *
     *
     */
    protected $classKindKeys = [
        'controller',
        'model',
        'validator',
    ];
    
    /**
     *
     */
    protected $publishList = [];

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->requireDefine();
        $this->registerConfigFiles();

        $this->registerBaseServiceProviders();

        $this->registerSystemIncetance();
        $this->registerCoreContainerAliases();
        
        // コマンドライン処理を登録する
        if ($this->app->runningInConsole()) {
            if(is_array($this->registCommands) && !empty($this->registCommands)) {
                $this->commands($this->registCommands);
            }
        }
    }

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->executeLoadResources();

        if ($this->app->runningInConsole()) {
            $this->setPublishes();
        }

        if($this->addVendorRouting()) {
            $this->addRoutes();
        }
        $this->registerMiddleware($this->middleware);

        // ErrorHandlerクラスを上書きする設定
        $this->overwriteExceptionHandler();
    }
    
    /**
     *
     *
     */
    protected function executeLoadResources()
    {
        $vendorName = $this->getVendorName();
        $this->loadMigrationsFrom($this->getBasePath().'/database/migrations');
        $this->loadFactoriesFrom($this->getBasePath().'/database/factories');
        $this->loadTranslationsFrom($this->getBasePath().'/resources/lang', $vendorName);
        $this->loadViewsFrom($this->getBasePath().'/resources/views', $vendorName);
    }
    
    /**
     *
     *
     */
    protected function getBasePath()
    {
        if(!isset($this->basePath) || empty($this->basePath)) {
            $this->basePath = rtrim($this->getVendorPath(), DIRECTORY_SEPARATOR);
        }
        return $this->basePath;
    }
    
    /**
     *
     *
     */
    protected function getConfigPath()
    {
        if(!isset($this->configPath) || empty($this->configPath)) {
            $this->configPath = $this->getBasePath().'/configs';
        }
        return $this->configPath;
    }
    
    /**
     *
     *
     */
    protected function getVendorName()
    {
        if(!isset($this->vendorName) || empty($this->vendorName)) {
            $namespace = (new \ReflectionClass($this))->getNamespaceName();
            $namespace = strtolower(
                preg_replace('/[A-Z]/', '-\0', lcfirst(str_replace('\\', '_', $namespace)))
            );
            $this->vendorName = $namespace;
        }
        return $this->vendorName;
    }
    
    /**
     *
     *
     */
    protected function getPublishVendorName()
    {
        return isset($this->publishVendorName) ? $this->publishVendorName : $this->getVendorName();
    }
    
    /**
     * function setPublishes
     *
     * vendor:publishでパブリッシュするファイルを設定する
     *
     */
    protected function setPublishes()
    {
        $this->setPublishDefine();
        $this->setPublishConfig();
        $this->setPublishSeed();
        $this->setPublishLang();
        $this->setPublishView();
        $this->setPublishRoute();
        $this->setPublishPublic();
    }
    
    /**
     *
     *
     */
    protected function setPublishDefine()
    {
        $configPath = $this->getConfigPath();
        foreach($this->defineFiles as $define) {
            $this->publishes([
                $configPath.'/'.$define.'.php' => config_path($define.'.php'),
            ], 'define');
        }
    }
    
    /**
     *
     *
     */
    protected function setPublishConfig()
    {
        $configPath = $this->getConfigPath();
        foreach($this->configList as $config => $settings) {
            if($settings['isPublish']) {
                $this->publishes([
                    $configPath . '/' . $config . '.php' => config_path($config.'.php'),
                ], 'config');
            }
        }
    }
    
    /**
     *
     *
     */
    protected function setPublishSeed()
    {
        $basePath   = $this->getBasePath();

        if(file_exists($basePath.'/database/seeders')) {
            $this->publishes([
                $basePath.'/database/seeders' => base_path('database/seeders'),
            ], 'seed');
        }
        if(!laravelVersionOverEight()) {
            $this->publishes([
                $basePath.'/database/seeds' => base_path('database/seeds'),
            ], 'seed');
        }
    }
    
    /**
     *
     *
     */
    protected function setPublishLang()
    {
        $vendorName = $this->getPublishVendorName();
        $basePath   = $this->getBasePath();
        
        if(file_exists($basePath.'/resources/lang')) {
            $this->publishes([
                $basePath.'/resources/lang/' => resource_path('lang/vendor/'.$vendorName),
            ], 'lang');
        }
        if(file_exists($basePath.'/resources/laravel_lang')) {
            $this->publishes([
                $basePath.'/resources/laravel_lang/' => resource_path('lang'),
            ], 'lang');
        }
    }
    
    /**
     *
     *
     */
    protected function setPublishView()
    {
        $vendorName = $this->getPublishVendorName();
        $basePath   = $this->getBasePath();
        $this->publishes([
            $basePath.'/resources/views' => resource_path('views/vendor/'.$vendorName),
        ], 'view');
    }
    
    /**
     *
     *
     */
    protected function setPublishRoute()
    {
        $basePath   = $this->getBasePath();
        $this->publishes([
            $basePath.'/src/routes' => base_path('routes'),
        ], 'route');
    }
    
    /**
     *
     *
     */
    protected function setPublishPublic()
    {
        $publicSrcPath = $this->getPublicSrcPath();
        $publicDstPath = $this->getPublicDstPath();
        
        foreach($this->getPublicDirList() as $dirs) {
            $src = $publicSrcPath.$dirs['src'];
            if(file_exists($src)) {
                $dst = $publicDstPath.$dirs['dst'];
                $this->publishes([
                    $src => public_path($dst),
                ], 'public');
            }
        }
    }
    
    /**
     *
     *
     */
    protected function getPublicSrcPath()
    {
        $publicSrcPath = $this->getBasePath().'/public/'.$this->getPublishVendorName();
        return $publicSrcPath;
    }
    
    /**
     *
     *
     */
    protected function getPublicDstPath()
    {
        $publicDstPath = 'vendor/'.$this->getPublishVendorName();
        return $publicDstPath;
    }
    
    /**
     *
     *
     */
    protected function getPublicDirList()
    {
        return $this->publishList;
    }
    
    /**
     * function requireDefine
     *
     *
     */
    protected function requireDefine()
    {
        $configPath = $this->getConfigPath();
        $defines = $this->defineFiles;
        foreach($defines as $define) {
            $defineFile = config_path($define.'.php');
            if(!file_exists($defineFile)) {
                $defineFile = $configPath.'/'.$define.'.php';
            }
            require_once($defineFile);
        }
    }
    
    /**
     *
     *
     */
    protected function registerConfigFiles()
    {
        if ($this->app->configurationIsCached()) {
            return ;
        }
        $configPath = $this->getConfigPath();
        foreach($this->configList as $config => $settings) {
            $configKey = isset($settings['as']) ? $settings['as'] : $config;
            $configFile = $configPath . '/' . $config . '.php';
            
            if(isset($settings['merge']) ) {
                if($settings['merge'] == self::CONF_MERGE_TYPE_RVS) {
                    $this->overWriteConfigFrom($configFile, $configKey);
                } else {
                    $this->mergeConfigFrom($configFile, $configKey);
                }
            } else {
                $this->app['config']->set($configKey, require $configFile);
            }

            // as 設定があるものはファイル名をキーとして登録されている設定値を
            // as 指定のキーで登録しなおしが必要
            if($configKey != $config) {
                $this->app['config']->set(
                    $configKey, array_merge(
                        $this->app['config']->get($configKey, []),
                        $this->app['config']->get($config, [])
                ));
            }
        }
    }

    /**
     * OverWrite the given configuration with the existing configuration.
     *
     * @param  string  $path
     * @param  string  $key
     * @return void
     */
    protected function overWriteConfigFrom($path, $key)
    {
        if (! $this->app->configurationIsCached()) {
            $this->app['config']->set($key, array_merge(
                $this->app['config']->get($key, []), require $path
            ));
        }
    }
    
    /**
     *
     *
     */
    protected function addVendorRouting()
    {
        return true;
    }
    
    /**
     *
     *
     */
    protected function overwriteExceptionHandler()
    {
        if(is_null($handler = $this->getNewExceptionHandler())) {
            return ;
        }
        if($this->app->make($handler) instanceof \Illuminate\Contracts\Debug\ExceptionHandler) {
            $this->app->singleton(
                \Illuminate\Contracts\Debug\ExceptionHandler::class,
                $handler
            );
        }
    }
    
    /**
     *
     *
     */
    protected function getNewExceptionHandler()
    {
        return null;
    }
    
    /**
     * function addRoutes
     *
     * ルーティング設定を追加する
     */
    protected function addRoutes()
    {
        $this
            ->getRouteControllerInstance($this->createRouteConfig())
            ->makeRouting($this->getRouter());
    }
    
    /**
     * function createRouteConfig
     *
     * ドメインに応じたルーティングファイルを読み込んで返す
     * ドメイン名をキーに、ルーティング設定の中身を値にもつ配列にして返す
     * ドメインに対応した設定ファイルがない場合はdefaultの設定を返す
     * ドメインと設定ファイルの紐づけはファイル名で完全一致
     * より柔軟に設定ファイルの読み込みを変更したい場合はこのメソッドを変更する
     *
     * @path   string $path ルーティング設定ファイルのあるディレクトリのパス
     * @return array  $ret
     */
    protected function createRouteConfig()
    {
        $ret = [];
        
        list($list, $host) = $this->getRouteFileList();
        foreach($list as $key) {
            if( !empty($file = $this->getRouteFile($key)) ) {
                $ret[$key] = require($file);
            }
        }
        
        if(isset($ret[$host])) {
            $unsetList = $this->unsetListIfExistsHostRoute();
            foreach($unsetList as $unset) {
                unset($ret[$unset]);
            }
        }
        
        return $ret;
    }
    
    /**
     * function getRouteFile
     *
     * ルーティング設定ファイルの優先順位と存在チェックの上適切なファイルをフルパスで返す
     */
    protected function getRouteFile($key)
    {
        $ret = '';
        $routeConfigPath = $this->getBasePath().'/src/routes';
        $routePath       = base_path('routes');

        $conf = $key.'.php';
        if( file_exists($routePath.'/'.$conf) ) {
            $ret = $routePath.'/'.$conf;
        } elseif( file_exists($routeConfigPath.'/'.$conf) ) {
            $ret = $routeConfigPath.'/'.$conf;
        }
        
        return $ret;
    }
    
    /**
     *
     *
     *
     */
    protected function getRouteFileList()
    {
        $host = getAccessDomainName();
//        $host = app('config')->get('app.host');
        
        $list = $this->routeFileList();
        $list[] = $host;
        
        return [$list, $host];
    }
    
    /**
     *
     *
     */
    protected function routeFileList()
    {
        $vendorName = $this->getVendorName();
        return [
            $vendorName.'.web',
            $vendorName.'.api'
        ];
    }
    
    /**
     *
     *
     */
    protected function unsetListIfExistsHostRoute()
    {
        $vendorName = $this->getVendorName();
        return [
            $vendorName.'.web',
        ];
    }

    /**
     * Get the active router.
     *
     * @return Router
     */
    protected function getRouter()
    {
        return $this->app['router'];
    }

    /**
     * Register Middleware
     *
     * @param  string $middleware
     */
    protected function registerMiddleware($middleware)
    {
        $router = $this->getRouter();
        foreach($middleware as $name => $class) {
            $router->aliasMiddleware($name, $class);
        }
    }
    
    /**
     * function registerBaseServiceProviders
     *
     * 基本システムのサービス登録を行います
     * まずは、サービスプロバイダを登録し
     * その後、サービスプロバイダを作っていない、コントローラ、モデル、バリデータクラスについて
     * サービス登録を行います
     * コントローラ、モデル、バリデータクラスのサービス登録はここで実態を切り替えられるようにするため
     * の仕組みです
     *
     */
    protected function registerBaseServiceProviders()
    {
        // 固有のサービスプロバイダ
        foreach($this->providers as $provider) {
            $this->app->register($provider);
        }
    }
    
    /**
     * function registerSystemIncetance
     *
     * コントローラ・モデル・バリデーターのインスタンスをあらかじめサービスとして登録しておく
     * これを行うことで実行クラスの置き換えが容易になる代わりに起動コストが大きくなる
     *
     */
    protected function registerSystemIncetance()
    {
        $vendorName = $this->getVendorName();
        foreach($this->classKindKeys as $kind) {
            $classes = $this->baseServiceClassList[$kind];
            foreach($classes as $key => $class) {
                $class = $this->getTargetClass($kind, $key, $class);
//                $this->app->bind($vendorName.'.'.$kind.'.'.$key, $class);
                $this->app->singleton($vendorName.'.'.$kind.'.'.$key, $class);
            }
        }
        
    }
    
    /**
     *
     *
     */
    protected function getTargetClass($kind, $key, $class)
    {
        $class = $this->getClassPrefix($kind) . $class;
        if(!is_null($alias = $this->getAliasClass($kind, $key))) {
            $class = $alias;
        }
        return $class;
    }
    
    /**
     *
     *
     */
    protected function getAliasClass($kind, $key)
    {
        $vendorName = $this->getVendorName();
        $param = $vendorName.'.app.aliases.'.$kind.'s.'.$key;
        return $this->app['config']->get($param);
    }
    
    /**
     * function registerCoreContainerAliases
     *
     * エリアス登録を行います
     * コントローラ、モデル、バリデータについてエリアス登録をします
     * コントローラ、モデル、バリデータをコントラクタで実装し
     * そのコントラクタをここでエリアス登録しておきます
     * そうすることで、コード変更することなく、必要最小限の部分を実体入れ替えしてオーバーライドできるように
     * しています
     *
     */
    protected function registerCoreContainerAliases()
    {
        // 個別設定がされていれば置き換えてインスタンスの登録を行う
        $vendorName = $this->getVendorName();
        foreach($this->classKindKeys as $kind) {
            $classes = $this->baseServiceClassList[$kind];
            foreach($classes as $key => $class) {
                $class = $this->getContractsClassPrefix($kind) . $class;
                $this->app->alias($vendorName.'.'.$kind.'.'.$key, $class);
            }
            
        }
    }
    
    /**
     * function OverloadInstance()
     *
     * コントラクタに紐づけらたエイリアスに対して
     * 登録されている実体クラスの紐づきを上書きします
     */
    protected function OverloadInstance($kind, $classes, $closure)
    {
        foreach($classes as $key => $class) {
            $class = $this->getClassPrefix($kind) . $class;
            $key = $closure($kind, $key);
            $this->app->singleton($key, $class);
        }
    }
    
    /**
     *
     *
     */
    protected function getClassPrefix($kind)
    {
        return $this-> getPrefixPath($kind);
    }
    
    /**
     *
     *
     */
    protected function getContractsClassPrefix($kind)
    {
        return $this-> getPrefixPath($kind, true);
    }
    
    /**
     *
     *
     */
    protected function getPrefixPath($kind, $isContract = false)
    {
        $vendorName = str_replace(' ','', ucwords(str_replace('-', ' ', $this->getVendorName())));
        $contract = $isContract ? '\\Contracts' : '';
        $classPrefixes = [
            'controller' => '%s\\App%s\\Http\\Controllers\\',
            'model'      => '%s\\App%s\\Models\\',
            'validator'  => '%s\\App%s\\Http\\Validation\\',
        ];
        
        return sprintf( $classPrefixes[$kind], $vendorName, $contract );
    }
    
    /**
     *
     */
    abstract protected function getVendorPath();
    
    /**
     *
     */
    abstract protected function getRouteControllerInstance($conf);
}
