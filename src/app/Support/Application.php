<?php
namespace Azuki\App\Support;

/**
 * 基本機能を提供するサービス
 *
 * @copyright Copyright 2020 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

/**
 * class Application
 *
 *
 */
abstract class Application
{
    /**
     *
     *
     */
    const V_NAME = 'azuki';

    /**
     *
     *
     */
    protected $vendorName = self::V_NAME;

    /**
     *
     */
    protected $app;
    
    /**
     *
     *
     */
    protected $fieldPartsPath = 'layouts/parts/share/layout-parts-form';

    /**
     * function __construct
     *
     * コンストラクタ
     *
     * @param Application $app
     */
    public function __construct($app)
    {
        $this->app = $app;
    }
    
    /**
     *
     *
     */
    protected function getVendorName($vendor = null)
    {
        return is_null($vendor) ? $this->vendorName : $vendor;
    }
    
    /**
     *
     */
    public function config($key = null, $vendor = null)
    {
        $vendor = $this->getVendorName($vendor);
        return $this->get($key, $vendor);
    }
    
    /**
     * function get
     *
     */
    public function get($key = null, $vendor = null)
    {
        $vendor = $this->getVendorName($vendor);

        $key = empty($key) ? $vendor : $vendor . '.'. $key;
        return app('config')->get($key);
    }
    
    /**
     * function getUserKind
     *
     */
    public function getUserKind($user)
    {
        $classPrefix = 'Azuki\\App\\Contracts\\Models\\';
        $list = [
            USER_KIND_DIRECTORS => $classPrefix.'Directors',
            USER_KIND_MANAGERS  => $classPrefix.'Managers',
            USER_KIND_USERS     => $classPrefix.'Users',
        ];

        $ret = null;
        foreach( $list as $kind => $class ) {
            if($user instanceof $class) {
                $ret = $kind;
                break;
            }
        }
        
        return $ret;
    }
    
    /**
     * function getFavicon
     *
     * faviconを取得
     */
    public function getFavicon()
    {
        return $this->getPageConfig('all', 'global', 'favicon');
    }
    
    /**
     * function getCopyLight
     *
     * copylghtを取得
     */
    public function getCopyLight()
    {
        return $this->getPageConfig('all', 'global', 'copylight');
    }
    
    /**
     * function getFieldPath
     *
     *
     */
    public function getFieldPath()
    {
        return $this->fieldPartsPath;
    }
    
    /**
     * function getFieldPath
     *
     *
     */
    public function getFieldTemplatePath()
    {
        return str_replace('/', '.', $this->getFieldPath());
    }
    
    /**
     * function viewPaths
     *
     *
     */
    public function viewPaths()
    {
        $paths = $this->get('view.paths');
        return $paths;
    }
    
    /**
     * function getFieldPartsPaths
     *
     *
     */
    public function getFieldPartsPaths()
    {
        return array_map(function($base){
            return $base. '/'. $this->fieldPartsPath;
        }, $this->viewPaths());
    }
    
    /**
     *
     *
     */
    public function getPageTitle($domain, $page, $default = '')
    {
        $ret = $this->getPageConfig($domain, $page, 'title');
        return empty($ret) ? $default : $ret;
    }
    
    /**
     *
     *
     */
    public function getLogoTitle($domain, $page, $default = '')
    {
        $ret = $this->getPageConfig($domain, $page, 'logoTitle');
        return $this->getTransName($domain.'.'.$page, 'logoTitle', empty($ret) ? $default : $ret);
    }
    
    /**
     *
     *
     */
    public function isLogoImage($domain, $page)
    {
        $logo = $this->getPageConfig($domain, $page, 'logo');
        return $this->existsLogoImage($logo);
    }
    
    /**
     *
     *
     */
    public function getLogoImagePath($domain, $page, $default = '', $check = false)
    {
        $logo = $this->getPageConfig($domain, $page, 'logo');
        
        if($check && !$this->existsLogoImage($logo)) {
            $logo = $default;
        }
        return $this->getLogoPath($logo, true);
    }
    
    /**
     *
     *
     */
    protected function existsLogoImage($logo)
    {
        $ret = true;
        if(empty($logo) || !file_exists($this->getLogoPath($logo))) {
            $ret = false;
        }
        return $ret;
    }
    
    /**
     *
     *
     */
    protected function getLogoPath($file, $isUrl = false)
    {
        $file = str_replace(['/', '\\'], '_', $file);
        
        $currents = $this->getLogoPathCurrents();
        foreach($currents as $current) {
            if( file_exists(sprintf('%s/%s', public_path($current), $file)) ) {
                break;
            }
        }
        
        if($isUrl) {
            return sprintf('/%s/%s', $current, $file);
        }
        
        return sprintf('%s/%s', public_path($current), $file);
    }
    
    /**
     *
     *
     */
    protected function getLogoPathCurrents()
    {
        return [
            'img',
            'vendor/'.$this->getVendorName().'/img',
        ];
    }
    
    /**
     *
     *
     */
    public function loginIs($domain)
    {
        return $this->getPageConfig($domain, 'login', 'loginIs');
    }
    
    /**
     *
     *
     */
    public function enableResetPassword($domain)
    {
        return $this->getPageConfig($domain, 'login', 'reset');
    }
    
    /**
     *
     *
     */
    protected function getPageConfig($domain, $page, $key)
    {
        $configKey = sprintf('page.%s.%s.%s', $domain, $page, $key);
        return $this->get($configKey);
    }
    
    /**
     * function getMenuName
     *
     */
    public function getMenuName($prefix, $menu)
    {
        $key = $prefix.'.'.$menu['url'];
        return $this->getTransName($key, 'menu', $menu['name']);
    }
    
    /**
     * function getTransPageTitle
     *
     */
    public function getTransPageTitle($key, $def)
    {
        return $this->getTransName($key, 'title', $def);
    }
    
    /**
     * function getTransPageName
     *
     */
    public function getTransPageName($key, $def)
    {
        return $this->getTransName($key, 'name', $def);
    }
    
    /**
     * function getTransLinkName
     *
     */
    public function getTransLinkName($key, $def)
    {
        return $this->getTransName('link', $key, $def);
    }
    
    /**
     * function getTransName
     *
     */
    protected function getTransName($pageKey, $kind, $def = '', $namespace = null)
    {
        $namespace = $this->getVendorName($namespace);

        $key = sprintf('%s::%s.%s.%s', $namespace, 'page', $pageKey, $kind);
        if($key == ($name = trans($key))) {
            $name = $def;
        }
        
        return $name;
    }
    
    /**
     *
     *
     *
     */
    public function getCronSettings()
    {
        return is_null($crons = $this->get('env.cron')) ? [] : $crons;
    }
}
