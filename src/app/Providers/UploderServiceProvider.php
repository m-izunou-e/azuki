<?php
namespace Azuki\App\Providers;

/**
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Services\Uploader\Factory;
use Illuminate\Support\ServiceProvider;

/**
 * class RouteControllServiceProvider
 *
 */
class UploderServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('UploaderFactory', function($app)
        {
            return new Factory($app);
        });
    }
}
