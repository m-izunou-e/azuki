<?php
namespace Azuki\App\Providers;

/**
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Services\ResultSession;
use Illuminate\Support\ServiceProvider;

/**
 * class RouteServiceProvider
 *
 */
class ResultSessionServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('ResultSession', function($app)
        {
            return new ResultSession($app);
        });
    }
}
