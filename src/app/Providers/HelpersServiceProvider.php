<?php
namespace Azuki\App\Providers;
/**
 * helperファイルを登録するサービスプロバイダ
 * Helpersディレクトリ以下のphpファイルをヘルパとして
 * システムに関数を展開する
 *
 * ヘルパの種類ごとにファイルを分けて管理しやすくする
 */

use Azuki\App\Services\Helpers\ViewFormatter;
use Illuminate\Support\ServiceProvider;

class HelpersServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('ViewFormatter', function($app)
        {
            return new ViewFormatter($app);
        });

        foreach (glob(sprintf('%s/../Helpers/*.php', __DIR__)) as $helper_file){
            require_once($helper_file);
        }
    }
}
