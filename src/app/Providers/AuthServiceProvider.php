<?php
namespace Azuki\App\Providers;

/**
 * ポリシー設定をするサービスプロバイダ
 * Azuki用の設定を追加はしているものの、現状未使用状態
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
#use App\Providers\AuthServiceProvider as ServiceProvider;
use Azuki\App\Services\Gate;

/**
 * class AuthServiceProvider
 *
 */
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $add_policies = [
        'Azuki\App\Models\Users' => 'Azuki\App\Policies\UsersPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot():void
    {
        // gateを設定
        $gate = app('AzukiGate');
        $gate->gateInitialize();

/*
        modelに対応するpolicyを自動的に見つけられるようにする設定だが、
        このgateに対しての設定なのでモデルから一般に使用できない
        $gate->guessPolicyNamesUsing(function ($modelClass) {
            $className = str_replace('Azuki\App\Models\\', '', $modelClass);
            return sprintf('Azuki\App\Policies\%sPolicy', $className);
        });
*/
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->policies = array_merge($this->policies, $this->add_policies);
        
        $this->app->singleton('AzukiGate', function($app)
        {
            return new Gate($app, function() {
                return app('request')->user();
            });
        });
    }
}
