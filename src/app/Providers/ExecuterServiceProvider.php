<?php
namespace Azuki\App\Providers;

/**
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Services\Executer\Manager;
use Azuki\App\Services\Executer\Factory;
use Azuki\App\Services\Executer\Config;
use Illuminate\Support\ServiceProvider;

/**
 * class RouteControllServiceProvider
 *
 */
class ExecuterServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('ExecuterConfig', function($app)
        {
            return new Config($app);
        });
        $this->app->singleton('ExecuteManager', function($app)
        {
            return new Manager($app);
        });

        $this->app->singleton('ExecuterFactory', function($app)
        {
            return new Factory($app);
        });
    }
}
