<?php
namespace Azuki\App\Providers;

/**
 * イベント登録を行うサービスプロバイダ
 * Laravelの初期設定から、メール送信前後に発生するイベントを追加登録
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
#use App\Providers\EventServiceProvider as ServiceProvider;

/**
 * class EventServiceProvider
 * App\Providers\EventServiceProviderを継承。
 *
 */
class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $add_listen = [
        // メールログを残すためのイベントの登録
        'Illuminate\Mail\Events\MessageSending' => [
            'Azuki\App\Listeners\BeforeSendMail',
        ],
        'Illuminate\Mail\Events\MessageSent' => [
            'Azuki\App\Listeners\AfterSendMail',
        ],

    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->listen = array_merge($this->listen, $this->add_listen);
        parent::register();
    }
}
