<?php
namespace Azuki\App\Providers;
/**
 * Azukiアプリケーションの基本機能を提供するサービスプロバイダ
 *
 */

use Azuki\App\Services\AzukiApplication;
use Illuminate\Support\Str;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;

class AzukiAppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        // 利用するクッキーをプレフィックスで分ける
        // session.cookie_xxxx に値を設定した場合はそれを使用する(xxxx は system や manage などのサブディレクトリ)
        // 上記に設定がない場合は、session.cookieの値に_xxxx をそれぞれ付加したものを使用する
        if(config('azuki.env.dusk_test')) {
            return;
        } // duskでのテスト時は画面別のセッション名切り替えを行わない
          // ↑browser->loginAsでログインするセッションとセッション名違いになってログイン状態に出来なくなる
        $url = explode('/', $request->path() );
        $subDir = getSubDir(getAccessDomainName());
        if($url[0] == $subDir['system'] || $url[0] == $subDir['manage']) {
            config([
                'session.cookie' => config(
                    'session.cookie_' . $url[0],
                    config('session.cookie',).'_'.$url[0]
                )
            ]);
            config(['session.path' => '/'.$url[0]]);
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(BASE_APP_ACCESSOR, function($app)
        {
            return new AzukiApplication($app);
        });
        $this->app->singleton('azuki', function($app)
        {
            return new AzukiApplication($app);
        });
    }
}
