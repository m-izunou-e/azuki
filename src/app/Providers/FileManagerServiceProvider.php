<?php
namespace Azuki\App\Providers;

/**
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Services\FileManager\UploadedFileManager;
use Azuki\App\Services\FileManager\DatabaseStorage;
use Azuki\App\Services\FileManager\DiskStorage;
use Illuminate\Support\ServiceProvider;

/**
 * class RouteControllServiceProvider
 *
 */
class FileManagerServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('FileManagerDiskStorage', function($app)
        {
            return new DiskStorage($app);
        });
        $this->app->singleton('FileManagerDatabaseStorage', function($app)
        {
            return new DatabaseStorage($app);
        });

        $this->app->singleton('UploadedFileManager', function($app)
        {
            return new UploadedFileManager($app);
        });
    }
}
