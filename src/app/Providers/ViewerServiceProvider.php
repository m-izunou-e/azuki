<?php
namespace Azuki\App\Providers;

/**
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Services\Viewer\UploadedFileViewer;
use Illuminate\Support\ServiceProvider;

/**
 * class RouteControllServiceProvider
 *
 */
class ViewerServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('UploadedFileViewer', function($app)
        {
            return new UploadedFileViewer($app);
        });
    }
}
