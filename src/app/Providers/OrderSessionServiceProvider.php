<?php
namespace Azuki\App\Providers;

/**
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Services\OrderSession;
use Illuminate\Support\ServiceProvider;

/**
 * class RouteServiceProvider
 *
 */
class OrderSessionServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('OrderSession', function($app)
        {
            return new OrderSession($app);
        });
    }
}
