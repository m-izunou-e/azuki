<?php
namespace Azuki\App\Providers;

use Illuminate\Support\ServiceProvider;

class CustomValidatorServiceProvider extends ServiceProvider
{
    /**
     *
     * implicitルールが必要なカスタムルールを定義する
     */
    protected $implicitExtensions = [
        // 'ルール名' => '呼び出すメソッド名',
    ];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['validator']->resolver(function($translator, $data, $rules, $messages, $customAttributes) {
            if((float)app()->version() >= 9.49) {
                $validator = new \Azuki\App\Services\CustomValidator($translator, $data, $rules, $messages, $customAttributes);
            } else {
                $validator = new \Azuki\App\Services\CustomValidatorOld($translator, $data, $rules, $messages, $customAttributes);
            }
            if(!empty($this->implicitExtensions)) {
                $validator->addImplicitExtensions($this->implicitExtensions);
            }
            return $validator;
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}
