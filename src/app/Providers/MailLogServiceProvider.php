<?php
namespace Azuki\App\Providers;

/**
 * メールログを行うクラスを提供するためのサービスプロバイダー
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Services\MailLog;
use Illuminate\Support\ServiceProvider;

/**
 * class MailLogServiceProvider
 *
 */
class MailLogServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('mailLog', function($app)
        {
            return new MailLog($app);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['mailLog'];
    }

}
