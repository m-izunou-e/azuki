<?php
namespace Azuki\App\Providers;

/**
 * メールログを行うクラスを提供するためのサービスプロバイダー
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Services\ExtendLog;
use Illuminate\Support\ServiceProvider;

/**
 * class MailLogServiceProvider
 *
 */
class ExtendLogServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('ExtendLog', function($app)
        {
            return new ExtendLog($app);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['ExtendLog'];
    }

}
