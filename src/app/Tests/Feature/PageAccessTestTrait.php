<?php
namespace Azuki\App\Tests\Feature;

use Azuki\App\Tests\TestCase;

trait PageAccessTestTrait
{
    /**
     *
     *
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->seedMasterData();
    }

    /**
     *
     *
     */
    public function tearDown(): void
    {
        $this->truncateTables();
        parent::tearDown();
    }
    
    /**
     * @test
     *
     */
    public function 画面アクセステスト()
    {
        $user = $this->createTestUser();
        $this->actingAs($user, $this->guard);
        
        $targetList = $this->accessTestTarget;
        foreach($targetList as $target) {
            $response = $this->get($this->baseUrl . $target['url']);
            if( $response->getStatusCode() != 200 ) {
                var_dump($target['url'] .' is NG! Code='.$response->getStatusCode());
                //$response->dump();
                //$response->dumpHeaders();
                //$response->dumpSession();
            } else {
                var_dump($target['url'] .' is OK!');
            }
            $response->assertStatus(200);
            // リダイレクトのチェックも
        }
    }
}
