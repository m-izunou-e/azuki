<?php
namespace Azuki\App\Tests\Feature\System;

use Azuki\App\Tests\TestCase;

abstract class AbstractFeatureTest extends TestCase
{
    /**
     *
     */
    protected $guard = 'system';

    /**
     *
     */
    protected $group = 'system';

    /**
     *
     */
    protected $baseUrl = '/system';
}
