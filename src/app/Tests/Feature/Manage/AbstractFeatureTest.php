<?php
namespace Azuki\App\Tests\Feature\Manage;

use Azuki\App\Tests\TestCase;

abstract class AbstractFeatureTest extends TestCase
{
    /**
     *
     */
    protected $group = 'manage';

    /**
     *
     */
    protected $baseUrl = '/manage';
}
