<?php
namespace Azuki\App\Tests\Feature;

use Azuki\App\Tests\TestCase;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
//use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;

trait ResetPasswordTrait
{
    /**
     *
     *
     */
    protected $resetSendMailUrl     = '/reset-password/send-reset-mail';
    protected $resetSentMailUrl     = '/reset-password/sent-reset-mail';
    protected $resetMailUrl         = '/reset-password/reset-password';
    protected $completeResetMailUrl = '/reset-password/complete-reset-password';
    
    /**
     *
     *
     */
    protected $newPassword = 'newPassword1234';

    /**
     *
     *
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->seedMasterData();
    }

    /**
     *
     *
     */
    public function tearDown(): void
    {
        $this->truncateTables();
        parent::tearDown();
    }
    
    /**
     * @test
     * パスワードリセットのトークンでパスワードをリセット
     */
    public function パスワードリセットの実行テスト()
    {
        $resetSendMailUrl     = $this->baseUrl . $this->resetSendMailUrl;
        $resetSentMailUrl     = $this->baseUrl . $this->resetSentMailUrl;
        $resetMailUrl         = $this->baseUrl . $this->resetMailUrl;
        $completeResetMailUrl = $this->baseUrl . $this->completeResetMailUrl;

        Notification::fake();

        // ユーザーを作成
        $user = $this->createTestUser();

        $this->assertFalse(Auth::check());
        // パスワードリセットをリクエスト
        $response = $this->from($resetSendMailUrl)->post($resetSendMailUrl, [
            'email' => $user->email
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
        $response->assertRedirect($resetSentMailUrl);

        // トークンを取得
        $token = '';
        Notification::assertSentTo(
            $user,
            $this->getResetPasswordClass(),
            function ($notification, $channels) use ($user, &$token) {
                $token = $notification->token;
                return true;
            }
        );

        // パスワードリセットの画面へ
        $response = $this->get($resetMailUrl.'/'.$token);
        $response->assertStatus(200);

        // パスワードをリセット
        $new = $this->newPassword;
        $response = $this->post($resetMailUrl, [
            'email'                 => $user->email,
            'token'                 => $token,
            'password'              => $new,
            'password_confirmation' => $new
        ]);

        // ホームへ遷移
        $response->assertStatus(302);
        $response->assertRedirect($completeResetMailUrl);
        // リセット成功のメッセージ
//        $response->assertSessionHas('status', 'パスワードはリセットされました!');

        // 認証されていることを確認
        $this->assertTrue(Auth::check());

        // 変更されたパスワードが保存されていることを確認
        $this->assertTrue(Hash::check($new, $user->fresh()->password));
    }
}
