<?php
namespace Azuki\App\Tests\Browser\Feature;

use Laravel\Dusk\Browser;

trait PageAccessTrait
{
    /**
     * @test
     *
     */
    public function 画面アクセステスト()
    {
        $data = $this->createTestData();
        $user = $data['loginUser'];

        $targetList = $this->accessTestTarget;
        $num = 1;
        foreach($targetList as $target) {

            var_dump('check is '.$target['url']);
            $screenshotName = sprintf('%03d_%s-[%s]', $num, $this->group, $target['scName']);
            $url = $this->baseUrl . $target['url'];
            if(isset($target['public']) && $target['public']) {
                $this->browse(function (Browser $browser) use($url, $screenshotName){
                    $browser
                        ->visit($url)
                        ->assertPathIs($url)
                        ->fullScreenshots($screenshotName);
                });
            } else {
                $this->loginBrowse(function(Browser $browser, $user) use($url, $screenshotName){
                    $browser
                        ->visit($url)
                        ->assertPathIs($url)
                        ->fullScreenshots($screenshotName);
                }, $user);
            }
            $num++;
        }
    }
}
