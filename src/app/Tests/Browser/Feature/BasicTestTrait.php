<?php
namespace Azuki\App\Tests\Browser\Feature;

use Laravel\Dusk\Browser;

trait BasicTestTrait
{
    /**
     * @test
     *
     *
     */
    public function 未ログインリダイレクトテスト()
    {
        $this->browse(function(Browser $browser) {
            $browser->visit($this->getPage())
                ->on($this->getLoginPage());
        });
    }

    /**
     * @test
     *
     *
     * -> 原則は一覧ページが表示されることの確認となる
     */
    public function ログイン時ページ表示テスト()
    {
        $user = $this->getLoginUser();

        $this->browse(function(Browser $browser) use($user){
            $page = $this->getPage()->setLogin();

            $browser->loginAs($user, $this->guard)
                ->visit($page);
        });
    }
    
    /**
     * @test
     *
     */
    public function 検索フィールドをテスト()
    {
        // ショート検索の有無で処理を変更
        // 検索フィールドの項目をテストする
        // →＋、-　の挙動も合わせて確認する必要あり
        $this->loginBrowse(function(Browser $browser, $user){
            $page = $this->getPage()->setLogin();
            
            $browser->visit($page->url())
                ->onWithoutAssert($page)
                ->assertSearchFields();

    
            // テストデータ投入
            $data = $this->getTestSearchData();
            $searchConditions = $this->getSearchConditions($data);
            
            $browser->refresh()
                ->screenshots('一覧に500件登録');
            
            foreach($searchConditions as $key => $condition) {
                // 検索条件セットしてコミット
                $browser
                    ->setSearchCondition($condition)
                    ->click('@toSearch')
                    ->screenshots($key.'_検索結果');
            
                // 条件に合致したデータが表示されているかチェック
                $except = $this->getExceptSearchResult($condition, $data);
                $browser->assertSeachResult($except);
            }
        });
    }
    
    /**
     *
     *
     */
    public function 一覧のソートをテスト()
    {
    }
    
    /**
     *
     *
     */
    public function 一覧データの表示項目をテスト()
    {
        // 表示項目を確認する
        
        // データなし時表示を確認する
        
        // テストデータを投入してアクセス
        
        // 表示件数を確認する
        
        // ページャーを確認する
    }
    
    /**
     * @test
     *
     */
    public function 新規作成ページの表示項目をテスト()
    {
        $this->loginBrowse(function(Browser $browser, $user){
            $page = $this->getPage()->setLogin();
            
            $browser->visit($page->registUrl())
                ->onWithoutAssert($page)
                ->assertRegistPage();
        });
    }
    
    /**
     * @test
     *
     */
    public function 詳細ページの表示項目をテスト()
    {
        // テストデータ投入
        $data = $this->createTestData();

        $this->loginBrowse(function(Browser $browser, $user) use($data){
            $page = $this->getPage()->setLogin();
            
            $browser->visit($page->detailUrl($data))
                ->onWithoutAssert($page)
                ->assertDetailPage($data);
        });
    }
    
    /**
     * @test
     *
     */
    public function 編集ページの表示項目をテスト()
    {
        // テストデータ投入
        $data = $this->createTestData();

        $this->loginBrowse(function(Browser $browser, $user) use($data){
            $page = $this->getPage()->setLogin();
            
            $browser->visit($page->editUrl($data))
                ->onWithoutAssert($page)
                ->assertEditPage($data);
        });
    }
    
    /**
     * @test
     *
     * 確認用のデータはテストとして意味のあるデータにする必要がある
     * かつパスワードのように変更の有無で表示が変わる場合があるのでそれについても
     * テストが必要->２回チェックがいる
     * ->とりあえずパスワードだけを考えて構成するか、ほかにもデータ変更して確認することを
     * 　想定した構成をそもそも考えるか検討がいる
     */
    public function 確認ページの表示項目をテスト()
    {
        // テストデータ投入
        $data = $this->createTestData();

        $this->loginBrowse(function(Browser $browser, $user) use($data){
            $page = $this->getPage()->setLogin();
            
            $browser->visit($page->editUrl($data))
                ->onWithoutAssert($page)
                ->click('@toConfirm')
                ->assertConfirmPage($data);
        });
    }
    
    /**
     * @test
     *
     * エラーになるデータでバリデーションに引っかかること
     * 戻るボタンのチェック
     * 確認画面でhiddenを変更して登録->エラーデータの場合はバリデーションに引っかかることを確認
     */
    public function 新規データ登録の処理をテスト()
    {
        $this->loginBrowse(function(Browser $browser, $user){
            $page = $this->getPage()->setLogin();
            $data = $this->getRegistData();
            
            // 正常シーケンス
            $browser->visit($page->registUrl())
                ->onWithoutAssert($page)
                ->registData($data)
//                ->screenshot('新規登録データ入力')
                ->click('@toConfirm')
//                ->screenshot('新規登録確認画面')
                ->waitFor('@toComplete')
                ->click('@toComplete')
//                ->screenshot('新規登録完了画面')
                ->assertRegistResult($data);
        });
    }
    
    /**
     *
     *
     */
    public function 既存データ編集の処理をテスト()
    {
    }
    
    /**
     *
     *
     */
    public function 既存データ削除の処理をテスト()
    {
        // テストデータ投入
    }
    
    /**
     *
     *
     */
    public function CSV一括アップロードをテスト()
    {
    }
    
    /**
     *
     *
     */
    public function CSV一括ダウンロードをテスト()
    {
    }
    
    /**
     *
     *
     */
    abstract protected function getPage();
    
    /**
     *
     *
     */
    abstract protected function createTestData();
    
    /**
     *
     *
     */
    abstract protected function getTestSearchData();
    
    /**
     *
     *
     */
    abstract protected function getSearchConditions($data);
    
    /**
     *
     *
     */
    abstract protected function getExceptSearchResult($cond, $data);
}
