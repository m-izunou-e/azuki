<?php
namespace Azuki\App\Tests\Browser\Feature;

use Laravel\Dusk\Browser;

trait ResetPasswordTrait
{
    /*
     *
     */
    protected $mailLogTable = 'azuki_mail_logs';
    
    /**
     *
     *
     */
    protected $invalidMail = 'invalid@example.co.jp';

    /**
     *
     * @test
     * @return void
     */
    public function パスワードリセットメール送信画面テスト()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit($this->getSendMailPage());
        });
    }

    /**
     * @test
     * 存在しないメールアドレスでパスワードリセットのリクエストをして失敗
     */
    public function 存在しないメールアドレス入力テスト()
    {
        // ユーザーを作成
        $user = $this->createTestUser();
        $mail = $this->invalidMail;
        // メールログテーブルを初期化
        $this->truncateTable($this->mailLogTable);

        // 存在しないユーザーのメールアドレスでパスワードリセットをリクエスト
        $this->browse(function (Browser $browser) use($mail) {
            $page = $this->getSendMailPage();

            $response = $this->postResetSendMail( $browser, $mail )
                ->assertPathIs($page->url())
                ->assertSee($page->getErrorMessage());
        });

    }

    /**
     * @test
     * パスワードリセットのリクエスト成功
     * Duskで試験すると、スクリーンショットとれるのがよいのだけど、
     * このテストパターンはメール送信されちゃう。。どうやって送信を阻止するか
     * →Duskやなかったら、Mail::fake()して送信をブロックできる
     */
    public function パスワードリセットメールアドレス送信テスト()
    {
        // ユーザーを作成
        $user = $this->createTestUser();
        $mail = $user->email;
        // メールログテーブルを初期化
        $this->truncateTable($this->mailLogTable);
    
        // パスワードリセットをリクエスト
        $this->browse(function (Browser $browser) use($mail){
            $response = $this->postResetSendMail( $browser, $mail )
                ->on($this->getSentMailPage());
        });
        
        // メールログのデータベースに記録されていることを確認
        $this->checkMailLog($mail);
    }
    
    /**
     * @test
     *
     * TODO リセット画面にて
     *      １．トークン無しでのアクセス　->　404
     *      ２．各種入力値エラー時　　　　->　対応するエラーメッセージ表示
     *      のテストを実装する
     */
    public function パスワードリセット画面テスト()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit($this->getResetPasswordPage());
        });
    }
    
    /**
     * @test
     *
     */
    public function パスワードリセット完了画面テスト()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit($this->getCompleteResetPasswordPage());
        });
    }

    /**
     *
     *
     *
     */
    protected function postResetSendMail( $browser, $mail )
    {
        $response = $browser->visit($this->getSendMailPage())
            ->type('@emailInput', $mail)
            ->press('@sendBtn');

        return $response;
    }
    
    /**
     *
     *
     *
     */
    protected function checkMailLog($mail)
    {
        $this->assertDatabaseHas($this->mailLogTable, [
            'subject' => $this->mailSubject,
            'to'      => sprintf('<%s>', $mail)
        ]);
    }
}
