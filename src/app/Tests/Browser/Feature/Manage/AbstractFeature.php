<?php
namespace Azuki\App\Tests\Browser\Feature\Manage;

use Azuki\App\Tests\DuskTestCase as TestCase;
use Azuki\App\Tests\Browser\Pages\Manage\LoginPage;

abstract class AbstractFeature extends TestCase
{
    /**
     *
     *
     */
    protected $guard = 'manage';

    /**
     *
     */
    protected $group = 'manage';

    /**
     *
     */
    protected $baseUrl = '/manage';

    /**
     *
     *
     */
    public function setUp(): void
    {
        parent::setUp();
//        $this->seedMasterData();
    }

    /**
     *
     *
     */
    public function tearDown(): void
    {
        $this->truncateTables();
        parent::tearDown();
    }
    
    /**
     *
     *
     */
    protected function getLoginPage()
    {
        return new LoginPage;
    }
    
    /**
     *
     *
     *
     */
    protected function getLoginUser()
    {
        $managers = $this->getManagersFactory(10, ['roleIsAdmin', 'userEnable'])->create();
        $manager = $managers->first();
        
        return $manager;
    }
}
