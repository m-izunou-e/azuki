<?php
namespace Azuki\App\Tests\Browser\Feature\System;

use Azuki\App\Tests\DuskTestCase as TestCase;
use Azuki\App\Tests\Browser\Pages\System\LoginPage;

abstract class AbstractFeature extends TestCase
{
    /**
     *
     *
     */
    protected $guard = 'system';
    
    /**
     *
     */
    protected $group = 'system';

    /**
     *
     */
    protected $baseUrl = '/system';

    /**
     *
     *
     */
    public function setUp(): void
    {
        parent::setUp();
//        $this->seedMasterData();
    }

    /**
     *
     *
     */
    public function tearDown(): void
    {
        $this->truncateTables();
        parent::tearDown();
    }
    
    /**
     *
     *
     */
    protected function getLoginPage()
    {
        return new LoginPage;
    }
    
    /**
     *
     *
     *
     */
    protected function getLoginUser()
    {
        $dirctors = $this->getDirectorsFactory(10, ['roleIsAdmin', 'userEnable'])->create();
        $dirctor = $dirctors->first();
        
        return $dirctor;
    }
}
