<?php
namespace Azuki\App\Tests\Browser\Feature;

use Laravel\Dusk\Browser;

trait LoginTrait
{

    /**
     * @test
     * @return void
     */
    public function ログイン画面テスト()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit($this->getLoginPage());
        });
    }

    /**
     * @test
     * @return void
     */
    public function ログインエラーテスト()
    {
        $this->createTestUser();

        $this->browse(function (Browser $browser) {
            $this->loginResponse($browser, 'error')
                ->assertPathIs($this->getLoginPage()->url())
                ->assertSee($this->loginSet['error']['message'])
                ->screenshot($this->loginSet['error']['afterScShotName']);
        });
    }

    /**
     * @test
     * @return void
     */
    public function ログイン成功テスト()
    {
        $this->createTestUser();

        $this->browse(function (Browser $browser) {
            $this->loginResponse($browser, 'success')
                ->on($this->getLoginSuccessPage())
                ->screenshot($this->loginSet['success']['afterScShotName']);
        });

    }
    
    /**
     *
     *
     *
     */
    protected function loginResponse($browser, $type)
    {
        $scShot   = $this->loginSet[$type]['screenShotName'];
        $loginId  = $this->loginSet[$type]['loginId'];
        $password = $this->loginSet[$type]['password'];
    
        $response = $browser->visit($this->getLoginPage())
            ->screenshot($scShot)
            ->type('@emailInput',    $loginId)
            ->type('@passwordInput', $password)
            ->press('@loginBtn');
            
        return $response;
    }
}
