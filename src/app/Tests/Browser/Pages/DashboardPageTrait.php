<?php
namespace Azuki\App\Tests\Browser\Pages;

trait DashboardPageTrait
{
    /**
     *
     *
     */
    protected $pageAssertions = [
        [
            'method' => 'assertTitleContains',
            'assert' => [
                'ダッシュボード',
            ],
        ],
    ];
    
    /**
     *
     *
     */
    protected $url = '/';
    
    /**
     *
     *
     */
    protected $elements = [
    ];
    
    /**
     *
     *
     *
     */
    protected function getAssertions()
    {
        $ret = $this->pageAssertions;
        if(!$this->isLogin) {
            // ログイン画面へリダイレクトされることを確認
            $ret = [
                [
                    'method' => 'assertRouteIs',
                    'assert' => [
                        'login',
                    ],
                ],
            ];
        }

        return $ret;
    }
}
