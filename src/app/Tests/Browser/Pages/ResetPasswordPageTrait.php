<?php
namespace Azuki\App\Tests\Browser\Pages;

trait ResetPasswordPageTrait
{
    /**
     *
     *
     */
    protected $pageAssertions = [
/*
        [
            'method' => 'assertPathIs',
            'assert' => [
                '/reset-password/reset-password',
            ],
        ],
*/
        [
            'method' => 'assertRouteIs',
            'assert' => [
                ['reset-password.reset-password', ['aaaaaaaaaaaaaaaaaa']],
            ],
        ],
        [
            'method' => 'assertTitleContains',
            'assert' => [
                'パスワード再設定',
            ],
        ],
        [
            'method' => 'assertSee',
            'assert' => [
                'メールアドレス',
                'パスワード',
                'パスワード(確認)',
            ],
        ],
        [
            'method' => 'assertValue',
            'assert' => [
                ['@resetBtn', 'パスワード再設定'],
            ],
        ],
    ];
    
    /**
     *
     *
     */
    protected $url = '/reset-password/reset-password';
    
    /**
     *
     */
    protected $dummyToken = 'aaaaaaaaaaaaaaaaaa';
    
    /**
     *
     *
     */
    protected $elements = [
        '@emailInput'      => 'input[name="email"]',
        '@passwordInput'   => 'input[name="password"]',
        '@passwordConfirm' => 'input[name="password_confirmation"]',
        '@resetBtn'        => 'input[name="toResetPassword"]',
    ];
    
    /**
     *
     *
     */
    protected $errorMessage = [
        'ご入力のメールアドレスは登録されておりません',
        'メールアドレスは必須です。',
        'パスワードは必須です。',
        'パスワードリセットトークンが正しくありません。パスワードリセット用URLの有効期限が切れたか、途中で切れたりしていませんでしょうか。',
    ];
    
    /**
     *
     * div.alert.callout の有無でエラー判定の方がメッセージでみるよりよいか？
     */
    public function getErrorMessage($kind)
    {
        return $this->errorMessage[$kind];
    }
    
    /**
     *
     *
     */
    public function url()
    {
        return parent::url() . '/' . $this->dummyToken;
    }
}
