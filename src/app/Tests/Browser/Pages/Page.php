<?php
namespace Azuki\App\Tests\Browser\Pages;

use Tests\Browser\Pages\Page as BasePage;
use Laravel\Dusk\Browser;

abstract class Page extends BasePage
{
    /**
     *
     *
     */
    protected $isLogin = false;

    /**
     *
     *
     */
    protected $headerMenuAssertions = [
        [
            'method' => 'assertSeeIn',
            'assert' => [
                ['@headerMenu', 'ダッシュボード'],
                ['@headerMenu', 'プロフィール'],
                ['@headerMenu', 'ログアウト'],
            ],
        ],
    ];

    /**
     * Get the global element shortcuts for the site.
     *
     * @return array
     */
    public static function siteElements()
    {
        $siteElements = parent::siteElements();
        return array_merge( $siteElements, 
        [
            '@headerMenuOpen' => '.is-dropdown-submenu-parent i',
            '@headerMenu'     => 'ul.menu.is-dropdown-submenu',
            '@globalMenuOpen' => 'p#open-global-navi',
            '@globalMenu'     => 'div#globalMenu ul.flex-area li.cell',
        ]);
    }

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return $this->baseUrl . $this->url;
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  \Laravel\Dusk\Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser = $this->prepareAssert($browser);
        $pageAssertions = $this->getAssertions();

        $this->execAssert($browser, $pageAssertions);
        $this->postAssert($browser);
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return $this->elements;
    }
    
    /**
     *
     *
     */
    protected function execAssert(Browser $browser, $asserts)
    {
        foreach($asserts as $method => $set) {
            $method = isset($set['method']) ? $set['method'] : $method;
            foreach($set['assert'] as $assert) {
                $assert = $this->apendAssert($method, $assert);
                if(is_array($assert)) {
                    $browser->{$method}(...$assert);
                } else {
                    $browser->{$method}($assert);
                }
            }
        }
    }
    
    /**
     *
     *
     */
    protected function apendAssert($method, $assert)
    {
        $ret = $assert;
        if( $method == 'assertPathIs' || $method == 'assertPathBeginsWith' ) {
            if(!is_array($assert)) {
                $ret = $this->baseUrl . $assert;
            }
        } elseif( $method == 'assertRouteIs' ) {
            if(is_array($assert)) {
                $ret = [$this->group . '.' . $assert[0], $assert[1]];
            } else {
                $ret = $this->group . '.' . $assert;
            }
        }
        return $ret;
    }
    
    /**
     *
     *
     */
    protected function prepareAssert(Browser $browser)
    {
//        $this->screenshots($browser);
        return $browser;
    }
    
    /**
     *
     * TODO 設定でメニュー変わるのでそれを吸収する仕組みが必要
     *      メニューチェックはリンクも含めてテストする形に調整したい
     */
    protected function postAssert(Browser $browser)
    {
        if($this->isLogin) {
            // メニューチェック
            $browser->click('@headerMenuOpen')
                ->waitFor('@headerMenu');
            $this->execAssert($browser, $this->headerMenuAssertions);

            $browser->click('@globalMenuOpen')
                ->waitFor('@globalMenu');
            $this->execAssert($browser, $this->globalMenuAssertions);
        }
    }
    
    /**
     *
     *
     *
     */
    protected function getAssertions()
    {
        return $this->pageAssertions;
    }
    
    /**
     *
     *
     */
    public function setLogin()
    {
        $this->isLogin = true;
        return $this;
    }
    
    /**
     *
     *
     */
    public function setLogout()
    {
        $this->isLogin = false;
        return $this;
    }
    
    /**
     *
     *
     */
/*
    protected function screenShots(Browser $browser, $prefix = null)
    {
        // 既存スクリーンショットの確認　かつ　一意な保存ファイル名を取得
        $path = Browser::$storeScreenshotsAt;
        $prefix = !is_null($prefix) ? $prefix : str_replace(
            '\\',
            '/',
            str_replace('Azuki\\Tests\\Browser\\', '', get_class($this))
        );
        
        $num = 0;
        while($num <= 1000) {
            $screenshot = sprintf('%s[%s]', $prefix, $num);
            if(!file_exists($path . '/' . $screenshot. '.png')) {
                break;
            }
            $num++;
        }
        
        // スクリーンショットを取得
        $browser->screenshot($screenshot);
    }
*/
}
