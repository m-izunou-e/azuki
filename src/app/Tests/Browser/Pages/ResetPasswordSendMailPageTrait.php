<?php
namespace Azuki\App\Tests\Browser\Pages;

trait ResetPasswordSendMailPageTrait
{
    /**
     *
     *
     */
    protected $pageAssertions = [
        [
            'method' => 'assertPathIs',
            'assert' => [
                '/reset-password/send-reset-mail',
            ],
        ],
        [
            'method' => 'assertRouteIs',
            'assert' => [
                'reset-password.send-reset-mail',
            ],
        ],
        [
            'method' => 'assertTitleContains',
            'assert' => [
                'パスワード再設定メール',
            ],
        ],
        [
            'method' => 'assertSee',
            'assert' => [
                'Email',
            ],
        ],
        [
            'method' => 'assertValue',
            'assert' => [
                ['@sendBtn', 'パスワード再設定メールの送信'],
            ],
        ],
    ];
    
    /**
     *
     *
     */
    protected $url = '/reset-password/send-reset-mail';
    
    /**
     *
     *
     */
    protected $elements = [
        '@emailInput' => 'input[name="email"]',
        '@sendBtn'    => 'input[name="toSentMail"]',
    ];
    
    /**
     *
     *
     */
    protected $errorMessage = 'ご入力のメールアドレスは登録されておりません';
    
    /**
     *
     *
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }
}
