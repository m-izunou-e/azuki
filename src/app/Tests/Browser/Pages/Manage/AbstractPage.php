<?php
namespace Azuki\App\Tests\Browser\Pages\Manage;

use Azuki\App\Tests\Browser\Pages\Page;
use Laravel\Dusk\Browser;

abstract class AbstractPage extends Page
{
    /**
     *
     */
    protected $group = 'manage';

    /**
     *
     */
    protected $baseUrl = '/manage';

    /**
     *
     *
     */
    protected $globalMenuAssertions = [
        [
            'method' => 'assertSeeIn',
            'assert' => [
                ['@globalMenu:nth-child(1)', 'スタッフ'],
                ['@globalMenu:nth-child(2)', 'ユーザー'],
                ['@globalMenu:nth-child(3)', 'コマンド実行状況'],
//                ['@globalMenu:nth-child(4)', 'サンプル'],
            ],
        ],
    ];
}
