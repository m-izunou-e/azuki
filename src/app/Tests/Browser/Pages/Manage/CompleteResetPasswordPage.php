<?php
namespace Azuki\App\Tests\Browser\Pages\Manage;

use Azuki\App\Tests\Browser\Pages\CompleteResetPasswordPageTrait;

class CompleteResetPasswordPage extends AbstractPage
{
    use CompleteResetPasswordPageTrait;
}
