<?php
namespace Azuki\App\Tests\Browser\Pages\Manage;

use Azuki\App\Tests\Browser\Pages\ResetPasswordSendMailPageTrait;

class ResetPasswordSendMailPage extends AbstractPage
{
    use ResetPasswordSendMailPageTrait;
}
