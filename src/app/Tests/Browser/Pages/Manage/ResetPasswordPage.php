<?php
namespace Azuki\App\Tests\Browser\Pages\Manage;

use Azuki\App\Tests\Browser\Pages\ResetPasswordPageTrait;

class ResetPasswordPage extends AbstractPage
{
    use ResetPasswordPageTrait;
}
