<?php
namespace Azuki\App\Tests\Browser\Pages\Manage;

use Azuki\App\Tests\Browser\Pages\ResetPasswordSentMailPageTrait;

class ResetPasswordSentMailPage extends AbstractPage
{
    use ResetPasswordSentMailPageTrait;
}
