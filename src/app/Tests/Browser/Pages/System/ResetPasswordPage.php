<?php
namespace Azuki\App\Tests\Browser\Pages\System;

use Azuki\App\Tests\Browser\Pages\ResetPasswordPageTrait;

class ResetPasswordPage extends AbstractPage
{
    use ResetPasswordPageTrait;
}
