<?php
namespace Azuki\App\Tests\Browser\Pages\System;

use Azuki\App\Tests\Browser\Pages\DashboardPageTrait;

class DashboardPage extends AbstractPage
{
    use DashboardPageTrait;
}
