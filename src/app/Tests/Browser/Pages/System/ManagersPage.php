<?php
namespace Azuki\App\Tests\Browser\Pages\System;

use Laravel\Dusk\Browser;

class ManagersPage extends AbstractPage
{
    /**
     *
     *
     */
    protected $pageAssertions = [
        'assertPathIs' => [
            'method' => 'assertPathIs',
            'assert' => [
                '/managers/list',
            ],
        ],
        'assertRouteIs' => [
            'method' => 'assertRouteIs',
            'assert' => [
                'managers.list',
            ],
        ],
        'assertTitleContains' => [
            'method' => 'assertTitleContains',
            'assert' => [
                '運用スタッフ管理',
            ],
        ],
    ];
    
    /**
     *
     *
     */
    protected $searchFields = [
        'name' => [
            'title' => 'お名前',
            'type'  => 'input',
            'name'  => 'search[name]',
            'placeholder' => '',
            'value' => '',
        ],
        'login_id' => [
            'title' => 'ログインID',
            'type'  => 'input',
            'name'  => 'search[login_id]',
            'placeholder' => '',
            'value' => '',
        ],
        'email' => [
            'title' => 'メールアドレス',
            'type'  => 'input',
            'name'  => 'search[email]',
            'placeholder' => '',
            'value' => '',
        ],
        'role' => [
            'title'  => '役割',
            'type'   => 'checkbox',
            'name'   => 'search[role][]',
            'select' => 'managerRoles',
            'value'  => '',
        ],
        'belong' => [
            'cond'  => [
                'kind' => 'config',
                'key'  => 'azuki.standard.organizations',
                'value'=> true,
            ],
            'title'  => '所属',
            'type'   => 'select',
            'name'   => 'search[belong]',
            'select' => 'managerBelongs',
            'value'  => '',
        ],
        'enable' => [
            'title'  => '有効・無効',
            'type'   => 'checkbox',
            'name'   => 'search[enable][]',
            'select' => 'validManagerLabel',
            'value'  => '',
        ],
    ];

    /**
     *
     *
     */
    protected $registPageAssertions = [
        'assertPathIs' => [
            'method' => 'assertPathIs',
            'assert' => [
                '/managers/regist',
            ],
        ],
        'assertRouteIs' => [
            'method' => 'assertRouteIs',
            'assert' => [
                'managers.regist',
            ],
        ],
        'assertTitleContains' => [
            'method' => 'assertTitleContains',
            'assert' => [
                '運用スタッフ管理',
            ],
        ],
        'assertSee' => [
            'method' => 'assertSee',
            'assert' => [
                'お名前',
            ],
        ],
    ];

    /**
     *
     *
     */
    protected $detailPageAssertions = [
        'assertPathIs' => [
            'method' => 'assertPathIs',
            'assert' => [
                '/managers/detail/',
            ],
        ],
        'assertRouteIs' => [
            'method' => 'assertRouteIs',
            'assert' => [
                'managers.detail',
            ],
        ],
        'assertTitleContains' => [
            'method' => 'assertTitleContains',
            'assert' => [
                '運用スタッフ管理',
            ],
        ],
        'assertSee' => [
            'method' => 'assertSee',
            'assert' => [
                'お名前',
            ],
        ],
    ];

    /**
     *
     *
     */
    protected $editPageAssertions = [
        'assertPathIs' => [
            'method' => 'assertPathIs',
            'assert' => [
                '/managers/edit/',
            ],
        ],
        'assertRouteIs' => [
            'method' => 'assertRouteIs',
            'assert' => [
                'managers.edit',
            ],
        ],
        'assertTitleContains' => [
            'method' => 'assertTitleContains',
            'assert' => [
                '運用スタッフ管理',
            ],
        ],
        'assertSee' => [
            'method' => 'assertSee',
            'assert' => [
                'お名前',
            ],
        ],
    ];

    /**
     *
     *
     */
    protected $confirmPageAssertions = [
        'assertPathIs' => [
            'method' => 'assertPathIs',
            'assert' => [
                '/managers/confirm',
            ],
        ],
        'assertRouteIs' => [
            'method' => 'assertRouteIs',
            'assert' => [
                'managers.p-confirm',
            ],
        ],
        'assertTitleContains' => [
            'method' => 'assertTitleContains',
            'assert' => [
                '運用スタッフ管理',
            ],
        ],
        'assertSee' => [
            'method' => 'assertSee',
            'assert' => [
                'お名前',
            ],
        ],
    ];
    
    /**
     *
     *
     *
     */
    protected $noListMessage = '表示できる情報がありません。';
    
    /**
     *
     *
     *
     */
    protected $completeMessage = 'を登録しました。';
    
    /**
     *
     *
     */
    protected $url = '/managers';
    
    /**
     *
     *
     */
    protected $elements = [
        '@toConfirm'  => 'button[name="confirm"]',
        '@toComplete' => 'button[name="complete"]',
        '@toSearch'   => 'button[name="list_search_btn"]',
    ];
    
    /**
     *
     *
     *
     */
    protected function getAssertions()
    {
        $ret = $this->pageAssertions;
        
        // 件数が０ならデータなしメッセージの表示をチェック
        // TODO 追加処理は別メソッド化して元のassertにassertSeeがあった場合に正しく追加できるようにする
        if( $this->countList() <= 0 ) {
            $ret['assertSee'] = [
                'method' => 'assertSee',
                'assert' => [
                    $this->noListMessage,
                ],
            ];
        }
        if(!$this->isLogin) {
            // ログイン画面へリダイレクトされることを確認
            $ret = [
                [
                    'method' => 'assertRouteIs',
                    'assert' => [
                        'login',
                    ],
                ],
            ];
        }

        return $ret;
    }
    
    /**
     *
     *
     */
    protected function countList()
    {
        $ret = 0;
        
        return $ret;
    }
    
    /**
     *
     *
     *
     */
    public function assertSearchFields(Browser $browser)
    {
        $fields = $this->searchFields;
        
        foreach($fields as $field) {
            $type  = $field['type'];
            $title = $field['title'];
        
            if(isset($field['cond'])) {
                if(!$this->checkFieldCondition($field['cond'])) {
                    $browser->assertDontSee($title);
                    continue;
                }
            }
            
            $name   = $field['name'];
            $value  = $field['value'];
            $select = isset($field['select']) ? getSelectValues($field['select']) : [];
            
            $browser->assertSee($title);
        
            if($type == 'input') {
                $browser->assertPresent('input[name="'.$name.'"]');
                $browser->assertInputValue('input[name="'.$name.'"]', $value);
            } elseif($type == 'checkbox') {
                $browser->assertPresent('input[name="'.$name.'"]');
                $browser->assertNotChecked('input[name="'.$name.'"]');
/*              選択肢の表示内容もチェックが必要
                checkboxはIDも降っているのでIDで指定するのも手か
                $i = 1;
                foreach($select as $val) {
                    $selector = 'input[name="'.$name.'"]:nth-of-type('.$i.')';
                    $browser->assertInputValue($selector, $val);
                    $i++;
                }
*/
            } elseif($type == 'select') {
                $browser->assertPresent('select[name="'.$name.'"]');
                // optionのvalue値だけではなく選択肢の文字列内容もチェックが必要
                $browser->assertSelectHasOptions('select[name="'.$name.'"]', $select);
            } else {
            }
        }
    }
    
    /**
     *
     *
     */
    protected function checkFieldCondition($condition)
    {
        $ret = true;
        if($condition['kind'] == 'config') {
            $ret = app('config')->get($condition['key']) == $condition['value'] ? true : false;
        }
        
        return $ret;
    }
    
    /**
     *
     *
     *
     */
    public function setSearchCondition(Browser $browser, $condition)
    {
        foreach($condition as $cond) {
            $method = $cond['method'];
            $attribute = $cond['attr'];
            $browser->$method($attribute);
        }
    }
    
    /**
     *
     *
     *
     */
    public function assertSeachResult(Browser $browser, $except)
    {
        // 一覧の表示件数、ヒット数、ページャーの表示を確認
        
    }
    
    /**
     *
     *
     */
    public function registUrl()
    {
        return parent::url() . '/regist';
    }
    
    /**
     *
     *
     *
     */
    public function assertRegistPage(Browser $browser)
    {
        $this->execAssert( $browser, $this->registPageAssertions );
    }
    
    /**
     *
     *
     */
    public function detailUrl($data)
    {
        $data = count($data) > 0 ? $data[0] : $data;
        return parent::url() . '/detail/' . $data->id;
    }
    
    /**
     *
     *
     *
     */
    public function assertDetailPage(Browser $browser, $data)
    {
        $data = count($data) > 0 ? $data[0] : $data; 
        $this->detailPageAssertions['assertPathIs']['assert'] = [
            $this->detailPageAssertions['assertPathIs']['assert'][0] . $data->id
        ];
        $this->detailPageAssertions['assertRouteIs']['assert'] = [
            [$this->detailPageAssertions['assertRouteIs']['assert'][0], $data->id]
        ];
        $this->execAssert( $browser, $this->detailPageAssertions );
    }
    
    /**
     *
     *
     */
    public function editUrl($data)
    {
        $data = count($data) > 0 ? $data[0] : $data;
        return parent::url() . '/edit/' . $data->id;
    }
    
    /**
     *
     *
     *
     */
    public function assertEditPage(Browser $browser, $data)
    {
        $data = count($data) > 0 ? $data[0] : $data; 
        $this->editPageAssertions['assertPathIs']['assert'] = [
            $this->editPageAssertions['assertPathIs']['assert'][0] . $data->id
        ];
        $this->editPageAssertions['assertRouteIs']['assert'] = [
            [$this->editPageAssertions['assertRouteIs']['assert'][0], $data->id]
        ];
        $this->execAssert( $browser, $this->editPageAssertions );
    }
    
    /**
     *
     *
     *
     */
    public function assertConfirmPage(Browser $browser, $data)
    {
        $this->execAssert( $browser, $this->confirmPageAssertions );
    }
    
    /**
     *
     *
     *
     */
    public function  registData(Browser $browser, $data)
    {
        $browser
            ->type('name',          $data->name)
            ->type('login_id',      $data->login_id)
            ->type('email',         $data->email)
            ->type('password_in',   $data->password)
            ->type('password_conf', $data->password)
            ->radio('role',         $data->role)
//            ->select('belong', $data->belong) // 所属は設定により変える
            ->radio('enable',       $data->enable);
    }
    
    /**
     *
     *
     *
     */
    public function  assertRegistResult(Browser $browser, $data)
    {
        // 完了メッセージ、一覧へのデータの表示を確認
        $browser
            ->assertSee($this->completeMessage)
            ->assertSee($data->name)
            ->assertSee($data->login_id)
            ->assertSee($data->email)
            ->assertSee(getSelectName('managerRoles', $data->role))
//            ->assertSee($data->belong>)
            ->assertSee(getSelectName('managerRoles', $data->enable));
    }
}
