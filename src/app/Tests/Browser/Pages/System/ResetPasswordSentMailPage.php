<?php
namespace Azuki\App\Tests\Browser\Pages\System;

use Azuki\App\Tests\Browser\Pages\ResetPasswordSentMailPageTrait;

class ResetPasswordSentMailPage extends AbstractPage
{
    use ResetPasswordSentMailPageTrait;
}
