<?php
namespace Azuki\App\Tests\Browser\Pages\System;

use Azuki\App\Tests\Browser\Pages\CompleteResetPasswordPageTrait;

class CompleteResetPasswordPage extends AbstractPage
{
    use CompleteResetPasswordPageTrait;
}
