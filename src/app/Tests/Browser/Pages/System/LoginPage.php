<?php
namespace Azuki\App\Tests\Browser\Pages\System;

class LoginPage extends AbstractPage
{
    /**
     *
     *
     */
    protected $pageAssertions = [
        [
            'method' => 'assertSee',
            'assert' => [
                'システム管理',
                'Email',
                'パスワード',
            ],
        ],
        [
            'method' => 'assertSeeLink',
            'assert' => [
                'パスワード再設定',
            ],
        ],
        [
            'method' => 'assertSourceHas',
            'assert' => [
                '<input type="password" name="password" value="" placeholder="パスワードを入力">',
            ],
        ],
        [
            'method' => 'assertValue',
            'assert' => [
                ['@loginBtn', 'ログイン'],
            ],
        ],
    ];
    
    /**
     *
     *
     */
    protected $url = '/login';
    
    /**
     *
     *
     */
    protected $elements = [
        '@emailInput'    => 'input[name="email"]',
        '@passwordInput' => 'input[name="password"]',
        '@loginBtn'      => 'input[name="toLogin"]',
    ];

}
