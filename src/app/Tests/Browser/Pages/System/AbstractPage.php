<?php
namespace Azuki\App\Tests\Browser\Pages\System;

use Azuki\App\Tests\Browser\Pages\Page;

abstract class AbstractPage extends Page
{
    /**
     *
     */
    protected $group = 'system';

    /**
     *
     */
    protected $baseUrl = '/system';

    /**
     *
     *
     */
    protected $globalMenuAssertions = [
        [
            'method' => 'assertSeeIn',
            'assert' => [
                ['@globalMenu:nth-child(1)',  'システム管理者'],
                ['@globalMenu:nth-child(2)',  '運用スタッフ'],
                ['@globalMenu:nth-child(3)',  'ユーザー'],
                ['@globalMenu:nth-child(4)',  'ロール'],
//                ['@globalMenu:nth-child(1)',  '組織'],
                ['@globalMenu:nth-child(5)',  'アクセスログ'],
                ['@globalMenu:nth-child(6)',  'ログインログ'],
                ['@globalMenu:nth-child(7)',  'オペレーションログ'],
                ['@globalMenu:nth-child(8)',  'システムログ'],
                ['@globalMenu:nth-child(9)',  'コマンド実行状況'],
                ['@globalMenu:nth-child(10)', '環境'],
                ['@globalMenu:nth-child(11)', 'システム構成'],
            ],
        ],
    ];
}
