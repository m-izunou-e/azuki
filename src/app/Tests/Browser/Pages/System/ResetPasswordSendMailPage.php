<?php
namespace Azuki\App\Tests\Browser\Pages\System;

use Azuki\App\Tests\Browser\Pages\ResetPasswordSendMailPageTrait;

class ResetPasswordSendMailPage extends AbstractPage
{
    use ResetPasswordSendMailPageTrait;
}
