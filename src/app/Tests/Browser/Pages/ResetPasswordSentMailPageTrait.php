<?php
namespace Azuki\App\Tests\Browser\Pages;

trait ResetPasswordSentMailPageTrait
{
    /**
     *
     *
     */
    protected $pageAssertions = [
        [
            'method' => 'assertPathIs',
            'assert' => [
                '/reset-password/sent-reset-mail',
            ],
        ],
        [
            'method' => 'assertRouteIs',
            'assert' => [
                'reset-password.sent-reset-mail',
            ],
        ],
        [
            'method' => 'assertTitleContains',
            'assert' => [
                'パスワード再設定メール送信完了',
            ],
        ],
        [
            'method' => 'assertSee',
            'assert' => [
                '登録メールアドレスにパスワードリセットメールを送信しました',
            ],
        ],
    ];
    
    /**
     *
     *
     */
    protected $url = '/reset-password/sent-reset-mail';
    
    /**
     *
     *
     */
    protected $elements = [
    ];
}
