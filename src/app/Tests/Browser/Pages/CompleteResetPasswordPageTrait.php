<?php
namespace Azuki\App\Tests\Browser\Pages;

trait CompleteResetPasswordPageTrait
{
    /**
     *
     *
     */
    protected $pageAssertions = [
        [
            'method' => 'assertPathIs',
            'assert' => [
                '/reset-password/complete-reset-password',
            ],
        ],
        [
            'method' => 'assertRouteIs',
            'assert' => [
                'reset-password.complete-reset-password',
            ],
        ],
        [
            'method' => 'assertTitleContains',
            'assert' => [
                'パスワード再設定完了',
            ],
        ],
        [
            'method' => 'assertSee',
            'assert' => [
                'パスワードの再設定を完了しました',
                'ダッシュボード',
            ],
        ],
    ];
    
    /**
     *
     *
     */
    protected $url = '/reset-password/complete-reset-password';
    
    /**
     *
     *
     */
    protected $elements = [
    ];
}
