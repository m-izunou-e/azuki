<?php
namespace Azuki\App\Tests;

use Azuki\App\Database\Factories\ManagersFactory;
use Azuki\App\Database\Factories\DirectorsFactory;

trait Factories
{
    /**
     *
     *
     *
     */
    public function getManagersFactory($cnt = 1, $states = [])
    {
        if(laravelVersionOverEight()) {
            $obj = ManagersFactory::factory($cnt);
            if(is_array($states) && !empty($states)) {
                foreach($states as $method) {
                    if(is_callable([$obj, $method])) {
                        $obj = $obj->{$method}();
                    }
                }
            }
        } else {
            $obj = factory(\Azuki\App\Models\Managers::class, $cnt);
            if(is_array($states) && !empty($states)) {
                $obj = $obj->states($states);
            }
        }
        
        return $obj;
    }

    /**
     *
     *
     *
     */
    public function getDirectorsFactory($cnt = 1, $states = [])
    {
        if(laravelVersionOverEight()) {
            $obj = DirectorsFactory::factory($cnt);
            if(is_array($states) && !empty($states)) {
                foreach($states as $method) {
                    if(is_callable([$obj, $method])) {
                        $obj = $obj->{$method}();
                    }
                }
            }
        } else {
            $obj = factory(\Azuki\App\Models\Directors::class, $cnt);
            if(is_array($states) && !empty($states)) {
                $obj = $obj->states($states);
            }
        }
        
        return $obj;
    }

}
