<?php
namespace Azuki\App\Tests;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

trait SharedFunctionsForDatabase
{
    protected $isSeedMasterData = false;

    /**
     *
     *
     */
    protected function truncateTables()
    {
        $tablesKey = 'Tables_in_' . env('DB_DATABASE');
        Schema::disableForeignKeyConstraints();

        $tables = DB::select('SHOW TABLES');
        foreach( $tables as $table ) {
            DB::table($table->$tablesKey)->truncate();
        }

        Schema::enableForeignKeyConstraints();
        $this->isSeedMasterData = false;
    }

    /**
     *
     *
     */
    protected function truncateTable($table)
    {
        $tables = $table;
        if(!is_array($table)) {
            $tables = [$table];
        }

        Schema::disableForeignKeyConstraints();
        foreach($tables as $t) {
            DB::table($t)->truncate();
        }
        Schema::enableForeignKeyConstraints();
    }
    
    /**
     *
     *
     */
    protected function seedMasterData()
    {
        if( !$this->isSeedMasterData ) {
            $this->seed('MasterDataSeeder');
            $this->isSeedMasterData = true;
        }
    }
}
