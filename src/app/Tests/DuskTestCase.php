<?php
namespace Azuki\App\Tests;

use Tests\DuskTestCase as TestCase;
use Closure;
use Laravel\Dusk\Browser;

//use Illuminate\Support\Facades\Artisan;
// テストごとにマイグレーション処理をするのは本来的には正しい方法なんだけど、
// ページテストとかだと挙動的にもパフォーマンス的にも最適ではない。
// なので一旦この処理の読み込みは保留扱いとしておく
//use Illuminate\Foundation\Testing\DatabaseMigrations;

abstract class DuskTestCase extends TestCase
{
//    use DatabaseMigrations;

    /**
     *
     *
     */
    protected $prefixClassDir = 'Azuki\\Tests\\Browser\\';

    /**
     *
     */
    private static $initializeBrowserMacro = false;

    public static function setUpBeforeClass(): void
    {
        if(!self::$initializeBrowserMacro) {
            self::$initializeBrowserMacro = true;
            self::initBrowserMacro();
        }
    }

    protected static function initBrowserMacro(): void
    {
        Browser::macro('screenshots', function ($name = null) {
            // 既存スクリーンショットの確認　かつ　一意な保存ファイル名を取得
            $path = Browser::$storeScreenshotsAt;
            if( !is_null($name) ) {
                $prefix = $name;
            } else {
                $class = 'undefined';
                $backtrace = debug_backtrace();
                foreach($backtrace as $num => $bt) {
                    if(isset($bt['args'][0]) && $bt['args'][0] == 'screenshots') {
                        if( isset($backtrace[$num+1]['class'])
                            && $backtrace[$num+1]['class'] != 'Laravel\Dusk\Browser') {
                            $class = $backtrace[$num+1]['class'];
                            break;
                        }
                    }
                }
                $prefix = str_replace(
                    '\\',
                    '/',
                    str_replace($this->prefixClassDir, '', $class)
                );
            }
            
            $num = 0;
            while($num <= 1000) {
                $screenshot = sprintf('%s[%s]', $prefix, $num);
                if(!file_exists($path . '/' . $screenshot. '_0.png')) {
                    break;
                }
                $num++;
            }
            
            // スクリーンショットを取得
            $this->fullScreenshots($screenshot);

            return $this;
        });

        Browser::macro('fullScreenshots', function ($name) {
            // ウィンドウ高さ
            $height = $this->script('return window.innerHeight')[0];
            // ウィンドウスクロール量取得
            $allHeight = $this->script('return document.documentElement.scrollHeight')[0];
            // スクリーンショットの連続保存
            $index = 0;
            for ($i = 0; ($i * $height) < $allHeight; $i++) {
                // 画面スクロール
                $this->script('window.scrollTo(0, '.$i * $height.');');
                // スクリーンショット撮影
                $this
                    ->pause(1000)
                    ->screenshot($name."_{$i}");
                // 0.8秒スリープ
//                usleep(800000);
            }

            return $this;
        });
    }

/*
    public static function tearDownAfterClass(): void
    {
    }
*/
    use SharedFunctionsForDatabase;
    use Factories;

    /**
     * Register the base URL with Dusk.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $basePath = getcwd() . '/tests';

//        Browser::$baseUrl = $this->baseUrl();
        Browser::$storeScreenshotsAt = $basePath.'/Browser/screenshots';
        Browser::$storeConsoleLogAt = $basePath.'/Browser/console';
        Browser::$storeSourceAt = $basePath.'/Browser/source';

//        Browser::$userResolver = function () {
//            return $this->user();
//        };
        $this->seedMasterData();
    }
    
    /**
     *
     *
     */
    protected function loginBrowse(Closure $closure, $user = null)
    {
        $user = !is_null($user) ? $user : $this->getLoginUser();

        $this->browse(function(Browser $browser) use($closure, $user){
            $browser->loginAs($user, $this->guard);
            $closure($browser, $user);
        });
    }
}
