<?php
namespace Azuki\App\Tests;

use Tests\TestCase as BaseTestCase;
// テストごとにマイグレーション処理をするのは本来的には正しい方法なんだけど、
// ページテストとかだと挙動的にもパフォーマンス的にも最適ではない。
// なので一旦この処理の読み込みは保留扱いとしておく
//use Illuminate\Foundation\Testing\DatabaseMigrations;
//use Illuminate\Foundation\Testing\RefreshDatabase;

abstract class TestCase extends BaseTestCase
{
//    use RefreshDatabase;
//    use DatabaseMigrations;
    use SharedFunctionsForDatabase;
    use Factories;

    public function from(string $url)
    {
        session()->setPreviousUrl(url($url));
        return $this;
    }
}
