<?php
namespace Azuki\App\Policies;

class UsersPolicy
{
    public function _update($target, $user)
    {
        $ret = false;
        if($target->id == $user->id) {
            $ret = true;
        }
        return $ret;
    }
}
