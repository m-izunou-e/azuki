<?php
namespace Azuki\App\Models;

/**
 * ManageRolesにアクセスするためのモデルクラス
 *
 * @copyright  Copyright 2019 --- Project.
 * @author        Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Models\BaseRolesModel as Model;
use Azuki\App\Models\ManageRoleAuthorities;
use Azuki\App\Contracts\Models\ManageRoles as ContractsInterfase;

/**
 * class ManageRoles
 *
 */
class ManageRoles extends Model implements ContractsInterfase
{
    /**
     * The database table account by the model.
     *
     * @var  string
     */
    protected $table = 'azuki_manage_roles';
    
    /**
     *
     */
    protected $rolesIdAttribute = 'manage_roles_id';
    
    /**
     *
     *
     */
    public function roleAuthorities()
    {
        return $this->hasMany('Azuki\App\Models\ManageRoleAuthorities', 'manage_roles_id', 'id');
    }
    
    /**
     *
     *
     *
     */
    protected function getRoleAuthoritiesModel()
    {
        return app(ManageRoleAuthorities::class);
    }
}
