<?php
namespace Azuki\App\Models;

/**
 * directorsにアクセスするためのモデルクラス
 *
 * @copyright  Copyright 2019 --- Project.
 * @author        Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Models\BaseMasterModel as Model;

use Azuki\App\Contracts\Models\Organizations as ContractsInterfase;

/**
 * class Directors
 *
 */
class Organizations extends Model implements ContractsInterfase
{
    /**
     * The database table account by the model.
     *
     * @var  string
     */
    protected $table = 'azuki_organizations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'name_id',
        'original_domain',
        'value',
        'order',
    ];

    /**
     * function boot
     *
     * boot処理を追加する
     * boot時にdelete処理のフックメソッドの登録などを行うことができる
     *
     */
    protected static function boot() {
        parent::boot();

        self::deleting(function($obj) {
            // 設定されいてるmanagersがいれば削除させない
            $ret = true;
            if(!supportOrganizations()) {
                return $ret;
            }
            
            $belong = $obj->value;
            $managers = new Managers();
            $cnt = $managers->where('belong', '=', $belong)->count();
            if($cnt > 0) {
                $ret = false;
                // ログ記録する
                $msg = sprintf(
                    '所属しているユーザーが存在するためデータが削除できません[%s:%s]',
                    $obi->id,
                    $obj->name
                );
                $obj->warningLog($msg);
            }
            
            return $ret;
        });
    }
    
    /**
     * function setCondition
     *
     * 検索条件を設定する
     *
     * @param  QueryBuilder $query
     * @param  Array        $condition
     * @return QueryBuilder $query
     *
     */
    public function setCondition($query, $condition)
    {
        $query = parent::setCondition($query, $condition);
        $query = $this->when($query, [$condition, 'id'], function($query, $value) {
            return $query->where('id', '=', $value);
        });
        $query = $this->when($query, [$condition, 'name'], function($query, $value) {
            $where = "name LIKE ?";
            $whereCond = array(
                "%".$value."%",
            );
            return $query->whereRaw($where, $whereCond);
        });
        $query = $this->when($query, [$condition, 'name_id'], function($query, $value) {
            $where = "name_id LIKE ?";
            $whereCond = array(
                "%".$value."%",
            );
            return $query->whereRaw($where, $whereCond);
        });
        $query = $this->when($query, [$condition, 'original_domain'], function($query, $value) {
            $where = "original_domain LIKE ?";
            $whereCond = array(
                "%".$value."%",
            );
            return $query->whereRaw($where, $whereCond);
        });
        $query = $this->when($query, [$condition, 'value'], function($query, $value) {
            return $query->where('value', '=', $value);
        });

        return $query;
    }
}
