<?php
namespace Azuki\App\Models;

/**
 * system_switchにアクセスするためのモデルクラス
 *
 * @copyright  Copyright 2019 --- Project.
 * @author        Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Models\BaseHardDeleteModel as Model;
use Azuki\App\Contracts\Models\SystemSwitch as ContractsInterfase;

/**
 * class SystemSettings
 *
 */
class SystemSwitch extends Model implements ContractsInterfase
{
    /**
     * The database table account by the model.
     *
     * @var  string
     */
    protected $table = 'azuki_system_switch';

    /**
     * The attributes that are mass assignable.
     *
     * @var  array
     */
    protected $fillable = [
        'title',
        'kind',
        'status',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var  array
     */
    protected $hidden = [];
    
    /**
     *
     *
     */
    public function isRun($kind)
    {
        $obj = $this->getSwitchBy($kind);
        if(is_null($obj)) {
            // TODO ログ残す
            return false;
        }
        return $obj->status == SWITCH_STATUS_IS_RUN;
    }
    
    /**
     *
     *
     */
    public function isStop($kind)
    {
        $obj = $this->getSwitchBy($kind);
        if(is_null($obj)) {
            // TODO ログ残す
            return true;
        }
        return $obj->status == SWITCH_STATUS_IS_STOP;
    }
    
    /**
     *
     *
     */
    public function getSwitchBy($kind)
    {
        return $this->where('kind', '=', $kind)->first();
    }
    
    /**
     *
     *
     */
    public function switchOn($kind)
    {
        return $this->switchChangeTo($kind, SWITCH_STATUS_IS_RUN);
    }
    
    /**
     *
     *
     */
    public function switchOff($kind)
    {
        return $this->switchChangeTo($kind, SWITCH_STATUS_IS_STOP);
    }
    
    /**
     *
     *
     */
    public function switchChangeTo($kind, $status)
    {
        $obj = $this->getSwitchBy($kind);
        if(is_null($obj)) {
            return false;
        }
        
        return $obj->fill(['status' => $status])->save();
    }
}
