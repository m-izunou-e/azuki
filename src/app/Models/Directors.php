<?php
namespace Azuki\App\Models;

/**
 * directorsにアクセスするためのモデルクラス
 *
 * @copyright  Copyright 2019 --- Project.
 * @author        Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Models\AbstractAccountable as Model;
use Azuki\App\Notifications\ResetPasswordForDirectorNotification as PasswordNotification;

use Azuki\App\Contracts\Models\Directors as ContractsInterfase;

/**
 * class Directors
 *
 */
class Directors extends Model implements ContractsInterfase
{
    /**
     * The database table account by the model.
     *
     * @var  string
     */
    protected $table = 'azuki_directors';

    /**
     * The attributes that are mass assignable.
     *
     * @var  array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role',
        'enable',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var  array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * function boot
     *
     * boot処理を追加する
     * boot時にdelete処理のフックメソッドの登録などを行うことができる
     *
     */
    protected static function boot() {
        parent::boot();

        self::deleting(function($obj) {
            // id:1の初期設定されているスーパーバイザーユーザーは削除不可
            $ret = true;
            if($obj->id == 1) {
                return false;
            }
        });
    }

    /**
     * function setCondition
     *
     * @param  QueryBuilder $query
     * @param  Array        $condition
     * @return QueryBuilder $query
     *
     */
    public function setCondition($query, $condition)
    {
        $query = parent::setCondition($query, $condition);
        return $query;
    }
    
    /**
     * function sendPasswordResetNotification
     *
     * パスワードリセットメールを処理するNotificationクラスを呼び出す
     *
     * @param  string $token パスワードリセットトークン
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordNotification($token));
    }
}
