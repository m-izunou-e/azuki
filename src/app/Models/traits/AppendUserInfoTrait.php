<?php
namespace Azuki\App\Models\traits;

use Azuki\App\Contracts\Models\Directors;
use Azuki\App\Contracts\Models\Managers;
use Azuki\App\Contracts\Models\Users;

trait AppendUserInfoTrait
{
    /**
     *
     */
    protected $dirctorsTable = null;
    protected $managersTable = null;
    protected $usersTable = null;

    /**
     * function setTableNames
     *
     */
    protected function setTableNames()
    {
        if(is_null($this->dirctorsTable)) {
            $this->dirctorsTable = app(Directors::class)->getTable();
        }
        if(is_null($this->managersTable)) {
            $this->managersTable = app(Managers::class)->getTable();
        }
        if(is_null($this->usersTable)) {
            $this->usersTable = app(Users::class)->getTable();
        }
    }
    
    /**
     * function prepare
     *
     */
    public function prepare($query = null)
    {
        $this->setTableNames();
        $userNameColumn = sprintf(
            '( case %s.user_kind'.
            ' when %s then %s.name'.
            ' when %s then %s.name'.
            ' when %s then %s.name'.
            ' ELSE "--" END ) AS user_name ',
            $this->table,
            USER_KIND_DIRECTORS,
            $this->dirctorsTable,
            USER_KIND_MANAGERS,
            $this->managersTable,
            USER_KIND_USERS,
            $this->usersTable
        );
        
        $query = parent::prepare($query);
        $query = $query->select([
            $this->table.'.*',
            \DB::raw($userNameColumn),
        ])->leftJoin($this->dirctorsTable, function($join){
            $join->on($this->table.'.user_id', '=', $this->dirctorsTable.'.id')
                ->where($this->table.'.user_kind', '=', USER_KIND_DIRECTORS);
        })->leftJoin($this->managersTable, function($join){
            $join->on($this->table.'.user_id', '=', $this->managersTable.'.id')
                ->where($this->table.'.user_kind', '=', USER_KIND_MANAGERS);
        })->leftJoin($this->usersTable, function($join){
            $join->on($this->table.'.user_id', '=', $this->usersTable.'.id')
                ->where($this->table.'.user_kind', '=', USER_KIND_USERS);
        });
        
        return $query;
    }
    
    /**
     * function _findOrFail
     *
     *
     * @param Integer $id 取得するデータの識別子
     */
    public function _findOrFail($id)
    {
        $ret = parent::_findOrFail($id);
        // 呼び元でtoArrayにて配列かしているがその際にuserカラムがセットされていない状態になるのでここで追加しておく
        $ret->user = $ret->user;
        return $ret;
    }
    
    /**
     * function getUserAttribute
     *
     * userカラムへのアクセサ。userカラムは実テーブル上存在しないカラム
     */
    public function getUserAttribute($value)
    {
        if(isset($this->user_id)) {
            return sprintf('%s(id:%s)', $this->user_name, $this->user_id );
        } else {
            return sprintf('%s', $this->user_name);
        }
    }
}
