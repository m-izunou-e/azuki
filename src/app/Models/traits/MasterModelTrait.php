<?php
namespace Azuki\App\Models\traits;


trait MasterModelTrait
{
    /**
     * function getMasterList
     *
     * マスターデータの一覧を取得する
     */
    public function getMasterList($config = null)
    {
        $config = $config ?? $this->getDefaultOrders();
        return $this->getMasterListQuery($config)->get();
    }
    
    /**
     * function getMasterListWithTrashed
     *
     * マスターデータの一覧を取得する
     */
    public function getMasterListWithTrashed($config = null)
    {
        $config = $config ?? $this->getDefaultOrders();
        return $this->getMasterListQuery($config)->withTrashed()->get();
    }
    
    /**
     *
     */
    protected function getDefaultOrders()
    {
        return $this->orderKey != 'id' ?
            ['order' => [$this->orderKey, 'id']] : 
            ['order' => $this->orderKey];
    }
    
    /**
     * function getMasterListQuery
     *
     * マスターデータリストを取得するためのデフォルトのクエリ生成メソッド
     *
     * @param  Array  $config マスタデータ設定。config/master.phpで定義
     * @return Object $query  マスタデータ取得用のクエリ
     */
    protected function getMasterListQuery($conf = null)
    {
        $query = $this->query();
    
        if(isset($conf['cond']) && count($conf['cond']) >= 2) {
            if( is_null($conf['cond'][1]) ) {
                $query = $query->whereNull($conf['cond'][0]);
            } else {
                $query = $query->where($conf['cond'][0], '=', $conf['cond'][1]);
            }
        }

        $orders = [$this->orderKey];
        if(isset($conf['order'])) {
            $orders = is_array($conf['order']) ? $conf['order'] : [$conf['order']];
        }
        foreach($orders as $order) {
            $odr   = is_array($order) && isset($order[0]) ? $order[0] : $order;
            $sc    = is_array($order) && isset($order[1]) ? $order[1]: 'asc';
            $query = $query->orderBy($odr, $sc);
        }
        
        return $query;
    }
    
    /**
     * function formatForSelectList
     *
     * マスタデータ一覧をEloquentのCollectionデータからviewのセレクトで使用する
     * 配列の形に整形するメソッド
     *
     * @param  Collection $list Eloquentモデルのコレクションデータ
     * @return Array      $ret  [ [ 'name' => 'xxx', 'value' => 'yyy' ], ・・・・・]
     */
    public function formatForSelectList($list)
    {
        $ret = [];

        foreach( $list as $row ) {
            $key = isset($this->primaryKey) ? $row->{$this->primaryKey} : $row->id;
            $ret[$key]['value'] = $row->{$this->valueKey};
            $ret[$key]['name']  = $row->{$this->nameKey};
            if(isset($this->addValue)) {
                foreach($this->addValue as $val) {
                    $ret[$key][$val]  = $row->{$val};
                }
            }
        }
        
        return $ret;
    }
}
