<?php
namespace Azuki\App\Models\traits;


trait DeleteOldDataTrait
{
    /**
     *
     *
     *
     */
    public function hardDeleteOldData($year)
    {
        $date = date('Y-m-d 00:00:00', strtotime(sprintf('-%s year', $year)));
        return $this->where('created_at', '<', $date)->forceDelete();
    }
}
