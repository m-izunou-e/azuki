<?php
namespace Azuki\App\Models;

/**
 * サイト管理者の権限管理をするモデルクラス
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Models\BaseSoftDeleteModel as Model;

/**
 * class ManageRoleAuthorities
 *
 */
class ManageRoleAuthorities extends Model /*implements ContractsInterfase*/
{
    /**
     * The database table account by the model.
     *
     * @var  string
     */
    protected $table = 'azuki_manage_role_authorities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'manage_roles_id',
        'url',
        'authority',
    ];
}
