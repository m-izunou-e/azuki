<?php
namespace Azuki\App\Models;

/**
 * 各種モデルの基底クラス
 * モデルで共通の処理はこのクラスに記載する
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Illuminate\Database\Eloquent\Model;
use Azuki\App\Services\ExtendLog;
use Azuki\App\Models\Scopes\BelongScope;

/**
 * class
 *
 */
abstract class BaseModel extends Model
{
    /**
     * $exLog
     *
     * Log出力用のインスタンス
     */
    private static $exLog;

    /**
     * $recordOperationLogs
     *
     * オペレーションログの記録の有無を決める
     */
    protected $defRecordOperationLogs = true;     // システム全体の設定。.envで設定可能
    protected $recordOperationLogs = true;        // 各モデルレベルでの設定。ログ関係の場合はオペレーションログ常にオフなど
    protected static $stopOperationLogs = false;  // 一時的にオペレーションログを止めるための設定

    /**
     * function boot
     *
     * boot処理を追加する
     * boot時にdelete処理のフックメソッドの登録などを行うことができる
     *
     */
    protected static function boot() {
        parent::boot();

        // 新規作成時の共通処理
        self::created(function($obj) {
            try {
                self::operationLog($obj, OPLOG_MODE_CREATE);
            } catch(\Exception $e) {
                // ログを残して処理は続行する
                $obj->exceptionLog($e);
            }
        });

        // レコード更新時の共通処理
        self::updated(function($obj) {
            try {
                self::operationLog($obj, OPLOG_MODE_UPDATE);
            } catch(\Exception $e) {
                // ログを残して処理は続行する
                $obj->exceptionLog($e);
            }
        });

        // レコード削除時の共通処理
        self::deleted(function($obj) {
            try {
                self::operationLog($obj, OPLOG_MODE_DELETE);
            } catch(\Exception $e) {
                // ログを残して処理は続行する
                $obj->exceptionLog($e);
            }
        });
    }
    
    /**
     * function setNoOperationLogs
     *
     */
    public function setNoOperationLogs()
    {
        $this->setRecordOperationLogs(true);
    }
    
    /**
     * function setOperationLogs
     *
     */
    public function setOperationLogs()
    {
        $this->setRecordOperationLogs(false);
    }
    
    /**
     * function setRecordOperationLogs
     *
     */
    protected function setRecordOperationLogs($flg)
    {
        self::$stopOperationLogs = $flg;
    }
    
    /**
     * function operationLog
     *
     */
    protected static function operationLog($obj, $mode)
    {
        if(!$obj->defRecordOperationLogs || !$obj->recordOperationLogs || self::$stopOperationLogs) {
            return ;
        }
        $user = self::getUserWithKind();
        $originalData = serialize($obj->getOriginal());
        $dirtyData    = serialize($obj->getDirty());
        $userInfo = is_null($user) ? null : $user->toArray();
        $userInfo = is_null($userInfo) ? null : serialize($userInfo);
        $logData = [
            'table'         => $obj->getTableName($obj),
            'target_id'     => $obj->id,
            'user_id'       => is_null($user) ? null : $user->id,
            'user_kind'     => is_null($user) ? USER_KIND_NONE : $user->kind,
            'user_info'     => $userInfo,
            'op_mode'       => $mode,
            'original_data' => $originalData,
            'dirty_data'    => $dirtyData,
        ];
        
        unset($user->kind);
//        $opLogs = new OperationLogs();
//        $opLogs->_create($logData);
        OperationLogs::create($logData);
    }
    
    /**
     * function getTableName
     *
     * オペレーションログの操作テーブル名を生成するメソッド
     * 原則として対象となるテーブル名のみを返せばよいので$obj->getTable()の実行結果を返す
     * 関連テーブルがある場合のオペレーションログでは操作テーブルのところで
     * 関連状況の記録もしておくと現状は都合がよいのでこのような形で切り出して
     * 各クラスでオーバーライドできるようにしている
     ⊛
     */
    protected function getTableName($obj)
    {
        return $obj->getTable();
    }
    
    /**
     * function getUser
     *
     */
    protected static function getUser()
    {
        return getLoginUser();
    }
    
    /**
     * function getUserWithKind
     *
     */
    protected static function getUserWithKind()
    {
        $user = self::getUser();
        if(!is_null($user)) {
            $user->kind = self::getUserKind($user);
        }
        return $user;
    }
    
    /**
     * function getUserKind
     *
     */
    protected static function getUserKind($user = null)
    {
        $user = is_null($user) ? self::getUser() : $user;
        return getUserKind( $user );
    }


    /**
     * function __construct
     *
     * Illuminate\Database\Eloquent\Model\Modelのコンストラクタとちゃんと引数合わす必要がある
     *
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        self::$exLog = app('ExtendLog');
        $this->defRecordOperationLogs = app(BASE_APP_ACCESSOR)->get('standard.record_operation_logs');
    }

    /**
     * function exceptionLog
     *
     * 主にトランザクション時のException発生時にログを記録するためのメソッド
     *
     */
    protected function exceptionLog($e)
    {
        self::$exLog->exception($e);
    }

    /**
     * function warningLog
     *
     * Exception以外で処理は続けるもののエラーが発生している際にログ記録するためのメソッド。
     *
     */
    protected function warningLog($msg)
    {
        self::$exLog->write(ExtendLog::WARNING, $msg);
    }

    /**
     * function toggleEnableDone 有効無効をトグル的に切り替える
     *
     * 有効無効をトグル的に切り替える
     *
     * @param integer $id 対象ID
     */
    public function toggleEnableDone( $id )
    {
        $row = $this->select(['id', 'enable'])->findOrFail($id);
        $row->enable = ($row->enable == 1) ? 2 : 1;
        
        return $row->save();
    }
    
    /**
     * function _create 各モデルにてオーバーライドし複数テーブル登録に対応するためのメソッド
     *
     * 新規データ登録。複数テーブルの更新が必要なモデルはこのメソッドをオーバーライドして
     * 複数モデルの登録処理をする
     *
     * @param Array $data 登録する配列
     */
    public function _create(array $data)
    {
        return $this->create($data);
    }
    
    /**
     * function _update 各モデルにてオーバーライドし複数テーブル更新に対応するためのメソッド
     *
     * データ更新。複数テーブルの更新が必要なモデルはこのメソッドをオーバーライドして
     * 複数モデルの更新処理をする
     *
     * @param Integer $id 対象データの識別子
     * @param Array   $data 更新する配列
     */
    public function _update($id, array $data)
    {
        // idで直接取得していくので、所属条件は不要。
        $obj = $this->withoutGlobalScopes()->find($id);
//        $obj = $this->prepare()->find($id);
        if(is_null($obj)) {
            return false;
        }
        return $obj->fill($data)->save();
    }
    
    /**
     * function _findOrFail 各モデルにてオーバーライドし複数テーブルからのデータ取得に対応するためのメソッド
     *
     * データ取得。複数テーブルからデータ取得が必要なモデルはこのメソッドをオーバーライドして
     * 複数モデルからデータを取得し、適切に加工したデータを返す
     *
     * @param Integer $id 取得するデータの識別子
     */
    public function _findOrFail($id)
    {
        return $this->prepare()->findOrFail($id);
    }

    /**
     * function when
     *
     * 引数で与えれた値の存在有無を確認し、値が存在すれば、コールバックメソッドを実行する
     *
     * @param QueryBuilder    $query
     * @param Array or string $value   [配列, キー名]となっている配列 配列[キー名]が存在し空でないことをチェックする
     *                                 string　で指定された場合はhそのままその値が存在し、空でないことをチェックする
     * @param callable        $closure
     *
     * @return QueryBuilder $query
     */
    protected function when( $query, $value, $closure )
    {
        $val = $value;
        if(is_array($value)) {
            $val = isset($value[0][$value[1]]) ? $value[0][$value[1]] : '';
        }
        
        // 0での検索はあり、空の場合は検索条件に含める必要なし
        if( isset($val) && $val !== '' ) {
            if( is_callable($closure) ) {
                return $closure($query, $val);
            }
        }
        return $query;
    }
    
    /**
     * function setPeriodCondition
     *
     *
     */
    protected function setPeriodCondition($query, $condition, $column = 'created_at')
    {
        $query = $this->when($query, [$condition, 'period_start'], function($query, $value) use($column) {
            return $query->where($column, '>=', $value);
        });
        $query = $this->when($query, [$condition, 'period_end'], function($query, $value) use($column) {
            return $query->where($column, '<', $value);
        });
        
        return $query;
   }
    
    /**
     * function prepare
     *
     * クエリに対する事前準備を行う
     * 例） $query->select(['id', 'name']);
     *      $query->join('xxxxxx');
     *
     * @param  QueryBuilder $query
     * @return QueryBuilder $query
     *
     */
    public function prepare( $query = null )
    {
        if( is_null($query) ) {
            $query = $this->query();
        }
        return $query;
    }
    
    /**
     * function setCondition
     *
     * 検索条件を設定する　継承先で中身を実装すること
     * whenはBaseModelで定義している。引数で与えた値が空でなければ、コールバックメソッド部分が実行される
     *
     * $conditionは
     *               ['id' => '3', 'free_word' => 'hogehoge'] のような形が原則だが検索フォームに名前を付けた場合は
     *               [ 'xxxx' => ['id' => '3', 'free_word' => 'hogehoge'] ] という形になり、この場合
     *               [
     *                  'xxxx' => ['id' => '3', 'free_word' => 'hogehoge'],
     *                  'yyyy' => ['id' => '3', 'free_word' => 'hogehoge'],
     *               ]
     *               のように複数の条件が入っている場合も想定される
     *
     * @param  QueryBuilder $query
     * @param  Array        $condition
     * @return QueryBuilder $query
     *
     */
    public function setCondition($query, $condition)
    {
        return $query;
    }

    /**
     * function setOrder
     *
     * オーダー条件を設定する
     *
     * $conditionは
     *               [
     *                   'order'     => 'xxxxx',
     *                   'condition' => [
     *                       'id' => 'asc', 'name' => 'none',
     *                   ],
     *               ] のような形が原則だが検索フォームに名前を付けた場合は
     *               [ 'xxxxx' => 
     *                   [
     *                       'order'     => 'xxxxx',
     *                       'condition' => [
     *                           'id' => 'asc', 'name' => 'none',
     *                       ],
     *                   ],
     *                   'yyyyy' => [
     *                       'order'     => 'xxxxx',
     *                       'condition' => [
     *                           'id' => 'asc', 'name' => 'none',
     *                       ],
     *                   ],
     *               ]
     *               のように名前と紐づいて管理され、複数の条件が入っている場合も想定される
     *
     * また、$condition['order']は並び替え条件対象のカラム名が「|」区切りでつながった文字列になっています
     *
     * @param  QueryBuilder $query
     * @param  Array        $condition
     * @return QueryBuilder $query
     *
     */
    public function setOrder($query, $condition)
    {
        // TODO カラム名部分中心にもう一ひねり工夫を入れる
        if( isset($condition['order']) && !empty($condition['order']) ) {
            $order = explode('|', $condition['order']);
            
            foreach($order as $odr) {
                if(isset($condition['condition'][$odr])) {
                    $column = $odr;
                    $type   = $condition['condition'][$odr];
                    if( $type != 'none' ) {
                        $type = $type == 'asc' ? 'asc' : 'desc';
                        $query = $query->orderBy($column, $type);
                    }
                }
            }
        } else {
            $query = $query->orderBy('id', 'desc');
        }
    
        return $query;
    }
    
    /**
     * function getFileIdName
     *
     * 一時ファイル名のデータを更新し、設定した正式ファイル名を返す
     * 実際の処理はFileInfo::updateFileNameFromTempで行っている
     * BaseModelでこのメソッドを持つことで各モデルが$this->によりアクセスできるようになる
     *
     * @param  string $prefix   正式なファイル名につけるプレフィックス
     * @param  number $belongId アップロードファイルが紐づけられているレコードのID
     * @param  string $name     現行のファイル名
     *
     * @return string $idName or $name 新しいファイル名もしくは現行のファイル名
     *                                 一時ファイルでなければそのまま返す
     *
     */
    protected function getFileIdName($prefix, $belongId, $name)
    {
        $fileInfo = new FileInfo();
        return $fileInfo->updateFileNameFromTemp($prefix, $belongId, $name);
    }
    
    /**
     * function deleteFileData
     *
     * fileinfoテーブルから指定ファイルを削除する
     * ファイル削除は正常に終了できなくてもシステムに影響は少ないので無視できるようにする
     *
     */
    protected static function deleteFileData($idName)
    {
        $ret = true;

        try {
            $ret = deleteUploadedFile($idName);
        } catch(\Exception $e) {
            $ret = false;
            $this->exceptionLog($e);
        }
        return $ret;
    }
}
