<?php
namespace Azuki\App\Models;

/**
 * フロント側のアカウント管理のモデル
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Models\AbstractAccountable as Model;
use Azuki\App\Notifications\ResetPasswordNotification;
use Azuki\App\Models\SocialIdentity;

use Azuki\App\Contracts\Models\Users as ContractsInterface;

/**
 * class User
 *
 */
class Users extends Model implements ContractsInterface
{
    /**
     * The database table account by the model.
     *
     * @var string
     */
    protected $table = 'azuki_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'login_id',
        'email',
        'password',
        'role',
        'enable'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    
    /**
     *
     *
     */
    public function socialIdentity()
    {
        return $this->hasMany(SocialIdentity::class, 'users_id', 'id');
    }
    
    /**
     *
     *
     */
    public function getSocialIdentityModel()
    {
        return app(SocialIdentity::class);
    }

    /**
     * function sendPasswordResetNotification
     *
     * パスワードリセットメールを処理するNotificationクラスを呼び出す
     *
     * @param string $token パスワードリセットトークン
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
}
