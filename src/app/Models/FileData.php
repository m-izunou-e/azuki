<?php
namespace Azuki\App\Models;

/**
 * file_dataにアクセスするためのモデルクラス
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Models\BaseHardDeleteModel as Model;

/**
 * class FileData
 *
 */
class FileData extends Model
{
    /**
     * The database table account by the model.
     *
     * @var string
     */
    protected $table = 'azuki_file_data';

    /**
     * $recordOperationLogs
     *
     * オペレーションログの記録の有無を決める
     */
    protected $recordOperationLogs = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'file_info_id',
        'data',
        'number',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
