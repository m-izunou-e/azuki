<?php
namespace Azuki\App\Models;

/**
 * インプットがファイルになる非同期実行処理のキューイング管理のモデル
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Models\BaseSoftDeleteModel as Model;

use Azuki\App\Contracts\Models\ExecuteTargetQueue as ContractsInterfase;

/**
 * class ExecuteTargetQueue
 *
 */
class ExecuteTargetQueue extends Model implements ContractsInterfase
{
    /**
     * The database table account by the model.
     *
     * @var string
     */
    protected $table = 'azuki_execute_target_queue';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'belong',
        'file_info_id',
        'kind',
        'status', // 実行状況。1:未実施。2:実行中。3:実行終了。4:実行時エラーあり。5:異常終了
        'total_line',
        'total_count',
        'success_count',
        'failure_count',
        'message',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * function scopeBelong
     *
     * 所属が有効な際に自分と同じ所属のデータしか閲覧できないように制限するフィルター
     */
    public function scopeBelong($query)
    {
        if( restrictByBelong() ) {
            $belong = getLoginUserBelong();
            $query->where('belong', '=', $belong);
        }
    }
    
    /**
     * function prepare
     *
     * 全てのリクエストに所属制限のフィルターを摘要する
     *
     */
    public function prepare($query = null, $scope = true)
    {
        $query = parent::prepare($query);
        if($scope) {
            $query = $query->belong();
        }
        return $query;
    }
    
    /**
     * function setCondition
     *
     * 検索条件を設定する
     * whenはBaseModelで定義している。引数で与えた値が空でなければ、コールバックメソッド部分が実行される
     *
     * @param  QueryBuilder $query
     * @param  Array        $condition
     * @return QueryBuilder $query
     *
     */
    public function setCondition($query, $condition)
    {
        $query = parent::setCondition($query, $condition);

        $query = $this->when($query, [$condition, 'belong'], function($query, $value) {
            $value = is_array($value) ? $value : [$value];
            return $query->whereIn('belong', $value);
        });
        $query = $this->when($query, [$condition, 'kind'], function($query, $value) {
            $value = is_array($value) ? $value : [$value];
            return $query->whereIn('kind', $value);
        });
        $query = $this->when($query, [$condition, 'status'], function($query, $value) {
            $value = is_array($value) ? $value : [$value];
            return $query->whereIn('status', $value);
        });
        
        $query = $this->setPeriodCondition($query, $condition, $this->table.'.created_at');

        return $query;
    }
    
    /**
     * function put
     *
     * データを登録する。同じfile_info_idがすでに存在する場合はエラーとする
     * statusは原則１固定。$dataの要素は２つなのでそれぞれを引数にした方が見通しがよいが
     * 将来的に増える可能性の高さを考慮して配列とする
     *
     * @param  array $data 登録するデータ配列。構成は以下の通り。
     *               $data = [
     *                   'fileInfoId' => '対応するfile_info.id',
     *                   'kind'       => '実行タイプ',
     *               ]
     * @param  int   $status 1 or 2
     *
     * @return Eloquent $obj ExecuteTargetQueueオブジェクト or null
     */
    public function put( $data, $status = ETQ_STATUS_NEW )
    {
        $obj = null;
        if( !$this->putValidate($data, $status) ) {
            $msg = sprintf('[ExecuteTargetQueue] put fail because [status is %s and obj is %s]', $status, var_export($obj, true));
            $this->warningLog($msg);
            return $obj;
        }

        $queue = [
            'file_info_id' => $data['fileInfoId'],
            'kind'         => $data['kind'],
            'status'       => $status,
        ];
        if(isset($data['belong'])) {
            $queue['belong'] = $data['belong'];
        }
        
        \DB::beginTransaction();
        try {
            $obj = $this->where('file_info_id', '=', $queue['file_info_id'])->first();
            if( !is_null($obj) ) {
                // 対象のfile_info_idがすでに登録済み
                throw new \Exception('queue already exists file_info_id is ['.$queue['file_info_id'].']');
            }
            $obj = $this->_create($queue);
            
            \DB::commit();
        } catch( \Exception $e ) {
            \DB::rollback();
            $this->exceptionLog($e);
            $obj = null;
        }
        
        return $obj;
    }
    
    /**
     * function putValidate
     *
     * put処理の引数をチェックする
     *
     * @param  array   $data 登録するデータ配列。
     * @param  int     $status 1 or 2
     * @return boolean $ret true or false
     */
    protected function putValidate($data, $status)
    {
        $ret = true;

        $allowStatus = [
            ETQ_STATUS_NEW,
            ETQ_STATUS_RUNNING,
        ];

        if( !in_array( $status, $allowStatus ) ) {
            // statusが１、２以外はエラーとして処理
            $ret = false;
        } elseif( !isset($data['fileInfoId']) or !isset($data['kind']) ) {
            $ret = false;
        }
        
        return $ret;
    }
    
    /**
     * function rem
     *
     * 対象データを１件ステータスを実行中に変更して取り出す
     * status=1 のfirstを取得し、status=2に更新して返す
     *
     * @return Eloquent $obj ExecuteTargetQueueオブジェクト
     *
     */
    public function rem($cond = null)
    {
        \DB::beginTransaction();
        try {
            $sql = $this->where('status', '=', ETQ_STATUS_NEW);
            if( !is_null($cond) ) {
                foreach( $cond as $key => $val ) {
                    if(is_null($val)) {
                        $sql = $sql->whereNull($key);
                    } else {
                        $sql = $sql->whereIn($key, $val);
                    }
                }
            }
            $obj = $sql->first();
            if( !is_null($obj) ) {
                $obj->status = ETQ_STATUS_RUNNING;
                $obj->save();
            }
            
            \DB::commit();
        } catch( \Exception $e ) {
            \DB::rollback();
            $this->exceptionLog($e);
        }
        
        return $obj;
    }
    
    /**
     * function close
     *
     * 正常終了ステータス更新処理
     *
     */
    public function close()
    {
        return $this->closeStatus(ETQ_STATUS_SUCCESS);
    }
    
    /**
     * function errorClose
     *
     * エラー終了ステータス更新処理
     *
     */
    public function errorClose()
    {
        return $this->closeStatus(ETQ_STATUS_ERROR);
    }
    
    /**
     * function excptionClose
     *
     * 異常終了ステータス更新処理
     *
     */
    public function excptionClose()
    {
        return $this->closeStatus(ETQ_STATUS_EXCEPTION);
    }
    
    /**
     * function closeStatus
     *
     * statusを指定された終了ステータスに変更する
     * 許可される終了ステータスならびに、終了ステータスに変更処理可能な
     * 現状ステータスには縛りががある
     *
     * @param  int      $status 3 or 4 or 5
     * @return boolean  $ret    true or false
     *
     */
    protected function closeStatus($status)
    {
        $ret = true;

        $allowStatus = [
            ETQ_STATUS_SUCCESS,
            ETQ_STATUS_ERROR,
            ETQ_STATUS_EXCEPTION,
        ];
        // statusが更新条件に合致しているか確認の上処理する
        if( $this->status == ETQ_STATUS_RUNNING && in_array( $status, $allowStatus ) ) {
            $this->status = $status;
            $this->save();
        } else {
            $msg = sprintf('[ExecuteTargetQueue] close status not updaste because [obj->status is %s and status is %s]', $this->status, $status);
            $this->warningLog($msg);
            $ret = false;
        }
        
        return $ret;
    }
    
    /**
     *
     *
     */
    public function updateData($data)
    {
        return $this->fill($data)->save();
    }
    
}
