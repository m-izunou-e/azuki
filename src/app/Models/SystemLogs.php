<?php
namespace Azuki\App\Models;

/**
 * システムログをDBに記録するためのモデル
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Models\BaseHardDeleteModel as Model;
use Azuki\App\Models\traits\DeleteOldDataTrait;

use Azuki\App\Contracts\Models\SystemLogs as ContractsInterfase;

/**
 * class MailLog
 *
 */
class SystemLogs extends Model implements ContractsInterfase
{
    /**
     * The database table account by the model.
     *
     * @var string
     */
    protected $table = 'azuki_system_logs';

    /**
     * $recordOperationLogs
     *
     * オペレーションログの記録の有無を決める
     */
    protected $recordOperationLogs = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kind',
        'status',
        'message',
        'check_at',
        'restore_at',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * データベースから古いログを一括削除するトレイトを呼び出す
     *
     */
    use DeleteOldDataTrait;
    
    /**
     * function setCondition
     *
     * 検索条件を設定する
     * whenはBaseModelで定義している。引数で与えた値が空でなければ、コールバックメソッド部分が実行される
     *
     * $conditionは
     *               ['id' => '3', 'free_word' => 'hogehoge'] のような形が原則だが検索フォームに名前を付けた場合は
     *               [ 'xxxx' => ['id' => '3', 'free_word' => 'hogehoge'] ] という形になり、この場合
     *               [
     *                  'xxxx' => ['id' => '3', 'free_word' => 'hogehoge'],
     *                  'yyyy' => ['id' => '3', 'free_word' => 'hogehoge'],
     *               ]
     *               のように複数の条件が入っている場合も想定される
     *
     * @param  QueryBuilder $query
     * @param  Array        $condition
     * @return QueryBuilder $query
     *
     */
    public function setCondition($query, $condition)
    {
        $query = parent::setCondition($query, $condition);

        $query = $this->when($query, [$condition, 'kind'], function($query, $value) {
            return $query->whereIn('kind', $value);
        });
        $query = $this->when($query, [$condition, 'status'], function($query, $value) {
            return $query->whereIn('status', $value);
        });
        $query = $this->when($query, [$condition, 'message'], function($query, $value) {
            $where = "message LIKE ?";
            $whereCond = array(
                "%".$value."%",
            );
            return $query->whereRaw($where, $whereCond);
        });

        $query = $this->when($query, [$condition, 'check'], function($query, $value) {
            if($value == 1) {
                return $query->whereNotNull('check_at');
            } else {
                return $query->whereNull('check_at');
            }
        });
        $query = $this->when($query, [$condition, 'restore'], function($query, $value) {
            if($value == 1) {
                return $query->whereNotNull('restore_at');
            } else {
                return $query->whereNull('restore_at');
            }
        });
        
        $query = $this->setPeriodCondition($query, $condition, $this->table.'.created_at');

        return $query;
    }
    
    /**
     *
     *
     *
     */
    public function existsNoCheckFault($kind = null)
    {
        $sql = $this->getFaultSql($kind, true);
        $count = $sql->count();

        return $count > 0 ? true : false;
    }
    
    /**
     *
     *
     *
     */
    public function existsFault($kind = null)
    {
        $sql = $this->getFaultSql($kind);
        $count = $sql->count();

        return $count > 0 ? true : false;
    }
    
    /**
     *
     *
     */
    protected function getFaultSql($kind = null, $noCheckOnly = false)
    {
        $sql = $this
            ->where('status', '=', SL_STATUS_FAULT)
            ->whereNull('restore_at');

        if(!is_null($kind)) {
            if( is_array($kind) ) {
                $sql = $sql->whereIn('kind', $kind);
            } else {
                $sql = $sql->where('kind', '=', $kind);
            }
        }
        
        if($noCheckOnly) {
            $sql = $sql->whereNull('check_at');
        }
        
        return $sql;
    }
    
    /**
     *
     *
     *
     */
    public function insertInfoLog( $kind, $message )
    {
        $this->insertLog($kind, SL_STATUS_INFO, $message);
    }
    
    /**
     *
     *
     *
     */
    public function insertWarningLog( $kind, $message )
    {
        $this->insertLog($kind, SL_STATUS_ALERT, $message);
    }
    
    /**
     *
     *
     *
     */
    public function insertExceptionLog( $kind, $message )
    {
        $this->insertLog($kind, SL_STATUS_FAULT, $message);
    }
    
    /**
     *
     *
     *
     */
    protected function insertLog($kind, $status, $message)
    {
        $data = [
            'kind'    => $kind,
            'status'  => $status,
            'message' => $message,
        ];
        
        $this->_create($data);
    }
    
    /**
     *
     *
     *
     */
    public function restoreFault($kind, $id = null)
    {
        $this->toRestore(SL_STATUS_RESTORE, $kind, $id);
    }
    
    /**
     *
     *
     *
     */
    public function manualRestore($kind, $id = null)
    {
        $this->toRestore(SL_STATUS_MANUAL_RESTORE, $kind, $id);
    }
    
    /**
     *
     *
     */
    protected function toRestore($status, $kind, $id = null)
    {
        if(is_null($kind) && is_null($id)) {
            $this->warningLog('$kind、$id 両方nullが指定されました。リストアには何れか、あるいは両方が必要です。');
            return ;
        }

        $sql = $this
            ->where('status', '=', SL_STATUS_FAULT)
            ->whereNull('restore_at');

        if(!is_null($kind)) {
            $sql = $sql->where('kind', '=', $kind);
        }
        if(!is_null($id)) {
            $sql = $sql->where('id', '=', $id);
        }

        $data = [
            'status'     => $status,
            'restore_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];
        $sql->update($data);
    }
    
    /**
     *
     *
     *
     */
    public function check($id)
    {
        $data = [
            'check_at' => date('Y-m-d H:i:s'),
        ];
        $this->_update($id, $data);
    }
    
    /**
     *
     *
     *
     */
    public function checkAndRestore($id)
    {
        $this->check($id);
        $this->restore(null, $id);
    }
    
    /**
     *
     *
     *
     */
    public function checkAndManualRestore($id)
    {
        $this->check($id);
        $this->manualRestore(null, $id);
    }
    
    /**
     *
     *
     *
     */
    public function getErrors()
    {
        $ret = [];
        $sql = $this->getFaultSql();
        $list = $sql->get();
        
        foreach($list as $row) {
            $ret[] = sprintf(
                'id:%d [%s] : [%s] %s',
                $row->id,
                is_null($row->check_at) ? '未確認' : '確認済',
                getSelectName('systemLogKindLabel', $row->kind),
                $row->message
            );
        }

        return $ret;
    }
    
    /**
     *
     *
     *
     */
    public function getInformations()
    {
        $condition = [
            'status' => [SL_STATUS_INFO, SL_STATUS_RESTORE, SL_STATUS_ALERT],
        ];
    
        $sql = $this->prepare();
        $sql = $this
            ->setCondition($sql, $condition)
            ->orderBy('updated_at', 'desc')
            ->orderBy('id', 'desc')
            ->limit(10);
        
        return $sql->get();
    }
}
