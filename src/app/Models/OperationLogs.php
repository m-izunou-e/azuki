<?php
namespace Azuki\App\Models;

/**
 * データベースへのデータ登録・更新・削除ログをDBに記録するためのモデル
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Models\BaseHardDeleteModel as Model;
use Azuki\App\Models\traits\AppendUserInfoTrait;
use Azuki\App\Models\traits\DeleteOldDataTrait;

use Azuki\App\Contracts\Models\OperationLogs as ContractsInterfase;

/**
 * class OperationLogs
 *
 */
class OperationLogs extends Model implements ContractsInterfase
{
    /**
     * The database table account by the model.
     *
     * @var string
     */
    protected $table = 'azuki_operation_logs';

    /**
     * $recordOperationLogs
     *
     * オペレーションログの記録の有無を決める
     */
    protected $recordOperationLogs = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'table',
        'target_id',
        'user_id',
        'user_kind',
        'user_info',
        'op_mode',
        'original_data',
        'dirty_data',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * user_id user_kindに紐づくユーザー情報を追加処理するトレイトを呼び出す
     *
     */
    use AppendUserInfoTrait;
    
    /**
     * データベースから古いログを一括削除するトレイトを呼び出す
     *
     */
    use DeleteOldDataTrait;
    
    
    /**
     * function setCondition
     *
     */
    public function setCondition($query, $condition)
    {
        $query = $this->when($query, [$condition, 'table'], function($query, $value) {
            $where = '(`'.$this->table.'`.`table` LIKE ?)';
            $whereCond = array(
                "%".$value."%",
            );
            return $query->whereRaw($where, $whereCond);
        });
        $query = $this->when($query, [$condition, 'user_kind'], function($query, $value) {
            return $query->whereIn($this->table.'.user_kind', $value);
        });
        $query = $this->when($query, [$condition, 'op_mode'], function($query, $value) {
            return $query->whereIn($this->table.'.op_mode', $value);
        });
        
        $query = $this->setPeriodCondition($query, $condition, $this->table.'.created_at');

        return $query;
    }
}
