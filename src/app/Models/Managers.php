<?php
namespace Azuki\App\Models;

/**
 * 管理画面用のアカウント管理のモデル
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Illuminate\Database\Eloquent\Builder;
use Azuki\App\Models\AbstractAccountable as Model;
use Azuki\App\Notifications\ResetPasswordForManageNotification;

use Azuki\App\Contracts\Models\Managers as ContractsInterfase;
/**
 * class Manager
 *
 */
class Managers extends Model implements ContractsInterfase
{
    /**
     * The database table account by the model.
     *
     * @var string
     */
    protected $table = 'azuki_managers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'login_id',
        'email',
        'password',
        'role',
        'enable',
        'belong',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    
    /**
     * function scopeBelong
     *
     * 所属が有効な際に自分と同じ所属のデータしか閲覧できないように制限するフィルター
     */
    public function scopeBelong($query)
    {
        if( restrictByBelong() ) {
            $belong = getLoginUserBelong();
            $query->where('belong', '=', $belong);
        }
    }
    
    /**
     * function prepare
     *
     * 全てのリクエストに所属制限のフィルターを摘要する
     *
     */
    public function prepare($query = null, $scope = true)
    {
        $query = parent::prepare($query);
        if($scope) {
            $query = $query->belong();
        }
        return $query;
    }
    
    /**
     * function sendPasswordResetNotification
     *
     * パスワードリセットメールを処理するNotificationクラスを呼び出す
     *
     * @param string $token パスワードリセットトークン
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordForManageNotification($token));
    }
}
