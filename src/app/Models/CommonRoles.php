<?php
namespace Azuki\App\Models;

/**
 * CommonRolesにアクセスするためのモデルクラス
 *
 * @copyright  Copyright 2019 --- Project.
 * @author        Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Models\BaseRolesModel as Model;
use Azuki\App\Models\CommonRoleAuthorities;
use Azuki\App\Contracts\Models\CommonRoles as ContractsInterfase;

/**
 * class CommonRoles
 *
 */
class CommonRoles extends Model implements ContractsInterfase
{
    /**
     * The database table account by the model.
     *
     * @var  string
     */
    protected $table = 'azuki_common_roles';
    
    /**
     *
     */
    protected $rolesIdAttribute = 'common_roles_id';
    
    /**
     *
     *
     */
    public function roleAuthorities()
    {
        return $this->hasMany('Azuki\App\Models\CommonRoleAuthorities', 'common_roles_id', 'id');
    }
    
    /**
     *
     *
     *
     */
    protected function getRoleAuthoritiesModel()
    {
        return app(CommonRoleAuthorities::class);
    }
}
