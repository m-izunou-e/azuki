<?php
namespace Azuki\App\Models\Interfaces;


interface MasterModelInterface
{
    public function getMasterList($config = null);
    public function formatForSelectList($list);
}
