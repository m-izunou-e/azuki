<?php
namespace Azuki\App\Models;

/**
 * 所属情報が付加されるモデルの基本クラス
 * 所属テーブルに所属に関する情報登録を強制し
 * 所属によるデータアクセスの制限処理も強制する
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Models\BaseSoftDeleteModel as Model;
use Azuki\App\Models\Scopes\BelongScope;

/**
 * class AbstractAccountable
 *
 */
abstract class AbstractBelongs extends Model
{
    /**
     * 所属値
     * セットされなければ必要時にログインユーザーの所属値が使われる
     *
     */
    protected static $belong = '';
    
    /**
     * function boot
     *
     * boot処理を追加する
     * boot時にdelete処理のフックメソッドの登録などを行うことができる
     *
     */
    protected static function boot() {
        parent::boot();

        // データ新規作成時に所属テーブルに情報を追記する
        // データのIDが必要になるのでcreatingではなくcreatedで行う
        // ただし、この時すでにデータ自体が登録されているので処理失敗時にデータを削除する必要がある
        // →create処理の方でtransactoinをかけtry-catchで囲みexceptionを投げることで登録を阻止する
        self::created(function($obj) {
            $ret = true;
            if(!supportOrganizations()) {
                return $ret;
            }
            
            $belongModel = new BelongsToOrganizations();
            try {
                $belongValue = $obj->getBelongValue();
                $belongs = [
                    'target_table'   => $obj->getTable(),
                    'belong'         => $belongValue,
                    'target_data_id' => $obj->id,
                ];
                $belongModel->_create($belongs);
                
            } catch(\Exception $e) {
                $ret = false;
                $obj->exceptionLog($e);
                throw $e;
            }
            
            return $ret;
        });
        
        // データ取得に対して所属による制限をかける
        // belongカラムに対してログインユーザーのbelong値でwhereを強制。
        static::addGlobalScope(new BelongScope);
    }
    
    /**
     * function setBelongValue
     *
     * 所属値を取得するメソッド
     * belongプロパティが設定されていなければログインユーザー値を使う
     */
    public function setBelongValue($belong, $force = false)
    {
        // 強制セットではない場合、ログインユーザーの所属が取得できる場合は所属のセットはさせない
        if(!$force && !is_null(getLoginUserBelong(null)) ) {
            return;
        }
    
        $obj = new Organizations();
        $organization = $obj->where('value', '=', $belong)->first();
        if( is_null($organization) ) {
            return false;
        }
        self::$belong = $belong;
    }
    
    /**
     * function getBelongValue
     *
     * 所属値を取得するメソッド
     * belongプロパティが設定されていなければログインユーザー値を使う
     */
    public function getBelongValue()
    {
        if( self::$belong === '' ) {
            self::$belong = getLoginUserBelong(null);
        }
        return self::$belong;
    }
    
    /**
     * function prepare
     *
     * クエリに対する事前準備を行う
     * 例） $query->select(['id', 'name']);
     *      $query->join('xxxxxx');
     *
     * @param  QueryBuilder $query
     * @return QueryBuilder $query
     *
     */
    public function prepare( $query = null )
    {
        $query = parent::prepare($query);
        
        // 所属情報を追加する
        // 元テーブルにbelongカラムが存在しないようにする必要がある
        $belongsTable = 'azuki_belongs_to_organizations';
        $query->select([
            $this->table.'.*',
            $belongsTable.'.belong',
        ])->leftJoin($belongsTable, function($query) use($belongsTable){
            $query
                ->on($this->table.'.id', '=', $belongsTable.'.target_data_id')
                ->where($belongsTable.'.target_table', '=', $this->table);
        });
        
        return $query;
    }
    
    /**
     * function _create
     *
     * @param Array $data 登録する配列
     */
    public function _create(array $data)
    {
        // 作成処理に対してtransactionをかける
        // 所属情報登録に失敗した際にデータ登録しないようにするための処理
        \DB::beginTransaction();
        try {
            $ret = parent::_create($data);
            \DB::commit();
        } catch(\Exception $e) {
            \DB::rollback();
            $this->exceptionLog($e);

            $ret = false;
        }

        return $ret;
    }
}
