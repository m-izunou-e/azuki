<?php
namespace Azuki\App\Models;

/**
 * アカウント管理モデルの基本クラス
 * パスワードリマインダー、リセットなど、アカウント管理を行うモデルに必要な
 * 共通処理部分を実装する
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Models\BaseSoftDeleteModel as Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

/**
 * class AbstractAccountable
 *
 */
abstract class AbstractAccountable extends Model implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Notifiable;
    use Authenticatable, Authorizable, CanResetPassword;
    
    /**
     * function _create
     *
     * パスワードの暗号化をする処理が必要なのでオーバーライド
     *
     * @param Array $data 登録する配列
     */
    public function _create(array $data)
    {
        if( isset($data['password']) && !empty($data['password']) ) {
            $data['password'] = bcrypt($data['password']);
        }
    
        return parent::_create($data);
    }
    
    /**
     * function _update
     *
     * パスワードの暗号化をする処理が必要なのでオーバーライド
     *
     * @param Integer $id 対象データの識別子
     * @param Array   $data 更新する配列
     */
    public function _update($id, array $data)
    {
        if( isset($data['password']) && !empty($data['password']) ) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }

        // TODO 一般ユーザーは自分の情報以外更新できない制限だけど
        //      この場所でいいのかなど踏まえ要検討
        $loginUser = app('request')->user();
        if( get_class($loginUser) == "Azuki\App\Models\Users" ) {
            if( !$this->find($id)->can('_update', $loginUser) ) {
                return false;
            }
        }

        return parent::_update($id, $data);
    }
    
    /**
     * function setCondition
     *
     * 検索条件を設定する
     * whenはBaseModelで定義している。引数で与えた値が空でなければ、コールバックメソッド部分が実行される
     *
     * $conditionは
     *               ['id' => '3', 'free_word' => 'hogehoge'] のような形が原則だが検索フォームに名前を付けた場合は
     *               [ 'xxxx' => ['id' => '3', 'free_word' => 'hogehoge'] ] という形になり、この場合
     *               [
     *                  'xxxx' => ['id' => '3', 'free_word' => 'hogehoge'],
     *                  'yyyy' => ['id' => '3', 'free_word' => 'hogehoge'],
     *               ]
     *               のように複数の条件が入っている場合も想定される
     *
     * @param  QueryBuilder $query
     * @param  Array        $condition
     * @return QueryBuilder $query
     *
     */
    public function setCondition($query, $condition)
    {
        $query = parent::setCondition($query, $condition);

        $query = $this->when($query, [$condition, 'id'], function($query, $value) {
            return $query->where('id', $value);
        });
        $query = $this->when($query, [$condition, 'free'], function($query, $value) {
            $where = "(name LIKE ? or email LIKE ?)";
            $whereCond = array(
                "%".$value."%",
                "%".$value."%",
            );
            return $query->whereRaw($where, $whereCond);
        });
        $query = $this->when($query, [$condition, 'name'], function($query, $value) {
            $where = "name LIKE ?";
            $whereCond = array(
                "%".$value."%",
            );
            return $query->whereRaw($where, $whereCond);
        });
        $query = $this->when($query, [$condition, 'email'], function($query, $value) {
            $where = "email LIKE ?";
            $whereCond = array(
                "%".$value."%",
            );
            return $query->whereRaw($where, $whereCond);
        });
        $query = $this->when($query, [$condition, 'login_id'], function($query, $value) {
            $where = "login_id LIKE ?";
            $whereCond = array(
                "%".$value."%",
            );
            return $query->whereRaw($where, $whereCond);
        });
        $query = $this->when($query, [$condition, 'role'], function($query, $value) {
            return $query->whereIn('role', $value);
        });
        $query = $this->when($query, [$condition, 'enable'], function($query, $value) {
            return $query->whereIn('enable', $value);
        });
        $query = $this->when($query, [$condition, 'free_word'], function($query, $value) {
            $where = "(name LIKE ? OR email LIKE ?)";
            $whereCond = array(
                "%".$value."%",
                "%".$value."%",
            );
            return $query->whereRaw($where, $whereCond);
        });

        return $query;
    }
    
    /**
     * パスワードリセットメール送信時に発生させるNotificationを設定する
     * CanResetPassword で定義されているメソッド。少なくとも日本語環境では
     * そのまま使用するには残念な状態になるので継承先で上書き実装することを強制
     */
    abstract public function sendPasswordResetNotification($token);
}
