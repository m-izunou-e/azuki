<?php
namespace Azuki\App\Models;

/**
 * 各種モデルの基底クラス(ハードデリートのモデル)
 * モデルで共通の処理はこのクラスに記載する
 *
 * @copyright Copyright 2018 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Models\BaseModel as Model;

/**
 * class
 *
 */
abstract class BaseHardDeleteModel extends Model
{
}
