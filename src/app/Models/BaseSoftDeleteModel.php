<?php
namespace Azuki\App\Models;

/**
 * 各種モデルの基底クラス(ソフトデリートのモデル)
 * モデルで共通の処理はこのクラスに記載する
 *
 * @copyright Copyright 2018 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Models\BaseModel as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * class
 *
 */
abstract class BaseSoftDeleteModel extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
