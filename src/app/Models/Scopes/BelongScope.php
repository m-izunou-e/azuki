<?php

namespace Azuki\App\Models\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class BelongScope implements Scope
{
    /**
     * Eloquentクエリビルダへ適用するスコープ
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        if( restrictByBelong() ) {
            if(method_exists($model, 'getBelongValue')) {
                $belong = $model->getBelongValue();
            } else {
                $belong = getLoginUserBelong();
            }
            $builder->where('belong', '=', $belong);
        }
    }
}
