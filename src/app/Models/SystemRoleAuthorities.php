<?php
namespace Azuki\App\Models;

/**
 * システム管理ユーザーの権限管理をするモデルクラス
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Models\BaseSoftDeleteModel as Model;

//use Azuki\App\Contracts\Models\SystemRoleAuthorities as ContractsInterfase;

/**
 * class Manager
 *
 */
class SystemRoleAuthorities extends Model /*implements ContractsInterfase*/
{
    /**
     * The database table account by the model.
     *
     * @var  string
     */
    protected $table = 'azuki_system_role_authorities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'system_roles_id',
        'url',
        'authority',
    ];
}
