<?php
namespace Azuki\App\Models;

/**
 * メールの送信ログをDBに記録するためのモデル
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Models\BaseSoftDeleteModel as Model;

/**
 * class MailLog
 *
 */
class MailLog extends Model
{
    /**
     * The database table account by the model.
     *
     * @var string
     */
    protected $table = 'azuki_mail_logs';

    /**
     * $recordOperationLogs
     *
     * オペレーションログの記録の有無を決める
     */
    protected $recordOperationLogs = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'messageId',
        'headers',
        'date',
        'subject',
        'from',
        'return_path',
        'reply_to',
        'to',
        'cc',
        'bcc',
        'sender',
        'body',
        'result',
        'failures',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * function saveResult
     *
     * メール送信結果ログを記録
     * messageIdで送信ログを検索し、結果を追記する処理
     * 該当する送信ログがなかった場合の処理は未実装。。Warningログ残すくらいは必要かも。
     *
     * @param string $messageId メッセージID
     * @param array  $result    結果配列　['result' => '数値', 'failures' => 'シリアライズされたデータ']
     */
    public function saveResult( $messageId, $result )
    {
        $target = $this->where('messageId', '=', $messageId)->first();
        if( !empty($target) ) {
            $target->update($result);
        }
    }
}
