<?php
namespace Azuki\App\Models;

/**
 * ロールモデルの基本クラス
 *
 * @copyright  Copyright 2019 --- Project.
 * @author        Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Models\BaseMasterModel as Model;

/**
 * class Directors
 *
 */
abstract class BaseRolesModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'value',
        'order',
        'deletable',
    ];

    /**
     * 常にロードするリレーション
     *
     * @var array
     */
    protected $with = ['roleAuthorities'];
    
    /**
     *
     *
     *
     */
    public function getAuthorities()
    {
        return $this->roleAuthorities;
    }

    /**
     * function boot
     *
     * boot処理を追加する
     * boot時にdelete処理のフックメソッドの登録などを行うことができる
     *
     */
    protected static function boot() {
        parent::boot();

        self::deleting(function($obj) {
            // 紐づく権限設定を削除する。削除に成功したら本体も削除
            $ret = true;
            if($obj->deletable == 1) {
                // 削除不可設定のレコードは削除させない
                return false;
            }
            
            \DB::beginTransaction();
            try {
                foreach($obj->getAuthorities() as $row) {
                    $row->delete();
                }
                \DB::commit();
            } catch(\Exception $e) {
                $ret = false;
                \DB::rollback();
                $obj->exceptionLog($e);
            }
            
            return $ret;
        });
    }
    
    /**
     * function _create 各モデルにてオーバーライドし複数テーブル登録に対応するためのメソッド
     *
     * @param Array $data 登録する配列
     */
    public function _create(array $data)
    {
        $ret = null;

        \DB::beginTransaction();
        try {
            $ret = parent::_create($data);
            $id = $ret->id;
            $this->createAndUpdateRoleAuthorities($id, $data);
            \DB::commit();
        } catch(\Exception $e) {
            $ret = null;
            \DB::rollback();
            $this->exceptionLog($e);
        }
        
        return $ret;
    }
    
    /**
     * function _update 各モデルにてオーバーライドし複数テーブル更新に対応するためのメソッド
     *
     * @param Integer $id 対象データの識別子
     * @param Array   $data 更新する配列
     */
    public function _update($id, array $data)
    {
        $ret = false;
    
        \DB::beginTransaction();
        try {
            $ret = parent::_update($id, $data);
            $this->createAndUpdateRoleAuthorities($id, $data);
            \DB::commit();
        } catch(\Exception $e) {
            $ret = false;
            \DB::rollback();
            $this->exceptionLog($e);
        }
        return $ret;
    }
    
    /**
     *
     *
     */
    protected function createAndUpdateRoleAuthorities($id, $data)
    {
        $ret = true;
        $rolesIdAttribute = $this->rolesIdAttribute;
    
        // 現在の登録情報のidを取得する
        $obj = $this->getRoleAuthoritiesModel();
        $nowAuthorities = $obj->select(['id'])->where($rolesIdAttribute, '=', $id)->get();
        
        if(!isset($data['role_authorities']) || !is_array($data['role_authorities'])) {
            return $ret;
        }
        // system_role_authoritiesについてforeachして処理をする
        $ids = [];
        foreach($data['role_authorities']  as $authority) {
            $authority[$rolesIdAttribute] = $id;
            // idがあれば現在のデータを取得して更新処理
            if( isset($authority['id']) && !empty($authority['id']) ) {
                $ids[] = $authority['id'];
                $auth = $obj->find($authority['id']);
                if(!is_null($auth)) {
                    $ret = $obj->_update($authority['id'], $authority);
                } else {
                    // TODO ここはおかしいのだけど、新規登録扱いにするか要検討
                }
            } else {
                // idがなければ新規登録処理
                $ret = $obj->_create($authority);
            }
        }
        
        // idがなくなっているものは削除されたものなので、削除処理を行う
        foreach($nowAuthorities as $row) {
            if(!in_array($row->id, $ids)) {
                $row->delete();
            }
        }
        
        return $ret;
    }
    
    /**
     *
     *
     *
     */
    abstract protected function getRoleAuthoritiesModel();
}
