<?php
namespace Azuki\App\Models;

/**
 * マスターデータを管理するテーブルの基底モデルクラス
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Models\BaseSoftDeleteModel as Model;
use Azuki\App\Models\Interfaces\MasterModelInterface;
use Azuki\App\Models\traits\MasterModelTrait;

/**
 * class Manager
 *
 */
abstract class BaseMasterModel extends Model implements MasterModelInterface
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'value',
        'order',
    ];

    /**
     *
     *
     */
    protected $valueKey = 'value';

    /**
     *
     *
     */
    protected $nameKey = 'name';

    /**
     *
     *
     */
    protected $orderKey = 'order';
    
    use MasterModelTrait;
}
