<?php
namespace Azuki\App\Models;

/**
 * directorsにアクセスするためのモデルクラス
 *
 * @copyright  Copyright 2019 --- Project.
 * @author        Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Models\BaseMasterModel as Model;

/**
 * class Directors
 *
 */
class BelongsToOrganizations extends Model
{
    /**
     * The database table account by the model.
     *
     * @var  string
     */
    protected $table = 'azuki_belongs_to_organizations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'target_table',
        'belong',
        'target_data_id',
    ];
}
