<?php
namespace Azuki\App\Models;

/**
 * social_identityにアクセスするためのモデルクラス
 *
 * @copyright  Copyright 2021 --- Project.
 * @author        Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Models\BaseSoftDeleteModel as Model;
use Azuki\App\Models\Users;

use Azuki\App\Contracts\Models\SocialIdentity as ContractsInterfase;

/**
 * class SocialIdentity
 *
 */
class SocialIdentity extends Model implements ContractsInterfase
{
    /**
     * The database table social_identity by the model.
     *
     * @var  string
     */
    protected $table = 'azuki_social_identity';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'users_id',
        'social_id',
        'provider',
        'provider_kind',
    ];
    
    /**
     *
     */
    public function users()
    {
        return $this->belongsTo(Users::class);
    }
    
    /**
     *
     *
     */
    public function getSocialIdentity($id, $prociderKind)
    {
        return $this
            ->where('social_id',     '=', $id)
            ->where('provider_kind', '=', $prociderKind)
            ->first();
    }
}
