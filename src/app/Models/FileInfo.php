<?php
namespace Azuki\App\Models;

/**
 * file_infoにアクセスするためのモデルクラス
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Models\BaseSoftDeleteModel as Model;

/**
 * class FileInfo
 *
 */
class FileInfo extends Model
{
    /**
     * The database table account by the model.
     *
     * @var string
     */
    protected $table = 'azuki_file_info';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_name',
        'ext',
        'category',
        'name',
        'path',
        'mime_type',
        'type',
        'storage',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * function boot
     *
     * boot処理を追加する
     * boot時にdelete処理のフックメソッドの登録などを行うことができる
     *
     */
    protected static function boot() {
        parent::boot();

        self::deleted(function($obj) {
            // 画像削除
            $ret = true;
            try {
                $uploadedFileManager = app('UploadedFileManager');
                $uploadedFileManager->deleteUploadedFile($obj);
            } catch(\Exception $e) {
                $ret = false;
                $obj->exceptionLog($e);
            }
            
            return $ret;
        });
    }
    
    /**
     * function updateFileNameFromTemp
     *
     * 一時ファイル名を正式なフィル名に変更するメソッド
     * 必要であればキャッシュの生成も行う
     * アップロードファイルを扱う登録処理時に必ず呼び出す必要がある
     * →BaseModel経由で呼び出す
     *
     * @param  string $prefix   正式なファイル名につけるプレフィックス
     * @param  number $belongId アップロードファイルが紐づけられているレコードのID
     * @param  string $name     現行のファイル名
     *
     * @return string $idName or $name 新しいファイル名もしくは現行のファイル名
     *                                 一時ファイルでなければそのまま返す
     */
    public function updateFileNameFromTemp($prefix, $belongId, $name)
    {
        // 一時ファイルのファイル名でなければ何もしない
        if(!preg_match('/^'.UPLOAD_TEMP_FILE_PREFIX.'.*/', $name, $matches)) {
            return $name;
        }
        
        $fileInfo = $this->where('id_name', '=', $name)->get();
        if( count($fileInfo) <= 0 ) {
            // ファイルが存在していないエラー。異常系なのでException
            throw new \Exception('not exists file:'.$name);
        }
        $uploader = app('UploadedFileManager');
        $newName = null;
        foreach($fileInfo as $info) {
            if(is_null($newName)) {
                $newName = str_replace(UPLOAD_TEMP_FILE_PREFIX, $prefix.'_', $info->id_name);
            }
            $info->id_name = $newName;
            $info->save();
            // キャッシュの作成処理を行う
            $uploader->createCacheFile($info);
        }
        
        return $newName;
    }
}
