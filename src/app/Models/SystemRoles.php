<?php
namespace Azuki\App\Models;

/**
 * SystemRolesにアクセスするためのモデルクラス
 *
 * @copyright  Copyright 2019 --- Project.
 * @author        Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Models\BaseRolesModel as Model;
use Azuki\App\Models\SystemRoleAuthorities;
//use Azuki\App\Contracts\Models\SystemRoleAuthorities;
use Azuki\App\Contracts\Models\SystemRoles as ContractsInterfase;

/**
 * class SystemRoles
 *
 */
class SystemRoles extends Model implements ContractsInterfase
{
    /**
     * The database table account by the model.
     *
     * @var  string
     */
    protected $table = 'azuki_system_roles';
    
    /**
     *
     */
    protected $rolesIdAttribute = 'system_roles_id';
    
    /**
     *
     *
     */
    public function roleAuthorities()
    {
//        return $this->hasMany(app(SystemRoleAuthorities::class), 'system_roles_id', 'id');
        return $this->hasMany('Azuki\App\Models\SystemRoleAuthorities', 'system_roles_id', 'id');
    }
    
    /**
     *
     *
     *
     */
    protected function getRoleAuthoritiesModel()
    {
        return app(SystemRoleAuthorities::class);
    }
}
