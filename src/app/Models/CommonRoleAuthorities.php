<?php
namespace Azuki\App\Models;

/**
 * 一般ユーザーの権限管理をするモデルクラス
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Models\BaseSoftDeleteModel as Model;

/**
 * class Manager
 *
 */
class CommonRoleAuthorities extends Model
{
    /**
     * The database table account by the model.
     *
     * @var  string
     */
    protected $table = 'azuki_common_role_authorities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'common_roles_id',
        'url',
        'authority',
    ];
}
