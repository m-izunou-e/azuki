<?php
namespace Azuki\App\Mail;

/**
 * リマインダーメール用のMailableクラス
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

/**
 * class ResetPassword
 * リマインダーメール用のMailableクラス
 *
 */
class ResetPassword extends AbstractMail
{
    /**
     * メール種類名称
     * string mailKind
     */
    private $mailKind = 'resetPassword';

   /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($token, $notifiable)
    {
        parent::__construct($this->mailKind, ['token' => $token]);
        $this->to($notifiable->email);
    }
}
