<?php
namespace Azuki\App\Mail;

/**
 * メール送信処理の基本部分を実装したクラス
 * Mailable形式、mail.phpで設定した内容に基づき、指定された種類のMailableクラスの処理を行う
 * メールの送信は Mail::to('宛先')->send( new Mailbleオブジェクト );
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Illuminate\Mail\Mailable;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * class AbstractMail
 * Azukiで使用するメール送信のためのMailableクラスにて共通の処理を記述するクラス
 *
 */
abstract class AbstractMail extends Mailable
{
    use Queueable, SerializesModels;
    
    /**
     * アサイン対象のデータ
     * publicにすればアサインしなくてもアサインされるが、
     * アサインするデータを柔軟に変更できるようにするためにここではprivateとし、
     * アサイン処理を行うメソッド部分を切り出す形にしている
     * array data
     */
    private $data;

    /**
     * mail.phpで定義された各種メール設定を保持する変数
     * array config
     */
    private $config;

    /**
     * リターンパス privateにして、ゲッター、セッターを用意する
     * string returnPath
     */
    private $returnPath;
    
    /**
     * メールテンプレートのルートパスの設定
     * string mailTemplatePath
     */
    protected $mailTemplatePath = 'azuki::mail_templetes';

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($kind, $data)
    {
        $this->data       = $data;
        $this->returnPath = app(BASE_APP_ACCESSOR)->get('mail.returnPath');

        $config = app(BASE_APP_ACCESSOR)->get('mail.kind');
        
        if( !in_array($kind, array_keys($config)) ) {
            // 設定がないのでExceptionにする
            throw new \Exception("[$kind]のメール設定がありません。");
        }
        $this->config = $config[$kind];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->setWithSymfonyMessage();
        $obj = $this->createMessageObject();
        
        return $this->assignData($obj);
    }
    
    /**
     * function createMessageObject
     *
     * 設定に応じてMailableオブジェクトを構成する
     */
    protected function createMessageObject()
    {
        $tplPath = $this->mailTemplatePath;
        $config = $this->config;
        $view = $config['view'];
        
        $obj = $this->view($tplPath.'.'.$view);
        
        if( isset($config['text']) && !empty($config['text']) ) {
            $obj = $obj->text($tplPath.'.text.'.$config['text']);
        }
        if( isset($config['subject']) && !empty($config['subject']) ) {
            $obj = $obj->subject($config['subject']);
        }
        
        return $obj;
    }
    
    /**
     * function assignData
     *
     * メールデータのアサイン処理をする
     * 基本は$dataをそのままアサイン。ここをオーバーライドして
     * 必要なデータのみアサインする、アサイン名称を変えるなどを行う
     */
    protected function assignData($obj)
    {
        return $obj->with(['data' => $this->data]);
    }
    
    /**
     * function setWithSymfonyMessage
     *
     * withSymfonyMessageの処理を行う
     * リターンパスの設定を行っている
     */
    protected function setWithSymfonyMessage()
    {
        $returnPath = $this->returnPath;
    
        $this->withSymfonyMessage(function($message) use($returnPath){
            $message->returnPath($returnPath);
        });
    }
    
}
