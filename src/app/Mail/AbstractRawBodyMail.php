<?php
namespace Azuki\App\Mail;

/**
 * メール送信処理の基本部分を実装したクラス(本文直接指定)
 * Mailable形式、mail.phpで設定した内容に基づき、指定された種類のMailableクラスの処理を行う
 * メールの送信は Mail::to('宛先')->send( new Mailbleオブジェクト );
 *
 * @copyright Copyright 2018 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

/**
 * class AbstractRawBodyMail
 *
 */
abstract class AbstractRawBodyMail extends AbstractMail
{
    /**
     *
     *
     */
    protected $raw;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject = '', $body = '')
    {
        $this->returnPath = app(BASE_APP_ACCESSOR)->get('mail.returnPath');

        $this->raw     = $body;
        $this->subject = $subject;
    }
    
    /**
     * function createMessageObject
     *
     * 設定に応じてMailableオブジェクトを構成する
     */
    protected function createMessageObject()
    {
        // textViewがセットされているとテンプレート指定の送信として処理が進むため
        unset($this->textView);
        $obj = $this->view(['raw' => $this->buildRawBody()]);

        return $obj;
    }
    
    /**
     * function assignData
     *
     * メールデータのアサイン処理をする
     * 基本は$dataをそのままアサイン。ここをオーバーライドして
     * 必要なデータのみアサインする、アサイン名称を変えるなどを行う
     */
    protected function assignData($obj)
    {
        return $obj;
    }
    
    /**
     * function buildRawBody
     *
     */
    protected function buildRawBody()
    {
        return $this->raw;
    }
}
