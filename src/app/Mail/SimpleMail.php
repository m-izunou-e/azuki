<?php
namespace Azuki\App\Mail;

/**
 * 基本的なメール送信のMailableクラス
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

/**
 * class SimpleMail
 *
 */
class SimpleMail extends AbstractMail
{
   /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($kind, $data)
    {
        parent::__construct($kind, $data);
    }
}
