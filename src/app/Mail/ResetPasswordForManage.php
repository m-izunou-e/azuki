<?php
namespace Azuki\App\Mail;

/**
 * リマインダーメール(管理画面ユーザー用)のMailableクラス
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

/**
 * class ResetPasswordForManage
 * リマインダーメール(管理画面ユーザー用)のMailableクラス
 *
 */
class ResetPasswordForManage extends AbstractMail
{
    /**
     * メール種類名称
     * string mailKind
     */
    private $mailKind = 'resetPasswordForManage';

   /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($token, $notifiable)
    {
        parent::__construct($this->mailKind, ['token' => $token]);
        $this->to($notifiable->email);
    }
}
