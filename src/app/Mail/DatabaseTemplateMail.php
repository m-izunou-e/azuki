<?php
namespace Azuki\App\Mail;

/**
 * 基本的なメール送信のMailableクラス
 *
 * @copyright Copyright 2018 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Models\MailTemplate;

/**
 * class SimpleMail
 *
 */
class DatabaseTemplateMail extends AbstractRawBodyMail
{
    /**
     *
     *
     */
    protected $mailTemplateModel;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($tplId, $data)
    {
        parent::__construct();
        $this->mailTemplateModel = new MailTemplate();
        $this->data    = $data;
        $mailTemplate  = $this->getMailTemplate($tplId);
        $this->tpl     = $mailTemplate->body;
        $this->subject = $mailTemplate->subject;
    }
    
    /**
     * function getMailTemplate
     *
     */
    private function getMailTemplate($tplId)
    {
        $ret = $this->mailTemplateModel->find($tplId);
        
        if( is_null($ret) ) {
            // 指定のテンプレートがDBに存在しない。
            throw new \Exception("id:[$tplId]のメールテンプレートがありません。");
        }
        
        return $ret;
    }
    
    /**
     * function buildRawBody
     *
     */
    protected function buildRawBody()
    {
        $body = $this->tpl;
        $data = $this->data;
        
        foreach($data as $key => $val) {
            $reg = sprintf('/\[#%s#\]/ui', $key);
            $body = preg_replace($reg, $val, $body);
        }

        return $body;
    }
}
