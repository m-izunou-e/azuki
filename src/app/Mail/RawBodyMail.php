<?php
namespace Azuki\App\Mail;

/**
 * 基本的な本文直接指定メールのMailableクラス
 *
 * @copyright Copyright 2018 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

/**
 * class SimpleMail
 *
 */
class RawBodyMail extends AbstractRawBodyMail
{
   /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject = '', $body = '')
    {
        parent::__construct($subject, $body);
    }
    
    /**
     * function setRaw
     *
     * 文字列での本文データをセットするメソッド
     */
    public function setRaw($body)
    {
        $this->raw = $body;
    }
    
    /**
     * function setSubject
     *
     * 文字列での本文データをセットするメソッド
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }
}
