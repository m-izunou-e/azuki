<?php

namespace Azuki\App\Console\Commands;

use Azuki\App\Console\Commands\AbstractCommand as Command;
use Azuki\App\Models\ExecuteTargetQueue;

class ExecuteUploadedFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exec:upload-file';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'regist and update data from uploaded-file';
    
    /**
     *
     *
     */
    protected $maxCount = 10;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        parent::handle();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    protected function execProcess()
    {
        $model = new ExecuteTargetQueue();
        $cond = [
            'kind' => $this->getQueueKinds(),
        ];
        
        $limit = $this->maxCount;
        for( $i = 0; $i <= $limit; $i++ ) {
            $obj = $model->rem($cond);
            if( is_null($obj) ) {
                break;
            }
            
            $executer = app('ExecuterFactory')->getExecuter($obj->kind);
            $result = $executer->exec($obj);
        }
    }
    
    /**
     * function getQueueKinds()
     *
     *
     */
    protected function getQueueKinds()
    {
        return app(BASE_APP_ACCESSOR)->get('execute_type.queue_kind');
    }
}
