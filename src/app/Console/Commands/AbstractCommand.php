<?php
namespace Azuki\App\Console\Commands;

/**
 * コマンドプログラムの規定抽象クラス
 * アドバイザリロックのためのメソッドやログ記録のためのメソッドなど
 * 共通メソッドはここで定義する
 *
 * @copyright Copyright 2018 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Illuminate\Console\Command;
use Azuki\App\Services\ExtendLog;
use Azuki\App\Contracts\Models\SystemSwitch;

/**
 * class
 *
 */
abstract class AbstractCommand extends Command
{
    /**
     * $exLog
     */
    private static $exLog;
    
    /**
     * $pid
     * コマンドのプロセスID
     */
    private $pid;
    
    /**
     * $className
     * コマンドのクラス名
     */
    private $className;
    private $shortClassName;
    
    /**
     *
     * 並行処理をするかどうか
     */
    protected $enableParallel = false;
    
    /**
     *
     * ブロッキングするかどうか
     */
    protected $nonBlock = true;
    
    /**
     *
     *
     */
    protected $myFile;
    
    /**
     * start,endのログを出力するか
     *
     */
    protected $basicLogWrite = true;
    
    /**
     *
     *
     */
    protected $startMessage = '<--コマンドを開始します。';
    
    /**
     *
     *
     */
    protected $endMessage = '-->コマンドを終了しました。';
    
    /**
     *
     *
     */
    protected $parallelMessage = '-->コマンドが並行処理されます。';
    
    /**
     *
     *
     */
    protected $alreadyRunMessage = '-->コマンドはすでに起動しているため終了します。';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ExtendLog $exLog)
    {
        parent::__construct();
        
        $this->myFile = __FILE__;
        self::$exLog = $exLog;
        $this->pid = getmypid();
        $ref = new \ReflectionClass($this);
        $this->className      = $ref->getName();
        $this->shortClassName = $ref->getShortName();
        
        $this->init();
    }
    
    /**
     * 追加の初期化処理を行うメソッド
     * 継承先でコンストラクタ定義をしないで追加の初期化処理ができるように
     * →コンストラクタで自動注入しているのでコンストラクタのオーバーライドの形を
     * 　とるよりこっちの方が継承先のコードが少なくできるため。
     *
     */
    protected function init()
    {
        $this->messageInitialize();
    }
    
    /**
     *
     *
     */
    protected function messageInitialize()
    {
        $thisClass = $this->shortClassName;
        $this->startMessage      = sprintf('コマンド [%s] を開始します', $thisClass);
        $this->endMessage        = sprintf('コマンド [%s] を終了しました', $thisClass);
        $this->parallelMessage   = sprintf('コマンド [%s] が並行処理されます', $thisClass);
        $this->alreadyRunMessage = sprintf('コマンド [%s] はすでに起動しているため終了します', $thisClass);
    }
    
    /**
     * function logWrite
     *
     */
    protected function logWrite($level, $msg)
    {
        self::$exLog->write($level, $this->messageFormat($msg));
    }
    
    /**
     * function systemLog
     *
     * systemLogを記録する
     * 通常のファイルのログもExtendLogクラスにて記録される
     *
     */
    protected function systemLog($kind, $level, $msg)
    {
        self::$exLog->write($level, $this->messageFormat($msg));
        self::$exLog->systemLogWriteDb($kind, $level, $msg);
    }
    
    /**
     * function messageFormat
     *
     * ログメッセージにクラス名を付加してフォーマットする
     *
     */
    protected function messageFormat($msg)
    {
        return sprintf("pid-%s Class [ %s ]\n%s", $this->pid, $this->className, $msg);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(!$this->enableRun()) {
            return $this->disableEnd();
        }
        $this->start();
        if($this->hasOption('runforce') && $this->option('runforce') == 1) {
            $this->logWrite(ExtendLog::INFO, sprintf('[%s] runforceオプションで実行', $this->shortClassName));
        }
        
        try {
            $this->execProcess();
        } catch(\Exception $e) {
            self::$exLog->exception($e);
        }
        
        $this->end();
    }
    
    /**
     *
     *
     */
    protected function enableRun()
    {
        return true;
    }
    
    /**
     *
     *
     */
    protected function disableEnd()
    {
        $this->logWrite(ExtendLog::INFO, sprintf('コマンド [%s] は実行できません', $this->shortClassName));
        return false;
    }
    
    /**
     *
     *
     */
    protected function start()
    {
        // startログを記録する
        if($this->basicLogWrite) $this->logWrite(ExtendLog::INFO, $this->startMessage);
        
        // 実行可能かチェックし実行中であればログを残して終了する
        if( !$this->canLock() ) {
            if(!$this->enableParallel) {
                $this->logWrite(ExtendLog::INFO, $this->alreadyRunMessage);
                exit();
            }
            $this->logWrite(ExtendLog::INFO, $this->parallelMessage);
        }
    }
    
    /**
     *
     *
     */
    protected function end()
    {
        // endログを記録する　end時に必要なクリーンアップ処理があればそれも実行する
        if($this->basicLogWrite) $this->logWrite(ExtendLog::INFO, $this->endMessage);
    }
    
    /**
     * function canLock
     *
     * アドバイザリロックにてロック可能かをチェックする
     * ロックできない場合は終了する必要がある
     * WindowsではLOCK_NBが使用できないので、注意
     *
     */
    protected function canLock()
    {
        static $fp;
        $ret = false;
        $fp = fopen($this->myFile, 'r');
        
        if(!is_resource($fp)) {
            return $ret;
        }
        
        $wouldblock = false;
        if($this->nonBlock) {
            $ret = flock($fp, LOCK_EX|LOCK_NB, $wouldblock);
        } else {
            $ret = flock($fp, LOCK_EX, $wouldblock);
        }
        
        return $ret;
    }
    
    /**
     *
     *
     */
    protected function switchIsRun($kind)
    {
        $model = app(SystemSwitch::class);
        return $model->isRun($kind);
    }
    
    /**
     *
     *
     */
    protected function getShortClassName()
    {
        return $this->shortClassName;
    }

    /**
     *
     *
     */
    abstract protected function execProcess();
}
