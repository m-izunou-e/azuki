<?php
namespace Azuki\App\Console\Commands;
/**
 *
 */

use Azuki\App\Console\Commands\AbstractCommand as Command;
use Azuki\App\Services\ExtendLog;

class DeleteLogsInDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exec:delete-logs-in-database {--runforce=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'delete logs in database, xx years ago.';
    
    /**
     *
     *
     */
    protected $myFile;
    
    /**
     *
     *
     */
    protected $targetLogModels = [
        'Azuki\\App\\Contracts\\Models\\AccessLogs',
        'Azuki\\App\\Contracts\\Models\\SystemLogs',
        'Azuki\\App\\Contracts\\Models\\LoginLogs',
        'Azuki\\App\\Contracts\\Models\\OperationLogs',
    ];
    
    /**
     *
     *
     */
    protected $targetYears = 1;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->myFile = __FILE__;
        parent::handle();
    }

    /**
     * Execute the console command.
     *
     * restoreは
     * mysql -u USER_NAME -p -h HOST_NAME DB_NAME < OUTPUT_FILE_NAME
     * @return mixed
     */
    protected function execProcess()
    {
        foreach($this->targetLogModels as $class) {
            try {
                $model = app($class);
            } catch(\Exception $e) {
                $this->logWrite(ExtendLog::ERROR, $e->getMessage());
                continue;
            }
            $cn = (new \ReflectionClass($model))->getShortName();
            if(is_null($model) || !method_exists($model, 'hardDeleteOldData')) {
                $this->logWrite(ExtendLog::WARNING, sprintf('[%s] は不正なクラス指定です', $cn));
                continue;
            }
            $result = $model->hardDeleteOldData($this->targetYears);
            if(is_numeric($result)) {
                $this->logWrite(ExtendLog::INFO, sprintf('%s-[%s] 件削除しました', $cn, $result));
            } else {
                $this->logWrite(ExtendLog::WARNING, sprintf('[%s] 削除に失敗', $cn));
            }
        }
    }
    
    /**
     *
     *
     */
    protected function enableRun()
    {
        return $this->option('runforce') == 1 ? true : $this->switchIsRun(SWITCH_KIND_AUTO_DELETE_LOGS);
    }
    
    /**
     *
     *
     */
    protected function disableEnd()
    {
        $this->logWrite(ExtendLog::INFO, sprintf('コマンド [%s] はSwitch Off です', $this->getShortClassName()));
        return false;
    }
    
    /**
     * function logWrite
     *
     */
    protected function logWrite($level, $msg)
    {
        $this->systemLog(SL_KIND_DATA_DELETE, $level, $msg);
    }
}
