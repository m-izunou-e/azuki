<?php
namespace Azuki\App\Http\Interfaces;

/**
 * CSVダウンロードを行うコントローラーが実装するインターフェース
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */


/**
 * interface CsvDownloadInterface
 *
 */
interface CsvDownloadInterface
{
    /**
     * CSVダウンロード処理を実装したサービスを返すメソッド
     */
    public function getCsvDownloadService();

    /**
     * CSVダウンロードファイル名を返すメソッド
     */
    public function getCsvDownloadFileName();
}
