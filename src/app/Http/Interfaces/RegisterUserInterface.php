<?php
namespace Azuki\App\Http\Interfaces;

/**
 * ユーザー登録を行うコントローラーが実装するインターフェース
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */


/**
 * interface RegisterUserInterface
 *
 */
interface RegisterUserInterface
{
    /**
     * バリデートルールを返すメソッド
     */
    public function getValidateRules();

    /**
     * バリデートメッセージを返すメソッド
     */
    public function getValidateMessages();

    /**
     * ユーザーを扱うモデルを返すメソッド
     */
    public function getModel();

    /**
     * 登録を行うデータを生成するメソッド
     */
    public function createData(array $data);
}
