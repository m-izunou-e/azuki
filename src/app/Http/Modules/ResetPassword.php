<?php
namespace Azuki\App\Http\Modules;

/**
 * パスワードリセット処理を行うトレイト
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

/**
 * trait ResetPassword
 *
 */
trait ResetPassword
{
    use ResetsPasswords;
    
    /**
     * function getResetPassword
     *
     * リマインダーメールに記載されるパスワードリセットURLを処理するアクション
     *
     */
    public function getResetPassword($token)
    {
        return view($this->getViewPrefix().'reset_password.reset_password',['token' => $token]);
    }
    
    /**
     * function postResetPassword
     *
     * パスワードリセットフォームからのポストを処理するアクション
     *
     */
    public function postResetPassword(Request $request)
    {
        return $this->reset($request);
    }
    
    /**
     * function getCompleteResetPassword
     *
     * パスワードリセット処理完了画面を表示するアクション
     *
     */
    public function getCompleteResetPassword()
    {
        return view($this->getViewPrefix().'reset_password.complete_reset_password');
    }
    
    /**
     * function sendResetResponse
     *
     * パスワードリセット完了時のレスポンスを生成するメソッド
     *
     */
    protected function sendResetResponse($response)
    {
        $prefix = !empty($this->prefix) ? trim($this->prefix, '/') . '.' : '';
        return redirect()->route($prefix.'reset-password.complete-reset-password');
/*
        return redirect($this->redirectPath())
                            ->with('status', trans($response));
*/
    }
}
