<?php
namespace Azuki\App\Http\Modules;

/**
 * フロント画面ユーザー登録処理を行うトレイト
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Azuki\App\Http\Modules\LoginLog;

/**
 * trait RegisterUser
 *
 */
trait RegisterUser
{
    use RegistersUsers;
    use LoginLog;
    
    /**
     * function postRegist
     *
     * ユーザー登録リクエストを処理するアクション
     *
     */
    public function postRegist(Request $request)
    {
        return $this->register($request);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, $this->getValidateRules(), $this->getValidateMessages());
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return $this->getModel()->_create($this->createData($data));
    }
}
