<?php
namespace Azuki\App\Http\Modules;

/**
 * ログインログ処理を行うトレイト
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Models\LoginLogs;

/**
 * trait Authenticate
 *
 */
trait LoginLog
{
    /**
     * function recordLoginLogs
     *
     * ログインログを記録する
     */
    protected function recordLoginLogs($user, $kind)
    {
        $recordLoginLogs = app(BASE_APP_ACCESSOR)->get('standard.record_login_logs');
        if(!$recordLoginLogs) {
            return;
        }
    
        $userInfo = is_null($user) ? null : $user->toArray();
        $userInfo = is_null($userInfo) ? null : serialize($userInfo);
        $login = [
            'user_id'   => is_null($user) ? null : $user->id,
            'user_kind' => $kind,
            'user_info' => $userInfo,
        ];
        
        $obj = new LoginLogs();
        $obj->_create($login);
    }
}
