<?php
namespace Azuki\App\Http\Modules;

trait CtrlSupporterParameterHelper
{
    /**
     * function getCtrlSuppoterParameters
     *
     *
     */
    protected function getCtrlSuppoterParameters($model, $validator)
    {
        return [
            'templatePrefix' => $this->getCtrlSuppoterTemplatePrefix(),
            'templates'      => $this->getCtrlSuppoterTemplates(),
            'model'          => $model,
            'validator'      => $validator,
            'order'          => $this->getCtrlSuppoterOrder(),
            'views'          => $this->getCtrlSuppoterViews(),
        ];
    }
    
    /**
     * function getCtrlSuppoterTemplatePrefix
     *
     *
     */
    protected function getCtrlSuppoterTemplatePrefix()
    {
        $viewPrefix = $this->getViewPrefix();
        return $viewPrefix.$this->controllerName.'.'.$this->controllerName.'-';
    }
    
    /**
     * function getCtrlSuppoterTemplates
     *
     *
     */
    protected function getCtrlSuppoterTemplates()
    {
        return isset($this->viewTemplates) ? $this->viewTemplates : [];
    }
    
    /**
     * function getCtrlSuppoterOrder
     *
     *
     */
    protected function getCtrlSuppoterOrder()
    {
        return isset($this->viewOrder) ? $this->viewOrder : [];
    }
    
    /**
     * function getCtrlSuppoterViews
     *
     *
     */
    protected function getCtrlSuppoterViews()
    {
        $ret = [
            'pageName'           => $this->pageNamePrefix,
            'controller'         => $this->controllerName,
            'search_shortcut'    => isset($this->searchShortcut)     ? $this->searchShortcut     : false,
            'needDatetimePicker' => isset($this->needDatetimePicker) ? $this->needDatetimePicker : false,
        ];
        
        if( !empty($csvSettings = $this->getCsvSettings()) ) {
            $ret['csvSettings'] = $csvSettings;
        }
        
        if( !empty($ctrlCondition = $this->getCtrlCondition()) ) {
            $ret['ctrlCondition'] = $ctrlCondition;
        }
        
        return $this->addCtrlSuppoterViews($ret);
    }
    
    /**
     * function addCtrlSuppoterViews
     *
     *
     */
    protected function addCtrlSuppoterViews($views)
    {
        $addViews = isset($this->addViews) ? $this->addViews : [];
        foreach($addViews as $key => $param) {
            $views[$key] = $param;
        }
        
        return $views;
    }
    
    /**
     *
     *
     *
     */
    protected function getCsvSettings()
    {
        return isset($this->csvSettings) ? $this->csvSettings : [];
    }
    
    /**
     *
     *
     *
     */
    protected function getCtrlCondition()
    {
        return isset($this->ctrlCondition) ? $this->ctrlCondition : [];
    }

}
