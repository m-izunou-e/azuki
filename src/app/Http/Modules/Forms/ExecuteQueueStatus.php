<?php
namespace Azuki\App\Http\Modules\Forms;

/**
 * 個人情報関係のページ構築に関する共通設定の管理を行う
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Modules\Forms\AbstractFormModules;

/**
 * class SimpleUser
 *
 */
class ExecuteQueueStatus extends AbstractFormModules
{
    /**
     * 画面構成要素
     *
     * array $elements
     */
    protected $elements = [
        'belong' => [
            'form'   => [
                'title' => [
                    'size'     => 3,
                    'type'     => 'title',
                    'name'     => '所属',
                ],
                'belong'    => [
                    'type'   => 'select',
                    'name'   => 'belong',
                    'select' => 'managerBelongs',
                    'column' => 'belong',
                    'searchType' => 'select',
                    'searchName'        => 'search[belong]',
                ],
            ],
            'list' => [
                'belong' => [
                    'type'           => CONTROL_TYPE_SELECT,
                    'width'          => '15%',
                ],
            ],
        ],
        'kind' => [
            'form'   => [
                'title' => [
                    'size'     => 3,
                    'type'     => 'title',
                    'name'     => '種類',
                ],
                'kind'    => [
                    'type'   => 'select',
                    'name'   => 'kind',
                    'select' => 'executeKind',
                    'column' => 'kind',
                    'searchType' => 'select',
                    'searchName'        => 'search[kind]',
                ],
            ],
            'list' => [
                'kind' => [
                    'type'           => CONTROL_TYPE_SELECT,
                    'width'          => '15%',
                ],
            ],
        ],
        'status' => [
            'form'   => [
                'title' => [
                    'size'     => 3,
                    'type'     => 'title',
                    'name'     => '状況',
                ],
                'status'    => [
                    'type'   => 'radio',
                    'name'   => 'status',
                    'select' => 'execQueuStatusLabel',
                    'column' => 'status',
                    'searchType' => 'checkbox',
                    'searchName'        => 'search[status]',
                ],
            ],
            'list' => [
                'status' => [
                    'type'           => CONTROL_TYPE_SELECT,
                    'width'          => '15%',
                ],
            ],
        ],
        'line' => [
            'form'   => [
                'title' => [
                    'size'     => 3,
                    'type'     => 'title',
                    'name'     => '行数',
                ],
                'line'    => [
                    'type'   => 'text',
                    'name'   => 'line',
                    'column' => 'total_line',
                ],
            ],
            'list' => [
                'line' => [
                    'type'           => CONTROL_TYPE_TEXT,
                    'width'          => '10%',
                ],
            ],
        ],
        'count' => [
            'form'   => [
                'title' => [
                    'size'     => 3,
                    'type'     => 'title',
                    'name'     => 'データ数',
                ],
                'count'    => [
                    'type'   => 'text',
                    'name'   => 'count',
                    'column' => 'total_count',
                ],
            ],
            'list' => [
                'count' => [
                    'type'           => CONTROL_TYPE_TEXT,
                    'width'          => '10%',
                ],
            ],
        ],
        'success' => [
            'form'   => [
                'title' => [
                    'size'     => 3,
                    'type'     => 'title',
                    'name'     => '成功数',
                ],
                'success'    => [
                    'type'   => 'text',
                    'name'   => 'success',
                    'column' => 'success_count',
                ],
            ],
            'list' => [
                'success' => [
                    'type'           => CONTROL_TYPE_TEXT,
                    'width'          => '10%',
                ],
            ],
        ],
        'fail' => [
            'form'   => [
                'title' => [
                    'size'     => 3,
                    'type'     => 'title',
                    'name'     => '失敗数',
                ],
                'fail'    => [
                    'type'   => 'text',
                    'name'   => 'fail',
                    'column' => 'failure_count',
                ],
            ],
            'list' => [
                'fail' => [
                    'type'           => CONTROL_TYPE_TEXT,
                    'width'          => '10%',
                ],
            ],
        ],
        'message' => [
            'form'   => [
                'title' => [
                    'size'     => 3,
                    'type'     => 'title',
                    'name'     => '詳細メッセージ',
                ],
                'message'    => [
                    'type'   => 'textarea',
                    'name'   => 'message',
                    'column' => 'message',
                ],
            ],
        ],
    ];
    
    /**
     * function __construct
     *
     * 親がelementsを持っていて、追加要素が必要な場合は、staticな要素配列を定義の上、
     * $this->initElements(self::$addElements);を呼び出して、追加要素配列をメソッドに渡し、
     * elementsに追加します
     *
     */
    public function __construct()
    {
        parent::__construct();
    }
}
