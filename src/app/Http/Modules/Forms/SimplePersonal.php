<?php
namespace Azuki\App\Http\Modules\Forms;

/**
 * 個人情報関係のページ構築に関する共通設定の管理を行う
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Modules\Forms\SimpleUser;

/**
 * class SimpleUser
 *
 */
class SimplePersonal extends SimpleUser
{
    /**
     * 画面構成要素
     *
     * array $elements
     */
    protected static $addElements = [
        'free' => [
            'form'   => [
                'title' => [
//                    'size'     => 3,
//                    'type'     => 'title',
                    'name'     => '名前・メールアドレス',
                ],
                'free'    => [
                    'type'   => 'text',
                    'name'   => 'search[free]',
                    'placeholder' => '名前・メールアドレス',
                    'column' => '',
                ],
            ],
        ],
        'address' => [
            'layout' => [
                'val' => [
                    'title_address', 
                    'row1_size_9' => [
                        'GroupInput' => [ 'zipcode1', 'haifun', 'zipcode2', 'searchBtn'],
                        'pref',
                        'address1',
                        'address2',
                    ]
                ],
            ],
            'searchLayout' => [
                'val' => [
                    'title_address',
                    'row1_size_9' => [
                        'GroupInput' => [ 'zipcode_label', 'zipcode'],
                        'pref',
                        'address1',
                        'address2',
                    ]
                ],
            ],
            'form'   => [
                'title' => [
                    'size'     => 3,
//                    'type'     => 'title',
                    'name'     => '住所',
                    'required' => IS_REQUIRED,
                ],
                'zipcode'    => [
                    'type'        => 'text',
                    'name'        => 'search[zipcode]',
                    'placeholder' => '0000000',
                    'column'      => 'zipcode',
                    'unset'       => true,
                ],
                'zipcode1'    => [
                    'required'    => IS_REQUIRED,
                    'size'        => 2,
                    'type'        => 'text',
                    'name'        => 'zipcode1',
                    'placeholder' => '000',
                    'column'      => 'zipcode1',
                    'validate'    => [
                        'rule'     => 'required|regex:/^[0-9]{3}$/',
                    ],
                ],
                'zipcode2'    => [
                    'required'    => IS_REQUIRED,
                    'size'        => 2,
                    'type'        => 'text',
                    'name'        => 'zipcode2',
                    'placeholder' => '0000',
                    'column'      => 'zipcode2',
                    'validate'    => [
                        'rule'     => 'required|regex:/^[0-9]{4}$/',
                    ],
                ],
                'pref'    => [
                    'required'          => IS_REQUIRED,
                    'type'        => 'select',
                    'name'        => 'pref',
                    'select'      => 'pref',
                    'placeholder' => '選択してください',
                    'column'      => 'pref',
                    'searchName'        => 'search[pref]',
                    'searchPlaceholder' => '選択してください',
                    'validate'          => [
                        'rule'     => 'required',
                    ],
                ],
                'address1'    => [
                    'required'          => IS_REQUIRED,
                    'size'        => 12,
                    'type'        => 'text',
                    'name'        => 'address1',
                    'placeholder' => '市区町村',
                    'column'      => 'address1',
                    'searchName'        => 'search[address1]',
                    'validate'          => [
                        'rule'     => 'required',
                    ],
                ],
                'address2'    => [
                    'required'          => IS_REQUIRED,
                    'type'        => 'text',
                    'name'        => 'address2',
                    'placeholder' => '番地・建物名以下',
                    'column'      => 'address2',
                    'searchName'        => 'search[address2]',
                ],
//                'address'    => [
//                    'type'        => 'text',
//                    'column'      => 'address',
//                ],
                'haifun' => ['size' => 2, 'type' => 'label',/* 'name' => 'haifun',*/ 'label' => '—',/* 'column' => ''*/],
                'zipcode_label' => ['size' => 4, 'type' => 'label', 'name' => 'zipcode_label', 'label' => '〒',/* 'column' => ''*/],
                'searchBtn' => [
                    'size' => 6, 'type' => 'button', /*'name' => 'searchBtn',*/ 'label' => '郵便番号から住所を検索する', /*'column' => '',*/
                    'event' => [
                        'onClick' => "AjaxZip3.zip2addr('zipcode1', 'zipcode2', 'pref', 'address1', 'address2')",
                    ],
                ],
            ],
            'list' => [
                'address' => [
                    'column'      => 'address',
                    'type'           => CONTROL_TYPE_TEXT,
                    'width'          => '15%',
                ],
            ],
        ],
        'gender' => [
            'form'   => [
                'title' => [
//                    'size'     => 3,
                    'type'     => 'title',
                    'name'     => '性別',
                    'required' => IS_REQUIRED,
                ],
                'gender'    => [
                    'required'          => IS_REQUIRED,
                    'type'   => 'radio',
                    'name'   => 'gender',
                    'select' => 'gender',
                    'column' => 'gender',
                    'searchType' => 'checkbox',
//                    'searchName'        => 'search[gender]',
                    'validate'          => [
                        'rule'     => 'required',
                    ],
                ],
            ],
            'list' => [
                'gender' => [
                    'type'           => CONTROL_TYPE_SELECT,
                    'width'          => '15%',
                ],
            ],
        ],
        'birth' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => '生年月日',
                    'required' => IS_REQUIRED,
                ],
                'birth'    => [
                    'required'          => IS_REQUIRED,
                    'type'   => 'date',
                    'name'   => 'birth',
                    'column' => 'birth',
//                    'searchName'        => 'search[birth]',
                    'validate'          => [
                        'rule'      => 'required|date',
                    ],
                ],
            ],
            'list' => [
                'birth' => [
                    'type'           => CONTROL_TYPE_DATE,
                    'width'          => '20%',
                    'format'         => 'Y年m月d日',
                ],
            ],
        ],
        'isLogin' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => 'ログイン可否',
                    'required' => IS_REQUIRED,
                ],
                'isLogin' => [
                    'type'   => 'radio',
                    'default' => 1,
                    'name'   => 'manage-isLogin',
                    'column' => 'is_login',
                    'select' => 'enableLoginLabel',
                    'searchType' => 'checkbox',
                    'searchName'        => 'search[isLogin]',
                    'validate'          => [
                        'rule'      => 'required',
                    ],
                ],
            ],
            'list' => [
                'isLogin' => [
                    'type'           => CONTROL_TYPE_SELECT,
                    'width'          => '20%',
                    'orderable'      => true,
                ],
            ],
        ],
    ];
    
    /**
     * function __construct
     *
     * 親がelementsを持っていて、追加要素が必要な場合は、staticな要素配列を定義の上、
     * $this->initElements(self::$addElements);を呼び出して、追加要素配列をメソッドに渡し、
     * elementsに追加します
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->initElements(self::$addElements);
        
//        $this->elements = array_merge($this->elements, $this->addElements);
        $this->elements['role']['form']['role']['select'] = 'userRoles';
    }
}
