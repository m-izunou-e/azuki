<?php
namespace Azuki\App\Http\Modules\Forms;

/**
 * Managers関係のページ構築に関する共通設定の管理を行う
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Modules\Forms\SimpleUser;

/**
 * class Managers
 *
 */
class Managers extends SimpleUser
{
    /**
     * 画面構成要素
     *
     * array $elements
     */
    protected static $addElements = [
        'login_id' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => 'ログインID',
                    'required' => IS_REQUIRED,
                ],
                'login_id'    => [
                    'required'          => IS_REQUIRED,
                    'size'              => 8,
                    'type'              => 'text',
                    'name'              => 'login_id', 
                    'column'            => 'login_id',
                    'searchName'        => 'search[login_id]',
                    'searchPlaceholder' => '部分一致します',
                    'helperText'        => '※255文字以内の英数大文字小文字記号のみ',
                    'validate'          => [
//                        'type'      => 'loginid',
//                        'rule'      => 'required|alpha_num_symbol|max:255|uniqueOnTable:%s,login_id,%s',
                        'rules'     => [
                            ['type' => 'required'],
                            ['type' => 'alpha_num_symbol'],
                            ['type' => 'max', 'condition' => '255'],
                            ['type' => 'loginid', 'ruleType' => 'uniqueOnTable', 'condition' => '%s,login_id,%s' ],
                        ],
                        'message'   => [
                            'unique_on_table' => 'その:attributeはすでに登録されています',
                        ],
                    ],
                ],
            ],
            'list' => [
                'login_id' => [
                    'type'           => CONTROL_TYPE_TEXT,
                    'width'          => '12%',
                    'orderable'      => false,
                ],
            ],
        ],
        'init_password' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => '初期化パスワード',
                    'required' => IS_REQUIRED,
                ],
                'init_password'    => [
//                    'required'     => IS_REQUIRED,
                    'size'        => 8,
                    'type'        => 'text',
                    'name'        => 'init_password', 
                    'column'      => 'init_password',
                    'helperText'  => '※マスターユーザー選択時は必須です',
                    'validate'          => [
                        'rule' => 'nullable|required_if:is_master,1|alpha_num_symbol|min:8|max:24',
                        'message' => [
                            'required_if' => 'マスターユーザー選択時は初期化パスワードは必須です',
                        ],
                    ],
                ],
            ],
        ],
        'deletable' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => '削除可否',
                    'required' => IS_REQUIRED,
                ],
                'deletable' => [
                    'required'          => IS_REQUIRED,
                    'size'              => 8,
                    'type'              => 'radio',
                    'default'           => 1,
                    'name'              => 'deletable', 
                    'column'            => 'deletable',
                    'select'            => 'deletableLabel',
                    'searchType'        => 'checkbox',
                    'searchName'        => 'search[deletable]',
                    'validate'          => [
                        'rule' => 'required|allowValueWhen:2,is_master,1',
                        'message' => [
                            'allow_value_when' => 'マスターユーザー選択時は削除可は選択できません',
                        ],
                    ],
                ],
            ],
            'list' => [
                'deletable' => [
                    'type'           => CONTROL_TYPE_SELECT,
                    'width'          => '10%',
                    'orderable'      => false,
                    'detailLinkable' => false,
                ],
            ],
        ],
        'is_master' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => 'ユーザータイプ',
                    'required' => IS_REQUIRED,
                ],
                'is_master' => [
                    'required'          => IS_REQUIRED,
                    'size'              => 8,
                    'type'              => 'radio',
                    'default'           => 0,
                    'name'              => 'is_master', 
                    'column'            => 'is_master',
                    'select'            => 'isMasterLabel',
                    'searchType'        => 'checkbox',
                    'searchName'        => 'search[is_master]',
                    'validate'          => [
                        'rule' => 'required',
                    ],
                ],
            ],
            'list' => [
                'is_master' => [
                    'type'           => CONTROL_TYPE_BOOLEAN,
                    'title'          => 'マスター',
                    'width'          => '10%',
                    'orderable'      => false,
                    'detailLinkable' => false,
                    'allowValue'     => 1,
                ],
            ],
        ],
        'belong' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => '所属',
                    'required' => NOT_REQUIRED,
                ],
                'belong' => [
                    'required'          => NOT_REQUIRED,
                    'size'              => 8,
                    'type'              => 'select',
                    'default'           => 0,
                    'name'              => 'belong', 
                    'column'            => 'belong',
                    'select'            => 'managerBelongs',
                    'searchType'        => 'select',
                    'searchName'        => 'search[belong]',
                    'validate'          => [
                        'rule' => 'nullable',
                    ],
                ],
            ],
            'list' => [
                'belong' => [
                    'type'           => CONTROL_TYPE_SELECT,
                    'title'          => '所属',
                    'width'          => '10%',
                ],
            ],
        ],
    ];

    /**
     * function __construct
     *
     * 親がelementsを持っていて、追加要素が必要な場合は、staticな要素配列を定義の上、
     * $this->initElements(self::$addElements);を呼び出して、追加要素配列をメソッドに渡し、
     * elementsに追加します
     */
    public function __construct()
    {
        parent::__construct();
        $this->initElements(self::$addElements);
        
        $this->elements['role']['form']['role']['select'] = 'managerRoles';
        $this->elements['email']['form']['title']['required'] = NOT_REQUIRED;
        $this->elements['email']['form']['email']['required'] = NOT_REQUIRED;
        
        $rule = $this->elements['email']['form']['email']['validate'];
        $rule = str_replace('required|', 'nullable|', $rule['rule']);
        $this->elements['email']['form']['email']['validate']['rule'] = $rule;
        
        if(supportOrganizations()) {
            $this->elements['belong']['form']['title']['required'] = IS_REQUIRED;
            $this->elements['belong']['form']['belong']['required'] = IS_REQUIRED;
            $this->elements['belong']['form']['belong']['validate'] = [
//                'rules' => [
//                    ['type' => 'required'],
//                ],
                'rule'      => 'required',
            ];
        }
    }
}
