<?php
namespace Azuki\App\Http\Modules\Forms;

/**
 * 一般にページ構築に必要な共通設定の管理を行う
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Modules\Forms\AbstractFormModules;

/**
 * class BasicElements
 *
 */
class BasicElements extends AbstractFormModules
{
    /**
     * 画面構成要素
     *
     * array $elements
     */
    protected $elements = [
        'id' => [
            'form'   => [
                'title' => ['size' => 3, 'type' => 'title', 'name' => 'ID'],
                'id'    => ['size' => 4, 'type' => 'text',  'name' => 'id', 'column' => 'id'],
            ],
            'list' => [
                'id' => [
                    'type'           => CONTROL_TYPE_TEXT,
                    'width'          => '5%',
                    'orderable'      => true,
                    'detailLinkable' => false,
                ],
            ],
        ],
        'created' => [
            'form'   => [
                'title'   => ['size' => 3, 'type' => 'title',    'name' => '登録日時'],
                'created' => ['size' => 4, 'type' => 'datetime', 'name' => 'created', 'column' => 'created_at'],
            ],
            'list' => [
                'created' => [
                    'type'           => CONTROL_TYPE_DATETIME,
                    'width'          => '15%',
                    'format'         => 'Y年m月d日 H:i',
                ],
            ],
        ],
        'deleted' => [
            'form'   => [
                'title'   => ['size' => 3, 'type' => 'title',    'name' => '削除日時'],
                'deleted' => ['size' => 4, 'type' => 'datetime', 'name' => 'deleted', 'column' => 'deleted_at'],
            ],
            'list' => [
                'deleted' => [
                    'type'           => CONTROL_TYPE_DATETIME,
                    'width'          => '15%',
                    'format'         => 'Y年m月d日 H:i',
                ],
            ],
        ],
        'ctrl' => [
            'list' => [
                'ctrl' => [
                    'type'           => CONTROL_TYPE_CONTROLLER,
                    'title'          => '',
                    'width'          => '15%',
                    'ctrl'           => [CTRL_DETAIL, CTRL_EDIT, CTRL_DELETE],
//                    'condition'      => [],
                ],
            ],
        ],
        'period' => [
            'searchLayout' => [
                'val' => ['title_period', ['GroupInput' => ['period_start', 'period', 'period_end']]],
            ],
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => '対象期間',
                ],
                'period_start'    => [
                    'size'              => 4,
                    'type'              => 'datetime',
                    'name'              => 'period_start', 
                    'column'            => 'created_at',
                    'searchName'        => 'search[period_start]',
                    'placeholder'       => 'ここから',
                ],
                'period_end'    => [
                    'size'              => 4,
                    'type'              => 'datetime',
                    'name'              => 'period_end', 
                    'column'            => 'created_at',
                    'searchName'        => 'search[period_end]',
                    'placeholder'       => 'ここまで',
                ],
                'period'    => [
                    'size'              => 1,
                    'type'              => 'label',
                    'name'              => 'period', 
                    'column'            => 'period',
                    'label'             => '～',
                ],
            ],
            'list' => [
                'period' => [
                    'type'           => CONTROL_TYPE_TEXT,
                    'width'          => '20%',
                    'orderable'      => false,
                    'detailLinkable' => false,
                ],
            ],
        ],
    ];
    
    /**
     * function __construct
     *
     *
     */
    public function __construct()
    {
        parent::__construct();
    }
}
