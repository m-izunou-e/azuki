<?php
namespace Azuki\App\Http\Modules\Forms;

/**
 * ユーザー関係のページ構築に関する共通設定の管理を行う
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Modules\Forms\AbstractFormModules;

/**
 * class SimpleUser
 *
 */
class SimpleUser extends AbstractFormModules
{
    /**
     * 画面構成要素
     *
     * array $elements
     */
    protected $elements = [
        'name' => [
            'form'   => [
                'title' => [
                    'size'     => 3,
                    'type'     => 'title',
                    'name'     => 'お名前',
                    'required' => IS_REQUIRED,
                ],
                'name'    => [
                    'required'          => IS_REQUIRED,
                    'size'              => 8,
                    'type'              => 'text',
                    'name'              => 'name', 
                    'column'            => 'name',
                    'placeholder'       => '大阪　太郎',
                    'searchName'        => 'search[name]',
                    'searchPlaceholder' => '部分一致します',
                    'validate'          => [
                        'rule'     => 'required|max:36',
                        'rules'     => [
                            ['type' => 'required'],
                            ['type' => 'max', 'condition' => '36'],
                        ],
                    ],
                ],
            ],
            'list' => [
                'name' => [
                    'type'           => CONTROL_TYPE_TEXT,
                    'width'          => '15%',
                    'orderable'      => false,
//                    'detailLinkable' => true,
                ],
            ],
        ],
        'email' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => 'メールアドレス',
                    'required' => IS_REQUIRED,
                ],
                'email'    => [
                    'required'          => IS_REQUIRED,
                    'size'              => 8,
                    'type'              => 'text',
                    'name'              => 'email', 
                    'column'            => 'email',
                    'placeholder'       => 'hoge@exsample.com',
                    'searchName'        => 'search[email]',
                    'searchPlaceholder' => '部分一致します',
                    'validate'          => [
                        'type'      => 'email',
                        'rule'      => 'required|email|uniqueOnTable:%s,email,%s',
                        'message'   => [
                            'unique_on_table' => 'その:attributeはすでに登録されています',
                        ],
                    ],
                ],
            ],
            'list' => [
                'email' => [
                    'type'           => CONTROL_TYPE_TEXT,
                    'width'          => '20%',
                    'orderable'      => false,
//                    'detailLinkable' => true,
                ],
            ],
        ],
        'password' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => 'パスワード',
                    'required' => NOT_REQUIRED,
                    'required_if_regist' => IS_REQUIRED,
                ],
                'password' => [
                    'required'           => NOT_REQUIRED,
                    'required_if_regist' => IS_REQUIRED,
                    'size'       => 8,
                    'type'       => 'password',
                    'name'       => 'password_in', 
                    'column'     => 'password',
                    'defString'  => [
                        'detail'   => '********',
                        'confirm'  => '変更なし',
                    ],
                    'helperText' => '※8文字以上16文字以内の英数大文字小文字記号のみ',
                    'validate'          => [
                        'rules'     => [
                            ['type' => 'required', 'flow' => 'regist'],
                            ['type' => 'nullable', 'flow' => 'edit'],
                            ['type' => 'alpha_num_symbol'],
                            ['type' => 'min', 'condition' => '8'],
                            ['type' => 'max', 'condition' => '24'],
                        ],
//                        'rule'      => 'required|alpha_num_symbol|min:8|max:24',
//                        'rule_edit' => 'nullable|alpha_num_symbol|min:8|max:24',
                    ],
                ],
            ],
        ],
        'password_conf' => [
            'layout' => [
                'key' => 'view_form',
                'val' => [ 'title_password_conf', 'password_conf' ],
            ],
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => 'パスワード（確認）',
                    'required' => NOT_REQUIRED,
                    'required_if_regist' => IS_REQUIRED,
                ],
                'password_conf' => [
                    'required'           => NOT_REQUIRED,
                    'required_if_regist' => IS_REQUIRED,
                    'size'              => 8,
                    'type'              => 'password',
                    'name'              => 'password_conf', 
                    // password_confのカラムは存在しないが、システムの都合で値を設定している
                    // モデルのfillableにて排除される
                    'column'            => 'password_conf',
                    'validate'          => [
                        'rules'     => [
                            ['type' => 'required', 'flow' => 'regist'],
                            ['type' => 'required_with', 'condition' => 'password', 'flow' => 'edit'],
                            ['type' => 'alpha_num_symbol'],
                            ['type' => 'min', 'condition' => '8'],
                            ['type' => 'max', 'condition' => '24'],
                            ['type' => 'same', 'condition' => 'password'],
                        ],
//                        'rule'      => 'required|alpha_num_symbol|min:8|max:24|same:password',
//                        'rule_edit' => 'required_with:password|alpha_num_symbol|min:8|max:24|same:password',
                    ],
                ],
            ],
        ],
        'enable' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => '有効・無効',
                    'required' => IS_REQUIRED,
                ],
                'enable' => [
                    'required'          => IS_REQUIRED,
                    'size'              => 8,
                    'type'              => 'radio',
                    'default'           => 1,
                    'name'              => 'enable', 
                    'column'            => 'enable',
                    'select'            => 'validManagerLabel',
                    'searchType'        => 'checkbox',
                    'searchName'        => 'search[enable]',
                    'validate'          => [
                        'rule' => 'required',
                    ],
                ],
            ],
            'list' => [
                [
                    'type'           => CONTROL_TYPE_SELECT,
                    'width'          => '8%',
                    'orderable'      => true,
                    'detailLinkable' => false,
                ],
            ],
        ],
        'role' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => '役割',
                    'required' => IS_REQUIRED,
                ],
                'role' => [
                    'required'          => IS_REQUIRED,
                    'size'              => 8,
                    'type'              => 'radio',
                    'default'           => 1,
                    'name'              => 'role', 
                    'column'            => 'role',
                    'select'            => '',
                    'searchType'        => 'checkbox',
                    'searchName'        => 'search[role]',
                    'validate'          => [
                        'rule' => 'required',
                    ],
                ],
            ],
            'list' => [
                'role' => [
                    'type'           => CONTROL_TYPE_SELECT,
                    'width'          => '13%',
                    'orderable'      => false,
                    'detailLinkable' => false,
                ],
            ],
        ],
    ];
    
    /**
     * function __construct
     *
     *
     */
    public function __construct()
    {
        parent::__construct();
    }
}
