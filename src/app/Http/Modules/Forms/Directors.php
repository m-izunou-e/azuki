<?php
namespace Azuki\App\Http\Modules\Forms;

/**
 * Directors関係のページ構築に関する共通設定の管理を行う
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Modules\Forms\SimpleUser;

/**
 * class SimpleUser
 *
 */
class Directors extends SimpleUser
{
    /**
     * function __construct
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->elements['role']['form']['role']['select'] = 'directorRoles';
    }
}
