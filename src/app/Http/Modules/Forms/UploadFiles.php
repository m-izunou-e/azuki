<?php
namespace Azuki\App\Http\Modules\Forms;

/**
 * 画像や動画などファイルアップロードに関するの共通設定の管理を行う
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Modules\Forms\AbstractFormModules;

/**
 * class SimpleUser
 *
 */
class UploadFiles extends AbstractFormModules
{
    /**
     * 画面構成要素
     *
     * array $elements
     */
    protected $elements = [
        'thumbnail' => [
            'form'   => [
                'title' => [
                    'size'     => 3,
                    'type'     => 'title',
                    'name'     => '画像アップロード',
                    'required' => NOT_REQUIRED,
                ],
                'thumbnail'    => [
                    'size'        => 8,
                    'type'        => 'image-upload',
                    'ufType'      => 'image1',
                    'name'        => 'thumbnail', 
                    'column'      => 'thumbnail',
                    'placeholder' => 'サムネイル画像',
                    'isMulti'     => true,
                    'ignoreIfEmpty' => true,
                ],
            ],
            'list' => [
                'thumbnail' => [
                    'type'           => CONTROL_TYPE_IMAGE,
                    'width'          => '15%',
                ],
            ],
        ],
        'movie' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => '動画アップロード',
                    'required' => NOT_REQUIRED,
                ],
                'movie'    => [
                    'size'        => 8,
                    'type'        => 'movie-upload',
                    'ufType'      => 'movie1',
                    'name'        => 'movie', 
                    'column'      => 'movie',
                    'placeholder' => 'サンプル動画',
                    'ignoreIfEmpty' => true,
                ],
            ],
            'list' => [
                'movie' => [
                    'type'           => CONTROL_TYPE_IMAGE,
                    'width'          => '20%',
                ],
            ],
        ],
    ];
    
    /**
     * function __construct
     *
     */
    public function __construct()
    {
        parent::__construct();
    }
}
