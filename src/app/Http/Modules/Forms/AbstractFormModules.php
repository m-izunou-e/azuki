<?php
namespace Azuki\App\Http\Modules\Forms;

/**
 * 管理画面の各機能における構成要素を定義するクラスの
 * 抽象クラス。要素を取得するメソッドを持ち、各要素配列は
 * 継承先のクラスにて定義する
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

/**
 * class AbstractFormModules
 *
 */
class AbstractFormModules
{
    /**
     * function __construct
     *
     *
     */
    public function __construct()
    {
    }

    /**
     * function getElements
     *
     * elements配列を返す
     */
    public function getElements()
    {
        return $this->elements;
    }

    /**
     * function setElements
     *
     * elements配列を返す
     */
    protected function setElements($elm)
    {
        $this->elements = $elm;
    }

    /**
     * function mergeElements
     *
     * elements配列を返す
     */
    protected function mergeElements($addElm)
    {
        $elm = array_merge($this->getElements(), $addElm);
        $this->setElements($elm);
    }

    /**
     * function initElements
     *
     * elements配列を返す
     */
    protected function initElements($addElm)
    {
        if(!isset($addElm) || empty($addElm)) {
            return ;
        }
        $elm = $this->getElements();
        if(isset($elm) && !empty($elm)) {
            $this->mergeElements($addElm);
        } else {
            $this->setElements($addElm);
        }
    }
    
    /**
     *
     *  TODO 必須を任意に変更するメソッド、selectのタイプを変更するメソッドなど有効な共通処理をメソッド化したい
     *       必須を任意に変更する場合、rulesではなくruleの方のrequire_withなどのルールには注意が必要
     */
}
