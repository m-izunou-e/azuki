<?php
namespace Azuki\App\Http\Modules\Forms;

/**
 * 権限設定に関する設定項目
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Modules\Forms\AbstractFormModules;

/**
 * class Roles
 *
 */
class Roles extends AbstractFormModules
{
    /**
     * 画面構成要素
     *
     * array $elements
     */
    protected $elements = [
        'name' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => 'ロール名称',
                    'required' => IS_REQUIRED,
                ],
                'name'    => [
                    'required'          => IS_REQUIRED,
                    'size'              => 8,
                    'type'              => 'text',
                    'name'              => 'name', 
                    'column'            => 'name',
                    'searchName'        => 'search[name]',
                    'searchPlaceholder' => '部分一致します',
//                    'helperText'        => '※255文字以内の英数大文字小文字記号のみ',
                    'validate'          => [
                        'rule'      => 'required|max:36',
                    ],
                ],
            ],
            'list' => [
                [
                    'type'           => CONTROL_TYPE_TEXT,
                    'width'          => '12%',
                    'orderable'      => false,
                ],
            ],
        ],
        'value' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => 'ロール値',
                    'required' => IS_REQUIRED,
                ],
                'value'    => [
                    'required'          => IS_REQUIRED,
                    'size'              => 8,
                    'type'              => 'text',
                    'name'              => 'value', 
                    'column'            => 'value',
                    'searchName'        => 'search[value]',
//                    'helperText'        => '数字',
                    'validate'          => [
                        'type'      => 'masterValue',
                        'rule'      => 'required|numeric|max:9999|unique:%s,value,null,id,deleted_at,NULL',
                        'rule_edit' => 'required|numeric|max:9999|unique:%s,value,%s,id,deleted_at,NULL',
                    ],
                ],
            ],
            'list' => [
                [
                    'type'           => CONTROL_TYPE_TEXT,
                    'width'          => '12%',
                    'orderable'      => false,
                ],
            ],
        ],
        'order' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => '並び順',
                    'required' => IS_REQUIRED,
                ],
                'order'    => [
                    'required'          => IS_REQUIRED,
                    'size'              => 8,
                    'type'              => 'text',
                    'name'              => 'order', 
                    'column'            => 'order',
                    'validate'          => [
                        'rule'      => 'required|numeric|max:9999',
                    ],
                ],
            ],
            'list' => [
                [
                    'type'           => CONTROL_TYPE_TEXT,
                    'width'          => '12%',
                    'orderable'      => true,
                ],
            ],
        ],
        'role_authorities' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => 'ロール権限',
                    'required' => NOT_REQUIRED,
                ],
                'role_authorities'    => [
                    'required'          => NOT_REQUIRED,
                    'size'              => 8,
                    'type'              => 'role-authorities',
                    'name'              => 'role_authorities', 
                    'column'            => 'role_authorities',
                    'parts' => [
                        'authority' => [
                            'name'   => 'authority',
                            'type'   => 'select',
                            'select' => 'roleAuthoritiesLabel',
                        ],
                    ],
                    'validate'          => [
                        'key' => '*.authority',
                        'rule'     => 'required_with:role_authorities.*.url',
                        'rules'    => [
                            ['type' => 'nullable', 'key' => '*.url'],
                            ['type' => 'alpha_num_symbol', 'key' => '*.url'],
                            ['type' => 'required_with', 'condition' => 'role_authorities.*.url', 'key' => '*.authority'],
                        ],
                        'message'  => [
                            '*.url.alpha_num_symbol'    => 'URLの形式が不正です。',
                            '*.authority.required_with' => 'URLを入力したフィールドは権限を選択してください。',
                        ],
                        'attribute' => [
                            '*.url'       => 'URL',
                            '*.authority' => '権限',
                        ],
                    ],
                ],
            ],
/*
            'list' => [
                [
                    'type'           => CONTROL_TYPE_TEXT,
                    'width'          => '12%',
                    'orderable'      => true,
                ],
            ],
*/
        ],
        'deletable' => [
            'form'   => [
                'title' => [
                    'type' => 'title',
                    'name' => '',
                ],
                'deletable'    => [
                    'type'              => 'hidden',
                    'name'              => 'deletable', 
                    'column'            => 'deletable',
                ],
            ],
        ],
    ];
    
    /**
     * function __construct
     *
     * 親がelementsを持っていて、追加要素が必要な場合は、staticな要素配列を定義の上、
     * $this->initElements(self::$addElements);を呼び出して、追加要素配列をメソッドに渡し、
     * elementsに追加します
     *
     */
    public function __construct()
    {
        parent::__construct();
    }
}
