<?php
namespace Azuki\App\Http\Modules\Forms;

/**
 * 一般にページ構築に必要な共通設定の管理を行う
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Modules\Forms\AbstractFormModules;

/**
 * class BasicElements
 *
 */
class WisyWig extends AbstractFormModules
{
    /**
     * 画面構成要素
     *
     * array $elements
     */
    protected $elements = [
        'contents' => [
            'layout' => [
                'val' => [
                    'title_contents',
                    'row1_size_9' => ['contents_html', 'contents'],
                ],
            ],
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => 'ブロックタイプWisyWig',
                    'required' => NOT_REQUIRED,
                ],
                'contents'    => [
                    'size'              => 11,
                    'type'              => 'wisywig',
                    'name'              => 'contents', 
                    'column'            => 'contents',
                    'helperText'        => '※確認画面に遷移前に必ず「保存」ボタンを押下してください', 
                ],
                'contents_html'    => [
                    'size'              => 11,
                    'type'              => 'hidden',
//                    'type'              => 'text',
                    'name'              => 'contents_html', 
                    'column'            => 'contents_html',
                ],
            ],
            'list' => [
                'period' => [
                    'type'           => CONTROL_TYPE_TEXT,
                    'width'          => '20%',
                    'orderable'      => false,
                    'detailLinkable' => false,
                ],
            ],
        ],
    ];
    
    /**
     * function __construct
     *
     */
    public function __construct()
    {
        parent::__construct();
    }
}
