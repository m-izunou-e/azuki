<?php
namespace Azuki\App\Http\Modules\Forms;

/**
 * ユーザー関係のページ構築に関する共通設定の管理を行う
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Modules\Forms\SimpleUser;

/**
 * class Users
 *
 */
class Users extends SimpleUser
{
    /**
     * 追加構成要素
     *
     * array $elements
     */
    protected static $addElements = [
        'login_id' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => 'ログインID',
                    'required' => IS_REQUIRED,
                ],
                'login_id'    => [
                    'required'          => IS_REQUIRED,
                    'size'              => 8,
                    'type'              => 'text',
                    'name'              => 'login_id', 
                    'column'            => 'login_id',
                    'searchName'        => 'search[login_id]',
                    'searchPlaceholder' => '部分一致します',
                    'helperText'        => '※255文字以内の英数大文字小文字記号のみ',
                    'validate'          => [
                        'type'      => 'loginid',
                        'rule'      => 'required|alpha_num_symbol|max:255|uniqueOnTable:%s,login_id,%s',
                        'message'   => [
                            'unique_on_table'  => 'その:attributeはすでに登録されています',
                        ],
                    ],
                ],
            ],
            'list' => [
                'login_id' => [
                    'type'           => CONTROL_TYPE_TEXT,
                    'width'          => '12%',
                    'orderable'      => false,
//                    'detailLinkable' => true,
                ],
            ],
        ],
    ];

    /**
     * function __construct
     *
     * 親がelementsを持っていて、追加要素が必要な場合は、staticな要素配列を定義の上、
     * $this->initElements(self::$addElements);を呼び出して、追加要素配列をメソッドに渡し、
     * elementsに追加します
     */
    public function __construct()
    {
        parent::__construct();
        $this->initElements(self::$addElements);
        
//        $this->elements = array_merge($this->elements, $this->addElements);
        $this->elements['email']['form']['title']['required'] = NOT_REQUIRED;
        $this->elements['email']['form']['email']['required'] = NOT_REQUIRED;

        $rule = $this->elements['email']['form']['email']['validate'];
        $rule = str_replace('required|', 'nullable|', $rule['rule']);
        $this->elements['email']['form']['email']['validate']['rule'] = $rule;
    }
}
