<?php
namespace Azuki\App\Http\Modules;

/**
 * パスワードリマインダーメール送信を行うトレイト
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Illuminate\Foundation\Auth\SendsPasswordResetEmails as PasswordReset;
use Illuminate\Http\Request;

/**
 * trait SendsPasswordResetEmails
 *
 */
trait SendsPasswordResetEmails
{
    use PasswordReset;
    
    /**
     * function getSendResetMail
     *
     * パスワード再設定画面を表示するアクション
     *
     */
    public function getSendResetMail()
    {
        return view($this->getViewPrefix().'reset_password.send_reset_mail');
    }
    
    /**
     * function postSendResetMail
     *
     * パスワード再設定リクエストのポストを処理するアクション
     *
     */
    public function postSendResetMail(Request $request)
    {
        return $this->sendResetLinkEmail($request);
    }
    
    /**
     * function getSentResetMail
     *
     * パスワードリマインダーメール送信完了画面
     *
     */
    public function getSentResetMail()
    {
        return view($this->getViewPrefix().'reset_password.sent_reset_mail');
    }

    /**
     * function sendResetLinkResponse
     *
     * リマインダーメール送信完了時のレスポンス生成メソッド
     *
     */
    protected function sendResetLinkResponse($response)
    {
        $prefix = !empty($this->prefix) ? trim($this->prefix, '/') . '.' : '';
        return redirect()->route($prefix.'reset-password.sent-reset-mail');
    }
}
