<?php
namespace Azuki\App\Http\Modules;

/**
 * ログイン・ログアウト処理を行うトレイト
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Azuki\App\Http\Modules\LoginLog;

/**
 * trait Authenticate
 *
 */
trait Authenticate
{
    use AuthenticatesUsers;
    use LoginLog;
    
    /**
     * ログイン失敗時エラーメッセージ
     *
     * string faildLoginMessage
     */
    protected $faildLoginMessage = 'メールアドレスかパスワードが間違っています。';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    
    /**
     * function getFaildLoginMessage
     *
     * ログイン失敗メッセージ取得メソッド
     *
     */
    protected function getFaildLoginMessage()
    {
        return $this->faildLoginMessage;
    }

    /**
     * The user has logged out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    protected function loggedOut(Request $request)
    {
        return redirect($this->logoutPath());
    }

    /**
     * function redirectPath
     *
     * ログイン成功時のリダイレクト先を返すメソッド
     *
     * @return string
     */
    public function redirectPath()
    {
        if (property_exists($this, 'redirectPath'))
        {
            return $this->redirectPath;
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/';
    }

    /**
     * function logoutPath
     *
     * ログアウト時のパスを返すメソッド
     *
     * @return string
     */
    public function logoutPath()
    {
        return property_exists($this, 'logoutPath') ? $this->logoutPath : '/login';
    }

    /**
     * function username
     *
     * ログイン時のログインIDに当たる値のDBカラム名称
     *
     * @return string
     */
    public function username()
    {
        return 'login_id';

    }
}
