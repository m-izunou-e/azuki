<?php
namespace Azuki\App\Http\Modules;

/**
 * SNSログイン処理を行うトレイト
 *
 * Userでの使用想定での実装
 * Managerでの使用や組織で分けて使用する場合は拡張が必要
 * Socialiteパッケージが対応できているかという問題もあるので、
 * 場合によってはSocialiteパッケージを一部継承してフックするような実装を考える必要がある
 *
 * @copyright Copyright 2021 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite as Socialite;

/**
 * trait SNSAuthenticate
 *
 */
trait SNSAuthenticate
{
    /**
     *
     *
     *
     */
    public function getRedirectToSnsLoginProvider($social)
    {
        return Socialite::driver($social)->redirect();
    }
    
    /**
     *
     *
     *
     */
    public function getProviderCallback($provider)
    {
        try {
            $user = Socialite::driver($provider)->user();
        } catch (Exception $e) {
            return redirect($this->logoutPath());
        }
        
        $authUser = $this->findOrCreateUser($user, $provider);
        app('auth')->login($authUser, true);
        
        $this->recordLoginLogs($authUser, USER_KIND_USERS);
        return redirect($this->redirectPath());
    }
    
    /**
     *
     *
     *
     */
    public function findOrCreateUser($providerUser, $provider)
    {
        $email = $providerUser->getEmail();
        if(empty($email)) {
            // メールアドレスなしException
            $msg = "メールアドレスが取得できませんでした。\nSNSログインではメールアドレスの連携が必須です";
            throw new \Exception($msg);
        }
        
        $model        = app(app('auth')->createUserProvider('users')->getModel());
        $socialId     = $providerUser->getId();
        $providerKind = $this->getProviderKind($provider);
        $ac = $model->getSocialIdentityModel()->getSocialIdentity($socialId, $providerKind);
        
        if(!is_null($ac)) {
            return $ac->users;
        }
        
        $ac = $model->where('email', '=', $email)->first();
        if(is_null($ac)) {
            $user = [
                'name'     => $providerUser->getName(),
                'login_id' => $email,
                'email'    => $email,
                'password' => $this->getTemporaryPassword(),
            ];
            $ac = $model->_create($user);
        }
        
        $ac->getSocialIdentityModel()->_create([
            'users_id'      => $ac->id,
            'social_id'     => $socialId,
            'provider'      => $provider,
            'provider_kind' => $providerKind,
        ]);
        return $ac;
    }
    
    /**
     *
     *
     */
    protected function getProviderKind($provider)
    {
        $key = sprintf('services.%s.kind', $provider);
        return Config()->get($key);
    }
    
    /**
     *
     *
     */
    protected function getTemporaryPassword()
    {
        $marks = ['!','$','%','&','(',')','*','+','/'];
        $passwordstr = array_merge(
            $this->getRandomCollect(range('a', 'z'), 10),
            $this->getRandomCollect(range('A', 'Z'), 10),
            $this->getRandomCollect(range(0, 9),     10),
            $this->getRandomCollect($marks,          2)
        );
        return str_shuffle(implode($passwordstr));
    }
    
    /**
     *
     *
     */
    protected function getRandomCollect($target, $num)
    {
        return collect($target)->random($num)->all();
    }
}
