<?php
namespace Azuki\App\Http\Modules\ActionTrait\TraitParts;

/**
 * TODO getRedirectPathFromComplete がController.phpで定義されているが、場所変える。
 */

trait TraitIndexAction
{

    /**
     * function getIndex 初期表示のアクション
     *
     * 初期表示のアクション
     * 初期表示は一覧とするため、一覧のアクションにリダイレクト
     * @return void
     */
    public function getIndex()
    {
        $redirect = $this->ctrlSupporter->getSubstituteIndexRoute();
        return redirect( $redirect );
    }
}

