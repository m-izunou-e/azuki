<?php
namespace Azuki\App\Http\Modules\ActionTrait\TraitParts;

/**
 * 詳細表示に関するアクショントレイト
 *
 */


trait TraitDetailAction
{
    /**
     * function getDetail 詳細画面表示のアクション
     *
     * 詳細画面表示のアクション
     *
     * @return void
     */
    public function getDetail($id, $prev = null)
    {
        $prev = $prev != null ? $prev : "list";
        $forms = $this->getDetailForm();
        $this->ctrlSupporter->setDetailForm($forms);

        $asign = $this->getDetailAsign([
            'flow'         => 'detail',
            'prev'         => $prev,
            'post'         => $this->ctrlSupporter->find($id),
            'detailLayout' => $this->getDetailLayout(),
            'detailForm'   => $forms,
        ]);

        return $this->ctrlSupporter->view('detail', $asign);
    }
    
    /**
     * function getDetailAsign
     *
     * アサインデータを取得する
     * コントローラーで追加アサイン処理ができるように用意
     *
     *
     * @param  Array $asign デフォルトのアサインデータ
     * @return Array $ret   追加・変更したアサインデータ
     */
    protected function getDetailAsign($asign)
    {
        return $asign;
    }

}

