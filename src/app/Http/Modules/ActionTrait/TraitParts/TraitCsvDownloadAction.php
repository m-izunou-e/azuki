<?php
namespace Azuki\App\Http\Modules\ActionTrait\TraitParts;

/**
 */
use Illuminate\Http\Request;

trait TraitCsvDownloadAction
{
    /**
     * function postCsvDownload CSVダウンロードのアクション
     *
     * CSVダウンロードのアクション
     *
     * @return void
     */
    public function postCsvDownload()
    {
        $service = $this->getCsvDownloadService();
        return $service->outputData($this->getCsvDownloadFileName());
    }
}

