<?php
namespace Azuki\App\Http\Modules\ActionTrait\TraitParts;

/**
 *
 */

trait TraitDeleteAction
{

    /**
     * function postDelete 削除処理のアクション
     *
     * 削除処理のアクション
     *
     * @return void
     */
    public function postDelete()
    {
        $this->ctrlSupporter->deleteDone();

        $redirect = $this->ctrlSupporter->getRedirectPathFromComplete();
        return redirect( $redirect );

    }
}

