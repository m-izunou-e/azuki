<?php
namespace Azuki\App\Http\Modules\ActionTrait\TraitParts;

/**
 * 一覧表示に関するアクションをまとめたトレイト
 *
 *
 */

trait TraitListAction
{
    /**
     * function listAction 一覧画面表示のアクション
     *
     * 一覧画面表示のアクション
     *
     * @return void
     */
    public function getList()
    {
        $this->ctrlSupporter->resetCondition();

        return $this->_displayList();
    }
    
    /**
     * function listAction 一覧画面表示のアクション
     *
     * 一覧画面表示のアクション
     *
     * @return void
     */
    public function postList()
    {
        $this->ctrlSupporter->setCondition();

        return $this->_displayList();
        
    }

    /**
     * function list 一覧画面表示処理
     *
     * 一覧画面表示処理
     *
     * @return void
     */
    protected function _displayList($flow = 'list')
    {
        $this->ctrlSupporter->setResultStatus();
        $pagination = $this->ctrlSupporter->paginate();
        
        $asign = $this->getListAsign([
            'flow'          => $flow,
            'list'          => $this->getListSettings(),
            'dataList'      => $pagination,
            'post'          => $this->ctrlSupporter->getSearchConditionPost(),
            'searchBtnkind' => $this->ctrlSupporter->getSearchBtnKind(),
        ]);

        return $this->ctrlSupporter->view('list', $asign);
    }
    
    /**
     * function getListAsign
     *
     * アサインデータを取得する
     * コントローラーで追加アサイン処理ができるように用意
     *
     * 基本的な追加アサインとして
     * ・一覧の設定
     * ・検索フィールドに関するアサインデータ
     * を追加しておく
     *
     * @param  Array $asign デフォルトのアサインデータ
     * @return Array $ret   追加・変更したアサインデータ
     */
    protected function getListAsign($asign)
    {
        $asign['searchForm']   = $this->getSearchForm();
        $asign['searchLayout'] = $this->getSearchLayout();
        
        return $asign;
    }

    /**
     * function validAction ユーザーの有効無効を切り替えるアクション
     *
     * ユーザーの有効無効を切り替える処理のアクション
     *
     * @return void
     */
    public function postValid()
    {
        $this->ctrlSupporter->toggleValidDone();

        $redirect = $this->ctrlSupporter->getRedirectPathFromComplete();
        return redirect( $redirect );
    }

    /**
     * function getEditOnList 一覧での編集画面表示のアクション
     *
     * 一覧での編集画面表示のアクション
     *
     * @return void
     */
    public function getEditOnList($id = '')
    {
        if( empty($id) ) {
            abort();
        }

        $data = $this->ctrlSupporter->find($id);
        if(is_null($data)) {
            abort();
        }

        $this->ctrlSupporter->resetCondition();
        $old = $this->ctrlSupporter->getOldInput();
        if( !empty($old) ) {
            $data = $old;
        }
        view()->share('id',   $id);
        view()->share('post', $data);
        $this->ctrlSupporter->beforeDisplayList($id, $data);

        return $this->_displayList('edit');
    }

}

