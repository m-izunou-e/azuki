<?php
namespace Azuki\App\Http\Modules\ActionTrait\TraitParts;

/**
 * DeleteOldDataTrait クラス
 *
 * @copyright Copyright 2020 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Illuminate\Support\Facades\Response;

/**
 * trait DeleteOldDataTrait
 *
 *
 */
trait DeleteOldDataTrait
{
    /**
     * function postDeleteCollectData
     *
     */
    public function postDeleteFromDatabase()
    {
        return $this->deleteOldData();
    }

    /**
     * function postDeleteCollectData
     *
     */
    protected function deleteOldData($model = null)
    {
        $model = is_null($model) ? $this->getDeleteTargetModel() : $model;
        if(is_null($model) || !method_exists($model, 'hardDeleteOldData')) {
            return Response::json([
                'result'  => false,
                'message' => '不正なリクエストです。',
            ]);
        }
    
        $request = app('request');
        $year  = $request->get('year');
        
        $result = $model->hardDeleteOldData($year);
        $msg = 'データの削除に失敗しました。';
        if($result === 0) {
            $msg = '対象データはありませんでした。';
            $result = false;
        } elseif(is_numeric($result)) {
            $msg = sprintf('%d件のデータを削除しました', $result);
            $result = true;
        }

        $ret = [
            'result'  => $result,
            'message' => $msg,
        ];
        
        return Response::json( $ret );
    }
    
    /**
     * function getDeleteTargetModel
     *
     *
     */
    protected function getDeleteTargetModel()
    {
        return null;
    }
}
