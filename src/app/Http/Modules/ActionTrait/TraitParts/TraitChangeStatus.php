<?php
namespace Azuki\App\Http\Modules\ActionTrait\TraitParts;

use Azuki\App\Contracts\Models\SystemSwitch;
use Illuminate\Support\Facades\Response;

/**
 * オン・オフを持つステータスの切り替えを提供するトレイト
 *
 *
 */

trait TraitChangeStatus
{
    /**
     * function postChangeOprationStatus
     *
     * @return void
     */
    public function postChangeOprationStatus()
    {
        list($result, $msg) = $this->changeStatus();
        $ret = [
            'result'  => $result,
            'message' => $msg,
        ];
        
        return Response::json( $ret );
    }
    
    /**
     * function changeStatus
     *
     *
     */
    protected function changeStatus()
    {
        $methods = [
            SWITCH_KIND_MAINTENANCE_MODE => 'changeMaintenance',
        ];
    
        $req    = app('request');
        $kind   = $req->get('kind');
        $status = $req->get('status');
        
        $method = 'changeStatusOfSwitchDatabase';
        if(isset($methods[$kind]) && method_exists($this, $methods[$kind])) {
            $method = $methods[$kind];
        }
        return call_user_func([$this, $method], $kind, $status);
    }
    
    /**
     *
     *
     */
    protected function changeStatusOfSwitchDatabase($kind, $status)
    {
        $model = app(SystemSwitch::class);
        if($status == 'on') {
            $result = $model->switchOn($kind);
        } else {
            $result = $model->switchOff($kind);
        }
        $msg = $result ? '': '切替に失敗しました。';
        
        return [$result, $msg];
    }
    
    /**
     *
     *
     */
    protected function changeMaintenance($kind, $status)
    {
        if(!$this->checkMaintenanceAllow()) {
            return [false, '許可されていない操作です'];
        }
        
        $msg = '';
        $result = true;
        if($status == 'on') {
            if(app()->isDownForMaintenance()) {
                $result = false;
                $msg = 'すでにメンテナンス中です';
            } else {
                list($allowIp, $secret) = $this->getMaintenanceConfigs();
                if(laravelVersionOverEight()) {
                    \Artisan::call('down', ['--secret' => $secret]);
                } else {
                    \Artisan::call('down', ['--allow' => $allowIp]);
                }
            }
        } else {
            if(app()->isDownForMaintenance()) {
                \Artisan::call('up');
            } else {
                $result = false;
                $msg = 'すでに稼働中です';
            }
        }
        if(empty($msg)) {
            $msg = $result ? '': '切替に失敗しました。';
        }
        
        return [$result, $msg];
    }
    
    /**
     *
     *
     */
    protected function checkMaintenanceAllow()
    {
        if(method_exists($this, 'checkAllowIp')) {
            return $this->checkAllowIp();
        }
        return ifRemoteIpInAllows(true);
    }
    
    /**
     *
     *
     *
     */
    protected function getMaintenanceConfigs()
    {
        $allowIp = getAllowIpConfig();
        if(method_exists($this, 'getAllowIp')) {
            $allowIp = $this->getAllowIp();
        }
        $secret = app(BASE_APP_ACCESSOR)->get('standard.maintenance_secret');
        if(method_exists($this, 'getSecret')) {
            $secret = $this->getSecret();
        }
        
        return [$allowIp, $secret];
    }
}
