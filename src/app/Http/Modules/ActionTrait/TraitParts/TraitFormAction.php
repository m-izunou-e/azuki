<?php
namespace Azuki\App\Http\Modules\ActionTrait\TraitParts;

/**
 * 登録・編集に関するアクションをまとめたトレイト
 *
 */
use Azuki\App\Http\Requests\FormRequest;


trait TraitFormAction
{
    /**
     * function initializeFormSetting
     *
     * フォーム関連処理を実行する前に必要な処理をこのメソッドにて行う
     * 各フォーム関連処理を行るメソッドにて最初に呼び出されることを想定している
     *
     * ※各コントローラにてフォーム系のメソッドのオーバーライドを行っている場合
     *   このメソッドの呼び出しを忘れないようにする必要がある
     *
     */
    private function initializeFormSetting()
    {
        $this->ctrlSupporter->initializeValidator($this->getValidSettings());
        $this->ctrlSupporter->setForm($this->getForm());
    }

    /**
     * function formAction 登録画面表示のアクション
     *
     * 登録画面表示のアクション
     *
     * @return void
     */
    public function getRegist()
    {
        $this->initializeFormSetting();
    
        $post = $this->ctrlSupporter->getDefaultPost();
        $old = $this->ctrlSupporter->getOldInput();
        if( !empty($old) ) {
            $post = $old;
        }
        return $this->_displayForm( 'regist', '', $post );
    }
    
    /**
     * function formAction 登録画面表示のアクション
     *
     * 登録画面表示のアクション
     *
     * @return void
     */
    public function postRegist()
    {
        $this->initializeFormSetting();

        $post = $this->ctrlSupporter->getPost();
        $prev = $this->ctrlSupporter->getPrev();

        return $this->_displayForm(
            'regist',
            $prev,
            $post
        );
    }

    /**
     * function editAction 編集画面表示のアクション
     *
     * 編集画面表示のアクション
     *
     * @return void
     */
    public function getEdit($id, $prev = null)
    {
        $this->initializeFormSetting();

        // TODO そのIDのデータに対しての権限があるかチェックが必要
        $prev = $prev != null ? $prev : "list";

        $post = $this->ctrlSupporter->find($id);
        $old = $this->ctrlSupporter->getOldInput();
        if( !empty($old) ) {
            $post = $old;
        }
        $this->ctrlSupporter->beforeDisplayEditForm($id, $post);

        return $this->_displayForm(
            'edit',
            $prev,
            $post
        );
    }
    
    public function postEdit()
    {
        $this->initializeFormSetting();

        $post = $this->ctrlSupporter->getPost();
        $prev = $this->ctrlSupporter->getPrev();
        $this->ctrlSupporter->beforeDisplayEditForm(app('request')->get('id'), $post);

        return $this->_displayForm(
            'edit',
            $prev,
            $post
        );
    }

    /**
     * function _displayForm フォーム画面表示処理
     *
     * フォーム画面表示処理
     *
     */
    private function _displayForm($flow, $prev, $post)
    {
        $asign = $this->getFormAsign([
            'flow'         => $flow,
            'prev'         => $prev,
            'post'         => $post,
            'formLayout'   => $this->getFormLayout(),
        ]);

        return $this->ctrlSupporter->view('form', $asign);
    }
    
    /**
     * function getFormAsign
     *
     * アサインデータを取得する
     * コントローラーで追加アサイン処理ができるように用意
     *
     * @param  Array $asign デフォルトのアサインデータ
     * @return Array $ret   追加・変更したアサインデータ
     */
    protected function getFormAsign($asign)
    {
        return $asign;
    }

    /**
     * function confirmAction 確認画面表示のアクション
     *
     * 確認画面表示のアクション
     *
     * @return void
     */
    public function postConfirm(FormRequest $request)
    {
        $this->initializeFormSetting();

        // TODO そのIDのデータに対しての権限があるかチェックが必要
        $post = $this->ctrlSupporter->validation($request);

        $this->ctrlSupporter->beforeDisplayConfirm($request->get('id'), $post);

        $asign = $this->getConfirmAsign([
            'post'         => $post,
            'formLayout'   => $this->getConfirmLayout(),
        ]);

        return $this->ctrlSupporter->view('confirm', $asign);
    }
    
    /**
     * function getConfirmAsign
     *
     * アサインデータを取得する
     * コントローラーで追加アサイン処理ができるように用意
     *
     * @param  Array $asign デフォルトのアサインデータ
     * @return Array $ret   追加・変更したアサインデータ
     */
    protected function getConfirmAsign($asign)
    {
        return $asign;
    }

    /**
     * function postDone 完了処理のアクション
     *
     * 完了処理のアクション
     *
     * @return void
     */
    public function postDone(FormRequest $request)
    {
        $this->initializeFormSetting();

        // TODO そのIDのデータに対しての権限があるかチェックが必要
        $post = $this->ctrlSupporter->validation($request);

        $this->ctrlSupporter->createOrUpdate($post);

        $this->beforeDoneRedirect($post);

        $redirect = $this->ctrlSupporter->getRedirectPathFromComplete();
        return redirect( $redirect );
    }
    
    /**
     * function beforeDoneRedirect
     *
     * 登録・更新処理後完了画面（デフォルトは一覧）に戻る前に呼び出されるメソッド
     * 追加処理が必要な場合にその内容を実装するためのメソッド
     * 共通処理として必要な実装はpostDoneにて実装されるためここはコントローラーごとに
     * 必要な追加処理がある場合にコントローラでオーバーライドして実装をするために用意している。
     *
     */
    protected function beforeDoneRedirect($data)
    {
    }
}

