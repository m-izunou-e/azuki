<?php
namespace Azuki\App\Http\Modules\ActionTrait;

/**
 * 管理画面 一覧と詳細のトレイトパック
 *
 */

use Azuki\App\Http\Modules\ActionTrait\TraitParts\TraitIndexAction;
use Azuki\App\Http\Modules\ActionTrait\TraitParts\TraitListAction;
use Azuki\App\Http\Modules\ActionTrait\TraitParts\TraitDetailAction;

trait ListActionPackageWithDetail
{
    use TraitIndexAction;
    use TraitListAction;
    use TraitDetailAction;
}
