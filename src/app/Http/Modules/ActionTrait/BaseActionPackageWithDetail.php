<?php
namespace Azuki\App\Http\Modules\ActionTrait;

/**
 * 管理画面 基本的なアクション+詳細画面を定義したトレイト
 *
 * @package admin
 * @subpackage System
 *
 * BaseActionPackageトレイトを読み込んだ上で、詳細画面に関するアクションを定義しています。
 * 本トレイトを利用することで、BaseActionPackageトレイト＋詳細画面を実装できます。
 * ただし、BaseActionPackageトレイト同様利用側で決まった変数の定義をする必要があります。
 */

use Azuki\App\Http\Modules\ActionTrait\BaseActionPackage;
use Azuki\App\Http\Modules\ActionTrait\TraitParts\TraitDetailAction;

trait BaseActionPackageWithDetail
{
    use BaseActionPackage;
    use TraitDetailAction;

}
