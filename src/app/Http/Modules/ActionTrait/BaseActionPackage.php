<?php
namespace Azuki\App\Http\Modules\ActionTrait;

/**
 * 管理画面 基本的なアクションを定義したトレイト
 *
 * @package admin
 * @subpackage System
 *
 * 一覧、登録、編集、削除、検索、並び替え、有効・無効の切替
 * に関するアクションが定義されています。
 * これらアクションが機能するためにはこのトレイトを利用する側で決まった変数定義がされていることが
 * 前提となります。
 *
 * 必要な変数定義は以下の通り
 *
 * コントローラ名
 * @var string ctrName
 *
 * 並び替えの基準とするカラム名
 * @var string defOrderColumn
 *
 * 一覧の１ページあたりの表示件数の設定
 * @var Int perPage
 *
 * 一覧にて並び替えを行うための、ポスト変数とカラム名を紐づける配列
 * @var Array orderColumns
 *
 * 入力画面で初期設定の必要のあるカラムを定義する配列
 * カラム名 => 値
 * @var Array defPost
 *
 * 追加で登録が必要なカラムを定義する配列
 * カラム名 => 値
 * @var Array postBase
 *
 * モデルのカラム名と入力画面のname属性を紐づける配列
 * カラム名 => name属性
 * @var string columnNameRelatives
 *
 * 検索に使用するname属性の指定
 * ここで定義されたname属性しか検索には利用できない
 * @var string searchNames
 */

use Azuki\App\Http\Modules\ActionTrait\TraitParts\TraitIndexAction;
use Azuki\App\Http\Modules\ActionTrait\TraitParts\TraitListAction;
use Azuki\App\Http\Modules\ActionTrait\TraitParts\TraitFormAction;
use Azuki\App\Http\Modules\ActionTrait\TraitParts\TraitDeleteAction;

trait BaseActionPackage
{
    use TraitIndexAction;
    use TraitListAction;
    use TraitFormAction;
    use TraitDeleteAction;
}
