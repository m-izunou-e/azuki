<?php
namespace Azuki\App\Http\Requests;
/**
 * FormRequestを継承した抽象クラス「App\Http\Requests\Request」を継承
 * することで、FormRequestの機能を拡張したクラスとしています。
 * このクラスをバリデーションクラスに渡すことでバリデーションを実行できるようになります。
 *
 * ※なお、FormRequestを直接継承すると、Laravelの内部で行われているFormRequest
 * 　の初期設定が引き継がれることがなく、POST値など重要な情報がセットされなくなります。
 */

use Azuki\App\Http\Requests\Request;
use Illuminate\Validation\Validator;

class FormRequest extends Request
{
    /**
     * The URI to redirect to if validation fails.
     *
     * @var string
     */
    protected $redirect;

    /**
     * The input keys that should not be flashed on redirect.
     *
     * @var array
     */
    protected $dontFlash = [];
    
    /**
     * リダイレクトURLへの追加処理を行わないフラグ
     *
     */
    protected $dontAddRedirect = false;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
    
    /**
     * function setNotAddRedirect
     *
     * リダイレクトURLへの追加処理を無効にする
     *
     */
    public function setNotAddRedirect()
    {
        $this->dontAddRedirect = true;
    }

    /**
     * Set the Redirect url.
     *
     * @param  String  $redirect
     */
    public function setRedirect($redirect)
    {
        $this->redirect = $redirect;
    }

    /**
     * Set the Redirect url.
     *
     * @param  String  $redirect
     */
    public function ifAddRedirect($cond, $add)
    {
        if($this->dontAddRedirect) {
            return;
        }
    
        $url = $this->getRedirectUrl();
        if(preg_match($cond, $url)) {
            $this->setRedirect($url.$add);
        }
    }

    /**
     * Handle a failed validation attempt.
     *
     * FormRequestクラスのfailedValidationがprotectedで外部より呼び出しが
     * できないため外部呼出しを可能にするためのメソッドとして用意
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return mixed
     */
    public function failedValidationWrapper(Validator $validator)
    {
        $this->failedValidation($validator);
    }
}
