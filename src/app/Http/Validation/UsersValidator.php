<?php
namespace Azuki\App\Http\Validation;

/**
 * UsersValidator バリデーション
 *
 * @copyright Copyright 2019 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Validation\SimpleUserValidator;
use Azuki\App\Contracts\Models\Users;

class UsersValidator extends SimpleUserValidator
{
    /**
     *
     *
     *
     */
    protected function getUserModel()
    {
        return Users::class;
    }
}
