<?php
namespace Azuki\App\Http\Validation;

/**
 * RolesValidator バリデーション
 *
 * @copyright Copyright 2019 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Validation\Validator;

abstract class RolesValidator extends Validator
{
    /**
     * function after
     *
     * バリデーション後の処理を実装
     * 権限設定のエラーを一つにまとめる処理をする
     */
    protected function after($validator)
    {
        $authoritiesErrors = $validator->errors()->get('role_authorities.*');
        if( count($authoritiesErrors) > 0 ) {
            $msgs = [];
            foreach( $authoritiesErrors as $er ) {
                if(is_array($er)) {
                    foreach($er as $e) {
                        if(!in_array($e, $msgs, true)) {
                            $msgs[] = $e;
                        }
                    }
                } else {
                    if(!in_array($er, $msgs, true)) {
                        $msgs[] = $er;
                    }
                }
            }
            foreach($msgs as $msg) {
                $validator->errors()->add('role_authorities', $msg);
            }
        }
    }

    /**
     *
     *
     */
    public function getValidateTypeMasterValue($format, $data, $flow, $id, $params = null)
    {
        $table = $this->getRoleTableName();
        if($flow == "edit") {
            return sprintf($format, $table, $id);
        }
        return sprintf($format, $table);
    }
    
    /**
     *
     *
     */
    abstract protected function getRoleTableName();
}
