<?php
namespace Azuki\App\Http\Validation;
/**
 * 管理画面アカウントのバリデーションを定義したクラス
 *
 * エラー時のリダイレクトURL
 * 属性値の日本語表記の設定
 * バリデーションルールを記載する
 *
 */

use Azuki\App\Http\Validation\SimpleUserValidator;
use Azuki\App\Contracts\Models\Managers;

class ManagersValidator extends SimpleUserValidator
{
    /**
     *
     *
     *
     */
    protected function getUserModel()
    {
        return Managers::class;
    }
}
