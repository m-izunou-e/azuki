<?php
namespace Azuki\App\Http\Validation\Manage;

/**
 * UsersValidator バリデーション
 *
 * @copyright Copyright 2019 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Validation\UsersValidator as Validator;
use Azuki\App\Contracts\Http\Validation\Manage\UsersValidator as ContractsInterface;

class UsersValidator extends Validator implements ContractsInterface
{
}
