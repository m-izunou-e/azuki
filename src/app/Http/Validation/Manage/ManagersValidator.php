<?php
namespace Azuki\App\Http\Validation\Manage;

use Azuki\App\Http\Validation\ManagersValidator as Validator;
use Azuki\App\Contracts\Http\Validation\Manage\ManagersValidator as ContractsInterface;

class ManagersValidator extends Validator implements ContractsInterface
{
}
