<?php
namespace Azuki\App\Http\Validation;
/**
 * バリデーションを行うための基底クラス
 *
 * 以下の項目
 * ・エラー時のリダイレクトURL
 * ・属性値の日本語表記の設定
 * ・バリデーションルール
 * を継承先で設定する。
 *
 * FormRequestクラスにはバリデーションを行う仕組みが実装されており、
 * それをアクションメソッドの引数として自動注入することで、バリデーションを
 * 行うことが出来るが、その場合任意のタイミングでバリデーションができなくなるため、
 * FormRequestを継承したクラスでバリデーションを定義する形ではなく、このバリデーションクラス
 * にFormRequestを渡し、任意のタイミングでバリデーションを行えるようにしています。
 *
 * また、バリデーション対象のデータもバリデーション時に渡す仕様とし、
 * POST値からだけではなく、内部管理していた配列やセッションのデータをバリデーション対象とすることも
 * 出来るようにしています。
 *
 * ただし、バリデーションの一連の流れ自体は、FormRequestに実装された流れに乗せるように作っています。
 */

use App;
use Azuki\App\Http\Requests\FormRequest;

class Validator
{
    /**
     * boolean $noRules
     *
     * バリデーションルールがないバリデータの場合明示的にtrueに設定すること
     */
    protected $noRules = false;

    protected $validSettings = [];

    protected $validate_attributes = [];
    protected $validate_messages   = [];
    
    /**
     * attributeの初期化を行うかどうか
     * 各Validatorで個別に設定する場合はfalseにする
     */
    protected $isInitialAttribute = true;
    
    /**
     * messagesの初期化を行うかどうか
     * 各Validatorで個別に設定する場合はfalseにする
     */
    protected $isInitialMessages = true;

    /**
     * バリデート対象データとリクエストオブジェクトを引数として
     * バリデートを処理するメソッド
     * 継承先のクラスにてrulesメソッドを実装し、バリデートルールを設定する
     *
     * @return array
     */
    public function validation(FormRequest $request, $data, $flow = null, $id = null)
    {
        $validator = $this->getValidator($data, $flow, $id);

        if (!$validator->passes())
        {
            if( $flow == 'edit' ) {
                $request->ifAddRedirect('/.*\/edit$/', '/'.$id);
            }
            $request->failedValidationWrapper($validator);
        }
        
    }
    
    /**
     * function getValidator
     *
     * 生成したバリデーションインスタンスを取得する
     * バリデーション処理自体を外部で行うためのメソッド
     */
    public function getValidator($data, $flow = null, $id = null)
    {
        $factory = App::make('Illuminate\Validation\Factory');
        
        $validator = $factory->make(
            $data,
            $this->rules($data, $flow, $id),
            $this->messages(),
            $this->attributes()
        );
        
        $validator->after(function($validator){
            $this->after($validator);
        });
        
        return $validator;
    }
    
    /**
     * function after
     *
     * validate処理後に必要な処理がある場合にこのメソッド内に記述することで
     * 実行される
     *
     */
    protected function after($validator)
    {
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * 各バリデートクラスの方で本メソッドをオーバーライドすることで
     * 自動生成ではない独自のルール定義をすることが出来る
     * 自動生成では対応できない複雑なルールの場合やコントローラー以外の場所からの
     * 呼び出しもあり、バリデートクラス側で共通処理として吸収したばい場合など
     * 各クラスの方でルール定義を行うことが出来るようになっている
     *
     * また、parent::rulesで自動生成のルールを構築したうえで独自のバリデートを
     * ルールに追加することも可能
     *
     * 自動生成のバリデーションを使用する場合で、バリデーションの必要がない処理の場合
     * noRulesメンバ変数をtrueに明示的に設定する必要がある
     * これは、自動生成時に誤ってバリデーションルールがなくなってしまうことを予防する
     * 目的の実装。基本的に自動生成処理の場合はルールが最低１つは存在している必要があるとしている
     *
     * @return array
     */
    public function rules($data, $flow = null, $id = null)
    {
        $ret = $this->makeValidateRules($data, $flow, $id);
        // $retが空の場合、正しくバリデーション情報が設定されていない可能性がある
        // 明示的にルールがないことが設定されていない場合は警告のログを残して、abortさせる
        if(empty($ret) && $this->noRules) {
            $msg = sprintf( '[%s]:バリデーションルールが未定義です', get_class($this) );
            warningExLog($msg);
            abort(500, $msg);
        }
        
        return $ret;
    }

    /**
     * SSet custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return $this->validate_messages;
    }
    
    /**
     * Set custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return $this->validate_attributes;
    }
    
    /**
     * function setValidInitialize
     *
     * バリデーションの初期化処理を行う
     * Formエレメントで定義したバリデーションルールを使用するバリデーションでは
     * 本メソッド呼び出しは必須
     * 引数で与えられた設定に基づいて、１．Attribute、２．messageの設定を行う
     * また、設定値はメンバ変数に登録され、rule構築時に使用される
     * rule構築はfactory->make時に行われる
     * rule構築がここではなくfactory->make時に行うのは、rule構築にはポストされたデータなどが必要になるため
     *
     */
    public function setValidInitialize($settings)
    {
        $this->setValidSettings($settings);
        if($this->isInitialAttribute) {
            $this->initAttribute();
        }
        if($this->isInitialMessages) {
            $this->initMessages();
        }
    }

    /**
     * function setValidSettings
     *
     * 引数で与えられた設定をメンバ変数に保存する
     *
     */
    protected function setValidSettings($settings)
    {
        if(!is_array($settings)) {
            return ;
        }
        $this->validSettings = $settings;
    }
    
    /**
     * function initAttribute
     *
     * 保存された設定値からattribute用のメンバ変数を構築する
     * フォームエレメントのvalidate.attributeが設定されていない場合、title.nameの値を自動的に使用する
     * title.nameの値をattributeに設定する処理はManagementTraitのgetValidSettingsメソッドで行っています
     *
     * また、各バリデートクラスにてvalidate_attributesを定義していた場合、そちらが優先となり、
     * 同名定義があれば先勝ちとなる
     *
     */
    protected function initAttribute()
    {
        $settings = $this->validSettings;
        
        foreach($settings as $key => $row) {
            $attribute = isset($row['validate']['attribute']) ? 
                $row['validate']['attribute'] : 
                (isset($row['attribute']) ? $row['attribute'] : '');

            if(!empty($attribute)) {
                if(!is_array($attribute)) {
                    $this->setAttribute($key, $attribute);
                } else {
                    foreach( $attribute as $aKey => $attr ) {
                        $aKey = $aKey == '--' ? $key : $key.'.'.$aKey;
                        $this->setAttribute($aKey, $attr);
                    }
                }
            }
        }
    }
    
    /**
     * function setAttribute($key, $attr)
     *
     * validate_attributesに指定のキー、文字列をセットします
     * すでにキーに対応する値が存在しており、空ではない場合はスルーします
     *
     * param string $key  attributeのキー名
     * param string $attr attributeの値
     */
    protected function setAttribute($key, $attr)
    {
        if( !isset($this->validate_attributes[$key]) || empty($this->validate_attributes[$key])) {
            $this->validate_attributes[$key] = $attr;
        }
    }
    
    /**
     * function initMessages
     *
     * 保存された設定値からバリデートメッセージ要のメンバ変数を構築する
     * 各バリデートで設定されている値があればそちらが優先となる
     * 同名定義は先勝ち
     *
     */
    protected function initMessages()
    {
        $settings = $this->validSettings;
        
        foreach($settings as $key => $row) {
            $message = isset($row['validate']['message']) ? $row['validate']['message'] : [];
            foreach($message as $messageKey => $msg) {
                $mkey = $key.'.'.$messageKey;
                if( !isset($this->validate_messages[$mkey]) || empty($this->validate_messages[$mkey])) {
                    $this->validate_messages[$mkey] = $msg;
                }
            }
        }
    }
    
    /**
     * function makeValidateRules
     *
     * バリデートルールを構築します
     * 設定値に基づいてバリデーションルールを構築
     * laravelのバリデーションと同様、文字列と配列どちらで定義することも
     * 可能としている
     *
     * 文字列定義のバリデーションは文字列定義で配列定義のものは配列定義で構築するため
     * 構築されるrulesとしては、文字列定義のものと配列定義のものが混じる場合もある
     */
    protected function makeValidateRules($data, $flow, $id)
    {
        $ret = [];
        $settings = $this->validSettings;
        
        foreach($settings as $key => $row) {
            $valid = $row['validate'];
            $rules = $this->getValidateRuleAtForm($key, $valid, $data, $flow, $id);
            if(!empty($rules)) {
                foreach($rules as $validKey => $rule) {
                    $ret[$validKey] = $rule;
                }
            }
        }
        return $ret;
    }
    
    /**
     * function getValidateRuleAtForm
     *
     * １フォームあたりのバリデーションルールを構築する
     * フォームエレメントの定義でvalidate->rulesが設定されていれば配列型定義の処理を
     * 設定されてなければvalidate->ruleから文字列型の定義の処理を行う
     * rules、rule両方設定されている場合rulesの方を使い、ruleの定義は無視される
     *
     */
    protected function getValidateRuleAtForm($key, $valid, $data, $flow, $id)
    {
        if( isset($valid['rules']) && is_array($valid['rules']) ) {
            return $this->getValidateRuleArray($key, $valid, $data, $flow, $id);
        } else {
            return $this->getValidateRuleString($key, $valid, $data, $flow, $id);
        }
    }
    
    /**
     * function getValidateRuleArray
     *
     * １フォームあたりの配列型のバリデーション定義を構築する
     * typeに応じたルール構築メソッドが定義されている場合、そのメソッド呼び出しをおこなってルール構築を行う
     *
     * rules => [
     *     [
     *         'type'      => 'バリデーションの種類、原則はルール名称',
     *         'ruleType'  => 'ルール名称がバリデーションの種類と異なる場合にこの変数を定義する',
     *         'condition' => '各バリデーションの種類に対する条件値（例：max:xxxxのxxxx部分）',
     *         'flow'      => '文字列もしくは配列で対象とするflowを定義する、flowに関係なく常に必要なルールにはこの変数自体記載不要',
     *         'options'   => typeに応じたルール構築メソッドに追加で引数を渡すための配列、現在未使用,
     *     ],
     *     ・・・・必要なルール分だけ設定
     * ]
     *
     * typeが必須。条件を必要とするtypeの場合、conditionも必須
     * ただし、conditionが必須かどうかはここでは判断できないので、必須なのに定義がされていなければ、別の場所でExceptionが発生する
     *
     * 戻り値は['required', 'max:36']のようなルール配列となる
     *
     */
    protected function getValidateRuleArray($key, $valid, $data, $flow, $id)
    {
        $ret = [];

        if( isset($valid['rules']) && is_array($valid['rules']) ) {
            foreach( $valid['rules'] as $rule ) {
                $vKey = $this->getValidKey($key, $rule);
                if(!isset($ret[$vKey])) {
                    $ret[$vKey] = [];
                }
                $rule = $this->getValidateRule($rule, $data, $flow, $id);
                if( !empty($rule) ) {
                    $ret[$vKey][] = $rule;
                }
            }
        }
        return $ret;
    }

    /**
     * function getValidateRuleString
     *
     * １フォームあたりの文字列型のバリデーション定義を構築する
     *
     * typeがあればtypeに応じたメソッド呼び出しをおこなってルール構築をする
     * rulesと違い、１フォームあたりに１typeしか定義できない仕様
     * 複数のtype定義メソッドを使用してルール構築をすることが出来ない
     *
     * 原則的にruleあるいはrule_editの値をそのまま返す構造
     *
     * 戻り値は　'required|max:36'　のような形
     */
    protected function getValidateRuleString($key, $valid, $data, $flow, $id)
    {
        $ret = [];
        $key = $this->getValidKey($key, $valid);
        $ret[$key] = $this->getValid($valid, $flow);
        if( isset($valid['type']) ) {
            $ret[$key] = $this->getValidateRuleByType($valid, $data, $flow, $id);
        }
        
        return $ret;
    }
    
    /**
     *
     *
     */
    protected function getValidKey($key, $valid)
    {
        return isset($valid['key']) && !empty($valid['key']) ? sprintf('%s.%s', $key, $valid['key']) : $key;
    }
    
    /**
     * function getValidateRule
     *
     * 配列型の１ルールを構築するメソッド
     * 原則は type:condition という形の文字列を返す
     * conditionがない場合はtypeを返すのみ
     *
     * typeに応じたメソッド定義がある場合、condition生成にそのメソッドが使用される
     * また、ruleTypeが定義されている場合、condition生成のメソッド判定にはtypeを使い、
     * ルール構築の type:condition のtypeには　ruleTypeを使う
     *
     * type ->     バリデーションルール構築メソッドのタイプ
     * ruleType -> バリデーションのタイプ
     * という形　ただし、ruleTypeがなければtypeがバリデーションのタイプになる
     * ※バリデーションのタイプとあわせてtypeに対応するメソッドを定義すればよいのだけど、
     * 　共通処理の切り出しをしていくと異なってくるパターンが存在するのでそれに対応した形
     *
     * typeは必須。flowが設定されている場合、flowが許可されていなければ処理をしない
     *
     */
    protected function getValidateRule($rule, $data, $flow, $id)
    {
        $ret = '';
        
        if( !isset($rule['type']) || empty($rule['type']) ) {
            return $ret;
        }
        $ruleFlow = isset($rule['flow']) ? $rule['flow'] : '';
        if( !empty($ruleFlow) && !$this->matchFlow($ruleFlow, $flow)) {
            return $ret;
        }
        
        $type = isset($rule['ruleType']) && !empty($rule['ruleType']) ? $rule['ruleType'] : $rule['type'];
        $cond = $this->getConditionByType($rule, $data, $flow, $id);
        $ret = $type;
        if( !empty($cond) || $cond === 0) {
            $ret = sprintf('%s:%s', $type, $cond);
        }
        
        return $ret;
    }
    
    /**
     * function matchFlow
     *
     * flowが設定値と合致するかどうかをチェックする
     * 配列の場合はin_arrayで確認する
     *
     * 合致：true、合致しない：false　となる
     *
     */
    protected function matchFlow($ruleFlow, $flow)
    {
        $ret = false;
        
        if( empty($ruleFlow) ) {
            $ret = true;
        } elseif( is_array($ruleFlow) ) {
            if( in_array($flow, $ruleFlow) ) {
                $ret = true;
            }
        } elseif($ruleFlow == $flow) {
            $ret = true;
        }
        
        return $ret;
    }
    
    /**
     * function getValidateRuleByType
     *
     * typeに応じたバリデーションルールを取得する
     * 文字列型の場合のみ使用される
     *
     */
    protected function getValidateRuleByType($valid, $data, $flow, $id)
    {
        $valid['condition'] = $this->getValid($valid, $flow);
        return $this->getConditionByType($valid, $data, $flow, $id);
    }
    
    /**
     * function getConditionByType
     *
     * typeに応じたメソッド定義があるかどうかを調べあればそのメソッドを使って
     * 条件を取得する
     * なければ、条件をそのまま返す
     *
     * 条件は原則として、バリデーションルールのフォーマット
     * optionsはルールフォーマットに対する置換文字設定を基本的に想定
     * options['model']やoptions['table']としuniqueルールなどで使えるようにする想定
     *
     */
    protected function getConditionByType($rule, $data, $flow, $id)
    {
        $ret = isset($rule['condition']) ? $rule['condition'] : '';
        $method = sprintf('getValidateType%s', ucfirst($rule['type']));
        $params = isset($rule['options']) ? $rule['options'] : null;

        if(is_callable([$this, $method])) {
            $ret = call_user_func([$this, $method], $ret, $data, $flow, $id, $params);
        }
        
        return $ret;
    }
    
    /**
     * function getValidateTypeIn
     *
     * type in　の際に、optionから設定名を取得して対象の条件を作成する
     *
     */
    protected function getValidateTypeIn($condition, $data, $flow, $id, $params)
    {
        $cond = $condition;
        if(isset($params['select'])) {
            $cond = implode(',', getSelectValues($params['select']));
        }
        
        return $cond;
    }

    
    /**
     * function getValid
     *
     * バリデーションルール文字列を取得する
     * select型の場合、inルールを追加している
     *
     */
    protected function getValid($valid, $flow)
    {
        $validKey = 'rule';
        if( $flow == 'edit' && isset($valid['rule_edit']) ) {
            $validKey = 'rule_edit';
        }
        $ret = $valid[$validKey];
        if( isset($valid['select']) ) {
            $ret .= '|in:'.implode(',', getSelectValues($valid['select']));
        }

        return $ret;
    }
}
