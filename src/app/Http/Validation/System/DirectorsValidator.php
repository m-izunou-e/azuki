<?php
namespace Azuki\App\Http\Validation\System;

/**
 * DirectorsValidator バリデーション
 *
 * @copyright Copyright 2019 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Validation\SimpleUserValidator;
use Azuki\App\Contracts\Http\Validation\System\DirectorsValidator as ContractsInterface;
use Azuki\App\Contracts\Models\Directors;

class DirectorsValidator extends SimpleUserValidator implements  ContractsInterface
{
    /**
     *
     *
     *
     */
    protected function getUserModel()
    {
        return Directors::class;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules($data, $flow = null, $id = null)
    {
        $rules = parent::rules($data, $flow, $id);

        // ID:1に対するロール値変更・無効化の制限を追加する
        if( $id == 1 ) {
            // role：１はseedで設定しているスーパーバイザーのvalue
            $rules['role']   = 'required|in:1';
            // enable：１はmasterで定義されている「有効」のvalue
            $rules['enable'] = 'required|in:1';
        }

        return $rules;
    }

    /**
     * SSet custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        $msg = parent::messages();
        
        $msg['role.in']   = ':attributeが設定できない値です';
        $msg['enable.in'] = ':attributeが設定できない値です';
        
        return $msg;
    }
}
