<?php
namespace Azuki\App\Http\Validation\System;

/**
 * CommonRolesValidator バリデーション
 *
 * @copyright Copyright 2019 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Validation\RolesValidator;
use Azuki\App\Contracts\Http\Validation\System\CommonRolesValidator as ContractsInterface;
use Azuki\App\Contracts\Models\CommonRoles;

class CommonRolesValidator extends RolesValidator implements ContractsInterface
{
    /**
     *
     *
     */
    protected function getRoleTableName()
    {
        return app(CommonRoles::class)->getTable();
    }
}
