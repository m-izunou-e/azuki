<?php
namespace Azuki\App\Http\Validation\System;

/**
 * DirectorsValidator バリデーション
 *
 * @copyright Copyright 2019 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Validation\System\DirectorsValidator;
use Azuki\App\Contracts\Http\Validation\System\ProfileValidator as ContractsInterface;

class ProfileValidator extends DirectorsValidator implements  ContractsInterface
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules($data, $flow = null, $id = null)
    {
        $rules = parent::rules($data, $flow, $id);
        unset($rules['role']);
        unset($rules['enable']);
        
        return $rules;
    }
}
