<?php
namespace Azuki\App\Http\Validation\System;

/**
 * UsersValidator バリデーション
 *
 * @copyright Copyright 2019 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Validation\UsersValidator as Validator;
use Azuki\App\Contracts\Http\Validation\System\UsersValidator as ContractsInterface;

class UsersValidator extends Validator implements ContractsInterface
{
}
