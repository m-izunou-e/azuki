<?php
namespace Azuki\App\Http\Validation\System;

/**
 * ManageRolesValidator バリデーション
 *
 * @copyright Copyright 2019 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Validation\RolesValidator;
use Azuki\App\Contracts\Http\Validation\System\ManageRolesValidator as ContractsInterface;
use Azuki\App\Contracts\Models\ManageRoles;

class ManageRolesValidator extends RolesValidator implements ContractsInterface
{
    /**
     *
     *
     */
    protected function getRoleTableName()
    {
        return app(ManageRoles::class)->getTable();
    }
}
