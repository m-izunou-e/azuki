<?php
namespace Azuki\App\Http\Validation\System;

/**
 * SystemRolesValidator バリデーション
 *
 * @copyright Copyright 2019 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Validation\RolesValidator;
use Azuki\App\Contracts\Http\Validation\System\SystemRolesValidator as ContractsInterface;
use Azuki\App\Contracts\Models\SystemRoles;

class SystemRolesValidator extends RolesValidator implements ContractsInterface
{
    /**
     *
     *
     */
    protected function getRoleTableName()
    {
        return app(SystemRoles::class)->getTable();
    }
}
