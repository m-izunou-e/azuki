<?php
namespace Azuki\App\Http\Validation\System;

/**
 *
 * @copyright Copyright 2019 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Validation\Validator;
use Azuki\App\Contracts\Http\Validation\System\OrganizationsValidator as ContractsInterface;

class OrganizationsValidator extends Validator implements  ContractsInterface
{
}
