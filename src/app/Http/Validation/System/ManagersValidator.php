<?php
namespace Azuki\App\Http\Validation\System;

use Azuki\App\Http\Validation\ManagersValidator as Validator;
use Azuki\App\Contracts\Http\Validation\System\ManagersValidator as ContractsInterface;

class ManagersValidator extends Validator implements ContractsInterface
{
}
