<?php
namespace Azuki\App\Http\Validation;
/**
 * 管理画面アカウントのバリデーションを定義したクラス
 *
 * エラー時のリダイレクトURL
 * 属性値の日本語表記の設定
 * バリデーションルールを記載する
 *
 */

use Azuki\App\Http\Validation\SharedValidator;

abstract class SimpleUserValidator extends SharedValidator
{
    /**
     * 所属有効でもEmailアドレスは所属毎ではなくユニークにする必要がある
     * →そうしないとパスワードリマインダーが機能しない
     * →また、組織管理層において１ユーザーが複数の所属に属することは想定していない
     *
     */
    public function getValidateTypeEmail($format, $data, $flow, $id, $params = null)
    {
        $params['model'] = $this->getUserModel();
        return $this->getValidateTypeUniqueOnTable($format, $data, $flow, $id, $params);
    }
    /**
     *
     *
     */
    public function getValidateTypeLoginid($format, $data, $flow, $id, $params = null)
    {
        $params['model'] = $this->getUserModel();
        return $this->getValidateTypeUniqueOnTable($format, $data, $flow, $id, $params);
    }
    
    /**
     *
     *
     */
    abstract protected function getUserModel();
}
