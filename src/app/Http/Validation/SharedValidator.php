<?php
namespace Azuki\App\Http\Validation;
/**
 * 管理画面アカウントのバリデーションを定義したクラス
 *
 * エラー時のリダイレクトURL
 * 属性値の日本語表記の設定
 * バリデーションルールを記載する
 *
 */

use Azuki\App\Http\Validation\Validator;
use Azuki\App\Services\ExtendLog;

//use Azuki\App\Contracts\Http\Validation\System\OrganizationsValidator;
//use Azuki\App\Contracts\Http\Validation\System\AccessLogsValidator;

class SharedValidator extends Validator
//    implements OrganizationsValidator//,
//               AccessLogsValidator
{
    /**
     *
     *
     */
    public function getValidateTypeUniqueOnTable($format, $data, $flow, $id, $params = null)
    {
        if(is_null($params) || !isset($params['model'])) {
            // ログを記録してExceptionを投げる
            $msg = sprintf(
                "[%s] getValidateTypeUniqueOnTable is necessary parameter model. \n format is [%s]",
                'SharedValidator',
                $format
            );
            $this->validateErrorThrow($msg);
        }
    
        $model = $params['model'];
        if($flow == "regist") {
            $id = NULL;
        }
        return sprintf($format, $model, $id);
    }

    /**
     *
     *
     * uniqueOnBelong:%s[\\Azuki\App\\Models\\モデル],カラム名,%s[belong],%s[id]
     *
     */
    public function getValidateTypeUniqueOnBelong($format, $data, $flow, $id, $params = null)
    {
        if(is_null($params) || !isset($params['model'])) {
            // ログを記録してExceptionを投げる
            $msg = sprintf(
                "[%s] getValidateTypeUniqueOnBelong is necessary parameter model. \n format is [%s]",
                'SharedValidator',
                $format
            );
            $this->validateErrorThrow($msg);
        }
    
        $model = $params['model'];
        $belong = $this->getTargetBelong($data);
        if($flow == "regist") {
            $id = NULL;
        }
        return sprintf($format, $model, $belong, $id);
    }
    
    /**
     *
     *
     */
    protected function getTargetBelong($data)
    {
        $belong = NULL;
        if(supportOrganizations()) {
            $belong = isset($data['belong']) ? $data['belong'] : getLoginUserBelong();
        }
        
        return $belong;
    }
    
    /**
     *
     *
     *
     */
    protected function validateErrorThrow($msg)
    {
        app('ExtendLog')->systemLogWriteDb(SL_KIND_VALIDATE, ExtendLog::ERROR, $msg);
        $e = new \Exception($msg);
        app('ExtendLog')->exception($e);
        throw $e;
    }
}
