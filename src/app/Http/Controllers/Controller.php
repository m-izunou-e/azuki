<?php
namespace Azuki\App\Http\Controllers;

/**
 * フロント画面を構成するコントローラの抽象クラス
 * 共通設定・共通メソッドなどをこのクラスで実装する
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

#use App\Http\Controllers\Controller as SystemController;
use Illuminate\Routing\Controller as SystemController;
use Azuki\App\Models\AccessLogs;
use Azuki\App\Services\ExtendLog;

/**
 * class BaseController
 *
 */
abstract class Controller extends SystemController
{
    /**
     * string vendorPrefix
     *
     * ベンダープレフィックス
     * standard.viewVendorPrefixにて上書き可能なテンプレートにつけるベンダープレフィックスの指定
     */
    protected $vendorPrefix = 'azuki::';
    
    /**
     * boolean addVendorPrefix
     *
     * ベンダープレフィックスをviewの指定に追加するかどうかのフラグ
     * Azukiシステムを使用した側でプレフィックスがついてほしくない場合があるのでこの値で
     * 付加を制御できるようにしている
     */
    protected $addVendorPrefix = true;
    
    /**
     *
     *
     */
    protected function setVendorPrefix()
    {
        $vendorPrefix = app(BASE_APP_ACCESSOR)->get('standard.viewVendorPrefix');
        if(!is_null($vendorPrefix)) {
            $this->vendorPrefix = $vendorPrefix;
        }
    }
    
    /**
     *
     *
     */
    protected function getViewPrefix()
    {
        return ($this->addVendorPrefix ? $this->vendorPrefix : '') . $this->viewPrefix;
    }
    
    /**
     * function setPageInfomation
     *
     * ページ情報を初期化します
     * 現状はページタイトルとページ名称について、transをした値が設定されているかを
     * 確認し設定されていればその内容で更新する処理をしています
     *
     */
    protected function setPageInfomation($prefix = null)
    {
        $prefix = is_null($prefix) ? $this->prefix : $prefix;
        $key = $prefix.'.'.(isset($this->controllerName) ? $this->controllerName : 'index');
        $this->pageTitle      = getTransPageTitle($key, $this->pageTitle);
        $this->pageNamePrefix = getTransPageName($key, isset($this->pageNamePrefix) ? $this->pageNamePrefix : $this->pageTitle);
    }
    
    /**
     *
     *
     *
     */
    protected function recordAccessLogs($request)
    {
        $recordAccessLogs = app(BASE_APP_ACCESSOR)->get('standard.record_access_logs');
        if(!$recordAccessLogs) {
            return ;
        }
        $user = $request->user();

        $userInfo = is_null($user) ? null : $user->toArray();
        $userInfo = is_null($userInfo) ? null : serialize($userInfo);
        $accessLog = [
            'url'         => $this->getAccessUrl($request),
            'user_id'     => is_null($user) ? null : $user->id,
            'user_kind'   => is_null($user) ? USER_KIND_NONE : getUserKind( $user ),
            'user_info'   => $userInfo,
            'access_info' => $this->getAccessInformation($request),
        ];

        $obj = new AccessLogs();
        $obj->_create($accessLog);
    }
    
    /**
     *
     *
     *
     */
    protected function getAccessUrl($request)
    {
        return preg_replace('/http.?:\/\/[^\/]*\//', '', $request->url());
    }
    
    /**
     *
     *
     *
     */
    protected function getAccessInformation($request)
    {
        return serialize($request->server->all());
    }
    
    /**
     * function setShareIcon
     *
     * 管理画面のiconを設定する
     * 設定はconfigで行う形にしておき、将来的にこの部分の変更だけで
     * DBで管理するなども可能にしておく
     *
     */
    protected function setShareIcon()
    {
        $icons = app(BASE_APP_ACCESSOR)->get('icon');
        
        foreach($icons as $key => $icon) {
            view()->share( $key, $icon );
        }
    }
    
    /**
     *
     *
     *
     */
    protected function getAnalyticsTags()
    {
        $tags = app(BASE_APP_ACCESSOR)->get('analytics_tags.google');
        return $tags;
    }

    /**
     * 
     */
    protected function logInfo($msg)
    {
        $this->logWrite(ExtendLog::INFO, $msg);
    }

    /**
     * 
     */
    protected function logWarn($msg)
    {
        $this->logWrite(ExtendLog::WARNING, $msg);
    }

    /**
     * 
     */
    protected function logErr($msg)
    {
        $this->logWrite(ExtendLog::ERROR, $msg);
    }
    
    /**
     * function logWrite
     *
     */
    protected function logWrite($level, $msg)
    {
        static $exLog = null;
        if(is_null($exLog)) {
            $exLog = app('ExtendLog');
        }
        $exLog->write($level, $this->messageFormat($msg));
    }
    
    /**
     * function messageFormat
     *
     * ログメッセージに拡張フォーマット用メソッド
     *
     */
    protected function messageFormat($msg)
    {
        return $msg;
    }
}
