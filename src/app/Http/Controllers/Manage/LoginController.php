<?php
namespace Azuki\App\Http\Controllers\Manage;

/**
 * ログイン処理を行うコントローラ
 * 管理画面用
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Controllers\Manage\BaseController;
use Azuki\App\Http\Modules\Authenticate;
use Illuminate\Http\Request;

/**
 * class 
 *
 */
class LoginController extends BaseController
{
    use Authenticate;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo;
    protected $loginPath;
    protected $logoutPath;

    /**
     * ページ名前とする文字列
     *
     * string pageTitle
     */
    protected $pageTitle = 'ログイン';

    /**
     * コントローラ名として使用する文字列
     *
     * string controllerName
     */
    protected $controllerName = 'login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
//        $this->middleware('guest.manage')->except('getLogout');
        
        $story = '/'.trim($this->story, '/');
        $this->redirectTo = $story;
        $this->loginPath  = $story . '/login';
        $this->logoutPath = $story . '/login';
    }

    /**
     * function getIndex
     * 
     * @return void
     */
    public function getIndex()
    {
        return view($this->getViewPrefix().'login');
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        if( supportOrganizations() && is_null($user->belong) ) {
            $this->guard()->logout();
            $request->session()->invalidate();
            return $this->sendFailedLoginResponse($request);
        }
        // ログイン成功時に呼び出されるのでここでログインログを記録する
        // return値はfalseかリダイレクト先のレスポンスにする必要がある
        $this->recordLoginLogs($user, USER_KIND_MANAGERS);
    }

    /**
     * function credentials
     *
     * ログイン処理時に使用するデータを作成する
     * ログインを許可するユーザーの条件をここで追加することができるが
     * 複雑な条件は設定できない
     *
     */
    protected function credentials(Request $request)
    {
        $username = $this->username();
    
        return [
            $username  => $request->get($username),
            'password' => $request->get('password'),
            'enable'   => 1,
        ];
    }

}
