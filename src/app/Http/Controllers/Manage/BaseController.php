<?php
namespace Azuki\App\Http\Controllers\Manage;

/**
 * 管理画面のコントローラの基本となる抽象クラス
 * 共通のメンバ変数、処理、メソッドの記述を行う
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Controllers\ManagementController;
use Azuki\App\Http\Controllers\ManagementTrait;
use Azuki\App\Services\ManageBasicCtrlSupporter;
use Azuki\App\Http\Modules\CtrlSupporterParameterHelper;
use Azuki\App\Contracts\Models\ManageRoles;

/**
 * class BaseController
 *
 */
abstract class BaseController extends ManagementController
{
    /**
     * authのドライバー種類
     */
    protected $authKind = 'manage';

    /**
     * viewのテンプレート指定時のプレフィックス文字列
     *
     * string viewPrefix
     */
    protected $viewPrefix = 'manage.';
    
    /**
     * ルーティングで設定されているプレフィックス
     *
     * string prefix
     */
    protected $prefix = 'manage';

    /**
     * ページ名前とする文字列
     *
     * string pageTitle
     */
    protected $pageTitle = '';
    
    /**
     * $globalConfig
     *
     * 全体手な設定
     */
    protected $globalConfig = [
        'globalMenu' => true,
        'headInfo'   => true
    ];

    /**
     * 画面構成要素
     *
     * array $elements
     */
    protected $elements = [];

    use ManagementTrait;
    use CtrlSupporterParameterHelper;

    /**
     * 基底クラスのコンストラクタ.
     *
     * @return void
     */
    public function __construct()
    {
        $this->initController();
    }
    
    /**
     * 管理画面の各種機能を提供するCtrlSuppoterクラスを設定する
     * 各設定値は各コントローラーから配列で受け取る
     *
     */
    protected function setCtrlSuppoter(Array $ctrlSuppoterInitSetting)
    {
        $this->elements = $this->loadModuleElements();
    
        $ctrlSupporter = new ManageBasicCtrlSupporter();
        $ctrlSupporter->init($ctrlSuppoterInitSetting);
        $this->ctrlSupporter = $ctrlSupporter;
    }
    
    /**
     *
     *
     *
     */
    protected function getMenuCheckUrl($menu)
    {
        $subDir = getSubDir(getAccessDomainName());
        return $subDir['manage'].'/'.$menu['url'];
    }
    
    /**
     *
     *
     *
     */
    protected function getUserKind()
    {
        return USER_KIND_MANAGERS;
    }
    
    /**
     *
     *
     */
    protected function getRoleAuthoritiesModel()
    {
        return app(ManageRoles::class);
    }
}
