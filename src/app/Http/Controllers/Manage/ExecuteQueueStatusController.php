<?php
namespace Azuki\App\Http\Controllers\Manage;

/**
 * ExecuteQueueStatusController クラス
 *
 * @copyright Copyright 2019 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Controllers\Manage\BaseController;
use Azuki\App\Http\Modules\ActionTrait\ListActionPackageWithDetail;
//use Azuki\App\Contracts\Http\Validation\Manage\ExecuteQueueStatusValidator as Validator;
use Azuki\App\Http\Validation\SharedValidator as Validator;
use Azuki\App\Contracts\Models\ExecuteTargetQueue as Model;

/**
 * class AccessLogsController
 *
 *
 */
class ExecuteQueueStatusController extends BaseController
{
    /**
     * サポートサービスのオブジェクト
     *
     * @var CtrlCupporter ctrlSupporter
     */
    protected $ctrlSupporter;
    
    /**
     * 画面構成に必要な要素のモジュールの定義
     *
     */
    protected $elementModues = [
        \Azuki\App\Http\Modules\Forms\ExecuteQueueStatus::class,
        \Azuki\App\Http\Modules\Forms\BasicElements::class,
    ];

    /**
     * 基本のアクションセットのトレイトを読み込みます
     */
    use ListActionPackageWithDetail;

    /**
     * ページのtitleタグに表示する文字列
     *
     * string pageTitle
     */
    protected $pageTitle = 'コマンド実行処理状況';

    /**
     * ページのh1に表示する文字列
     *
     * string pageNamePrefix
     */
    protected $pageNamePrefix = 'コマンド実行処理状況';

    /**
     * コントローラ名として使用する文字列
     *
     * string controllerName
     */
    protected $controllerName = 'execute-queue-status';
    
    /**
     * 一覧の表示順設定
     * 必要なければ変数定義自体不要
     *
     * array viewOrder
     */
    protected $viewOrder = ['order' => 'created_at', 'condition' => ['created_at' => 'desc']];
    
    /**
     * 一覧の操作アイコン設定
     * 必要なければ変数定義自体不要
     *
     * array ctrlCondition
     */
    protected $ctrlCondition = [
        'edit' => [
            'default' => false,
        ],
        'delete' => [
            'default' => false,
        ],
    ];
    
    /**
     * デートタイムピッカーの使用有無
     * 必要なければ変数定義自体不要
     *
     * boolean needDatetimePicker
     */
    protected $needDatetimePicker = true;
    
    /**
     * 各画面タイプごとの要素のオーダー設定
     *
     * array $elementsOrder
     */
    protected $elementsOrder = [
        'search' => ['kind', 'status', 'period' ],
        'detail' => ['id', 'kind', 'status', 'line', 'count', 'success', 'fail', 'message' ],
        'list'   => ['id', 'kind', 'status', 'line', 'count', 'success', 'fail', 'created', 'ctrl' ],
    ];

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Model $model, Validator $validator)
    {
        parent::__construct();
        
        $this->setCtrlSuppoter(
            $this->getCtrlSuppoterParameters($model, $validator)
        );
        $this->elements['created']['form']['title']['name'] = '日時';
        $this->elements['created']['list']['created']['orderable'] = true;
        $this->elements['ctrl']['list']['ctrl']['ctrl'] = [CTRL_DETAIL/*, CTRL_RESTORE*/];
    }
    
    /**
     * function getCtrlSuppoterTemplatePrefix
     *
     *
     */
    protected function getCtrlSuppoterTemplatePrefix()
    {
        return $this->getViewPrefix().'templates.';
    }
}
