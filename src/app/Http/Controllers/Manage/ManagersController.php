<?php
namespace Azuki\App\Http\Controllers\Manage;

/**
 * 管理者ユーザーを処理するコントローラ
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Controllers\Manage\BaseController;
use Azuki\App\Http\Modules\ActionTrait\BaseActionPackageWithDetail;
use Azuki\App\Contracts\Http\Validation\Manage\ManagersValidator as Validator;
use Azuki\App\Contracts\Models\Managers as Model;

/**
 * class IndexController
 *
 * 管理者ユーザーを実装するクラス
 */
class ManagersController extends BaseController
{
    /**
     * サポートサービスのオブジェクト
     *
     * @var CtrlCupporter ctrlSupporter
     */
    protected $ctrlSupporter;
    
    /**
     * 画面構成に必要な要素のモジュールの定義
     *
     */
    protected $elementModues = [
        \Azuki\App\Http\Modules\Forms\BasicElements::class,
        \Azuki\App\Http\Modules\Forms\Managers::class,
    ];

    /**
     * 基本のアクションセットのトレイトを読み込みます
     */
    use BaseActionPackageWithDetail;

    /**
     * ページのtitleタグに表示する文字列
     *
     * string pageTitle
     */
    protected $pageTitle = 'スタッフ管理';

    /**
     * ページのh1に表示する文字列
     *
     * string pageNamePrefix
     */
    protected $pageNamePrefix = 'スタッフ';

    /**
     * コントローラ名として使用する文字列
     *
     * string controllerName
     */
    protected $controllerName = 'managers';

    /**
     * 各画面タイプごとの要素のオーダー設定
     *
     * array $elementsOrder
     */
    protected $elementsOrder = [
        'search' => ['name', 'login_id', 'email', 'role', 'enable' ],
        'detail' => ['id', 'created', 'name', 'login_id', 'email', 'password', 'role', 'enable' ],
        'form'   => ['name', 'login_id', 'email', 'password', 'password_conf', 'role', 'enable' ],
        'list'   => ['id', 'name', 'login_id', 'email', 'created', 'role', 'enable', 'ctrl', ],
    ];

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Model $model, Validator $validator)
    {
        parent::__construct();

        $this->setCtrlSuppoter(
            $this->getCtrlSuppoterParameters($model, $validator)
        );
        
        $this->ctrlSupporter->bind('beforeRegistData', [$this, 'beforeRegistData']);
    }
    
    /**
     * function beforeRegistData
     *
     * 所属が有効な場合に所属を追加する
     *
     */
    public function beforeRegistData($data, $flow)
    {
        if(supportOrganizations()) {
            $data['belong'] = getLoginUserBelong();
        }
        
        return $data;
    }
}
