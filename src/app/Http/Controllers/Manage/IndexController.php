<?php
namespace Azuki\App\Http\Controllers\Manage;

/**
 * 管理画面トップを処理するコントローラ
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Controllers\Manage\BaseController;

/**
 * class IndexController
 *
 * 管理画面側トップページを実装するクラス
 */
class IndexController extends BaseController
{

    /**
     * ページ名前とする文字列
     *
     * string pageTitle
     */
    protected $pageTitle = 'ダッシュボード';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * function getIndex
     *
     * @return void
     */
    public function getIndex()
    {
        return view($this->getViewPrefix().'index');
    }
}
