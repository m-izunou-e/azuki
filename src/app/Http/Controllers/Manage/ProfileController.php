<?php
namespace Azuki\App\Http\Controllers\Manage;

/**
 * 管理者ユーザーを処理するコントローラ
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Controllers\Manage\BaseController;
use Azuki\App\Http\Modules\ActionTrait\TraitParts\TraitProfileAction;
use Azuki\App\Contracts\Http\Validation\Manage\ManagersValidator as Validator;
use Azuki\App\Contracts\Models\Managers as Model;

/**
 * class IndexController
 *
 * 管理者ユーザーを実装するクラス
 */
class ProfileController extends BaseController
{
    /**
     * サポートサービスのオブジェクト
     *
     * @var CtrlCupporter ctrlSupporter
     */
    protected $ctrlSupporter;
    
    /**
     * 画面構成に必要な要素のモジュールの定義
     *
     */
    protected $elementModues = [
        \Azuki\App\Http\Modules\Forms\Managers::class,
    ];

    /**
     * 基本のアクションセットのトレイトを読み込みます
     */
    use TraitProfileAction;

    /**
     * ページのtitleタグに表示する文字列
     *
     * string pageTitle
     */
    protected $pageTitle = 'スタッフプロフィール';

    /**
     * ページのh1に表示する文字列
     *
     * string pageNamePrefix
     */
    protected $pageNamePrefix = 'スタッフプロフィール';

    /**
     * コントローラ名として使用する文字列
     *
     * string controllerName
     */
    protected $controllerName = 'profile';
    
    /**
     * 一覧の操作アイコン設定
     * 必要なければ変数定義自体不要
     *
     * array ctrlCondition
     */
    protected $ctrlCondition = [
        'delete' => ['default' => false],
    ];
    
    /**
     * 追加のview設定
     *
     * array addViews
     */
    protected $addViews = [
        'toBack'     => 'detail',
        'canBack'    => false,
        'isProfile'  => true,
        'displayResultOnDetail' => true,
    ];

    /**
     * 各画面タイプごとの要素のオーダー設定
     *
     * array $elementsOrder
     */
    protected $elementsOrder = [
        'detail' => ['name', 'login_id', 'email', 'password', 'role', 'enable' ],
        'form'   => ['name', 'login_id', 'email', 'password', 'password_conf' ],
    ];

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Model $model, Validator $validator)
    {
        parent::__construct();
        
        $this->setCtrlSuppoter(
            $this->getCtrlSuppoterParameters($model, $validator)
        );
        
        $this->ctrlSupporter->bind('beforeRegistData', [$this, 'beforeRegistData']);
        
        $this->ctrlSupporter->bind('getNameListRoute', function(){
            return 'manage.profile.detail';
        });
    }
    
    /**
     * function beforeRegistData
     *
     * 所属が有効な場合に所属を追加する
     *
     */
    public function beforeRegistData($data, $flow)
    {
        if(supportOrganizations()) {
            $data['belong'] = getLoginUserBelong();
        }
        
        return $data;
    }
}
