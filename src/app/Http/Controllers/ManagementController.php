<?php
namespace Azuki\App\Http\Controllers;

/**
 * 管理画面のコントローラの基本となる抽象クラス
 * 共通のメンバ変数、処理、メソッドの記述を行う
 *
 * システム管理、サイト管理の２階層ともに共通の部分として切り出ししている。
 * 各階層ごとに共通の部分は各階層のBaseControllerに記述する方針
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Illuminate\Support\Str;
use Azuki\App\Http\Controllers\Controller;

/**
 * class BaseController
 *
 */
abstract class ManagementController extends Controller
{
    /**
     *
     *
     */
    protected $roleAuth;

    /**
     * 基底クラスのコンストラクタ.
     *
     * @return void
     */
    public function initController()
    {
        $this->setVendorPrefix();
        $this->setPageInfomation();
    
        $auth = app('auth');
        // Authのデフォルトガードを変更する
        $auth->shouldUse($this->authKind);
        $this->auth = $auth;
        
        $router = app('router')->getCurrentRoute();
        $this->story = !empty($router) ? $router->action['prefix'] : '/';

        view()->share( "story", '/'.trim($this->story, '/').'/');
        view()->share( "prefix", $this->prefix);
        view()->share( "pageTitle",  $this->pageTitle );
        view()->share( "controller", '' );
        view()->share( "globalConfig", $this->globalConfig);
        view()->share( "vendorPrefix", $this->vendorPrefix );
        view()->share( "analyticsTags", $this->getAnalyticsTags() );

        // iconの設定を行う
        $this->setShareIcon();

        // 5.3以降$auth->user()の扱いが変わり、コンストラクタでログインユーザー情報の取得ができなくなった。
        // 代わりに以下のような方法で取得する仕組みが提供されている。
        $this->middleware(function($request, $next){
            $this->recordAccessLogs($request);
            $this->callbackMiddleware($request);
            return $next($request);
        });
    }
    
    /**
     * function callbackMiddleware
     *
     * ミドルウェアからコールバックされた際に実行するメソッド
     * ユーザーデータの設定やユーザーの状態に応じて変化するデータの生成、アサインを行う
     *
     * @param $request 
     */
    protected function callbackMiddleware($request)
    {
        // ユーザーデータの設定
        $this->user = $request->user();
        view()->share( "user", $this->user );
        
        // TODO アクセス元によるアクセス制限を実装する
        // $request->server->get('REMOTE_ADDR')　を使って判定
        
        
        // メニュー、ヘルプの設定
        view()->share( "globalMenu",    $this->getGlobalMenu() );
        view()->share( "shortcutMenu",  $this->getShortcutMenu() );
        view()->share( "importantHelp", $this->getImportantHelp() );
    }

    /**
     * function getGlobalMenu
     *
     * グローバルメニューを返す
     * ログインユーザーの権限、システムの設定に合わせてカスタムする
     *
     */
    protected function getGlobalMenu()
    {
        $menues = $this->loadGlobalMenu($this->authKind);
        $user   = $this->user;
        
        $ret = [];
        foreach( $menues as $menu ) {
            if( $this->isAllowMenu($user, $menu) ) {
                $ret[] = $menu;
            }
        }

        return $ret;
    }
    
    /**
     * function loadGlobalMenu
     *
     *
     */
    protected function loadGlobalMenu($kind)
    {
        $menu = app(BASE_APP_ACCESSOR)->get('menu_list.'.$kind);
        
        return $menu;
    }
    
    /**
     * function isAllowMenu
     *
     */
    protected function isAllowMenu($user, $menu)
    {
        // $userがセットされていなければすべてのメニューを無効とする
        if(is_null($user) || $this->disableMenu($menu)) {
            return false;
        }
        return $this->checkMenuAuthorities($user, $menu);
    }
    
    /**
     *
     *
     */
    protected function disableMenu($menu)
    {
        $ret = false;
    
        $prefix = $menu['url'];
        if(app(BASE_APP_ACCESSOR)->get('standard.contents.'.$prefix) === 'disabled') {
            $ret = true;
        }
        
        return $ret;
    }
    
    /**
     * function getShortcutMenu
     *
     */
    protected function getShortcutMenu()
    {
        $ret = [];
        
        return $ret;
    }
    
    /**
     * function getImportantHelp
     *
     */
    protected function getImportantHelp()
    {
        $ret = [];
        
        return $ret;
    }
    
    /**
     *
     *
     *
     */
    protected function checkMenuAuthorities($user, $menu)
    {
        $ret = null;
        $url = $this->getMenuCheckUrl($menu);

        $defRet = false;
        $roleAuth = $this->getRolAuth($user);
        foreach( $roleAuth as $row ) {
            if( $row->url == '*' ) {
                $defRet = $row->authority == AUTHRITY_ALLOW ? true : false;
            } elseif( Str::is($row->url, $url) ) {
//            } elseif( preg_match('@^'.str_replace('*', '.*', $row->url).'$@', $url) === 1 ) {
                $ret = $row->authority == AUTHRITY_ALLOW ? true : false;
                break;
            }
        }
        $ret = is_null($ret) ? $defRet : $ret;
        return $ret;
    }
    
    /**
     * function getRolAuth
     *
     */
    protected function getRolAuth($user)
    {
        $userId = $user->id;
        $kind   = $this->getUserKind();

        if(!isset($this->roleAuth[$kind][$userId])) {
            $obj  = $this->getRoleAuthoritiesModel();
            $role = $obj->where('value', $user->role)->first();
            $this->roleAuth[$kind][$userId] = !is_null($role) ? $role->getAuthorities() : [];
        }
        
        return $this->roleAuth[$kind][$userId];
    }
}
