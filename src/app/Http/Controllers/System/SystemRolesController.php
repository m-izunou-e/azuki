<?php
namespace Azuki\App\Http\Controllers\System;

/**
 * SystemRolesController クラス
 *
 * @copyright Copyright 2019 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Controllers\System\BaseRoleController as BaseController;
use Azuki\App\Contracts\Http\Validation\System\SystemRolesValidator as Validator;
use Azuki\App\Contracts\Models\SystemRoles as Model;

/**
 * class SystemRolesController
 *
 *
 */
class SystemRolesController extends BaseController
{
    /**
     * ページのtitleタグに表示する文字列
     *
     * string pageTitle
     */
    protected $pageTitle = 'システムロール管理';

    /**
     * ページのh1に表示する文字列
     *
     * string pageNamePrefix
     */
    protected $pageNamePrefix = 'システムロール';

    /**
     * コントローラ名として使用する文字列
     *
     * string controllerName
     */
    protected $controllerName = 'system-roles';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Model $model, Validator $validator)
    {
        parent::__construct($model, $validator);
    }
}
