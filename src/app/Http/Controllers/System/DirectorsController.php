<?php
namespace Azuki\App\Http\Controllers\System;

/**
 * DirectorsController クラス
 *
 * @copyright Copyright 2019 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Controllers\System\BaseController;
use Azuki\App\Http\Modules\ActionTrait\BaseActionPackageWithDetail;
use Azuki\App\Contracts\Http\Validation\System\DirectorsValidator as Validator;
use Azuki\App\Contracts\Models\Directors as Model;

/**
 * class DirectorsController
 *
 *
 */
class DirectorsController extends BaseController
{
    /**
     * サポートサービスのオブジェクト
     *
     * @var CtrlCupporter ctrlSupporter
     */
    protected $ctrlSupporter;
    
    /**
     * 画面構成に必要な要素のモジュールの定義
     *
     */
    protected $elementModues = [
        \Azuki\App\Http\Modules\Forms\BasicElements::class,
        \Azuki\App\Http\Modules\Forms\Directors::class,
    ];

    /**
     * 基本のアクションセットのトレイトを読み込みます
     */
    use BaseActionPackageWithDetail;

    /**
     * ページのtitleタグに表示する文字列
     *
     * string pageTitle
     */
    protected $pageTitle = 'システムマスター管理';

    /**
     * ページのh1に表示する文字列
     *
     * string pageNamePrefix
     */
    protected $pageNamePrefix = 'システムマスター';

    /**
     * コントローラ名として使用する文字列
     *
     * string controllerName
     */
    protected $controllerName = 'directors';
    
    /**
     * 一覧の操作アイコン設定
     * 必要なければ変数定義自体不要
     *
     * array ctrlCondition
     */
    protected $ctrlCondition = [
        'delete' => [
            'default' => true,
            'disable' => [
                'id' => 1,
            ],
        ],
    ];
    
    /**
     * 各画面タイプごとの要素のオーダー設定
     *
     * array $elementsOrder
     */
    protected $elementsOrder = [
        'search' => ['name', 'email', 'enable' ],
        'detail' => ['id', 'created', 'name', 'email', 'password', 'role', 'enable' ],
        'form'   => ['name', 'email', 'password', 'password_conf', 'role', 'enable' ],
        'list'   => ['id', 'name', 'email', 'created', 'role', 'enable', 'ctrl', ],
    ];

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Model $model, Validator $validator)
    {
        parent::__construct();
        
        $this->setCtrlSuppoter(
            $this->getCtrlSuppoterParameters($model, $validator)
        );
    }
}
