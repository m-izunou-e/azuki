<?php
namespace Azuki\App\Http\Controllers\System;

/**
 * BaseRoleController クラス
 *
 * @copyright Copyright 2019 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Controllers\System\BaseController;
use Azuki\App\Http\Modules\ActionTrait\BaseActionPackageWithDetail;

/**
 * class BaseRoleController
 *
 *
 */
class BaseRoleController extends BaseController
{
    /**
     * サポートサービスのオブジェクト
     *
     * @var CtrlCupporter ctrlSupporter
     */
    protected $ctrlSupporter;
    
    /**
     * 画面構成に必要な要素のモジュールの定義
     *
     */
    protected $elementModues = [
        \Azuki\App\Http\Modules\Forms\BasicElements::class,
        \Azuki\App\Http\Modules\Forms\Roles::class,
    ];

    /**
     * 基本のアクションセットのトレイトを読み込みます
     */
    use BaseActionPackageWithDetail;
    
    /**
     * 一覧の操作アイコン設定
     * 必要なければ変数定義自体不要
     *
     * array ctrlCondition
     */
    protected $ctrlCondition = [
        'delete' => [
            'default' => true,
            'disable' => [
                'deletable' => 1,
            ],
        ],
    ];
    
    /**
     * 各画面タイプごとの要素のオーダー設定
     *
     * array $elementsOrder
     */
    protected $elementsOrder = [
        'search' => ['name', 'value' ],
        'detail' => ['id', 'name', 'value', 'order', 'role_authorities', 'deletable' ],
        'form'   => ['name', 'value', 'order', 'role_authorities' ],
        'list'   => ['id', 'name', 'value', 'order', 'created', 'ctrl', ],
    ];

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct($model, $validator)
    {
        parent::__construct();
        
        $this->setCtrlSuppoter(
            $this->getCtrlSuppoterParameters($model, $validator)
        );
    }
    
    /**
     * function getCtrlSuppoterTemplatePrefix
     *
     *
     */
    protected function getCtrlSuppoterTemplatePrefix()
    {
        return $this->getViewPrefix().'templates.';
    }
    
    /**
     * function getCtrlSuppoterTemplates
     *
     *
     */
    protected function getCtrlSuppoterTemplates()
    {
        return ['list' => $this->getViewPrefix().'roles-list'];
    }
    
    /**
     * function getConfirmAsign
     *
     * 確認画面表示前に空の権限設定を削除する処理を行う
     */
    protected function getConfirmAsign($assign)
    {
        if(!isset($assign['post']['role_authorities']) || !is_array($assign['post']['role_authorities'])) {
            return $assign;
        }

        $post = $assign['post'];
        $authorities = [];
        
        foreach($post['role_authorities'] as $key => $auth) {
            if(!empty($auth['url'])) {
                $authorities[$key] = $auth;
            }
        }
        $assign['post']['role_authorities'] = $authorities;
        
        return $assign;
    }
}
