<?php
namespace Azuki\App\Http\Controllers\System;

/**
 * ManagersController クラス
 *
 * @copyright Copyright 2019 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Controllers\System\BaseController;
use Azuki\App\Http\Modules\ActionTrait\BaseActionPackageWithDetail;
use Azuki\App\Contracts\Http\Validation\System\ManagersValidator as Validator;
use Azuki\App\Contracts\Models\Managers as Model;

/**
 * class ManagersController
 *
 *
 */
class ManagersController extends BaseController
{
    /**
     * サポートサービスのオブジェクト
     *
     * @var CtrlCupporter ctrlSupporter
     */
    protected $ctrlSupporter;
    
    /**
     * 画面構成に必要な要素のモジュールの定義
     *
     */
    protected $elementModues = [
        \Azuki\App\Http\Modules\Forms\BasicElements::class,
        \Azuki\App\Http\Modules\Forms\Managers::class,
    ];

    /**
     * 基本のアクションセットのトレイトを読み込みます
     */
    use BaseActionPackageWithDetail;

    /**
     * ページのtitleタグに表示する文字列
     *
     * string pageTitle
     */
    protected $pageTitle = 'サイト管理者管理';

    /**
     * ページのh1に表示する文字列
     *
     * string pageNamePrefix
     */
    protected $pageNamePrefix = 'サイト管理者';

    /**
     * コントローラ名として使用する文字列
     *
     * string controllerName
     */
    protected $controllerName = 'managers';
    
    /**
     * 各画面タイプごとの要素のオーダー設定
     *
     * array $elementsOrder
     */
    protected $elementsOrder = [
        'search' => ['name', 'login_id', 'email', 'role', 'enable' ],
        'detail' => ['id', 'created', 'name', 'login_id', 'email', 'password', 'role', 'enable' ],
        'form'   => ['name', 'login_id', 'email', 'password', 'password_conf', 'role', 'enable' ],
        'list'   => ['id', 'name', 'login_id', 'email', 'created', 'role', 'enable', 'ctrl', ],
    ];
    protected $elementsOrderWithBelong = [
        'search' => ['name', 'login_id', 'email', 'role', 'belong', 'enable' ],
        'detail' => ['id', 'created', 'name', 'login_id', 'email', 'password', 'role', 'belong', 'enable' ],
        'form'   => ['name', 'login_id', 'email', 'password', 'password_conf', 'role', 'belong', 'enable' ],
        'list'   => ['id', 'name', 'login_id', 'email', 'created', 'role', 'belong', 'enable', 'ctrl', ],
    ];

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Model $model, Validator $validator)
    {
        parent::__construct();

        if(supportOrganizations()) {
            $this->elementsOrder = $this->elementsOrderWithBelong;
        }

        $this->setCtrlSuppoter(
            $this->getCtrlSuppoterParameters($model, $validator)
        );
    }
}
