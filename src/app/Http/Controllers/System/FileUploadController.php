<?php
namespace Azuki\App\Http\Controllers\System;

/**
 * 画像アップロード用インターフェースを実装する
 *
 * @package pc
 *
 */

use Illuminate\Support\Facades\Response;
use Azuki\App\Http\Requests\FormRequest;
use Azuki\App\Http\Controllers\System\BaseController;

/**
 * 画像アップロードを処理するクラス
 *
 */
class FileUploadController extends BaseController
{
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * function postImage 画像アップロード処理
     * 
     * @Get("/", as="upload.index")
     *
     * @return void
     */
    public function postFile(FormRequest $request)
    {
        $fileFieldName = 'upload-file';
        $fileObject    = $_FILES[$fileFieldName];
        $fileObject['ufType'] = $request->has('type') ? $request->get('type') : '';
        $fileObject['exType'] = $request->has('kind') ? $request->get('kind') : '';
    
        $uploader = app('UploaderFactory')->getUploader($fileObject['ufType']);
        $res = $uploader->upload($fileObject);
        $res['through'] = $request->has('through') ? $request->get('through') : '';
        
        // 過去の不要と思われる一時ファイルの削除処理を実行する
        app('UploadedFileManager')->deleteTempFile();
        
        //結果をJsonで出力
        return Response::json( $res );
    }
}
