<?php
namespace Azuki\App\Http\Controllers\System;

/**
 * StructureController クラス
 *
 * @copyright Copyright 2019 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Controllers\System\BaseController;
use Azuki\App\Contracts\Models\SystemSwitch;

use Azuki\App\Http\Modules\ActionTrait\TraitParts\TraitChangeStatus;

/**
 * class StructureController
 *
 *
 */
class StructureController extends BaseController
{
    use TraitChangeStatus;

    /**
     * ページのtitleタグに表示する文字列
     *
     * string pageTitle
     */
    protected $pageTitle = 'システム構成情報';

    /**
     * コントローラ名として使用する文字列
     *
     * string controllerName
     */
    protected $controllerName = 'structure';
    
    /**
     *
     */
    protected $informations = [
        [
            'title'  => '所属機能',
            'method' => 'getStatusFromConfig',
            'args'   => 'organizations',
        ],
        [
            'title'  => 'ログ機能',
            'method' => 'getLoggingStatus',
        ],
        [
            'title' => 'システム管理',
            'value'  => '有効',
        ],
        [
            'title'  => '環境情報',
            'method' => 'getStatusContentsEnabled',
            'args'   => 'environment',
        ],
        [
            'title'  => 'システム情報',
            'value'  => '有効',
        ],
        [
            'title'  => 'サイト管理',
            'method' => 'getStatusContentsEnabled',
            'args'   => 'manage',
        ],
        [
            'depend' => 'azuki_sample',
            'title'  => 'サンプルページ',
            'method' => 'getStatusContentsEnabled',
            'args'   => 'sample',
        ],
        [
            'title'  => 'ユーザーログイン',
            'method' => 'getStatusFromConfig',
            'args'   => 'user_management',
        ],
        [
            'depend' => 'azuki_information',
            'title'  => 'azukiページ',
            'method' => 'getStatusFromConfig',
            'args'   => 'azuki_information',
        ],
        [
            'title'  => 'メンテナンス情報',
            'method' => 'getMaintenanceAttention',
        ],
    ];
    
    /**
     *
     */
    protected $oprationStatus = [
        ['title' => 'メンテナンス', 'kind' => SWITCH_KIND_MAINTENANCE_MODE],
        ['title' => 'ログ自動削除', 'kind' => SWITCH_KIND_AUTO_DELETE_LOGS],
    ];

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * function getIndex 初期表示のアクション
     *
     * 初期表示のアクション
     * @return void
     */
    public function getIndex()
    {
        $systemInformation = array_map(
            [$this, 'getSystemInformation'],
            array_filter($this->informations, [$this, 'filterInformations'])
        );
        $oprationStatus = array_map(
            [$this, 'getOprationStatus'],
            $this->oprationStatus
        );
    
        return view(
            $this->getViewPrefix().$this->controllerName.'.index',
            [
                'title'          => $this->pageTitle,
                'controller'     => $this->controllerName,
                'systemInfo'     => $systemInformation,
                'oprationStatus' => $oprationStatus
            ]
        );
    }
    
    /**
     * function filterInformations
     *
     */
    protected function filterInformations($info)
    {
        if(isset($info['depend'])) {
            $ret = $this->getAddonConfigs($info['depend']);
            return is_null($ret) ? false : $ret;
        }
        
        return true;
    }
    
    /**
     * function getSystemInformation
     *
     *
     */
    protected function getSystemInformation($info)
    {
        $value = isset($info['value']) ? $info['value'] : '';
        if(isset($info['method']) && method_exists($this, $info['method'])) {
            $args  = isset($info['args']) ? $info['args'] : [];
            $value = call_user_func( [$this, $info['method']], $args );
        }
        return [
            'title' => $info['title'],
            'value' => $value,
        ];
    }
    
    /**
     *
     *
     *
     */
    protected function getOprationStatus($operation)
    {
        $operation['status'] = $this->getStatus($operation['kind']);
        $operation['title']  = $this->getSwitchTitle($operation);
        
        return $operation;
    }
    
    /**
     * function getLoggingStatus
     *
     *
     */
    protected function getLoggingStatus()
    {
        $logs = [
            [ 'key' => 'record_access_logs',    'title' => 'アクセスログ' ],
            [ 'key' => 'record_system_logs',    'title' => 'システムログ' ],
            [ 'key' => 'record_login_logs',     'title' => 'ログインログ' ],
            [ 'key' => 'record_operation_logs', 'title' => 'オペレーションログ' ],
        ];
        
        return array_map(function($log){
            return sprintf('%s/%s', $log['title'], $this->getStatusFromConfig($log['key']));
        }, $logs);
    }
    
    /**
     * function getStatusFromConfig
     *
     */
    protected function getStatusFromConfig($key)
    {
        return $this->getStatusLabel($this->getStandardConfigs($key));
    }
    
    /**
     * function getStatusFromConfig
     *
     */
    protected function getStatusContentsEnabled($key)
    {
        return $this->getStatusLabel($this->getStandardConfigs('contents.'.$key), 'enabled');
    }
    
    /**
     *
     *
     */
    protected function getStandardConfigs($key)
    {
        return app(BASE_APP_ACCESSOR)->get('standard.'. $key);
    }
    
    /**
     *
     *
     */
    protected function getAddonConfigs($key)
    {
        return app(BASE_APP_ACCESSOR)->get('addon.'. $key);
    }
    
    /**
     * function getStatusLabel
     *
     */
    protected function getStatusLabel($value, $compare = true)
    {
        return $value == $compare ? '有効' : '無効';
    }
    
    /**
     *
     *
     */
    protected function getMaintenanceAttention()
    {
        $ret = sprintf('接続許可IP:[%s]', implode(',', $this->getAllowIp()));
        if(laravelVersionOverEight()) {
            $ret = sprintf('バイパストークン:[%s]', $this->getSecret());
        }
        return $ret;
    }
    
    /**
     *
     *
     */
    protected function checkAllowIp()
    {
        return ifRemoteIpInAllows(true);
    }
    
    /**
     *
     *
     */
    protected function getAllowIp()
    {
        return getAllowIpConfig();
    }
    
    /**
     *
     *
     */
    protected function getSecret()
    {
        return $this->getStandardConfigs('maintenance_secret');
    }
    
    /**
     *
     *
     */
    protected function getStatus($kind)
    {
        $methods = [
            SWITCH_KIND_MAINTENANCE_MODE => 'getMaintenanceStatus',
        ];
    
        $method = 'getStatusOfSwitchDatabase';
        if(isset($methods[$kind]) && method_exists($this, $methods[$kind])) {
            $method = $methods[$kind];
        }
        return call_user_func([$this, $method], $kind);
    }
    
    /**
     *
     *
     */
    protected function getSwitchTitle($operation)
    {
        $kind = $operation['kind'];
        $switch = app(SystemSwitch::class)->getSwitchBy($kind);
        return is_null($switch) ? $operation['title'] : $switch->title;
    }
    
    /**
     *
     *
     */
    protected function getStatusOfSwitchDatabase($kind)
    {
        $model = app(SystemSwitch::class);
        return $model->isRun($kind) ? SWITCH_STATUS_IS_RUN : SWITCH_STATUS_IS_STOP;
    }
    
    /**
     *
     *
     */
    protected function getMaintenanceStatus($kind)
    {
        return app()->isDownForMaintenance() ? SWITCH_STATUS_IS_RUN : SWITCH_STATUS_IS_STOP;
    }
}

