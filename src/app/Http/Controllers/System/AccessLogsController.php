<?php
namespace Azuki\App\Http\Controllers\System;

/**
 * AccessLogsController クラス
 *
 * @copyright Copyright 2019 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Controllers\System\BaseController;
use Azuki\App\Http\Modules\ActionTrait\ListActionPackageWithDetail;
use Azuki\App\Http\Modules\ActionTrait\TraitParts\DeleteOldDataTrait;
//use Azuki\App\Contracts\Http\Validation\System\AccessLogsValidator as Validator;
use Azuki\App\Http\Validation\SharedValidator as Validator;
use Azuki\App\Contracts\Models\AccessLogs as Model;

/**
 * class AccessLogsController
 *
 *
 */
class AccessLogsController extends BaseController
{
    /**
     * サポートサービスのオブジェクト
     *
     * @var CtrlCupporter ctrlSupporter
     */
    protected $ctrlSupporter;

    /**
     * 追加構成要素
     *
     * array $elements
     */
    protected $elements = [
        'url' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => 'アクセスURL',
                ],
                'url'    => [
                    'size'              => 8,
                    'type'              => 'text',
                    'name'              => 'url', 
                    'column'            => 'url',
                    'searchName'        => 'search[url]',
                    'searchPlaceholder' => '部分一致します',
                ],
            ],
            'list' => [
                [
                    'type'           => CONTROL_TYPE_TEXT,
                    'width'          => '20%',
                    'orderable'      => false,
                ],
            ],
        ],
        'user' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => 'アクセスユーザー',
                ],
                'user'    => [
                    'size'              => 8,
                    'type'              => 'text',
                    'name'              => 'user', 
                    'column'            => 'user',
                ],
            ],
            'list' => [
                [
                    'type'           => CONTROL_TYPE_TEXT,
                    'width'          => '12%',
                    'orderable'      => false,
                ],
            ],
        ],
        'user_kind' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => 'ユーザー種別',
                ],
                'user_kind'    => [
                    'size'              => 8,
                    'type'              => 'radio',
                    'name'              => 'user_kind', 
                    'column'            => 'user_kind',
                    'select'            => 'userKindLabel',
                    'searchType'        => 'checkbox',
                    'searchName'        => 'search[user_kind]',
                ],
            ],
            'list' => [
                [
                    'type'           => CONTROL_TYPE_SELECT,
                    'width'          => '12%',
                    'orderable'      => true,
//                    'defVal'         => '一般ユーザー',
                ],
            ],
        ],
        'user_info' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => 'アクセス時ユーザー情報',
                ],
                'user_info'    => [
                    'size'              => 8,
                    'type'              => 'serialize-array',
                    'name'              => 'user_info', 
                    'column'            => 'user_info',
                ],
            ],
        ],
        'access_info' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => 'アクセス情報',
                ],
                'access_info'    => [
                    'size'              => 8,
                    'type'              => 'serialize-array',
                    'name'              => 'access_info', 
                    'column'            => 'access_info',
                ],
            ],
        ],
    ];
    
    /**
     * 画面構成に必要な要素のモジュールの定義
     *
     */
    protected $elementModues = [
        \Azuki\App\Http\Modules\Forms\BasicElements::class,
    ];

    /**
     * 基本のアクションセットのトレイトを読み込みます
     */
    use ListActionPackageWithDetail;
    use DeleteOldDataTrait;

    /**
     * ページのtitleタグに表示する文字列
     *
     * string pageTitle
     */
    protected $pageTitle = 'アクセスログ管理';

    /**
     * ページのh1に表示する文字列
     *
     * string pageNamePrefix
     */
    protected $pageNamePrefix = 'アクセスログ';

    /**
     * コントローラ名として使用する文字列
     *
     * string controllerName
     */
    protected $controllerName = 'access-logs';
    
    /**
     * 一覧の表示順設定
     * 必要なければ変数定義自体不要
     *
     * array viewOrder
     */
    protected $viewOrder = ['order' => 'created_at', 'condition' => ['created_at' => 'desc']];
    
    /**
     * 一覧の操作アイコン設定
     * 必要なければ変数定義自体不要
     *
     * array ctrlCondition
     */
    protected $ctrlCondition = [
        'edit' => [
            'default' => false,
        ],
        'delete' => [
            'default' => false,
        ],
    ];
    
    /**
     *
     *
     */
    protected $addViews = [
        'isBulkDelete' => true,
    ];
    
    /**
     * デートタイムピッカーの使用有無
     * 必要なければ変数定義自体不要
     *
     * boolean needDatetimePicker
     */
    protected $needDatetimePicker = true;
    
    /**
     * 各画面タイプごとの要素のオーダー設定
     *
     * array $elementsOrder
     */
    protected $elementsOrder = [
        'search' => ['url', 'user_kind', 'period' ],
        'detail' => ['url', 'user', 'user_kind', 'user_info', 'access_info' ],
        'list'   => ['url', 'user', 'user_kind', 'created', 'ctrl' ],
    ];

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Model $model, Validator $validator)
    {
        parent::__construct();
        
        $this->setCtrlSuppoter(
            $this->getCtrlSuppoterParameters($model, $validator)
        );
        $this->elements['created']['form']['title']['name'] = 'アクセス日時';
        $this->elements['created']['list']['created']['orderable'] = true;
        $this->elements['ctrl']['list']['ctrl']['ctrl'] = [CTRL_DETAIL];
    }
    
    /**
     * function getCtrlSuppoterTemplatePrefix
     *
     *
     */
    protected function getCtrlSuppoterTemplatePrefix()
    {
        return $this->getViewPrefix().'templates.';
    }
    
    /**
     * function getDeleteTargetModel
     *
     *
     */
    protected function getDeleteTargetModel()
    {
        return app(Model::class);
    }
}
