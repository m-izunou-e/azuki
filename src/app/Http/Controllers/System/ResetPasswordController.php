<?php
namespace Azuki\App\Http\Controllers\System;

/**
 * パスワード再設定に関するページを提供するコントローラ
 * 管理画面用
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Controllers\System\BaseController;
use Azuki\App\Http\Modules\SendsPasswordResetEmails;
use Azuki\App\Http\Modules\ResetPassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

/**
 * class ResetPasswordController
 *
 */
class ResetPasswordController extends BaseController
{
    use ResetPassword, SendsPasswordResetEmails {
        ResetPassword::credentials       insteadof SendsPasswordResetEmails;
        SendsPasswordResetEmails::broker insteadof ResetPassword;
    }

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        // パスワードリセット後、ログイン後のトップページに遷移させたい場合は
        // 以下のexceptを除外してしまう方法でも可能
        $this->middleware('guest.system')->except('getCompleteResetPassword');
        $this->redirectTo = '/'.trim($this->story, '/');
    }

    /**
     * function broker
     *
     * 管理画面用のブローカーを返す必要があるのでオーバーライド
     * directorsというブローカーはconfig/auth.phpにて追加設定している
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker('directors');
    }

    /**
     * function guard
     *
     * 管理画面用のguardを返す必要があるのでオーバーライド
     * 設定はブローカー同様config/auth.phpにて追加している
     * BaseControllerでguardの変更処理をしているがここでも指定しておく
     * 必要がある
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('system');
    }

}
