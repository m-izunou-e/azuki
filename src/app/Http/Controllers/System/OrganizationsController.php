<?php
namespace Azuki\App\Http\Controllers\System;

/**
 * OrganizationsController クラス
 *
 * @copyright Copyright 2019 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Controllers\System\BaseController;
use Azuki\App\Http\Modules\ActionTrait\BaseActionPackageWithDetail;
//use Azuki\App\Contracts\Http\Validation\System\OrganizationsValidator as Validator;
use Azuki\App\Http\Validation\SharedValidator as Validator;
use Azuki\App\Contracts\Models\Organizations as Model;

/**
 * class OrganizationsController
 *
 *
 */
class OrganizationsController extends BaseController
{
    /**
     * サポートサービスのオブジェクト
     *
     * @var CtrlCupporter ctrlSupporter
     */
    protected $ctrlSupporter;

    /**
     * 追加構成要素
     *
     * array $elements
     */
    protected $elements = [
        'name' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => '組織名称',
                    'required' => IS_REQUIRED,
                ],
                'name'    => [
                    'required'          => IS_REQUIRED,
                    'size'              => 8,
                    'type'              => 'text',
                    'name'              => 'name', 
                    'column'            => 'name',
                    'searchName'        => 'search[name]',
                    'searchPlaceholder' => '部分一致します',
//                    'helperText'        => '※255文字以内の英数大文字小文字記号のみ',
                    'validate'          => [
                        'rule'      => 'required|max:255',
                    ],
                ],
            ],
            'list' => [
                [
                    'type'           => CONTROL_TYPE_TEXT,
                    'width'          => '12%',
                    'orderable'      => false,
                ],
            ],
        ],
        'name_id' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => '識別名称',
                    'required' => IS_REQUIRED,
                ],
                'name_id'    => [
                    'required'          => IS_REQUIRED,
                    'size'              => 8,
                    'type'              => 'text',
                    'name'              => 'name_id', 
                    'column'            => 'name_id',
                    'searchName'        => 'search[name_id]',
                    'searchPlaceholder' => '部分一致します',
                    'helperText'        => '※255文字以内の英数大文字小文字のみ',
                    'validate'          => [
                        'type'      => 'uniqueOnTable',
                        'rule'      => 'required|alpha_num|max:255|uniqueOnTable:%s,name_id,%s',
                        'options'   => ['model' => '\\Azuki\\App\\Models\\Organizations'],
                        'message'   => [
                            'unique_on_table' => 'その:attributeはすでに登録されています',
                            'alpha_num'       => ':attributeは英数大文字小文字で入力してください',
                        ],
                    ],
                ],
            ],
            'list' => [
                [
                    'type'           => CONTROL_TYPE_TEXT,
                    'width'          => '12%',
                    'orderable'      => false,
                ],
            ],
        ],
        'original_domain' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => '独自ドメイン',
                    'required' => NOT_REQUIRED,
                ],
                'original_domain'    => [
                    'required'          => NOT_REQUIRED,
                    'size'              => 8,
                    'type'              => 'text',
                    'name'              => 'original_domain', 
                    'column'            => 'original_domain',
                    'searchName'        => 'search[original_domain]',
                    'searchPlaceholder' => '部分一致します',
                    'helperText'        => '※255文字以内のドメイン形式のみ',
                    'validate'          => [
                        'rule'      => 'nullable|domain|max:255',
                        'message'   => [
                            'domain' => ':attributeはドメインとして形式が正しくありません',
                        ],
                    ],
                ],
            ],
            'list' => [
                [
                    'type'           => CONTROL_TYPE_TEXT,
                    'width'          => '12%',
                    'orderable'      => false,
                ],
            ],
        ],
        'value' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => '所属値',
                    'required' => IS_REQUIRED,
                ],
                'value'    => [
                    'required'          => IS_REQUIRED,
                    'size'              => 8,
                    'type'              => 'text',
                    'name'              => 'value', 
                    'column'            => 'value',
                    'searchName'        => 'search[value]',
                    'helperText'        => '所属値を変更する際は登録済みの関連データの方も変更しなおしが必要です',
                    'searchHelperText'  => '',
                    'validate'          => [
                        'type'      => 'uniqueOnTable',
                        'rule'      => 'required|alpha_num|max:255|uniqueOnTable:%s,value,%s',
                        'options'   => ['model' => '\\Azuki\\App\\Models\\Organizations'],
                        'message'   => [
                            'unique_on_table' => 'その:attributeはすでに登録されています',
                        ],
                    ],
                ],
            ],
            'list' => [
                [
                    'type'           => CONTROL_TYPE_TEXT,
                    'width'          => '12%',
                    'orderable'      => false,
                ],
            ],
        ],
        'order' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => '並び順',
                    'required' => IS_REQUIRED,
                ],
                'order'    => [
                    'required'          => IS_REQUIRED,
                    'size'              => 8,
                    'type'              => 'text',
                    'name'              => 'order', 
                    'column'            => 'order',
                    'validate'          => [
                        'rule'      => 'required|numeric|max:9999',
                    ],
                ],
            ],
            'list' => [
                [
                    'type'           => CONTROL_TYPE_TEXT,
                    'width'          => '12%',
                    'orderable'      => true,
                ],
            ],
        ],
    ];
    
    /**
     * 画面構成に必要な要素のモジュールの定義
     *
     */
    protected $elementModues = [
        \Azuki\App\Http\Modules\Forms\BasicElements::class,
    ];

    /**
     * 基本のアクションセットのトレイトを読み込みます
     */
    use BaseActionPackageWithDetail;

    /**
     * ページのtitleタグに表示する文字列
     *
     * string pageTitle
     */
    protected $pageTitle = '組織管理';

    /**
     * ページのh1に表示する文字列
     *
     * string pageNamePrefix
     */
    protected $pageNamePrefix = '組織管理';

    /**
     * コントローラ名として使用する文字列
     *
     * string controllerName
     */
    protected $controllerName = 'organizations';
    
    /**
     * 一覧の表示順設定
     * 必要なければ変数定義自体不要
     *
     * array viewOrder
     */
    protected $viewOrder = ['order' => 'order', 'condition' => ['order' => 'asc']];
    
    /**
     * 各画面タイプごとの要素のオーダー設定
     *
     * array $elementsOrder
     */
    protected $elementsOrder = [
        'search' => ['name', 'name_id', 'original_domain', 'value' ],
        'detail' => ['id', 'name', 'name_id', 'original_domain', 'value', 'order' ],
        'form'   => ['name', 'name_id', 'original_domain', 'value', 'order' ],
        'list'   => [/*'id',*/ 'name', 'name_id', 'original_domain', 'value', 'order', 'created', 'ctrl', ],
    ];

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Model $model, Validator $validator)
    {
        parent::__construct();
        
        $this->setCtrlSuppoter(
            $this->getCtrlSuppoterParameters($model, $validator)
        );
    }
    
    /**
     * function getCtrlSuppoterTemplatePrefix
     *
     *
     */
    protected function getCtrlSuppoterTemplatePrefix()
    {
        return $this->getViewPrefix().'templates.';
    }
}
