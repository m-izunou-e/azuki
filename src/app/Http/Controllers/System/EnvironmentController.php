<?php
namespace Azuki\App\Http\Controllers\System;

/**
 * EnvironmentController クラス
 *
 * @copyright Copyright 2019 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Controllers\System\BaseController;
use Illuminate\Support\Facades\Response;


/**
 * class EnvironmentController
 *
 *
 */
class EnvironmentController extends BaseController
{
    /**
     * ページのtitleタグに表示する文字列
     *
     * string pageTitle
     */
    protected $pageTitle = '環境情報';

    /**
     * コントローラ名として使用する文字列
     *
     * string controllerName
     */
    protected $controllerName = 'environment';
    
    /**
     *
     *
     */
    protected $crontabText = [
    ];

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->crontabText = getCronTextSettings();
    }

    /**
     * function getIndex 初期表示のアクション
     *
     * 初期表示のアクション
     * @return void
     */
    public function getIndex()
    {
        $serverInformation = $this->getServerInformation();
        $appInformation    = $this->getAppInformation();
    
        return view(
            $this->getViewPrefix().$this->controllerName.'.index',
            [
                'title'      => $this->pageTitle,
                'controller' => $this->controllerName,
                'serverInfo' => $serverInformation,
                'appInfo'    => $appInformation,
                'cronText'   => $this->crontabText,
                'crontab'    => $this->getCronInformation(),
            ]
        );
    }
    
    /**
     *
     *
     */
    public function postRegistCron()
    {
        if(empty($this->crontabText)) {
            return $this->responceNoCronError();
        }

        return $this->cronOperation('cronを登録しました', function(){

            $file = storage_path('crontab.txt');
            file_put_contents(
                $file,
                implode("\n", $this->crontabText)."\n"
            );
            chmod($file, 0664);
            $com = sprintf('crontab %s 2>&1', $file);
            exec($com, $output, $retVal);
            unlink($file);
            
            return [$retVal, $output];
        });
    }
    
    /**
     *
     *
     */
    public function postRemoveCron()
    {
        return $this->cronOperation('cronを削除しました', function(){
            exec('crontab -r 2>&1', $output, $retVal);
            return [$retVal, $output];
        });
    }
    
    /**
     *
     *
     */
    protected function cronOperation($defMsg, $closure)
    {
        $result = true;
        $msg   = $defMsg;
        
        if(!$this->canCronOperation()) {
            return $this->responceAuthError();
        }
        
        list($retVal, $output) = $closure();

        if($retVal !== 0) {
            // 実行時エラー
            $result = false;
            $msg   = is_array($output) ? implode(',', $output) : $output;
        }
        return Response::json([
            'result'  => $result,
            'message' => $msg,
        ]);
    }
    
    /**
     *
     */
    protected function canCronOperation()
    {
        return !isWindows() && ifRemoteIpInAllows(true);
    }
    
    /**
     *
     *
     */
    protected function responceAuthError()
    {
        return Response::json([
            'result'  => false,
            'message' => '権限がありません',
        ]);
    }
    
    /**
     *
     *
     */
    protected function responceNoCronError()
    {
        return Response::json([
            'result'  => false,
            'message' => 'Cronの設定がありません',
        ]);
    }
    
    /**
     * function getServerInformation
     *
     *
     */
    protected function getServerInformation()
    {
        return [
            [
                'title' => 'OS',
                'value' => sprintf('%s / %s', php_uname('s'), php_uname('r')),
            ],
            [
                'title' => 'IPアドレス',
                'value' => $this->getLocalIpAddr(),
            ],
            [
                'title' => 'サーバー名',
                'value' => php_uname('n'),
            ],
            [
                'title' => 'FQDN',
                'value' => parse_url(app('config')->get('app.url'), PHP_URL_HOST),
            ],
            [
                'title' => 'ディスク状況',
                'value' => $this->diskInfo(),
            ],
        ];
    }
    
    /**
     * function getAppInformation
     *
     *
     */
    protected function getAppInformation()
    {
        return [
            [
                'title' => 'Webサーバー',
                'value' => $this->getWebServerInfo(),
            ],
            [
                'title' => 'DBサーバー',
                'value' => $this->getDBServerInfo(),
            ],
            [
                'title' => 'PHP',
                'value' => phpversion(),
            ],
            $this->frameworkVersionInformation(),
            [
                'title' => 'Laravel Version',
                'value' => sprintf('Laravel Framework %s', app()->version()),
            ],
        ];
    }
    
    /**
     *
     *
     */
    protected function getCronInformation()
    {
        return getCronInformation();
    }
    
    /**
     *
     *
     */
    protected function getLocalIpAddr()
    {
//        $localIp = getHostByName(getHostName());
        return app('request')->server()['SERVER_ADDR'];
    }
    
    /**
     *
     *
     */
    protected function frameworkVersionInformation()
    {
        $azukiVersion   = app(BASE_APP_ACCESSOR)->get('standard.version');
        return [
            'title' => 'Azuki Version',
            'value' => sprintf('v.%s', $azukiVersion),
        ];
    }
    
    /**
     *
     *
     */
    protected function diskInfo()
    {
        $targetDiskPath = '/var';
        
        list($total, $free) = getDiskSpaceInfo();
        $use = $total - $free;
        
        return sprintf('空容量：%s （使用量：%s/%s）',
            getSymbolByQuantity($free),
            getSymbolByQuantity($use),
            getSymbolByQuantity($total)
        );
    }
    
    /**
     *
     *
     *
     */
    protected function getWebServerInfo()
    {
        $excludes = [
            'PHP',
            'mod_',
        ];
        
        $req = app('request');
        $info = $req->server()['SERVER_SOFTWARE'];
        
        $infos = explode(' ', $info);
        foreach($infos as $key => $info) {
            foreach($excludes as $exclude) {
                if(strpos($info, $exclude) !== FALSE) {
                    unset($infos[$key]);
                    break;
                }
            }
        }
        return implode(' ', $infos);
    }
    
    /**
     *
     *
     *
     */
    protected function getDBServerInfo()
    {
        $pdo = \DB::getPdo();
        $databaseEngine  = $pdo->getAttribute(\PDO::ATTR_DRIVER_NAME);
        $databaseVersion = $pdo->getAttribute(\PDO::ATTR_SERVER_VERSION);
        
        if(strpos($databaseVersion, 'MariaDB')) {
            $databaseEngine = 'MariaDB';
        }
        return sprintf('%s/%s', $databaseEngine, $databaseVersion);
    }

    /**
     * function getPhpinfo phpinfo表示のアクション
     *
     * phpinfo表示のアクション
     * @return void
     */
    public function getPhpinfo()
    {
        $title = 'PHPインフォ';
        $isError = false;
        if($this->disablePhpInfo()) {
            $title   = '403エラー';
            $isError = true;
            $code    = '403';
            $msg     = 'アクセス権限がありません';
        }
    
        return view(
            $this->getViewPrefix().$this->controllerName.'.phpinfo',
            [
                'title'   => $title,
                'isError' => $isError,
                'code'    => isset($code) ? $code : '',
                'message' => isset($msg)  ? $msg  : '',
            ]
        );
    }
    
    /**
     *
     *
     *
     */
    protected function disablePhpInfo()
    {
        $request = app('request');
        if( route('system.environment.index') != $request->headers->get('referer')) {
            return true;
        }
        return ifRemoteIpInAllows(false);
    }
}
