<?php
namespace Azuki\App\Http\Controllers\System;

/**
 * SystemLogsController クラス
 *
 * @copyright Copyright 2019 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Controllers\System\BaseController;
use Azuki\App\Http\Modules\ActionTrait\ListActionPackageWithDetail;
use Azuki\App\Http\Modules\ActionTrait\TraitParts\DeleteOldDataTrait;
//use Azuki\App\Contracts\Http\Validation\System\SystemLogsValidator as Validator;
use Azuki\App\Http\Validation\SharedValidator as Validator;
use Azuki\App\Contracts\Models\SystemLogs as Model;

/**
 * class SystemLogsController
 *
 *
 */
class SystemLogsController extends BaseController
{
    /**
     * サポートサービスのオブジェクト
     *
     * @var CtrlCupporter ctrlSupporter
     */
    protected $ctrlSupporter;

    /**
     * 追加構成要素
     *
     * array $elements
     */
    protected $elements = [
        'message' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => 'メッセージ',
                ],
                'message'    => [
                    'size'              => 8,
                    'type'              => 'textarea',
                    'name'              => 'message', 
                    'column'            => 'message',
                    'searchName'        => 'search[message]',
                    'searchType'        => 'text',
                    'searchPlaceholder' => '部分一致します',
                ],
            ],
            'list' => [
                [
                    'type'           => CONTROL_TYPE_TEXT,
                    'width'          => '20%',
                ],
            ],
        ],
        'kind' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => '種別',
                ],
                'kind'    => [
                    'size'              => 8,
                    'type'              => 'radio',
                    'name'              => 'kind', 
                    'column'            => 'kind',
                    'select'            => 'systemLogKindLabel',
                    'searchType'        => 'checkbox',
                    'searchName'        => 'search[kind]',
                ],
            ],
            'list' => [
                [
                    'type'           => CONTROL_TYPE_SELECT,
                    'width'          => '12%',
                    'orderable'      => true,
                ],
            ],
        ],
        'status' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => 'ステータス',
                ],
                'status'    => [
                    'size'              => 8,
                    'type'              => 'radio',
                    'name'              => 'status', 
                    'column'            => 'status',
                    'select'            => 'systemLogStatusLabel',
                    'searchType'        => 'checkbox',
                    'searchName'        => 'search[status]',
                ],
            ],
            'list' => [
                [
                    'type'           => CONTROL_TYPE_SELECT,
                    'width'          => '12%',
                    'orderable'      => true,
                ],
            ],
        ],
        'check_at' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => '確認日時',
                ],
                'check_at'    => [
                    'size'              => 8,
                    'type'              => 'datetime',
                    'name'              => 'check_at', 
                    'column'            => 'check_at',
                ],
            ],
            'list' => [
                [
                    'type'           => CONTROL_TYPE_DATETIME,
                    'width'          => '20%',
                    'orderable'      => false,
                ],
            ],
        ],
        'restore_at' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => '復旧日時',
                ],
                'restore_at'    => [
                    'size'              => 8,
                    'type'              => 'datetime',
                    'name'              => 'restore_at', 
                    'column'            => 'restore_at',
                ],
            ],
            'list' => [
                [
                    'type'           => CONTROL_TYPE_DATETIME,
                    'width'          => '20%',
                    'orderable'      => false,
                ],
            ],
        ],
    ];
    
    /**
     * 画面構成に必要な要素のモジュールの定義
     *
     */
    protected $elementModues = [
        \Azuki\App\Http\Modules\Forms\BasicElements::class,
    ];

    /**
     * 基本のアクションセットのトレイトを読み込みます
     */
    use ListActionPackageWithDetail {
        getDetail  as getDetailTraitMethod;
    }
    use DeleteOldDataTrait;

    /**
     * ページのtitleタグに表示する文字列
     *
     * string pageTitle
     */
    protected $pageTitle = 'システムログ管理';

    /**
     * ページのh1に表示する文字列
     *
     * string pageNamePrefix
     */
    protected $pageNamePrefix = 'システムログ';

    /**
     * コントローラ名として使用する文字列
     *
     * string controllerName
     */
    protected $controllerName = 'system-logs';
    
    /**
     * 一覧の表示順設定
     * 必要なければ変数定義自体不要
     *
     * array viewOrder
     */
    protected $viewOrder = ['order' => 'created_at', 'condition' => ['created_at' => 'desc']];
    
    /**
     * 一覧の操作アイコン設定
     * 必要なければ変数定義自体不要
     *
     * array ctrlCondition
     */
    protected $ctrlCondition = [
        'edit' => [
            'default' => false,
        ],
        'delete' => [
            'default' => false,
        ],
        'restore' => [
            'default' => false,
            'enable' => [
                'status'     => SL_STATUS_FAULT,
                'restore_at' => null,
            ],
        ],
    ];
    
    /**
     *
     *
     */
    protected $addViews = [
        'isBulkDelete' => true,
    ];
    
    /**
     * デートタイムピッカーの使用有無
     * 必要なければ変数定義自体不要
     *
     * boolean needDatetimePicker
     */
    protected $needDatetimePicker = true;
    
    /**
     * 各画面タイプごとの要素のオーダー設定
     *
     * array $elementsOrder
     */
    protected $elementsOrder = [
        'search' => ['kind', 'status', 'period' ],
        'detail' => ['id', 'kind', 'status', 'message', 'check_at' ],
        'list'   => ['id', 'kind', 'status', 'message', 'created', 'ctrl' ],
    ];

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Model $model, Validator $validator)
    {
        parent::__construct();
        
        $this->setCtrlSuppoter(
            $this->getCtrlSuppoterParameters($model, $validator)
        );
        $this->elements['created']['form']['title']['name'] = '日時';
        $this->elements['created']['list']['created']['orderable'] = true;
        $this->elements['ctrl']['list']['ctrl']['ctrl'] = [CTRL_DETAIL/*, CTRL_RESTORE*/];
        
        $this->systemLogs = $model;
    }
    
    /**
     * function getCtrlSuppoterTemplatePrefix
     *
     *
     */
    protected function getCtrlSuppoterTemplatePrefix()
    {
        return $this->getViewPrefix().'templates.';
    }

    /**
     * function getDetail 詳細画面表示のアクション
     *
     * 詳細画面表示のアクション
     *
     * @return void
     */
    public function getDetail($id, $prev = null)
    {
        $systemLog = $this->systemLogs;
        $systemLog->check($id);
        return $this->getDetailTraitMethod($id, $prev);
    }

    /**
     * function postRestore リストア処理のアクション
     *
     * リストア処理のアクション
     *
     * @return void
     */
    public function postRestore()
    {
        $request = app('request');
        $id = $request->get('restore_id');
        
        $systemLog = $this->systemLogs;
        $obj = $systemLog
            ->where('status', '=', SL_STATUS_FAULT)
            ->whereNull('restore_at')
            ->where('id', '=', $id)
            ->first();
        
        if(!is_null($obj)) {
            $systemLog->manualRestore(null, $id);
        }

        $redirect = $this->ctrlSupporter->getRedirectPathFromComplete();
        return redirect( $redirect );

    }
    
    /**
     * function getDeleteTargetModel
     *
     *
     */
    protected function getDeleteTargetModel()
    {
        return app(Model::class);
    }
}
