<?php
namespace Azuki\App\Http\Controllers\System;

/**
 * OperationLogsController クラス
 *
 * @copyright Copyright 2019 --- Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Controllers\System\BaseController;
use Azuki\App\Http\Modules\ActionTrait\ListActionPackageWithDetail;
use Azuki\App\Http\Modules\ActionTrait\TraitParts\DeleteOldDataTrait;
//use Azuki\App\Contracts\Http\Validation\System\OperationLogsValidator as Validator;
use Azuki\App\Http\Validation\SharedValidator as Validator;
use Azuki\App\Contracts\Models\OperationLogs as Model;

/**
 * class OperationLogsController
 *
 *
 */
class OperationLogsController extends BaseController
{
    /**
     * サポートサービスのオブジェクト
     *
     * @var CtrlCupporter ctrlSupporter
     */
    protected $ctrlSupporter;

    /**
     * 追加構成要素
     *
     * array $elements
     */
    protected $elements = [
        'table' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => '操作テーブル名',
                ],
                'table'    => [
                    'size'              => 8,
                    'type'              => 'text',
                    'name'              => 'table', 
                    'column'            => 'table',
                    'searchName'        => 'search[table]',
                    'searchPlaceholder' => '部分一致します',
                ],
            ],
            'list' => [
                [
                    'type'           => CONTROL_TYPE_TEXT,
                    'width'          => '20%',
                    'orderable'      => false,
                ],
            ],
        ],
        'target_id' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => '対象ID',
                ],
                'target_id'    => [
                    'size'              => 8,
                    'type'              => 'text',
                    'name'              => 'target_id', 
                    'column'            => 'target_id',
                    'searchName'        => 'search[target_id]',
                ],
            ],
            'list' => [
                [
                    'type'           => CONTROL_TYPE_TEXT,
                    'width'          => '8%',
                    'orderable'      => true,
                ],
            ],
        ],
        'user' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => '操作ユーザー',
                ],
                'user'    => [
                    'size'              => 8,
                    'type'              => 'text',
                    'name'              => 'user', 
                    'column'            => 'user',
                ],
            ],
            'list' => [
                [
                    'type'           => CONTROL_TYPE_TEXT,
                    'width'          => '12%',
                    'orderable'      => false,
                ],
            ],
        ],
        'user_kind' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => 'ユーザー種別',
                ],
                'user_kind'    => [
                    'size'              => 8,
                    'type'              => 'radio',
                    'name'              => 'user_kind', 
                    'column'            => 'user_kind',
                    'select'            => 'userKindLabel',
                    'searchType'        => 'checkbox',
                    'searchName'        => 'search[user_kind]',
                ],
            ],
            'list' => [
                [
                    'type'           => CONTROL_TYPE_SELECT,
                    'width'          => '12%',
                    'orderable'      => true,
                ],
            ],
        ],
        'op_mode' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => '操作',
                ],
                'op_mode'    => [
                    'size'              => 8,
                    'type'              => 'radio',
                    'name'              => 'op_mode', 
                    'column'            => 'op_mode',
                    'select'            => 'opModeLabel',
                    'searchType'        => 'checkbox',
                    'searchName'        => 'search[op_mode]',
                ],
            ],
            'list' => [
                [
                    'type'           => CONTROL_TYPE_SELECT,
                    'width'          => '12%',
                    'orderable'      => true,
                ],
            ],
        ],
        'user_info' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => '操作時ユーザー情報',
                ],
                'user_info'    => [
                    'size'              => 8,
                    'type'              => 'serialize-array',
                    'name'              => 'user_info', 
                    'column'            => 'user_info',
                ],
            ],
        ],
        'original_data' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => '操作前データ',
                ],
                'original_data'    => [
                    'size'              => 8,
                    'type'              => 'serialize-array',
                    'name'              => 'original_data', 
                    'column'            => 'original_data',
                ],
            ],
        ],
        'dirty_data' => [
            'form'   => [
                'title' => [
                    'size' => 3,
                    'type' => 'title',
                    'name' => '操作差分情報',
                ],
                'dirty_data'    => [
                    'size'              => 8,
                    'type'              => 'serialize-array',
                    'name'              => 'dirty_data', 
                    'column'            => 'dirty_data',
                ],
            ],
        ],
    ];
    
    /**
     * 画面構成に必要な要素のモジュールの定義
     *
     */
    protected $elementModues = [
        \Azuki\App\Http\Modules\Forms\BasicElements::class,
    ];

    /**
     * 基本のアクションセットのトレイトを読み込みます
     */
    use ListActionPackageWithDetail;
    use DeleteOldDataTrait;

    /**
     * ページのtitleタグに表示する文字列
     *
     * string pageTitle
     */
    protected $pageTitle = '操作ログ管理';

    /**
     * ページのh1に表示する文字列
     *
     * string pageNamePrefix
     */
    protected $pageNamePrefix = '操作ログ';

    /**
     * コントローラ名として使用する文字列
     *
     * string controllerName
     */
    protected $controllerName = 'operation-logs';
    
    /**
     * 一覧の表示順設定
     * 必要なければ変数定義自体不要
     *
     * array viewOrder
     */
    protected $viewOrder = ['order' => 'created_at', 'condition' => ['created_at' => 'desc']];
    
    /**
     * 一覧の操作アイコン設定
     * 必要なければ変数定義自体不要
     *
     * array ctrlCondition
     */
    protected $ctrlCondition = [
        'edit' => [
            'default' => false,
        ],
        'delete' => [
            'default' => false,
        ],
    ];
    
    /**
     *
     *
     */
    protected $addViews = [
        'isBulkDelete' => true,
    ];
    
    /**
     * デートタイムピッカーの使用有無
     * 必要なければ変数定義自体不要
     *
     * boolean needDatetimePicker
     */
    protected $needDatetimePicker = true;
    
    /**
     * 各画面タイプごとの要素のオーダー設定
     *
     * array $elementsOrder
     */
    protected $elementsOrder = [
        'search' => ['table', 'user_kind', 'op_mode', 'period' ],
        'detail' => ['created', 'target_id', 'table', 'op_mode', 'original_data', 'dirty_data', 'user', 'user_kind', 'user_info' ],
        'list'   => ['table', 'target_id', 'op_mode', 'user', 'user_kind', 'created', 'ctrl' ],
    ];

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Model $model, Validator $validator)
    {
        parent::__construct();
        
        $this->setCtrlSuppoter(
            $this->getCtrlSuppoterParameters($model, $validator)
        );
        $this->elements['created']['form']['title']['name'] = '操作日時';
        $this->elements['created']['list']['created']['orderable'] = true;
        $this->elements['ctrl']['list']['ctrl']['ctrl'] = [CTRL_DETAIL];
    }
    
    /**
     * function getCtrlSuppoterTemplatePrefix
     *
     *
     */
    protected function getCtrlSuppoterTemplatePrefix()
    {
        return $this->getViewPrefix().'templates.';
    }
    
    /**
     * function getDeleteTargetModel
     *
     *
     */
    protected function getDeleteTargetModel()
    {
        return app(Model::class);
    }
}
