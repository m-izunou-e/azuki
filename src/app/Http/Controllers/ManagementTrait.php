<?php
namespace Azuki\App\Http\Controllers;

/**
 *
 * 管理機能に共通した機能を定義したトレイト
 *
 */
use Azuki\App\Models\SystemSettings;

/**
 * class ManagementTrait
 *
 */
trait ManagementTrait
{
    /**
     * function loadModuleElements
     *
     * メンバ変数の$elementsとelementModulesから$elementsを作成するメソッド
     *
     */
    protected function loadModuleElements()
    {
        $elm = $this->elements;
    
        if( isset($this->elementModues) && is_array($this->elementModues) ) {
            foreach( $this->elementModues as $module ) {
                $instance = new $module();
                if( $instance instanceof \Azuki\App\Http\Modules\Forms\AbstractFormModules ) {
//                    $elm = array_merge($elm, $instance->getElements());
                    $elm = array_merge($instance->getElements(), $elm);
                }
            }
        }
        
        return $elm;
    }
    
    /**
     * function getSearchLayout
     *
     * 検索フィールドのレイアウトを定義する配列を返す
     *
     * @return Array 検索フィールドのレイアウト
     */
    protected function getSearchLayout()
    {
        return $this->getLayoutSetting('search', true);
    }
    
    /**
     * function getSearchForm
     *
     * 検索フィールドのフォームを定義する配列を返す
     *
     * @return Array 検索フィールドのフォーム設定
     */
    protected function getSearchForm()
    {
        return $this->getFormSetting('search', true);
    }
    
    /**
     * function getDetailLayout
     *
     * 詳細画面のレイアウトを定義する配列を返す
     * 登録・編集画面のレイアウトにIDと作成日を自動追加した形を基本とする
     *
     * @return Array 詳細画面のレイアウト設定
     */
    protected function getDetailLayout()
    {
        return $this->getLayoutSetting('detail');
    }
    
    /**
     * function getDetailForm
     *
     * 詳細画面のフォームを定義する配列を返す
     * 登録・編集画面のフォームにIDと作成日を自動追加した形を基本とする
     *
     * @return Array 詳細画面のフォーム設定
     */
    protected function getDetailForm()
    {
        return $this->getFormSetting('detail');
    }
    
    /**
     * function getFormLayout
     *
     * 登録・編集画面のレイアウトを定義する配列を返す
     *
     * @return Array 登録・編集画面のレイアウト設定
     */
    protected function getFormLayout()
    {
        return $this->getLayoutSetting('form');
    }
    
    /**
     * function getConfirmLayout
     *
     * 確認画面のレイアウトを定義する配列を返す
     *
     * @return Array 確認画面のレイアウト設定
     */
    protected function getConfirmLayout()
    {
        return $this->getLayoutSetting('confirm');
    }
    
    /**
     * function getForm
     *
     * 登録・編集画面のフォームを定義する配列を返す
     *
     * @return Array 登録・編集画面のフォーム設定
     */
    protected function getForm()
    {
        return $this->getFormSetting('form');
    }
    
    /**
     * function getListSettings
     *
     * 一覧表示に表示する列設定を定義する
     *
     * @return Array 一覧表示の表示設定
     */
    protected function getListSettings()
    {
        return $this->getListColumnSetting();
    }
    
    /**
     * function getListColumnSetting
     *
     * 一覧表示のための設定を返す
     *
     */
    protected function getListColumnSetting()
    {
        $ret = [];
        $elementsOrder = $this->getElementsOrder('list');
        $element = $this->elements;
        
        foreach( $elementsOrder as $id ) {
            $forms = isset($element[$id]['form']) ? $element[$id]['form'] : null;
            $list  = $element[$id]['list'];
            
            foreach($list as $set) {
                if( is_array($forms) ) {
                    $title  = $forms['title']['name'];
                    $set['title']  = isset($set['title']) ? $set['title'] : $title;
                    $set['column'] = isset($set['column']) ? $set['column'] : $forms[$id]['column'];
                    if( isset($set['orderable']) ) {
                        $set['orderColumn'] = isset($set['orderColumn']) ? $set['orderColumn'] : $set['column'];
                    }
                    if( isset($forms[$id]['select']) ) {
                        $set['select'] = $forms[$id]['select'];
                    }
                }
                $ret[] = $set;
            }
        }
        return $ret;
    }
    
    /**
     * function getLayoutSetting
     *
     * レイアウト設定を返す
     *
     */
    protected function getLayoutSetting($kind, $isSearch = false)
    {
        $ret = [];
        $mode = $kind == 'confirm' ? 'form' : $kind;
        $elementsOrder = $this->getElementsOrder($mode);
        $element = $this->elements;
        $layoutKey = $isSearch ? 'searchLayout' : 'layout';
        
        foreach( $elementsOrder as $id ) {
            $key = '';
            $layout = ['title_'.$id, $id];
            
            $lKey = $layoutKey;
            if($kind == 'confirm' && isset($element[$id]['confirmLayout'])) {
                $lKey = 'confirmLayout';
            }
            
            if(isset($element[$id][$lKey]) && is_array($element[$id][$lKey])) {
                $set = $element[$id][$lKey];
                $key = isset($set['key']) ? $set['key'] : '';
                $val = isset($set['val']) ? $set['val'] : '';
                if( !empty($val) ) {
                    $layout = $val;
                }
            }
            if(!empty($key)) {
                $ret[$key] = $layout;
            } else {
                $ret[] = $layout;
            }
        }
        return $ret;
    }
    
    /**
     * function getFormSetting
     *
     * フォームの設定を返す。
     * この設定は、検索フィールド・登録・編集・詳細画面にて使用される
     *
     */
    protected function getFormSetting($kind, $isSearch = false)
    {
        $ret = [];
        $elementsOrder = $this->getElementsOrder($kind);
        $element = $this->elements;
        
        foreach( $elementsOrder as $id ) {
            $forms = $element[$id]['form'];
            if( isset($forms['title']) ) {
                if(!isset($forms['title']['type'])) {
                    $forms['title']['type'] =  'title';
                }
                $forms['title_'.$id] = $forms['title'];
                unset($forms['title']);
            }
            
            foreach($forms as $name => $value) {
                $tmp = $value;
                // 検索の場合にname属性を置き換える
                if($isSearch && isset($tmp['name']) && $tmp['type'] != 'title') {
                    if(!in_array($tmp['type'], ['title', 'label', 'button'])) {
                        $tmp['name'] = sprintf('search[%s]', $tmp['name']);
                    }
                }
                foreach( $value as $key => $val ) {
                    if( preg_match('/^search(.*)/', $key, $matches) ) {
//                        if( $kind == 'search' ) {
                        if( $isSearch ) {
                            $overRideKey = lcfirst($matches[1]);
                            $tmp[$overRideKey] = $val;
                        }
                        unset($tmp[$key]);
                    } elseif( $isSearch && preg_match('/^required(.*)/', $key, $matches) ) {
                        // 検索フィールドの場合、必須・任意マークの指定を解除
                        unset($tmp[$key]);
                    }
                }
                unset($tmp['validate']);
                $ret[$name] = $tmp;
            }
        }
        return $ret;
    }
    
    /**
     * function getElementsOrder
     *
     * 要素の構成・並び順を返す
     *
     */
    protected function getElementsOrder($kind)
    {
        $elements = $this->elementsOrder;
        return isset($elements[$kind]) ? $elements[$kind] : [];
    }
    
    /**
     * function getValidSettings
     *
     * validate設定を取得する
     *
     */
    protected function getValidSettings()
    {
        $ret = [];

        $elementsOrder = $this->getElementsOrder('form');
        $element = $this->elements;
        
        foreach( $elementsOrder as $id ) {
            $tmp = [];
            $forms = $element[$id]['form'];
            if( isset($forms['title']) ) {
                $tmp['attribute'] = $forms['title']['name'];
//                $tmp['required']  = isset($forms['title']['required']) ? $forms['title']['required'] : '';
                unset($forms['title']);
            }

            foreach($forms as $name => $value) {
                if(isset($value['validate'])) {
                    $column = $value['column'];
                    $tmp['validate'] = $value['validate'];
                    if(isset($value['select'])) {
                        $tmp['validate']['select'] = $value['select'];

                        if( isset($tmp['validate']['rules']) && is_array($tmp['validate']['rules']) ) {
                            $selectRule = ['type' => 'in', 'condition' => implode(',', getSelectValues($value['select']))];
                            $tmp['validate']['rules'][] = $selectRule;
                        }
                    }
                    $ret[$column] = $tmp;
                }
            }
        }

        return $ret;
    }
}
