<?php
namespace Azuki\App\Http\Controllers\Common;

/**
 * ユーザー登録画面を生成するコントローラ
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Controllers\Common\BaseController;
use Azuki\App\Http\Modules\RegisterUser;
use Azuki\App\Http\Interfaces\RegisterUserInterface;
use Azuki\App\Contracts\Models\Users;
use Illuminate\Http\Request;

/**
 * class 
 *
 */
class RegistController extends BaseController implements RegisterUserInterface
{
    use RegisterUser;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/mypage';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest');
        $this->usersTable = $this->getModel()->getTable();
    }

    /**
     * function indexAction 初期表示のアクション
     * 
     * @Get("/", as="index")
     *
     * 初期表示のアクション
     * 初期表示は一覧とするため、一覧のアクションにリダイレクト
     * @return void
     */
    public function getIndex()
    {
        return view($this->getViewPrefix().'regist');
    }
    
    /**
     * function getValidateRules
     *
     * バリデーションルールの設定
     *
     */
    public function getValidateRules()
    {
        return [
            'name'     => 'required|string|max:255',
            'login_id' => 'required|alpha_num_symbol:@.|max:255|unique:'.$this->usersTable.',login_id,null,id,deleted_at,NULL',
            'email'    => 'nullable|email|unique:'.$this->usersTable.',email,null,id,deleted_at,NULL',
            'password' => 'required|string|min:6|confirmed',
        ];
    }
    
    /**
     * function getValidateMessages
     *
     * バリデーションメッセージの設定
     *
     */
    public function getValidateMessages()
    {
        return [
            'login_id.alpha_num_symbol' => ':attributeは半角英数字記号[@.]のみとしてください',
        ];
    }
    
    /**
     * function getModel
     *
     * ユーザーを処理するモデルを返すメソッドUserモデルを返している
     * →仮に管理画面側だったらManagerモデルを返すようにする
     *
     */
    public function getModel()
    {
        return app(Users::class);
    }
    
    /**
     * function createData
     *
     * ユーザー登録を行うデータを作成する
     *
     * @param array $data
     */
    public function createData(array $data)
    {
        return [
            'name'     => $data['name'],
            'login_id' => $data['login_id'],
            'password' => $data['password'],
            'email'    => isset($data['email']) ? $data['email'] : '',
            'role'     => 0,
            'is_login' => 1,
            'enable'   => 1,
        ];
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        // 新規登録に成功すると自動的にログイン状態になり
        // このメソッドがリダイレクト前に呼び出される
        // ここでログインログの記録を行う
        $this->recordLoginLogs($user, USER_KIND_USERS);
    }
}
