<?php
namespace Azuki\App\Http\Controllers\Common;

/**
 * マイページを扱うコントローラ
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Controllers\Common\MypageBaseController;
use Azuki\App\Contracts\Http\Validation\Common\UsersValidator as Validator;
use Azuki\App\Contracts\Models\Users as Model;

use Azuki\App\Http\Requests\FormRequest;
use Azuki\App\Http\Controllers\Common\Trait\ProfileTrait;

/**
 * class MypageController
 *
 */
class MypageController extends MypageBaseController
{
    /**
     * ページのtitleタグに表示する文字列
     *
     * string pageTitle
     */
    protected $pageTitle = 'マイページ';

    /**
     * ページのh1に表示する文字列
     *
     * string pageNamePrefix
     */
    protected $pageNamePrefix = 'マイページ';

    /**
     * コントローラ名として使用する文字列
     *
     * string controllerName
     */
    protected $controllerName = 'mypage';

    use ProfileTrait;
    
    /**
     *
     */
    protected $indexTemplateName = 'mypage';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Model $model, Validator $validator)
    {
        parent::__construct();
        
        $this->setCtrlSuppoter(
            $this->getCtrlSuppoterParameters($model, $validator)
        );

    }
    
    /**
     * function getCtrlSuppoterTemplatePrefix
     *
     *
     */
    protected function getCtrlSuppoterTemplatePrefix()
    {
        return $this->getViewPrefix().$this->controllerName . '.';
    }
}
