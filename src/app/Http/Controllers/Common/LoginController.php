<?php
namespace Azuki\App\Http\Controllers\Common;

/**
 * ログイン処理を扱うコントローラ
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Controllers\Common\BaseController;
use Azuki\App\Http\Modules\Authenticate;
use Illuminate\Http\Request;

use Azuki\App\Http\Modules\SNSAuthenticate;

/**
 * class LoginController
 *
 */
class LoginController extends BaseController
{
    use Authenticate;
    use SNSAuthenticate;
    
    /**
     * ログイン成功時のリダイレクト先指定
     *
     */
    protected $redirectTo = '/mypage';

    /**
     * コントローラ名として使用する文字列
     *
     * string controllerName
     */
    protected $controllerName = 'login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * function getIndex
     *
     * ログイン画面を表示
     *
     * @return void
     */
    public function getIndex()
    {
        return view($this->getViewPrefix().'login');
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        // ログイン成功時に呼び出されるのでここでログインログを記録する
        // return値はfalseかリダイレクト先のレスポンスにする必要がある
        $this->recordLoginLogs($user, USER_KIND_USERS);
    }

    /**
     * function credentials
     *
     * ログイン処理時に使用するデータを作成する
     * ログインを許可するユーザーの条件をここで追加することができるが
     * 複雑な条件は設定できない
     *
     */
    protected function credentials(Request $request)
    {
        $username = $this->username();
    
        return [
            $username  => $request->get($username),
            'password' => $request->get('password'),
            'enable'   => 1,
        ];
    }
}
