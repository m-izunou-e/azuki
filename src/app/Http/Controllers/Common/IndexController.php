<?php
namespace Azuki\App\Http\Controllers\Common;

/**
 * フロント画面トップページを扱うコントローラ
 * 
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Controllers\Common\BaseController;

/**
 * class IndexController
 *
 */
class IndexController extends BaseController
{
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * function getIndex
     *
     * @return void
     */
    public function getIndex()
    {
        return view($this->getViewPrefix().'index');
    }
}
