<?php
namespace Azuki\App\Http\Controllers\Common;

/**
 * 画像アップロード用インターフェースを実装する
 *
 * @package pc
 *
 */

use Azuki\App\Http\Controllers\Common\BaseController;

/**
 * 画像アップロードを処理するクラス
 *
 */
class UploadedFileViewController extends BaseController
{
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * function indexAction 画像表示のアクション
     * 
     * @Get("/", as="upload.index")
     *
     * @return void
     */
    public function getFile($kind, $path1, $path2 = null, $path3 = null, $path4 = null)
    {
        $viewer = app('UploadedFileViewer');
        $fileInfo = $viewer->getInfo($path1, $path2, $path3, $path4);
        
        if( is_null($fileInfo) ) {
            // エラー処理
            $message = sprintf(
                '指定のファイルは存在していません。 [kind:%s,path1:%s,path2:%s,path3:%s,path4:%s]',
                $kind, $path1, $path2, $path3, $path4
            );
            throw new \Exception($message);
        }
        
        return response()->stream(function() use ($viewer, $fileInfo){
                $viewer->getStreamData(
                    $fileInfo,
                    $viewer->parseRequestRange(
                        $viewer->getRequestRange(),
                        $viewer->getDataSize($fileInfo)
                    )
                );
            },
            $viewer->getResponseFileDataCode(),
            $viewer->getResponseFileDataHeader($fileInfo)
        );
    }
}
