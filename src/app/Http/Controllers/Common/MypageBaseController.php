<?php
namespace Azuki\App\Http\Controllers\Common;

/**
 * マイページを扱うコントローラ
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Controllers\Common\BaseController;
use Azuki\App\Services\FrontBasicCtrlSupporter;

/**
 * class MypageController
 *
 */
class MypageBaseController extends BaseController
{
    /**
     * 画面構成要素
     *
     * array $elements
     */
    protected $elements = [];

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $auth = app('auth');
        $this->auth = $auth;

        $this->middleware(function($request, $next){
            $this->callbackMiddleware($request);
            return $next($request);
        });
    }
    
    /**
     * function callbackMiddleware
     *
     * ミドルウェアからコールバックされた際に実行するメソッド
     * ユーザーデータの設定やユーザーの状態に応じて変化するデータの生成、アサインを行う
     *
     * @param $request 
     */
    protected function callbackMiddleware($request)
    {
        // ユーザーデータの設定
        $this->user = $request->user();
        view()->share( "user", $this->user );
    }
    
    /**
     * 管理画面の各種機能を提供するCtrlSuppoterクラスを設定する
     * 各設定値は各コントローラーから配列で受け取る
     *
     */
    protected function setCtrlSuppoter(Array $ctrlSuppoterInitSetting)
    {
        $this->elements = $this->loadModuleElements();
    
        $valid = $ctrlSuppoterInitSetting['validator'];
        $valid->setValidInitialize($this->getValidSettings());
        
        $ctrlSupporter = new FrontBasicCtrlSupporter();
        $ctrlSupporter->init($ctrlSuppoterInitSetting);
        $ctrlSupporter->setForm($this->getForm());
        $this->ctrlSupporter = $ctrlSupporter;
    }
    
    /**
     * function loadModuleElements
     *
     */
    protected function loadModuleElements()
    {
        $elm = $this->elements;
    
        if( isset($this->elementModues) && is_array($this->elementModues) ) {
            foreach( $this->elementModues as $module ) {
                $instance = new $module();
                if( $instance instanceof \Azuki\App\Http\Modules\Forms\AbstractFormModules ) {
                    $elm = array_merge($elm, $instance->getElements());
                }
            }
        }
        
        return $elm;
    }
}
