<?php
namespace Azuki\App\Http\Controllers\Common;

/**
 * フロント画面を構成するコントローラの抽象クラス
 * 共通設定・共通メソッドなどをこのクラスで実装する
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Controllers\Controller;
use Azuki\App\Http\Modules\CtrlSupporterParameterHelper;

/**
 * class BaseController
 *
 */
abstract class BaseController extends Controller
{

    /**
     * viewのテンプレート指定時のプレフィックス文字列
     *
     * string viewPrefix
     */
    protected $viewPrefix = 'common.';
    
    /**
     * ルーティングで設定されているプレフィックス
     *
     * string prefix
     */
    protected $prefix;

    /**
     * ページ名前とする文字列
     *
     * string pageTitle
     */
    protected $pageTitle = '';

    use CtrlSupporterParameterHelper;

    /**
     * 基底クラスのコンストラクタ.
     *
     * @return void
     */
    public function __construct()
    {
        $this->setVendorPrefix();
        $this->setPageInfomation('common');

        view()->share( "story", "/" );
        view()->share( "pageTitle",  $this->pageTitle );
        view()->share( "controller", '' );
        view()->share( "vendorPrefix", $this->vendorPrefix );
        view()->share( "analyticsTags", $this->getAnalyticsTags() );

        // iconの設定を行う
        $this->setShareIcon();

        $this->middleware(function($request, $next){
            $this->recordAccessLogs($request);
            return $next($request);
        });
    }
}
