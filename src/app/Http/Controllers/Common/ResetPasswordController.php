<?php
namespace Azuki\App\Http\Controllers\Common;

/**
 * パスワード再設定に関するページを提供するコントローラ
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

// パスワードリセット完了時にログイン状態になるのでMypageBaseを継承させてる
use Azuki\App\Http\Controllers\Common\MypageBaseController;
use Azuki\App\Http\Modules\SendsPasswordResetEmails;
use Azuki\App\Http\Modules\ResetPassword;
use Illuminate\Http\Request;

/**
 * class ResetPasswordController
 *
 */
class ResetPasswordController extends MypageBaseController
{
    use ResetPassword, SendsPasswordResetEmails {
        ResetPassword::credentials       insteadof SendsPasswordResetEmails;
        SendsPasswordResetEmails::broker insteadof ResetPassword;
    }

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/mypage';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest')->except('getCompleteResetPassword');
    }

}
