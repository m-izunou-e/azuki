<?php
namespace Azuki\App\Http\Controllers\Common\Trait;

use Azuki\App\Http\Requests\FormRequest;
use Azuki\App\Http\Controllers\ManagementTrait;


trait ProfileTrait
{
    use ManagementTrait;
    
    /**
     * 画面構成に必要な要素のモジュールの定義
     *
     */
    protected $elementModues = [
        \Azuki\App\Http\Modules\Forms\Users::class,
    ];
    
    /**
     * 各画面タイプごとの要素のオーダー設定
     *
     * array $elementsOrder
     */
    protected $elementsOrder = [
        'detail' => ['name', 'login_id', 'email' ],
        'form'   => ['name', 'login_id', 'email', 'password', 'password_conf' ],
    ];

    /**
     * function indexAction 初期表示のアクション
     * 
     * @Get("/", as="index")
     *
     * 初期表示のアクション
     * 初期表示は一覧とするため、一覧のアクションにリダイレクト
     * @return void
     */
    public function getIndex()
    {
        $id = $this->user->id;

        $forms = $this->getDetailForm();
        $this->ctrlSupporter->setDetailForm($forms);
        $this->ctrlSupporter->setResultStatus();

        return $this->ctrlSupporter->view($this->indexTemplateName ?? 'index', [
            'flow'         => 'mypage',
//            'prev'         => $prev,
            'post'         => $this->ctrlSupporter->find($id),
            'detailLayout' => $this->getDetailLayout(),
            'detailForm'   => $forms,
        ]);
    }

    /**
     * function editAction 編集画面表示のアクション
     *
     * 編集画面表示のアクション
     *
     * @return void
     */
    public function getEdit($prev = null)
    {
        $id = $this->user->id;

        $post = $this->ctrlSupporter->find($id);
        $old = $this->ctrlSupporter->getOldInput();
        if( !empty($old) ) {
            $post = $old;
        }
        $this->ctrlSupporter->beforeDisplayEditForm($id, $post);

        return $this->_displayForm(
            'edit',
            $prev,
            $post
        );
    }
    
    public function postEdit()
    {
        $id = $this->user->id;

        $post = $this->ctrlSupporter->getPost();
        $prev = $this->ctrlSupporter->getPrev();
        $this->ctrlSupporter->beforeDisplayEditForm($id, $post);

        return $this->_displayForm(
            'edit',
            $prev,
            $post
        );
    }

    /**
     * function _displayForm フォーム画面表示処理
     *
     * フォーム画面表示処理
     *
     */
    private function _displayForm($flow, $prev, $post)
    {

        return $this->ctrlSupporter->view('form', [
            'flow'         => $flow,
            'prev'         => $prev,
            'post'         => $post,
            'formLayout'   => $this->getFormLayout(),
            'toBack'       => 'index',
        ]);
    }

    /**
     * function confirmAction 確認画面表示のアクション
     *
     * 確認画面表示のアクション
     *
     * @return void
     */
    public function postConfirm(FormRequest $request)
    {
        $id = $this->user->id;

        $post = $this->ctrlSupporter->validation($request);

        $this->ctrlSupporter->beforeDisplayConfirm($id, $post);

        return $this->ctrlSupporter->view('confirm', [
            'post'         => $post,
            'formLayout'   => $this->getConfirmLayout(),
//            'form'         => $forms,
        ]);
    }

    /**
     * function postDone 完了処理のアクション
     *
     * 完了処理のアクション
     *
     * @return void
     */
    public function postDone(FormRequest $request)
    {
        $id = $this->user->id;
        $post = $this->ctrlSupporter->validation($request);

        $this->ctrlSupporter->createOrUpdate($post);
        
        $redirect = $this->redirectName ?? $this->controllerName;
        return redirect( $redirect );
    }
}
