<?php
namespace Azuki\App\Http\Middleware;

/**
 * 管理画面ユーザーがログイン認証されていた場合にリダイレクト処理を
 * 行うためのミドルウェア
 * ログイン画面などログイン状態では表示しない画面に対して登録する
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Closure;
use Illuminate\Support\Facades\Auth;

/**
 * class RedirectIfAuthenticatedManage
 * トップURLに遷移
 *
 */
class RedirectIfAuthenticatedSystem
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            $prefix = str_replace( '/', '', app('router')->getCurrentRoute()->action['prefix'] );
            return redirect('/'.$prefix);
        }

        return $next($request);
    }
}
