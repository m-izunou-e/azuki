<?php
namespace Azuki\App\Http\Middleware;

/**
 * フロント画面用のユーザーがログイン状態の際にリダイレクト処理を
 * 行うミドルウェア
 * ログイン画面などログイン時には表示しない画面に登録する
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Closure;
use Illuminate\Support\Facades\Auth;

/**
 * class RedirectIfAuthenticated
 * mypageに遷移
 *
 */
class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            return redirect('/mypage');
        }

        return $next($request);
    }
}
