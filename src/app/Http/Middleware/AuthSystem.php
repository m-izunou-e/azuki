<?php
namespace Azuki\App\Http\Middleware;

/**
 * 管理画面側の認証チェックを行うミドルウェア
 * コンストラクタにて認証ドライバーを「master」に設定。
 * このミドルウェアを登録した画面は認証を通っていないと表示されない
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Closure;
use Auth;

/**
 * class AuthManage
 *
 */
class AuthSystem {

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct()
    {
        $auth = app('auth');
        $auth->shouldUse('system');
        $this->auth = $auth;
        $this->prefix = trim(app('router')->getCurrentRoute()->action['prefix'], '/');
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->guest())
        {
            if ($request->ajax())
            {
                return response('Unauthorized.', 401);
            }
            else
            {
                return redirect()->guest('/'.$this->prefix.'/login');
            }
        }

        return $next($request);
    }

}
