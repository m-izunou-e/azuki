<?php
namespace Azuki\App\Http\Middleware;

/**
 * 管理画面各URLに対してアクセスコントロールを行うミドルウェア
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Closure;

/**
 * class AclManage
 *
 */
class Acl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // ここでURLに対する権限チェックを行う
        $res = $this->allow($request);
        if( $res->allowed() ) {
            return $next($request);
        }

        // 権限なしの場合は403にアボートする
        return abort('403', $res->message());
    }
    
    /**
     * function allow
     *
     * 許可されているかどうかを判定して返すメソッド
     * 許可されていればtrue、許可されていなければfalseを返す
     *
     * 判断は権限層で行い、URLをベースにしている
     * URLに紐づけた名前ベースで行う場合は、$request->route()->getName()　を使うとよい
     *
     * @param Request $request
     */
    protected function allow( $request )
    {
        $gate = app('AzukiGate');
        
        $stage = $this->getStage($request);
        return $gate->inspect($stage, $request);
    }
    
    /**
     * function getStage
     *
     * requestから対象階層の判定を行う
     *
     * @param Request $request
     */
    protected function getStage($request)
    {
        $subDir = getSubDir(getAccessDomainName());
        
        $stage = 'common';
        if( $request->is($subDir['system']) || $request->is($subDir['system'].'/*') ) {
            $stage = 'system';
        } elseif( $request->is($subDir['manage']) || $request->is($subDir['manage'].'/*') ) {
            $stage = 'manage';
        }
        return $stage;
    }
}
