<?php

/**
 * ルーティングを生成する元になる設定ファイル
 * 実際のルーティング生成は　[Azuki\App\Services\RouteControll]　にて行われる。
 *
 * この設定ファイルは
 * 1.ルーティングの設定のみを記述し、ロジックを排除することでルーティングを見やすくすることを目的としている
 * 2.また、実際のルーティングの生成をプログラム切り離してプログラム化することでDBや複数ファイルでルーティング
 *   を定義するなどの拡張性が増すことも考慮している
 *
 * 大きく、group と list からなり
 * group　に設定した配列はルーティングでの Route::group へ設定される
 * list　　に設定した配列は　Route::post　や　Route::get　などへ設定される
 *
 * 原則としてひとつひとつのルーティングの設定は
 * 'url'
 * 'method'
 * 'name'
 * 'middleware'
 * 'uses'
 * 'where'
 * を設定する必要がある。ただし、ミドルウェアがなく、決まったルールでnameならびにusesの生成をして問題ないものについては
 *   [
 *       'prefix'     => 'account',
 *       'set'        => [
 *           [ 'method' => 'get',  'url' => 'list' ],
 *           [ 'method' => 'post', 'url' => 'list' ],
 *           [ 'method' => 'post', 'url' => 'form' ],
 *           [ 'method' => 'get',  'url' => 'form' ],
 *           [ 'method' => 'get',  'url' => 'detail/{id}', 'where' => ['id' => '[0-9]+'] ],
 *           [ 'method' => 'post', 'url' => 'edit' ],
 *           [ 'method' => 'get',  'url' => 'edit/{id}', 'where' => ['id' => '[0-9]+'] ],
 *           [ 'method' => 'get',  'url' => 'edit/{id}/prev/{prev}', 'where' => ['id' => '[0-9]+', 'prev' => 'detail|list'] ],
 *           [ 'method' => 'post', 'url' => 'confirm' ],
 *           [ 'method' => 'post', 'url' => 'done' ],
 *           [ 'method' => 'post', 'url' => 'valid' ],
 *           [ 'method' => 'post', 'url' => 'delete' ],
 *       ],
 *       'controller' => 'AccountController',
 *   ],
 *
 * のように記載することでRouteController側で展開して登録される。set内容にwhere属性、name属性も設定が可能。
 *
 * また、group設定されている場合、nameにはgroupのprefixが「.」区切りで頭につく仕様。
 *
 * ※ドメイン別の設定ファイルが存在し、そのドメインでアクセスされている場合は読み込まれない
 *
 */

$routeList = [
    'group' => [
        // /system/～　に関するルーティング。ミドルウェアの設定のないURL
        [
            'prefix'     => 'system',
            'middleware' => [],
            'list'       => [
                [   // 管理画面ログインページ
                    'url'        => '/login',
                    'method'     => 'get',
                    'name'       => 'login',
                    'middleware' => [],
                    'uses'       => 'LoginController@getIndex',
                    'where'      => '',
                ],
                [   // 管理画面ログイン認証処理 ログイン後は/system/　に移動する想定
                    'url'        => '/login/auth',
                    'method'     => 'post',
                    'name'       => 'login.p-auth',
                    'middleware' => [],
                    'uses'       => 'LoginController@login',
                    'where'      => '',
                ],
                [   // 管理画面パスワードリセットに関する処理
                    'prefix'     => 'reset-password',
                    'set'        => [
                        [ 'method' => 'get',  'url' => 'send-reset-mail' ],
                        [ 'method' => 'post', 'url' => 'send-reset-mail' ],
                        [ 'method' => 'get',  'url' => 'sent-reset-mail' ],
                        [ 'method' => 'get',  'url' => 'reset-password/{token}' ],
                        [ 'method' => 'post', 'url' => 'reset-password' ],
                        [ 'method' => 'get',  'url' => 'complete-reset-password' ],
                    ],
                    'controller' => 'ResetPasswordController',
                ],
            ],
        ],
        // /system/～　に関するルーティング。ミドルウェアにてログイン、権限の制限がかかったURL
        [
            'prefix'     => 'system',
            'middleware' => ['auth.system', 'acl'],
            'list'       => [
                [   // 管理画面トップページ
                    'url'        => '/',
                    'method'     => 'get',
                    'name'       => '',
                    'middleware' => [],
                    'uses'       => 'IndexController@getIndex',
                    'where'      => '',
                ],
                [   // 管理画面ログアウトページ
                    'url'        => '/login/logout',
                    'method'     => 'get',
                    'name'       => 'logout',
                    'middleware' => [],
                    'uses'       => 'LoginController@logout',
                    'where'      => '',
                ],
                [   // ファイルアップロード用のルーティング（管理画面内）
                    'url'        => '/file-upload',
                    'method'     => 'post',
                    'name'       => 'file-upload',
                    'middleware' => [],
                    'uses'       => 'FileUploadController@postFile',
                    'where'      => '',
                ],
/*
                [   // マスターユーザーパスワードリセット管理画面
                    'prefix'     => 'reset-master-user',
                    'set'        => [
                        [ 'method' => 'get',  'url' => 'index' ],
                        [ 'method' => 'post', 'url' => 'done' ],
                        [ 'method' => 'get',  'url' => 'complete' ],
                    ],
                    'controller' => 'ResetMasterUserController',
                ],
*/
                [   // システムユーザープロフィール画面
                    'prefix'     => 'profile',
                    'set'        => [
                        [ 'method' => 'get',  'url' => 'index' ],
                        [ 'method' => 'get',  'url' => 'detail' ],
                        [ 'method' => 'post', 'url' => 'edit' ],
                        [ 'method' => 'get',  'url' => 'edit' ],
                        [ 'method' => 'post', 'url' => 'confirm' ],
                        [ 'method' => 'post', 'url' => 'done' ],
                    ],
                    'controller' => 'ProfileController',
                ],
                [   // システムユーザー管理画面
                    'prefix'     => 'directors',
                    'set'        => [
                        [ 'method' => 'get',  'url' => 'index' ],
                        [ 'method' => 'get',  'url' => 'list' ],
                        [ 'method' => 'post', 'url' => 'list' ],
                        [ 'method' => 'post', 'url' => 'regist' ],
                        [ 'method' => 'get',  'url' => 'regist' ],
                        [ 'method' => 'get',  'url' => 'detail/{id}', 'where' => ['id' => '[0-9]+'] ],
                        [ 'method' => 'post', 'url' => 'edit' ],
                        [ 'method' => 'get',  'url' => 'edit/{id}', 'where' => ['id' => '[0-9]+'] ],
                        [ 'method' => 'get',  'url' => 'edit/{id}/prev/{prev}', 'where' => ['id' => '[0-9]+', 'prev' => 'detail|list'] ],
                        [ 'method' => 'post', 'url' => 'confirm' ],
                        [ 'method' => 'post', 'url' => 'done' ],
                        [ 'method' => 'post', 'url' => 'delete' ],
                    ],
                    'controller' => 'DirectorsController',
                ],
                [   // 管理ユーザー管理画面
                    'prefix'     => 'managers',
                    'set'        => [
                        [ 'method' => 'get',  'url' => 'index' ],
                        [ 'method' => 'get',  'url' => 'list' ],
                        [ 'method' => 'post', 'url' => 'list' ],
                        [ 'method' => 'post', 'url' => 'regist' ],
                        [ 'method' => 'get',  'url' => 'regist' ],
                        [ 'method' => 'get',  'url' => 'detail/{id}', 'where' => ['id' => '[0-9]+'] ],
                        [ 'method' => 'post', 'url' => 'edit' ],
                        [ 'method' => 'get',  'url' => 'edit/{id}', 'where' => ['id' => '[0-9]+'] ],
                        [ 'method' => 'get',  'url' => 'edit/{id}/prev/{prev}', 'where' => ['id' => '[0-9]+', 'prev' => 'detail|list'] ],
                        [ 'method' => 'post', 'url' => 'confirm' ],
                        [ 'method' => 'post', 'url' => 'done' ],
                        [ 'method' => 'post', 'url' => 'delete' ],
                    ],
                    'controller' => 'ManagersController',
                ],
                [   // 一般ユーザー管理画面
                    'prefix'     => 'users',
                    'set'        => [
                        [ 'method' => 'get',  'url' => 'index' ],
                        [ 'method' => 'get',  'url' => 'list' ],
                        [ 'method' => 'post', 'url' => 'list' ],
                        [ 'method' => 'post', 'url' => 'regist' ],
                        [ 'method' => 'get',  'url' => 'regist' ],
                        [ 'method' => 'get',  'url' => 'detail/{id}', 'where' => ['id' => '[0-9]+'] ],
                        [ 'method' => 'post', 'url' => 'edit' ],
                        [ 'method' => 'get',  'url' => 'edit/{id}', 'where' => ['id' => '[0-9]+'] ],
                        [ 'method' => 'get',  'url' => 'edit/{id}/prev/{prev}', 'where' => ['id' => '[0-9]+', 'prev' => 'detail|list'] ],
                        [ 'method' => 'post', 'url' => 'confirm' ],
                        [ 'method' => 'post', 'url' => 'done' ],
                        [ 'method' => 'post', 'url' => 'delete' ],
                    ],
                    'controller' => 'UsersController',
                ],

                [   // システムロール管理画面
                    'prefix'     => 'system-roles',
                    'set'        => [
                        [ 'method' => 'get',  'url' => 'index' ],
                        [ 'method' => 'get',  'url' => 'list' ],
                        [ 'method' => 'post', 'url' => 'list' ],
                        [ 'method' => 'post', 'url' => 'regist' ],
                        [ 'method' => 'get',  'url' => 'regist' ],
                        [ 'method' => 'get',  'url' => 'detail/{id}', 'where' => ['id' => '[0-9]+'] ],
                        [ 'method' => 'post', 'url' => 'edit' ],
                        [ 'method' => 'get',  'url' => 'edit/{id}', 'where' => ['id' => '[0-9]+'] ],
                        [ 'method' => 'get',  'url' => 'edit/{id}/prev/{prev}', 'where' => ['id' => '[0-9]+', 'prev' => 'detail|list'] ],
                        [ 'method' => 'post', 'url' => 'confirm' ],
                        [ 'method' => 'post', 'url' => 'done' ],
                        [ 'method' => 'post', 'url' => 'delete' ],
                    ],
                    'controller' => 'SystemRolesController',
                ],
                [   // サイト管理者ロール管理画面
                    'prefix'     => 'manage-roles',
                    'set'        => [
                        [ 'method' => 'get',  'url' => 'index' ],
                        [ 'method' => 'get',  'url' => 'list' ],
                        [ 'method' => 'post', 'url' => 'list' ],
                        [ 'method' => 'post', 'url' => 'regist' ],
                        [ 'method' => 'get',  'url' => 'regist' ],
                        [ 'method' => 'get',  'url' => 'detail/{id}', 'where' => ['id' => '[0-9]+'] ],
                        [ 'method' => 'post', 'url' => 'edit' ],
                        [ 'method' => 'get',  'url' => 'edit/{id}', 'where' => ['id' => '[0-9]+'] ],
                        [ 'method' => 'get',  'url' => 'edit/{id}/prev/{prev}', 'where' => ['id' => '[0-9]+', 'prev' => 'detail|list'] ],
                        [ 'method' => 'post', 'url' => 'confirm' ],
                        [ 'method' => 'post', 'url' => 'done' ],
                        [ 'method' => 'post', 'url' => 'delete' ],
                    ],
                    'controller' => 'ManageRolesController',
                ],
                [   // ユーザーロール管理画面
                    'prefix'     => 'common-roles',
                    'set'        => [
                        [ 'method' => 'get',  'url' => 'index' ],
                        [ 'method' => 'get',  'url' => 'list' ],
                        [ 'method' => 'post', 'url' => 'list' ],
                        [ 'method' => 'post', 'url' => 'regist' ],
                        [ 'method' => 'get',  'url' => 'regist' ],
                        [ 'method' => 'get',  'url' => 'detail/{id}', 'where' => ['id' => '[0-9]+'] ],
                        [ 'method' => 'post', 'url' => 'edit' ],
                        [ 'method' => 'get',  'url' => 'edit/{id}', 'where' => ['id' => '[0-9]+'] ],
                        [ 'method' => 'get',  'url' => 'edit/{id}/prev/{prev}', 'where' => ['id' => '[0-9]+', 'prev' => 'detail|list'] ],
                        [ 'method' => 'post', 'url' => 'confirm' ],
                        [ 'method' => 'post', 'url' => 'done' ],
                        [ 'method' => 'post', 'url' => 'delete' ],
                    ],
                    'controller' => 'CommonRolesController',
                ],
                [   // 所属管理画面
                    'prefix'     => 'organizations',
                    'set'        => [
                        [ 'method' => 'get',  'url' => 'index' ],
                        [ 'method' => 'get',  'url' => 'list' ],
                        [ 'method' => 'post', 'url' => 'list' ],
                        [ 'method' => 'post', 'url' => 'regist' ],
                        [ 'method' => 'get',  'url' => 'regist' ],
                        [ 'method' => 'get',  'url' => 'detail/{id}', 'where' => ['id' => '[0-9]+'] ],
                        [ 'method' => 'post', 'url' => 'edit' ],
                        [ 'method' => 'get',  'url' => 'edit/{id}', 'where' => ['id' => '[0-9]+'] ],
                        [ 'method' => 'get',  'url' => 'edit/{id}/prev/{prev}', 'where' => ['id' => '[0-9]+', 'prev' => 'detail|list'] ],
                        [ 'method' => 'post', 'url' => 'confirm' ],
                        [ 'method' => 'post', 'url' => 'done' ],
                        [ 'method' => 'post', 'url' => 'delete' ],
                    ],
                    'controller' => 'OrganizationsController',
                ],
                // ログ関係
                [   // アクセスログ管理画面
                    'prefix'     => 'access-logs',
                    'set'        => [
                        [ 'method' => 'get',  'url' => 'index' ],
                        [ 'method' => 'get',  'url' => 'list' ],
                        [ 'method' => 'post', 'url' => 'list' ],
                        [ 'method' => 'get',  'url' => 'detail/{id}', 'where' => ['id' => '[0-9]+'] ],
                        [ 'method' => 'post', 'url' => 'delete-from-database' ],
                    ],
                    'controller' => 'AccessLogsController',
                ],
                [   // ログインログ管理画面
                    'prefix'     => 'login-logs',
                    'set'        => [
                        [ 'method' => 'get',  'url' => 'index' ],
                        [ 'method' => 'get',  'url' => 'list' ],
                        [ 'method' => 'post', 'url' => 'list' ],
                        [ 'method' => 'get',  'url' => 'detail/{id}', 'where' => ['id' => '[0-9]+'] ],
                        [ 'method' => 'post', 'url' => 'delete-from-database' ],
                    ],
                    'controller' => 'LoginLogsController',
                ],
                [   // 操作ログ管理画面
                    'prefix'     => 'operation-logs',
                    'set'        => [
                        [ 'method' => 'get',  'url' => 'index' ],
                        [ 'method' => 'get',  'url' => 'list' ],
                        [ 'method' => 'post', 'url' => 'list' ],
                        [ 'method' => 'get',  'url' => 'detail/{id}', 'where' => ['id' => '[0-9]+'] ],
                        [ 'method' => 'post', 'url' => 'delete-from-database' ],
                    ],
                    'controller' => 'OperationLogsController',
                ],
                [   // システムログ管理画面
                    'prefix'     => 'system-logs',
                    'set'        => [
                        [ 'method' => 'get',  'url' => 'index' ],
                        [ 'method' => 'get',  'url' => 'list' ],
                        [ 'method' => 'post', 'url' => 'list' ],
                        [ 'method' => 'get',  'url' => 'detail/{id}', 'where' => ['id' => '[0-9]+'] ],
                        [ 'method' => 'post', 'url' => 'restore' ],
                        [ 'method' => 'post', 'url' => 'delete-from-database' ],
                    ],
                    'controller' => 'SystemLogsController',
                ],
                [   // コマンド実行処理状況画面
                    'prefix'     => 'execute-queue-status',
                    'set'        => [
                        [ 'method' => 'get',  'url' => 'index' ],
                        [ 'method' => 'get',  'url' => 'list' ],
                        [ 'method' => 'post', 'url' => 'list' ],
                        [ 'method' => 'get',  'url' => 'detail/{id}', 'where' => ['id' => '[0-9]+'] ],
                    ],
                    'controller' => 'ExecuteQueueStatusController',
                ],
                [   // 環境設定管理画面
                    'prefix'     => 'environment',
                    'set'        => [
                        [ 'method' => 'get',  'url' => 'index' ],
                        [ 'method' => 'get',  'url' => 'phpinfo'],
                        [ 'method' => 'post', 'url' => 'regist-cron'],
                        [ 'method' => 'post', 'url' => 'remove-cron'],
                    ],
                    'controller' => 'EnvironmentController',
                ],
                [   // システム情報管理画面
                    'prefix'     => 'structure',
                    'set'        => [
                        [ 'method' => 'get',  'url' => 'index' ],
                        [ 'method' => 'post', 'url' => 'change-opration-status' ],
                    ],
                    'controller' => 'StructureController',
                ],
            ],
        ],

        // /manage/～　に関するルーティング。ミドルウェアの設定のないURL
        [
            'prefix'     => 'manage',
            'middleware' => [],
            'list'       => [
                [   // 管理画面ログインページ
                    'url'        => '/login',
                    'method'     => 'get',
                    'name'       => 'login',
                    'middleware' => [],
                    'uses'       => 'LoginController@getIndex',
                    'where'      => '',
                ],
                [   // 管理画面ログイン認証処理 ログイン後は/manage/　に移動する想定
                    'url'        => '/login/auth',
                    'method'     => 'post',
                    'name'       => 'login.p-auth',
                    'middleware' => [],
                    'uses'       => 'LoginController@login',
                    'where'      => '',
                ],
                [   // 管理画面パスワードリセットに関する処理
                    'prefix'     => 'reset-password',
                    'set'        => [
                        [ 'method' => 'get',  'url' => 'send-reset-mail' ],
                        [ 'method' => 'post', 'url' => 'send-reset-mail' ],
                        [ 'method' => 'get',  'url' => 'sent-reset-mail' ],
                        [ 'method' => 'get',  'url' => 'reset-password/{token}' ],
                        [ 'method' => 'post', 'url' => 'reset-password' ],
                        [ 'method' => 'get',  'url' => 'complete-reset-password' ],
                    ],
                    'controller' => 'ResetPasswordController',
                ],
            ],
        ],
        // /manage/～　に関するルーティング。ミドルウェアにてログイン、権限の制限がかかったURL
        [
            'prefix'     => 'manage',
            'middleware' => ['auth.manage', 'acl'],
            'list'       => [
                [   // 管理画面トップページ
                    'url'        => '/',
                    'method'     => 'get',
                    'name'       => '',
                    'middleware' => [],
                    'uses'       => 'IndexController@getIndex',
                    'where'      => '',
                ],
                [   // 管理画面ログアウトページ
                    'url'        => '/login/logout',
                    'method'     => 'get',
                    'name'       => 'logout',
                    'middleware' => [],
                    'uses'       => 'LoginController@logout',
                    'where'      => '',
                ],
                [   // ファイルアップロード用のルーティング（管理画面内）
                    'url'        => '/file-upload',
                    'method'     => 'post',
                    'name'       => 'file-upload',
                    'middleware' => [],
                    'uses'       => 'FileUploadController@postFile',
                    'where'      => '',
                ],
                [   // ユーザープロフィール画面
                    'prefix'     => 'profile',
                    'set'        => [
                        [ 'method' => 'get',  'url' => 'index' ],
                        [ 'method' => 'get',  'url' => 'detail' ],
                        [ 'method' => 'post', 'url' => 'edit' ],
                        [ 'method' => 'get',  'url' => 'edit' ],
                        [ 'method' => 'post', 'url' => 'confirm' ],
                        [ 'method' => 'post', 'url' => 'done' ],
                    ],
                    'controller' => 'ProfileController',
                ],
                [   // 管理者ユーザー管理画面
                    'prefix'     => 'managers',
                    'set'        => [
                        [ 'method' => 'get',  'url' => 'index' ],
                        [ 'method' => 'get',  'url' => 'list' ],
                        [ 'method' => 'post', 'url' => 'list' ],
                        [ 'method' => 'post', 'url' => 'regist' ],
                        [ 'method' => 'get',  'url' => 'regist' ],
                        [ 'method' => 'get',  'url' => 'detail/{id}', 'where' => ['id' => '[0-9]+'] ],
                        [ 'method' => 'post', 'url' => 'edit' ],
                        [ 'method' => 'get',  'url' => 'edit/{id}', 'where' => ['id' => '[0-9]+'] ],
                        [ 'method' => 'get',  'url' => 'edit/{id}/prev/{prev}', 'where' => ['id' => '[0-9]+', 'prev' => 'detail|list'] ],
                        [ 'method' => 'post', 'url' => 'confirm' ],
                        [ 'method' => 'post', 'url' => 'done' ],
                        [ 'method' => 'post', 'url' => 'enable' ],
                        [ 'method' => 'post', 'url' => 'delete' ],
                    ],
                    'controller' => 'ManagersController',
                ],
                [   // 一般ユーザー管理画面
                    'prefix'     => 'users',
                    'set'        => [
                        [ 'method' => 'get',  'url' => 'index' ],
                        [ 'method' => 'get',  'url' => 'list' ],
                        [ 'method' => 'post', 'url' => 'list' ],
                        [ 'method' => 'post', 'url' => 'regist' ],
                        [ 'method' => 'get',  'url' => 'regist' ],
                        [ 'method' => 'get',  'url' => 'detail/{id}', 'where' => ['id' => '[0-9]+'] ],
                        [ 'method' => 'post', 'url' => 'edit' ],
                        [ 'method' => 'get',  'url' => 'edit/{id}', 'where' => ['id' => '[0-9]+'] ],
                        [ 'method' => 'get',  'url' => 'edit/{id}/prev/{prev}', 'where' => ['id' => '[0-9]+', 'prev' => 'detail|list'] ],
                        [ 'method' => 'post', 'url' => 'confirm' ],
                        [ 'method' => 'post', 'url' => 'done' ],
                        [ 'method' => 'post', 'url' => 'delete' ],
                    ],
                    'controller' => 'UsersController',
                ],
                [   // コマンド実行処理状況画面
                    'prefix'     => 'execute-queue-status',
                    'set'        => [
                        [ 'method' => 'get',  'url' => 'index' ],
                        [ 'method' => 'get',  'url' => 'list' ],
                        [ 'method' => 'post', 'url' => 'list' ],
                        [ 'method' => 'get',  'url' => 'detail/{id}', 'where' => ['id' => '[0-9]+'] ],
                    ],
                    'controller' => 'ExecuteQueueStatusController',
                ],
            ],
        ],
        // /～　に関するログインが必要なURLのルーティング設定
        [
            'prefix'     => '',
            'middleware' => ['auth', 'acl'],
            'list'       => [
                [   // マイページ・登録情報編集
                    'prefix'     => 'mypage',
                    'middleware' => [],
                    'set'        => [
                        [ 'method' => 'get',  'url' => 'index', 'name' => 'mypage' ],
                        [ 'method' => 'get',  'url' => 'edit/{id?}', 'where' => ['id' => '[0-9]+'] ],
//                        [ 'method' => 'get',  'url' => 'edit' ],
                        [ 'method' => 'post', 'url' => 'edit' ],
                        [ 'method' => 'post', 'url' => 'confirm' ],
                        [ 'method' => 'post', 'url' => 'done' ],
                    ],
                    'controller' => 'Common\\MypageController',
                ],
            ],
        ],
    ],

    // /～に関するログインが不要なURLのルーティング
    'list' => [
        [   // 会員登録ページ
            'url'        => '/regist',
            'method'     => 'get',
            'name'       => 'regist',
            'middleware' => [],
            'uses'       => 'Common\\RegistController@getIndex',
            'where'      => '',
        ],
        [   // 会員登録処理
            'url'        => '/regist',
            'method'     => 'post',
            'name'       => 'p-regist',
            'middleware' => [],
            'uses'       => 'Common\\RegistController@postRegist',
            'where'      => '',
        ],

        [   // フロント画面パスワードリセット処理
            'prefix'     => 'reset-password',
            'middleware' => [],
            'set'        => [
                [ 'method' => 'get',  'url' => 'send-reset-mail' ],
                [ 'method' => 'post', 'url' => 'send-reset-mail' ],
                [ 'method' => 'get',  'url' => 'sent-reset-mail' ],
                [ 'method' => 'get',  'url' => 'reset-password/{token}' ],
                [ 'method' => 'post', 'url' => 'reset-password' ],
                [ 'method' => 'get',  'url' => 'complete-reset-password' ],
            ],
            'controller' => 'Common\\ResetPasswordController',
        ],
        [   // フロント画面ログインページ
            'url'        => '/login',
            'method'     => 'get',
            'name'       => 'login',
            'middleware' => [],
            'uses'       => 'Common\\LoginController@getIndex',
            'where'      => '',
        ],
        [   // フロント画面ログイン認証処理
            'url'        => '/login/auth',
            'method'     => 'post',
            'name'       => 'p-auth',
            'middleware' => [],
            'uses'       => 'Common\\LoginController@login',
            'where'      => '',
        ],
        [   // フロント画面ログアウト処理
            'url'        => '/login/logout',
            'method'     => 'get',
            'name'       => 'logout',
            'middleware' => ['auth'],
            'uses'       => 'Common\\LoginController@logout',
            'where'      => '',
        ],
        [   // ファイル閲覧用のルーティング
            'url'        => '/uploaded-file-view/{kind?}/{path1?}/{path2?}/{path3?}/{path4?}',
            'method'     => 'get',
            'name'       => 'fileview',
            'middleware' => [],
            'uses'       => 'Common\\UploadedFileViewController@getFile',
            'where'      => [
                'kind'  => '[^\/]*',
                'path1' => '[-_0-9a-zA-Z\.]+',
                'path2' => '[-_0-9a-zA-Z\.]+',
                'path3' => '[-_0-9a-zA-Z\.]+',
                'path4' => '[-_0-9a-zA-Z\.]+'
            ],
        ],
    ],
];

$snsList = implode('|', Config()->get('services.snsProviders'));
if(!empty($snsList)) {
    $routeList['list'] = array_merge($routeList['list'], [
        [   // SNSログイン処理
            'url'        => '/login/{social}',
            'method'     => 'get',
            'name'       => 'login-social',
            'middleware' => [],
            'uses'       => 'Common\\LoginController@getRedirectToSnsLoginProvider',
            'where'      => ['social' => $snsList],
        ],
        [   // SNSログイン処理CB
            'url'        => '/login/{social}/callback',
            'method'     => 'get',
            'name'       => 'login-social-callback',
            'middleware' => [],
            'uses'       => 'Common\\LoginController@getProviderCallback',
            'where'      => ['social' => $snsList],
        ],
    ]);
}

return $routeList;
