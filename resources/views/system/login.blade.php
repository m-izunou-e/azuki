@extends($vendorPrefix.'layouts.simple')

@section('headTags')
@include($vendorPrefix.'layouts.parts.addon_tags')
@stop

@section('headMeta')
@parent
<meta name="description" content="" />
<meta name="keywords" content="" />
<link rel="canonical" href="{{route('system.login')}}">
@stop

@section('headLink')
@parent
@stop

@section('headCss')
@parent
@stop

@section('headScript')
@parent
@stop

@section('bodyTags')
@include($vendorPrefix.'layouts.parts.addon_tags_body')
@stop

@section('title')
<title>{{$pageTitle}}</title>
@stop

@section('contents')
@include($vendorPrefix.'layouts.parts.login_contents', ['domain' => 'system', 'page' => 'login'])
@stop
