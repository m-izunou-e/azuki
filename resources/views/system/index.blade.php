@extends($vendorPrefix.'layouts.system_simple')

@section('headMeta')
@parent
<meta name="description" content="" />
<meta name="keywords" content="" />
@stop

@section('headLink')
@parent
@stop

@section('headCss')
@parent
@stop

@section('headScript')
@parent
@stop

@section('title')
<title>{{$pageTitle}}</title>
@stop

@section('contents-body')
  <div class="row">
    <div class="dashbord-information large-12 medium-12 small-12 margin-bottom-2" id="initialize-information">
    </div>
  </div>
@stop
