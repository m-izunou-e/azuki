<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="" />
    <meta name="keywords" content="" />

    <title>{{$title ?? ''}}</title>

    <link rel="shortcut icon" href="/favicon.ico" />
    <link rel="index" href="{{$story}}" />
    <style>
      div.message {
          text-align: center;
          position: absolute;
          top: 50%;
          left: 50%;
          transform: translateY(-50%) translateX(-50%);
          -webkit- transform: translateY(-50%) translateX(-50%);
      }
    </style>
  </head>
  <body>

@if($isError)
    <div class="message">
      <p>{{$code}}:{{$message}}</p>
    </div>

@else
    <div class="frame">
    @php
    phpinfo();
    @endphp
    </div>
@endif
  </body>
</html>
