@extends($vendorPrefix.'layouts.system_simple')

@section('headMeta')
@parent
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('headCss')
@parent
<style>
#phpinfo-modal {
    min-width:65%;
    padding:0;
    overflow:hidden;
}
iframe.full-size {
    width:100%;
    height:100%;
}
</style>
@stop

@section('title')
<title>{{$title ?? ''}}</title>
@stop

@section('contents-body')
  <div class="row">
    <div class="information-section large-12 medium-12 small-12 margin-bottom-2" id="initialize-information">
      <h2>サーバー情報</h2>
      <table class="table table-striped table-bordered table-hover table-list">
        <tbody>
          @foreach($serverInfo as $info)
          <tr>
            <th width="30%">
              {{$info['title']}}
            </th>
            <td class="text-right padding-right-2">
            @if(is_array($info['value']))
              @foreach($info['value'] as $value)
              {{$value}}<br>
              @endforeach
            @else
              {{$info['value']}}
            @endif
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    

    <div class="information-section large-12 medium-12 small-12 margin-bottom-1">
      <h2>
        アプリケーション情報
        <button type="button" class="button primary modal-trigger margin-bottom-0 float-right" data-target="#phpinfo-modal">
          PHPインフォを開く
        </button>
      </h2>
      <table class="table table-striped table-bordered table-hover table-list">
        <tbody>
          @foreach($appInfo as $info)
          <tr>
            <th width="30%">
              {{$info['title']}}
            </th>
            <td class="text-right padding-right-2">
            @if(is_array($info['value']))
              @foreach($info['value'] as $value)
              {{$value}}<br>
              @endforeach
            @else
              {{$info['value']}}
            @endif
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>

    @if(!isWindows())
    <div class="information-section large-12 medium-12 small-12 margin-bottom-1">
      <h2>
        Cron情報
        @if(isset($crontab) && !empty($crontab))
        <button type="button" class="button primary margin-bottom-0 float-right remove-cron"
          data-url="{{$story}}{{$controller}}/remove-cron">
          Cronを削除する
        </button>
        @elseif(!empty($cronText))
        <button type="button" class="button primary modal-trigger margin-bottom-0 float-right" data-target="#cron-modal">
          Cronを登録する
        </button>
        @endif
      </h2>
      <table class="table table-striped table-bordered table-hover table-list">
        <tbody>
        @if(isset($crontab) && !empty($crontab))
          @foreach($crontab as $cron)
          <tr>
            <td class="padding-right-2">
              {{$cron}}
            </td>
          </tr>
          @endforeach
        @else
          <tr>
            <td>登録されている情報はありません。</td>
          </tr>
        @endif
        </tbody>
      </table>
    </div>
    @endif

    <div class="reveal" id="phpinfo-modal" data-reveal data-close-on-click="true">
      <iframe src="{{$story}}{{$controller}}/phpinfo" class="full-size">
      </iframe>
    </div>
  </div>
@stop

@section('modalContents')
@parent
@if(!isWindows() && !empty($cronText))
<div class="reveal" id="cron-modal" data-reveal data-close-on-click="true">
  <div>
    <button class="close-button" data-close aria-label="Close reveal" type="button">
      <span aria-hidden="true">&times;</span>
    </button>
    <p class="title-label">以下の内容でCroinの登録をします</p>
    <p>
    @foreach($cronText as $cron)
      {{$cron}}<br>
    @endforeach
    </p>
    <div class="cron-regist-buttons">
      <button class="button secondary margin-bottom-0" data-close aria-label="Close reveal" type="button" name="cancel">キャンセル</button>
      <button type="button" class="button primary margin-bottom-0 float-right regist-cron"
        data-url="{{$story}}{{$controller}}/regist-cron">
        Cronを登録する
      </button>
    </div>
  </div>
</div>

@endif
@stop

