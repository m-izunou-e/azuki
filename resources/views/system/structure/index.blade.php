@extends($vendorPrefix.'layouts.system_simple')

@section('headMeta')
@parent
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('title')
<title>{{$title ?? ''}}</title>
@stop

@section('contents-body')
  <div class="row">
    <div class="information-section large-12 medium-12 small-12 margin-bottom-2">
      <h2>システム構成</h2>
      <table class="table table-striped table-bordered table-hover table-list">
        <tbody>
          @foreach($systemInfo as $info)
          <tr>
            <th width="30%">
              {{$info['title']}}
            </th>
            <td class="text-right padding-right-2">
            @if(is_array($info['value']))
              @foreach($info['value'] as $value)
              {{$value}}<br>
              @endforeach
            @else
              {{$info['value']}}
            @endif
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>

    <div class="information-section large-12 medium-12 small-12 margin-bottom-2">
      <h2>稼働切り替え</h2>
      @include($vendorPrefix.'layouts.parts.share.system-switch')
    </div>
  </div>
@stop
