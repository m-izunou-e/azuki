@extends('errors::base')

@section('code', $code)
@section('message', empty($message) ? '認証されていません' : $message)
