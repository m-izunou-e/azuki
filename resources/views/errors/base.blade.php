<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('code')エラー</title>

    <style>
      div.message {
          text-align: center;
          position: absolute;
          top: 50%;
          left: 50%;
          transform: translateY(-50%) translateX(-50%);
          -webkit- transform: translateY(-50%) translateX(-50%);
      }
    </style>

  </head>
  <body class="error-page">
    <div class="message">
      <p>@yield('code'):@yield('message')</p>
@if($accessLevel == HTTP_ACCESS_LEVEL_SYSTEM)
      <a href="{{$story}}">システム画面トップ</a>
@elseif($accessLevel == HTTP_ACCESS_LEVEL_MANAGE)
      <a href="{{$story}}">サイト管理画面トップ</a>
@else
      <a href="{{$story}}">サイトトップ</a>
@endif
    </div>
  </body>
</html>
