@extends('errors::base')

@section('code', $code)
@section('message', empty($message) ? 'リクエストされたページは利用できません' : $message)
