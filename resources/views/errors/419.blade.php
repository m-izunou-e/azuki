@extends('errors::base')

@section('code', $code)
@section('message', empty($message) ? 'リクエストされたページは破棄されました' : $message)
