@extends('errors::base')

@section('code', $code)
@section('message', empty($message) ? 'リクエストされたページにアクセスできません' : $message)
