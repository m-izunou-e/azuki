@extends('errors::base')

@section('code', $code)
@section('message', empty($message) ? 'リクエストされたページは存在しません' : $message)
