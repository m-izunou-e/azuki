@extends('errors::base')

@section('code', $code)
@section('message', empty($message) ? 'リクエストが混みあっています' : $message)
