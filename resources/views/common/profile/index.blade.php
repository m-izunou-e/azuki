@extends($vendorPrefix.'layouts.common_simple')

@section('headMeta')
@parent
<meta name="description" content="" />
<meta name="keywords" content="" />
@stop

@section('headLink')
@parent
@stop

@section('headCss')
@parent
@stop

@section('headScript')
@parent
@stop

@section('title')
<title>{{$pageTitle}}</title>
@stop

@section('contents-body')
      <div class="in-contents-body">
        <h2>プロフィール</h2>
        <h4 class="header line-style">登録情報</h4>
        @include ($vendorPrefix.'layouts.parts.share.process_result_on_list', ['name' => $pageName])
        @include ($vendorPrefix.'common.profile.profile_index_parts')
      </div>
@stop
