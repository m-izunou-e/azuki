@extends($vendorPrefix.'layouts.common_simple')

@section('headMeta')
@parent
<meta name="description" content="" />
<meta name="keywords" content="" />
@stop

@section('headLink')
@parent
@stop

@section('headCss')
@parent
@stop

@section('headScript')
@parent
@stop

@section('title')
<title>{{$pageTitle}}</title>
@stop

@section('contents-body')
    @include($vendorPrefix.'layouts.parts.share.form', ['controller' => $controller, 'formMode' => FORM_MODE_INPUT])
@stop

@section('modalContents')
@parent
@stop

@section('footer-javascript')
@parent
@stop
