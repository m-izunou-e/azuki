        <section>
          <div class="">
            @include ($vendorPrefix."layouts.parts.share.data_view", ['areaName' => 'detail','layouts' => $detailLayout, 'forms' => $detailForm, 'formMode' => FORM_MODE_VIEW])

            <div class="detail-controll">
              <a href="{{$story}}{{$controller}}/edit">
                <button type="button" name="edit" value="true" class="button button-primary text-right">編集</button>
              </a>
            </div>
          </div>
        </section>
