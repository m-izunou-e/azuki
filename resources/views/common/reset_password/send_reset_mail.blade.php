@extends($vendorPrefix.'layouts.common_simple')

@section('headMeta')
@parent
<meta name="description" content="" />
<meta name="keywords" content="" />
@stop

@section('headLink')
@parent
@stop

@section('headCss')
@parent
@stop

@section('headScript')
@parent
@stop

@section('title')
<title>パスワード再設定メール－Azuki</title>
@stop

@section('contents-body')
<div class="grid-frame">
    <div class="grid-x align-center align-middle large-12">
        <div class="large-4 margin-tb-40">
@if ($errors->any())
            <div class="alert callout" data-closable>
    @foreach ($errors->all() as $error)
                <p class="alert">{{$error}}</p>
    @endforeach
                <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
@endif
        <form method="post" action="{{ route('reset-password.send-reset-mail', [], false) }}">
            {{ csrf_field() }}
            <dl class="cell">
                <dt class="cell">Email</dt>
                <dd class="cell">
                    <input type="text" name="email" value="{{old('email')}}" placeholder="Email">
                </dd>
            </dl>
            <div class="text-right">
                <input class="hollow button radius bordered shadow primary" type="submit" name="toLogin" value="パスワード再設定メールの送信">
            </div>
        </form>
        </div>
    </div>
</div>
@stop
