@extends($vendorPrefix.'layouts.common_simple')

@section('headMeta')
@parent
<meta name="description" content="" />
<meta name="keywords" content="" />
@stop

@section('headLink')
@parent
@stop

@section('headCss')
@parent
@stop

@section('headScript')
@parent
@stop

@section('title')
<title>パスワード再設定メール送信完了－Azuki</title>
@stop

@section('contents-body')
<div class="grid-frame">
    <div class="grid-x align-center align-middle large-12">
        <div class="large-4 margin-tb-40">
            <p>登録メールアドレスにパスワードリセットメールを送信しました。</p>
        </div>
    </div>
</div>
@stop
