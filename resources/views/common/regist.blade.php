@extends($vendorPrefix.'layouts.common_simple')

@section('headMeta')
@parent
<meta name="description" content="" />
<meta name="keywords" content="" />
@stop

@section('headLink')
@parent
@stop

@section('headCss')
@parent
@stop

@section('headScript')
@parent
@stop

@section('title')
<title>新規登録</title>
@stop

@section('contents-body')
<div class="grid-frame">
    <div class="grid-x align-center align-middle large-12">
        <div class="large-4 margin-tb-40 form">
{{--
@if ($errors->any())
            <div class="alert callout" data-closable>
    @foreach ($errors->all() as $error)
                <p class="alert">{{$error}}</p>
    @endforeach
                <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
@endif
--}}
        <form method="post" action="{{$story}}regist">
            {{ csrf_field() }}
            <dl class="cell">
                <dt class="cell"><label class='title'>お名前<span class="required">必須</span></label></dt>
                <dd class="cell">
                    <input type="text" name="name" value="{{old('name')}}" placeholder="大阪　太郎">
                    @if($errors->has('name'))
                    <div class="form error margin-top-m1 margin-bottom-1">
                      @foreach ($errors->get('name') as $error)
                      <p>{{trans($error)}}</p>
                      @endforeach
                    </div>
                    @endif
                </dd>
                <dt class="cell"><label class='title'>ログインID<span class="required">必須</span></label></dt>
                <dd class="cell">
                    <input type="text" name="login_id" value="{{old('login_id')}}" placeholder="ログインID">
                    @if($errors->has('login_id'))
                    <div class="form error margin-top-m1 margin-bottom-1">
                      @foreach ($errors->get('login_id') as $error)
                      <p>{{trans($error)}}</p>
                      @endforeach
                    </div>
                    @endif
                </dd>
                <dt class="cell"><label class='title'>メールアドレス<span class="optional">任意</span></label></dt>
                <dd class="cell">
                    <input type="text" name="email" value="{{old('email')}}" placeholder="メールアドレス">
                    @if($errors->has('email'))
                    <div class="form error margin-top-m1 margin-bottom-1">
                      @foreach ($errors->get('email') as $error)
                      <p>{{trans($error)}}</p>
                      @endforeach
                    </div>
                    @endif
                </dd>
                <dt class="cell"><label class='title'>パスワード<span class="required">必須</span></label></dt>
                <dd class="cell">
                    <input type="password" name="password" value="">
                    @if($errors->has('password'))
                    <div class="form error margin-top-m1 margin-bottom-1">
                      @foreach ($errors->get('password') as $error)
                      <p>{{trans($error)}}</p>
                      @endforeach
                    </div>
                    @endif
                </dd>
                <dt class="cell"><label class='title'>パスワード(確認)<span class="required">必須</span></label></dt>
                <dd class="cell"><input type="password" name="password_confirmation" value=""></dd>
            </dl>
            <div class="text-right">
                <input class="hollow button radius bordered shadow primary" type="submit" name="toLogin" value="ユーザー登録">
            </div>
        </form>
        </div>
    </div>
</div>
@stop
