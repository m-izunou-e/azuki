@extends($vendorPrefix.'layouts.'.$baseLayout)

@section('headMeta')
@parent
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('headLink')
@parent
@stop

@section('headCss')
@parent
@stop

@section('headScript')
@parent
<script src="https://ajaxzip3.github.io/ajaxzip3.js"></script>
@stop

@section('title')
<title>{{$pageTitle}}</title>
@stop

@section('contents-body')
    @include($vendorPrefix.'layouts.parts.share.form', ['controller' => $controller])
@stop

@section('modalContents')
@parent
@stop


@section('footer-javascript')
@parent
<script type="text/javascript">
</script>
@stop
