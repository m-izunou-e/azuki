@extends($vendorPrefix.'layouts.'.$baseLayout)

@section('headMeta')
@parent
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('headLink')
@parent
@stop

@section('headCss')
@parent
@stop

@section('headScript')
@parent
@stop

@section('title')
<title>{{$pageTitle}}</title>
@stop

@section('contents-body')

    <div class="row">
      <div class="large-12 medium-12 small-12">
        <h3 class="header line-style">
          {{$pageName}}一覧
          @yield('contents-head-link')
        </h3>
        
        @include ($vendorPrefix.'layouts.parts.share.process_result_on_list', ['name' => $pageName])
        
        <section>
        @include ($vendorPrefix.'layouts.parts.share.search_box', ['controller' => $controller])
        </section>
        <!-- #searchBox // -->
        
        @if(showRegistButton($prefix, $controller))
        <div class="row">
          <div class="large-12 columns text-right margin-bottom-1">
            <a href="{{$story}}{{$controller}}/{{$listAction ?? 'regist'}}">
              <button type="button" name="register_btn" value="true" class="button margin-bottom-0">新規登録</button>
            </a>
          </div>
        </div>
        @endif

@section('before-list-table')
  @if(isset($csvSettings))
        @include (
            $vendorPrefix.'layouts.parts.share.csv-controller-onlist',
            ['controller' => $controller, 'ufType' => $csvSettings['ufType'], 'csv_kind' => $csvSettings['kind']]
        )
  @endif
  @if(isset($isBulkDelete) && $isBulkDelete)
        @include($vendorPrefix.'layouts.parts.share.delete-from-database', ['deleteUrl' => 'delete-from-database'])
  @endif
@show

        @include ($vendorPrefix.'layouts.parts.share.pager.pager1', ['controller' => $controller, 'pager' => $dataList])
        <section>
        @include ($vendorPrefix.'layouts.parts.share.list_table', ['controller' => $controller])
        </section>
        @include ($vendorPrefix.'layouts.parts.share.pager.pager1', ['controller' => $controller, 'pager' => $dataList])
        <!-- #pagetop // -->

      </div>
    </div>
@stop

@section('modalContents')
@parent
@if(Route::has(sprintf('%s.%s.p-delete', $prefix, $controller)))
  @include ($vendorPrefix."layouts.parts.share.delete-modal", ["controller" => $controller])
@endif
@if(Route::has(sprintf('%s.%s.p-restore', $prefix, $controller)))
  @include ($vendorPrefix."layouts.parts.share.restore-modal", ["controller" => $controller])
@endif
@stop


@section('footer-javascript')
@parent
@stop
