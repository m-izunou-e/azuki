<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
@yield('headTags')
@section('headMeta')
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
@show

@section('title')
<title></title>
@show

@section('headLink')
        <link rel="shortcut icon" href="{{getFavicon()}}" />
        <link rel="index" href="{{$story}}" />
@show

@section('headCss')
        <link rel="stylesheet" href="/vendor/azuki/foundation-icons/foundation-icons.css">
        <!-- https://fonts.google.com/icons -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Sharp" rel="stylesheet">
        <link rel="stylesheet" href="/vendor/azuki/css/app.css">
@show

@section('headScript')
<script src="/vendor/azuki/js/app.js"></script>
@show
    </head>
    <body>
@yield('bodyTags')
        <div class="frame">
            <div class="contents">
@section('contents')
@show
@include ($vendorPrefix."layouts.parts.site_footer")
            </div>
        </div>
@yield('modalContents')
    </body>
</html>
