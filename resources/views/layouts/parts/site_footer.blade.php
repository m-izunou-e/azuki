<footer class="bottom-footer">
    <div class="text-center"><small>{!! str_replace('&amp;copy;', '&copy;', e(getCopyLight())) !!}</small></div>
</footer>
