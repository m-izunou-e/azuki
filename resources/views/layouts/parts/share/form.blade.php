  <div class="">
    <form method="post" action="{{$story}}{{$controller}}/confirm" class="">
      @include ($vendorPrefix."layouts.parts.share.data_view", ['areaName' => 'form','layouts' => $formLayout, 'formMode' => FORM_MODE_INPUT])

      <div class="form-controll row text-right">
        <div class="cell medium-12">
        @if ($flow == "edit" && isset($prev) && $prev == "detail")
          <a href="{{$story}}{{$controller}}/detail/{{$id}}">
            <button type="button" name="back" value="true" class="button secondary margin-right-1">戻る</button>
          </a>
        @elseif (isset($toBack) && $toBack == "detail")
          <a href="{{$story}}{{$controller}}/detail">
            <button type="button" name="back" value="true" class="button secondary margin-right-1">戻る</button>
          </a>
        @elseif (isset($toBack) && $toBack == "index")
          <a href="{{$story}}{{$controller}}">
            <button type="button" name="back" value="true" class="button secondary margin-right-1">戻る</button>
          </a>
        @elseif (isset($toBack) && !empty($toBack))
          <a href="{{$story}}{{$controller}}/{{$toBack}}">
            <button type="button" name="back" value="true" class="button secondary margin-right-1">戻る</button>
          </a>
        @else
          <a href="{{$story}}{{$controller}}/list">
            <button type="button" name="back" value="true" class="button secondary margin-right-1">戻る</button>
          </a>
        @endif
          <button type="submit" name="confirm" value="true" class="button button-primary text-right">確認</button>
        </div>
      </div>

      <input type='hidden' name="flow" value="{{$flow}}" />
      <input type='hidden' name="id" value="{{$id ?? ''}}" />
      <input type="hidden" name="_token" value="{{csrf_token()}}" />
    </form>
  </div>
