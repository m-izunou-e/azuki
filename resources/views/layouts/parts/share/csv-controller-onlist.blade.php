  <div class="row">
    <div class="large-12 columns text-left margin-bottom-1 input-group">
      <label class="input-group margin-bottom-0" style="width:auto;">
        <i class="{{$csvFileIcon}} fi-medium button margin-bottom-0" title="CSVファイル"></i>
        <span id="upload-csv-name" class="margin-bottom-0 input-group-label">選択してください</span>
        <input id="csv-select_file" type="file" class="azuki-upload-field" name="" accept=".csv" style="display:none;">
        <input type="hidden" name="kind" value="{{$csv_kind}}" />
        <input type="hidden" name="type" value="{{$ufType}}" />
      </label>
      <i id="csv-upload_btn" class="{{$uploadIcon}} fi-medium button margin-bottom-0" title="CSVアップロード"></i>

      <i id="csv-download_btn" class="{{$downloadIcon}} fi-medium button margin-bottom-0 margin-left-1" title="CSVダウンロード"></i>
      <form id="csv-download_form" method="POST" action="{{$story}}{{$controller}}/csv-download" target="csv-dl-post">
        <input type="hidden" name="_token" value="{{csrf_token()}}" />
        <input type="hidden" name="kind" value="{{$csv_kind}}" />
      </form>
    </div>
    <iframe name='csv-dl-post' style="display:none;"></iframe>
  </div>
