  <div class="">
    @include ($vendorPrefix."layouts.parts.share.data_view", ['areaName' => 'detail','layouts' => $detailLayout, 'forms' => $detailForm, 'formMode' => FORM_MODE_VIEW])

    <div class="detail-controll">
      @if( deletableRecord($post, $ctrlCondition) )
      <a class="deleteBtn"href="javascript:void(0);">
        <button type="button" name="delete" value="true" data-target="#delete-modal" data-id="{{$id}}" class="button margin-right-1 warning trigger-delete-modal">削除</button>
      </a>
      @endif

      @if( !isset($canBack) || $canBack != false )
      <a href="{{$story}}{{$controller}}/list">
        <button type="button" name="back" value="true" class="button secondary margin-right-1">戻る</button>
      </a>
      @endif
      @if( editableRecord($post, $ctrlCondition) )
        @if( (isset($isProfile) && $isProfile == true) || (isset($noSelectData) && $noSelectData == true) )
      <a href="{{$story}}{{$controller}}/edit">
        @else
      <a href="{{$story}}{{$controller}}/edit/{{$id}}/prev/detail">
        @endif
        <button type="button" name="edit" value="true" class="button button-primary text-right">編集</button>
      </a>
      @endif
    </div>
  </div>
