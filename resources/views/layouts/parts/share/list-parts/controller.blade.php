      <td class="td-list-ctrl">
      @foreach( $row['ctrl'] as $ctrl )
        @if( $ctrl == CTRL_DETAIL )
        <a class="edit" href="{{$story}}{{$controller}}/detail/{{$val->id}}" data-title="詳細">
          <i class="{{$listDetailIcon}} fi-ctrl-medium" title="詳細"></i>
        </a>
        @elseif( $ctrl == CTRL_EDIT && editableRecord($val, $ctrlCondition) )
        <a class="edit" href="{{$story}}{{$controller}}/edit/{{$val->id}}" data-title="編集">
          <i class="{{$listEditIcon}} fi-ctrl-medium" title="編集"></i>
        </a>
        @elseif( $ctrl == CTRL_EDITONLIST && editableRecord($val, $ctrlCondition) )
        <a class="edit" href="{{$story}}{{$controller}}/edit-on-list/{{$val->id}}" data-title="編集">
          <i class="{{$listEditIcon}} fi-ctrl-medium" title="編集"></i>
        </a>
        @elseif( $ctrl == CTRL_COPY )
        <a class="edit" href="{{$story}}{{$controller}}/copy/{{$val->id}}" data-title="コピー">
          <i class="{{$listCopyIcon}} fi-ctrl-medium" title="コピー"></i>
        </a>
        @elseif( $ctrl == CTRL_PREVIEW )
{{--
        <a class="edit" href="{{$story}}{{$controller}}/preview/{{$val->id}}" data-title="プレビュー" target="_blank">
          <i class="{{$listPreviewIcon}} fi-ctrl-medium" title="プレビュー"></i>
        </a>
--}}
        @elseif( $ctrl == CTRL_DELETE && deletableRecord($val, $ctrlCondition) )
        <a class="deleteBtn" href="javascript:void(0);" id="del_{{$val->id}}" data-title="削除">
          <i class="{{$listDeleteIcon}} fi-ctrl-medium trigger-delete-modal" data-target="#delete-modal" data-id="{{$val->id}}" title="削除"></i>
        </a>
        @elseif( $ctrl == CTRL_RESTORE && restorebleRecord($val, $ctrlCondition) )
        <a class="restoreBtn" href="javascript:void(0);" id="restore_{{$val->id}}" data-title="復旧">
          <i class="{{$listRefreshIcon}} fi-ctrl-medium trigger-restore-modal" data-target="#restore-modal" data-id="{{$val->id}}" title="復旧"></i>
        </a>
        @elseif( $ctrl == CTRL_CHANGE_ORDER )
        <button class="btn btn-xs btn-primary upOrder" type="button" data-id="{{$val->id}}" name="change-order" value="{{$val->id}}">
          △
        </button>
        <button class="btn btn-xs btn-primary downOrder" type="button" data-id="{{$val->id}}" name="change-order" value="{{$val->id}}">
          ▽
        </button>
        @endif
      @endforeach
      </td>
