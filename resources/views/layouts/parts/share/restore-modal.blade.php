<div class="reveal" id="restore-modal" data-reveal data-close-on-click="true" {{--data-animation-in="fade-in" data-animation-out="spin-out" --}}>
    <div>
        <button class="close-button" data-close aria-label="Close reveal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
        
        <div class="form-controll">
            <p class="">選択したデータを復旧状態に変更しますか。</p>
            <form action="{{$story}}{{$controller}}/restore" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}" />
                <input id="modal-form-restore-id" type="hidden" name="restore_id" value="">
                <button class="button" data-close aria-label="Close reveal" type="button" name="cancel">キャンセル</button>
                <button type="submit" name="doRestore" value="true" class="button button-primary text-right">復旧状態にする</button>
            </form>
        </div>
    </div>
</div>
