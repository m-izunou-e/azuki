                    @if (isset($insertSuccess) && $insertSuccess === true)
                    <div class="alert alert-sm alert-success">
                        <p>{{$name}}を登録しました。</p>
                    </div>
                    @elseif (isset($insertSuccess) && $insertSuccess === false)
                    <div class="alert alert-sm alert-danger">
                        <p>{{$name}}の登録に失敗しました。</p>
                    </div>
                    @endif
                    @if (isset($updateSuccess) && $updateSuccess === true)
                    <div class="alert alert-sm alert-success">
                        <p>{{$name}}を更新しました。</p>
                    </div>
                    @elseif (isset($updateSuccess) && $updateSuccess === false)
                    <div class="alert alert-sm alert-danger">
                        <p>{{$name}}の更新に失敗しました。</p>
                    </div>
                    @endif
                    @if (isset($deleteSuccess) && $deleteSuccess === true)
                    <div class="alert alert-sm alert-warning">
                        <p>{{$name}}を削除しました。</p>
                    </div>
                    @elseif (isset($deleteSuccess) && $deleteSuccess === false)
                    <div class="alert alert-sm alert-danger">
                        <p>{{$name}}の削除に失敗しました。</p>
                    </div>
                    @endif
                    @if (isset($openSuccess) && $openSuccess === true)
                    <div class="alert alert-sm alert-success">
                        <p>{{$name}}の有効・無効を切り替えました。</p>
                    </div>
                    @elseif (isset($openSuccess) && $openSuccess === false)
                    <div class="alert alert-sm alert-danger">
                        <p>{{$name}}の有効・無効の切り替えに失敗しました。</p>
                    </div>
                    @endif
                    @if (isset($changeOrderSuccess) && $changeOrderSuccess === true)
                    <div class="alert alert-sm alert-success">
                        <p>{{$name}}の順序を変更ました。</p>
                    </div>
                    @elseif (isset($changeOrderSuccess) && $changeOrderSuccess === false)
                    <div class="alert alert-sm alert-danger">
                        <p>{{$name}}の順序の変更に失敗しました。</p>
                    </div>
                    @elseif (isset($csvUploadResult))
                      @if($csvUploadResult['isBackground'])
                    <div class="alert alert-sm alert-success">
                        <p>バックグラウンドでCSVアップロードを処理しています。</p>
                    </div>
                      @elseif($csvUploadResult['result']['fail_num'] == 0)
                    <div class="alert alert-sm alert-success">
                        <p>{{$csvUploadResult['fileName']}}を処理しました（成功：{{$csvUploadResult['result']['success_num']}}件、失敗：{{$csvUploadResult['result']['fail_num']}}件）。</p>
                    </div>
                      @else
                    <div class="alert alert-sm alert-warning">
                        <p>{{$csvUploadResult['fileName']}}を処理しました（成功：{{$csvUploadResult['result']['success_num']}}件、失敗：{{$csvUploadResult['result']['fail_num']}}件）。</p>
                    </div>
                      @endif
                    @endif
