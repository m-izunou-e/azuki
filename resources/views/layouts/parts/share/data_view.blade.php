    <div class="{{$areaName}}">
    @if(isset($layouts) && is_array($layouts))
      @foreach( $layouts as $key => $row )
        @include($vendorPrefix.'layouts.parts.share.layout-parts-form.row', ['row' => $row])
      @endforeach
    @endif
    </div>
