@php
  $search_shortcut = isset($search_shortcut) ? $search_shortcut :false;
  $searchBtnkind = isset($searchBtnkind) && !empty($searchBtnkind) ? $searchBtnkind : ($search_shortcut ? 'short' : 'normal');
@endphp

@if(isShortcutSearch($search_shortcut))
<form id="list_search_box_short_form" method="post" action="{{$story}}{{$controller}}/{{$listAction ?? 'list'}}" class="form-horizontal @if(!searchShortcut($searchBtnkind)) hidden @endif">
  <input type="hidden" name="_token" value="{{csrf_token()}}" />
  <div class="row">
    <div class="large-6 columns">
      <input type="text" name="search[free_word]" value="{{$post['search[free_word]'] ?? ''}}">
    </div>
    <div class="large-5 columns">
      <button type="submit" name="list_search_short_btn" value="true" class="button margin-bottom-0">検索</button>
      <i id="open-search-box"  class="{{$plusIcon}} button margin-bottom-0"></i>
    </div>
  </div>
</form>
@endif

<form id="list_search_box_form" method="post" action="{{$story}}{{$controller}}/{{$listAction ?? 'list'}}" class="form-horizontal @if(isShortcutSearch($search_shortcut) && searchShortcut($searchBtnkind)) hidden @endif">
  <input type="hidden" name="_token" value="{{csrf_token()}}" />
  <fieldset class="fieldset">
    <legend>検索</legend>

    @if(isset($searchLayout) && is_array($searchLayout))
      @foreach( $searchLayout as $key => $row )
        @include($vendorPrefix.'layouts.parts.share.layout-parts-form.row', ['areaName' => 'search','forms' => $searchForm, 'formMode' => FORM_MODE_INPUT])
      @endforeach
    @endif

    <div class="row">
      <div class="large-12 columns text-right">
        <button type="button" id="all_clear_btn" name="all_clear_btn" value="true" class="button warning margin-bottom-0 margin-right-1">すべてクリア</button>
        <button type="submit" name="list_search_btn" value="true" class="button margin-bottom-0">検索</button>
      @if(isShortcutSearch($search_shortcut))
        <i id="close-search-box" class="{{$minusIcon}} button margin-bottom-0"></i>
      @endif
      </div>
    </div>
  </fieldset>
</form>

