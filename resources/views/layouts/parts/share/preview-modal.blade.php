<div class="reveal" id="media-preview-modal" data-reveal data-close-on-click="true" {{--data-animation-in="fade-in" data-animation-out="spin-out" --}}>
  <div>
    <button class="close-button" data-close aria-label="Close reveal" type="button">
      <span aria-hidden="true">&times;</span>
    </button>
    
    <div class="flex-box">
      <video src="" class="preview-modal" id="preview-modal-movie" alt="preview" controls playsinline preload="metadata"></video>
      <img src="" class="preview-modal" id="preview-modal-image" alt="preview">
    </div>
  </div>
</div>
