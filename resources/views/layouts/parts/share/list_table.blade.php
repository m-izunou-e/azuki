<form id="table_order_form" method="post" action="{{$story}}{{$controller}}/{{$listAction ?? 'list'}}">
  <input type="hidden" name="_token" value="{{csrf_token()}}" />
  <input type="hidden" name="order[order]" value="{{$order['order']}}" />
@if( isset($order['condition']) )
  @foreach( $order['condition'] as $column => $val )
  <input type="hidden" name="order[condition][{{$column}}]" value="{{$val}}" />
  @endforeach
@endif
</form>

<div class="table-frame">

<table summary="一覧" class="table table-striped table-bordered table-hover table-list">
  <thead>
    <tr>
    @foreach( $list as $row )
      <th width="{{$row['width']}}">
        {{$row['title'] ?? ''}}
        <div class="sortable-icons">
          @if( isset($row['orderable']) && $row['orderable'] === true )
          <a href="javascript:void(0);" data-col="{{$row['column']}}" data-sort="desc" class="sort">
            <i class="{{$arrowUpIcon}} small @if(isSelectedSort($order, $row['column'], 'desc')) selected @endif"></i>
          </a>
          <a href="javascript:void(0);" data-col="{{$row['column']}}" data-sort="asc" class="sort">
            <i class="{{$arrowDownIcon}} small @if(isSelectedSort($order,$row['column'], 'asc')) selected @endif"></i>
          </a>
          @endif
        </div>
      </th>
    @endforeach
    </tr>
  </thead>

  <tbody>
  @forelse($dataList as $val)
    <tr>
    @foreach( $list as $row )
      @if( $row['type'] == CONTROL_TYPE_VALID )
      <td>
        @if ($val->is_valid == 1)<a class="btn btnNopublic" href="javascript:void(0);" id="publish_{{$val->id}}"><img src="{{$story}}img/common/table_img_public_on.png" alt="有効" /></a> @else <a class="btn btnPublic" href="javascript:void(0);" id="publish_{{$val->id}}"><img src="{{$story}}img/common/table_img_public_off.png" alt="無効" /></a> @endif
      </td>

      @elseif( $row['type'] == CONTROL_TYPE_IMAGE )
      <td>
        <div class="imgup_area">
          <div class="preview" style="{{displayImageSizeStyle(2)}}">
          @if(!empty($val->{$row['column']}))
            <img src="{{getUploadedFileUrl($val->{$row['column']})}}" alt="preview">
          @else
            <img src="/admin/img/no_img.jpg" alt="preview">
          @endif
          </div>
        </div>
      </td>

      @elseif( $row['type'] == CONTROL_TYPE_CONTROLLER )
      @include($vendorPrefix.'layouts.parts.share.list-parts.controller')

      @else
      <td>
        @if(isset($row['detailLinkable']) && $row['detailLinkable'] == true)
        <a href="{{$story}}{{$controller}}/detail/{{$val->id}}">
          {!! nl2br(e( getFormatDataForList( $row, $val ) )) !!}
        </a>
        @else
        {!! nl2br(e( getFormatDataForList( $row, $val ) )) !!}
        @endif
      </td>
      @endif
    @endforeach
    </tr>

  @empty
    <tr>
      <td colspan="{{count($list)}}">表示できる情報がありません。</td>
    </tr>
  @endforelse
  </tbody>
</table>

</div>
