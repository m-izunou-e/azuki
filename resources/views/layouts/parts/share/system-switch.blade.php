@if(isset($oprationStatus) && !empty($oprationStatus))
  <input type="hidden" id="systemSwitchUrl" value="{{$story}}structure/change-opration-status">
  <table>
    <tbody>
    @foreach($oprationStatus as $list)
      <tr>
        <td>{{$list['title']}}</td>
        <td class="text-right">
          <div class="switch large margin-bottom-0">
            <input class="switch-input"
                id="{{$list['kind']}}"
                data-kind="{{$list['kind']}}"
                type="checkbox"
                name="{{$list['kind']}}"
                @if($list['status'] == SWITCH_STATUS_IS_RUN) checked="checked"@endif>
            <label class="switch-paddle" for="{{$list['kind']}}">
              <span class="switch-active" aria-hidden="true">On</span>
              <span class="switch-inactive" aria-hidden="true">Off</span>
            </label>
          </div>
        </td>
      </tr>
    @endforeach
    </tbody>
  </table>
@endif
