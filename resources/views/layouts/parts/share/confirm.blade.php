  <div class="">
    @include ($vendorPrefix."layouts.parts.share.data_view", ['areaName' => 'confirm','layouts' => $formLayout, 'formMode' => FORM_MODE_VIEW])

    <div class="row text-right confirm-btns">
      <div class="cell medium-12">
        @if( $flow == 'edit' )
        <form action="{{$story}}{{$controller}}/edit" method="POST">
        @else
        <form action="{{$story}}{{$controller}}/regist" method="POST">
        @endif
          @include ($vendorPrefix."layouts.parts.share.layout-parts-form.hidden-input")
          <button type="submit" name="back" value="true" class="button secondary margin-right-1">戻る</button>
        </form>
  
        <form action="{{$story}}{{$controller}}/done" method="POST">
          @include ($vendorPrefix."layouts.parts.share.layout-parts-form.hidden-input")
          <button type="submit" name="complete" value="true" class="button button-primary">
            @if ($flow == "regist")登録@elseif ($flow == "edit")編集@endif完了
          </button>
        </form>
      </div>
    </div>

  </div>
