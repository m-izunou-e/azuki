  <div class="delete-from-database">
    <select name='delete-target' class="medium-2 small-2">
      <option value="">--</option>
      @if(!isset($deleteTargetYear) || !is_array($deleteTargetYear))
        @php
        $deleteTargetYear = [1, 2, 3];
        @endphp
      @endif
      @foreach($deleteTargetYear as $year)
      <option value="{{$year}}">{{$year}}</option>
      @endforeach
    </select>
    年以上前のデータを
    <button type="button" name="btn-delete" class="button" data-url="{{$story}}{{$controller}}/{{$deleteUrl}}">削除する</button>
  </div>
