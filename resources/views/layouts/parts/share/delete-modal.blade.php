<div class="reveal" id="delete-modal" data-reveal data-close-on-click="true" {{--data-animation-in="fade-in" data-animation-out="spin-out" --}}>
    <div>
        <button class="close-button" data-close aria-label="Close reveal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
        
        <div class="form-controll">
            <p class="">選択したデータを削除しますか。</p>
            <form action="{{$story}}{{$controller}}/delete" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}" />
                <input id="modal-form-delete-id" type="hidden" name="delete_id" value="">
                <button class="button" data-close aria-label="Close reveal" type="button" name="cancel">キャンセル</button>
                <button type="submit" name="doDelete" value="true" class="button button-primary text-right">削除する</button>
            </form>
        </div>
    </div>
</div>
