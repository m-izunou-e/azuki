@if($formMode == FORM_MODE_VIEW)

    @if( isset($post[$forms[$parts]['name']]) && !empty($post[$forms[$parts]['name']]) )
      <ul class="preview-list">
      @if( is_array($post[$forms[$parts]['name']]) )
        @foreach( $post[$forms[$parts]['name']] as $imagefile )
        <li class="image">
          @include($vendorPrefix.'layouts.parts.share.layout-parts-form.field-file-upload-view-tags',
            ['field' => 'image', 'file' => $imagefile, 'type' => $forms[$parts]['ufType']]
          )
        </li>
        @endforeach
      @else
        <li class="single-image">
          @include($vendorPrefix.'layouts.parts.share.layout-parts-form.field-file-upload-view-tags',
            ['field' => 'image', 'file' => $post[$forms[$parts]['name']], 'type' => $forms[$parts]['ufType']]
          )
        </li>
      @endif
      </ul>
    @else
      <p class="">登録されている画像はありません。</p>
    @endif

@else
{{--
  <label>{{$set['label'] ?? ''}}</label>
  <p><small id="image_attention_{{$set['name']}}">{{displayImageAttention($set['kind'])}}</small></p>
--}}
  <div class="image-upload-field"
      data-fieldname="{{$forms[$parts]['name']}}"
      data-type="{{$forms[$parts]['ufType']}}"
      data-ismulti=@if(isMultiple($forms[$parts]))"1"@else"0"@endif>
    <div class="preview draggable sortable" id="{{$forms[$parts]['name']}}-image">
    @if( isset($post[$forms[$parts]['name']]) && !empty($post[$forms[$parts]['name']]) )
      <ul class="preview-list">
      @if( is_array($post[$forms[$parts]['name']]) )
        @foreach( $post[$forms[$parts]['name']] as $imagefile )
        <li class=""{{-- style="{{displayImageSizeStyle($set['kind'])}}" --}}>
          @include($vendorPrefix.'layouts.parts.share.layout-parts-form.field-file-upload-view-tags',
            ['field' => 'image', 'file' => $imagefile, 'type' => $forms[$parts]['ufType']]
          )
          <input type="hidden" class="" name="{{$forms[$parts]['name']}}[]" value="{{$imagefile}}">
          <i class="{{$trashIcon}} small"></i>
        </li>
        @endforeach
      @else
        <li class="single-image">
          @include($vendorPrefix.'layouts.parts.share.layout-parts-form.field-file-upload-view-tags',
            ['field' => 'image', 'file' => $post[$forms[$parts]['name']], 'type' => $forms[$parts]['ufType']]
          )
          <input type="hidden" name="{{$forms[$parts]['name']}}" value="{{$post[$forms[$parts]['name']] ?? ''}}">
          <i class="{{$trashIcon}} small"></i>
        </li>
      @endif
      </ul>
    @else
      <div class="no-file-view"></div>
    @endif
    </div>
    <div class="btn">
      <label class="" style="display:inline;">
        <p class="button primary">ファイル選択</p>
        <input type="file" @if(isMultiple($forms[$parts]))multiple="multiple"@endif
            class="azuki-upload-field" name="" accept="image/*" style="display:none;">
      </label>
      @if(isMultiple($forms[$parts]))
      <button class="button secondary btn-all-image-delete" type="button"
          data-target="{{$forms[$parts]['name']}}-image">すべて削除</button>
      @else
      <button class="button secondary btn-single-image-delete" type="button"
          data-target="{{$forms[$parts]['name']}}-image">削除</button>
      @endif
    </div>
    <div class="upload-error-message"></div>
    <p class="help-text">{!!nl2br(e(getUploadFileInfomation($forms[$parts]['ufType'])))!!}</p>
  </div>
@endif
