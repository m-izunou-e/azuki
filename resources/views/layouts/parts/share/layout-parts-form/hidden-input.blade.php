        @foreach( $post as $name => $value )
          @if(is_array($value))
            @include($vendorPrefix.'layouts.parts.share.layout-parts-form.hidden-input-nest', ['group' => $value, 'name' => $name])
          @else
        <input type="hidden" name="{{$name}}" value="{{$value}}" />
          @endif
        @endforeach

        <input type="hidden" name="_token" value="{{csrf_token()}}" />
        <input type='hidden' name="flow" value="{{$flow}}" />
        <input type='hidden' name="id" value="{{$id ?? ''}}" />
