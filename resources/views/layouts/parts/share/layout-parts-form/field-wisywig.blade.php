@if($formMode == FORM_MODE_VIEW)
  @if( isset($post[$forms[$parts]['name'].'_html']) && !empty($post[$forms[$parts]['name'].'_html']) )
{!! $post[$forms[$parts]['name'].'_html'] !!}
  @else
    <p>&nbsp;</p>
  @endif
@else
<div id="editorjs" data-target="{{$forms[$parts]['name']}}">
</div>

<button type="button" id="editorClear" class="button primary">クリア</button>
<button type="button" id="editorLoad" class="button primary">読み込み</button>
<button type="button" id="editorSave" class="button primary">保存</button>

<input type="hidden" id="{{$forms[$parts]['name']}}" name="{{$forms[$parts]['name']}}" value="{{$post[$forms[$parts]['name']] ?? ''}}">

{{--
editor.jsはIE非対応なのでIEの場合は使用できないように調整が必要
--}}
<script src="/vendor/azuki/js/editor.js"></script>
<script>
var targetId = $('#editorjs').data('target');
var jsonString = $('#'+targetId).val();
//var eData = jsonString != '' ? $.parseJSON(decodeURIComponent(jsonString)) : null;
var eData = jsonString != '' ? $.parseJSON(jsonString) : null;


var editor = new EditorJS({
  /**
   * Id of Element that should contain Editor instance
   */
  holderId: 'editorjs',

  tools: { 
    header: Header, 
    list: List 
  }, 
  
  minHeight : 50,
  
  data: eData,
});

$('#editorSave').on('click', function(){
    editor.save().then(function(saveData) {
        eData = saveData;
        
        var targetId = $('#editorjs').data('target');
//        $('#'+targetId).val(encodeURIComponent(JSON.stringify(saveData)));
        $('#'+targetId).val(JSON.stringify(saveData));
        $('input[name="'+targetId+'_html"]').val($('#editorjs .codex-editor__redactor').html());
    });
});

$('#editorLoad').on('click', function(){
    editor.render(eData);
});

$('#editorClear').on('click', function(){
    editor.clear();
});

</script>

<style>
div#editorjs {
    border: 1px solid #CDCDCD;
/*
    height: 10rem;
    scroll-y: auto;
*/
}

div#editorjs .codex-editor .codex-editor__loader {
    height: 50px;
}
</style>
@endif
