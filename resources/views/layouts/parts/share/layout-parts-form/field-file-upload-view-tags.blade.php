@if($field == 'movie')
  @if(existsUploadFileThumbnail($file))
  <a href="javascript:void(0);" class="pop-movie-tag" data-org-src="{{getUploadedFileUrl($file, $type)}}">
    <img src="{{getUploadedFileThumbnailUrl($file, $type)}}" class="" alt="preview">
  </a>
  @else
  <video src="{{getUploadedFileUrl($file, $type)}}" class="" alt="preview" controls playsinline preload="metadata"></video>
  @endif
@else
  @if(existsUploadFileThumbnail($file))
  <a href="javascript:void(0);" class="pop-img-tag" data-org-src="{{getUploadedFileUrl($file, $type)}}">
    <img src="{{getUploadedFileThumbnailUrl($file, $type)}}" class="" alt="preview">
  </a>
  @else
  <a href="javascript:void(0);" class="pop-img-tag">
    <img src="{{getUploadedFileUrl($file, $type)}}" class="" alt="preview">
  </a>
  @endif
@endif
