@if(isset($forms[$parts]))
  @if($forms[$parts]['type'] == 'title')
    @if($formMode == FORM_MODE_VIEW)
    <label class="title @if(isset($forms[$parts]['class'])){{$forms[$parts]['class']}}@endif">{{$forms[$parts]['name']}}</label>
    @else
    <label class="title @if(isset($forms[$parts]['class'])){{$forms[$parts]['class']}}@endif">{{$forms[$parts]['name']}}@if(requiredFieldType($forms[$parts], $flow) == IS_REQUIRED)<span class="required">必須</span>@elseif(requiredFieldType($forms[$parts], $flow) == NOT_REQUIRED)<span class="optional">任意</span>@endif</label>
    @endif

  @elseif($forms[$parts]['type'] == 'label')
    <label>{{$forms[$parts]['label']}}</label>

  @else
    @include($vendorPrefix.getFormPartsTemplate($forms[$parts]['type']))

    @include($vendorPrefix.'layouts.parts.share.layout-parts-form.error')
    @include($vendorPrefix.'layouts.parts.share.layout-parts-form.helper-text')

  @endif
@endif
