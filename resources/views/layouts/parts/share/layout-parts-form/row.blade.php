@if(isViewRow($key, $pageType))
  <div class="row grid-padding-x">
  @if(is_array($row))
    @foreach($row as $key => $cell)
      @include($vendorPrefix.'layouts.parts.share.layout-parts-form.cell', ['cell' => $cell, 'groupName' => $key])
    @endforeach
  @else
    @include($vendorPrefix.'layouts.parts.share.layout-parts-form.cell', ['cell' => $row, 'groupName' => ''])
  @endif
  </div>
@endif
