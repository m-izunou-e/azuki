@if($formMode == FORM_MODE_VIEW)
  @if(isset($forms[$parts]['name']) && isset($post[$forms[$parts]['name']]) && !empty($post[$forms[$parts]['name']]))
    <p>{!! getFormatTextareaString($post[$forms[$parts]['name']]) !!}</p>
  @else
    <p>&nbsp;</p>
  @endif
@else
    <textarea class="{{$addClass ?? ''}}" rows="{{getTextareaRows($post[$forms[$parts]['name']] ?? '', $forms[$parts])}}" name="{{$forms[$parts]['name']}}" @if(requiredFieldType($forms[$parts], $flow) == IS_REQUIRED) required aria-required="true" @endif>{{$post[$forms[$parts]['name']] ?? ''}}</textarea>
@endif
