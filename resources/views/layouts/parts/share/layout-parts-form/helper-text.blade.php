  @if($formMode == FORM_MODE_INPUT)
    @if( isset($forms[$parts]['helperText']) && !empty($forms[$parts]['helperText']) )
      <p class="help-text">{!!nl2br(e($forms[$parts]['helperText']))!!}</p>
    @endif
  @endif
