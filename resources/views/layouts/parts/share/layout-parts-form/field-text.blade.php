{{--
  formModeがVIEWだったら値の表示。それ以外はフォームを表示する
  →formModeがVIEWの時は、詳細や確認画面。
--}}
@if($formMode == FORM_MODE_VIEW)
  @if(isset($forms[$parts]) && $forms[$parts]['type'] != 'hidden')
    @if( ($val = getFormatView($post, $forms[$parts], $areaName)) == '')
    <p>&nbsp;</p>
    @else
    <p>{{$val}}</p>
    @endif
  @endif
@else
    <input class="{{$addClass ?? ''}}" name="{{$forms[$parts]['name']}}" type="{{$forms[$parts]['type']}}" placeholder="{{$forms[$parts]['placeholder'] ?? ''}}" value="{{$post[$forms[$parts]['name']] ?? ''}}" @if(requiredFieldType($forms[$parts], $flow) == IS_REQUIRED) required aria-required="true" @endif>
@endif
