  @if(is_array($cell))
  <div class="{{getSizeClassFormCell($groupName)}} cell">
    @foreach($cell as $key => $row)
      @if(isGroupInput($key))
        @include($vendorPrefix.'layouts.parts.share.layout-parts-form.input-group', ['group' => $row, 'groupName' => $key])
      @else
        @include($vendorPrefix.'layouts.parts.share.layout-parts-form.row', ['row' => $row])
      @endif
    @endforeach
  </div>
  @else
    @if($forms[$cell]['type'] == 'hidden')
  <div class="">
    @elseif($forms[$cell]['type'] == 'title')
  <div class="{{getSizeClassFormCell($forms[$cell], 3)}} cell">
    @else
  <div class="{{getSizeClassFormCell($forms[$cell])}} cell">
    @endif
    @include($vendorPrefix.'layouts.parts.share.layout-parts-form.parts', ['parts' => $cell])
  </div>
  @endif
