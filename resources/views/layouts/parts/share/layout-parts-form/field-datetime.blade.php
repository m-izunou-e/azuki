@if($formMode == FORM_MODE_VIEW)
  @if(isset($forms[$parts]))
    <p>{{getFormatView($post, $forms[$parts], $areaName)}}</p>
  @endif
@else
    <input class="datetime-picker {{$addClass ?? ''}}"
        name="{{$forms[$parts]['name']}}" type="text"
        placeholder="{{$forms[$parts]['placeholder'] ?? ''}}"
        value="{{$post[$forms[$parts]['name']] ?? ''}}"
        @if(requiredFieldType($forms[$parts], $flow) == 1) required aria-required="true" @endif
    >
@endif
