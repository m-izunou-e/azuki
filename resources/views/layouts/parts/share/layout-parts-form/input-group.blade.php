@if(is_array($group))
<div class="row grid-padding-x">
  <div class="{{getSizeClassFormCell($groupName)}} cell margin-bottom-0">
    <div class="input-group">
    @foreach($group as $key => $parts)
      @if($forms[$parts]['type'] == 'label')
        @include($vendorPrefix.'layouts.parts.share.layout-parts-form.input-group-label')
      @elseif($forms[$parts]['type'] == 'button')
        @include($vendorPrefix.'layouts.parts.share.layout-parts-form.input-group-button')
      @else
        @include($vendorPrefix.getFormPartsTemplate($forms[$parts]['type']), ['addClass' => 'input-group-field '. getSizeClassFormCell($forms[$parts])])
      @endif
    @endforeach
    </div>

    @foreach($group as $key => $parts)
      @include($vendorPrefix.'layouts.parts.share.layout-parts-form.error')
    @endforeach

    @foreach($group as $key => $parts)
      @include($vendorPrefix.'layouts.parts.share.layout-parts-form.helper-text')
    @endforeach

  </div>
</div>

@endif
