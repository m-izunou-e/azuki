@if($formMode == FORM_MODE_VIEW)
@else
    <div class="{{getSizeClassFormCell($forms[$parts])}} input-group-button">
      <button type="button" name="" value="true" class="button margin-bottom-0"{!!addEventAttribute($forms[$parts])!!}>{{$forms[$parts]['label'] ?? ''}}</button>
    </div>
@endif
