@if($formMode == FORM_MODE_VIEW)

    @if( isset($post[$forms[$parts]['name']]) && !empty($post[$forms[$parts]['name']]) )
      <ul class="preview-list">
      @if( is_array($post[$forms[$parts]['name']]) )
        @foreach( $post[$forms[$parts]['name']] as $moviefile )
        <li class="movie">
          @include($vendorPrefix.'layouts.parts.share.layout-parts-form.field-file-upload-view-tags',
            ['field' => 'movie', 'file' => $moviefile, 'type' => $forms[$parts]['ufType']]
          )
        </li>
        @endforeach
      @else
        <li class="single-movie">
          @include($vendorPrefix.'layouts.parts.share.layout-parts-form.field-file-upload-view-tags',
            ['field' => 'movie', 'file' => $post[$forms[$parts]['name']], 'type' => $forms[$parts]['ufType']]
          )
        </li>
      @endif
      </ul>
    @else
      <p class="">登録されている動画はありません。</p>
    @endif

@else
  <div class="movie-upload-field"
      data-fieldname="{{$forms[$parts]['name']}}"
      data-type="{{$forms[$parts]['ufType']}}"
      data-ismulti=@if(isMultiple($forms[$parts]))"1"@else"0"@endif>
    <div class="preview draggable sortable" id="{{$forms[$parts]['name']}}-movie">
    @if( isset($post[$forms[$parts]['name']]) && !empty($post[$forms[$parts]['name']]) )
      <ul class="preview-list">
      @if( is_array($post[$forms[$parts]['name']]) )
        @foreach( $post[$forms[$parts]['name']] as $moviefile )
        <li class="movie">
          @include($vendorPrefix.'layouts.parts.share.layout-parts-form.field-file-upload-view-tags',
            ['field' => 'movie', 'file' => $moviefile, 'type' => $forms[$parts]['ufType']]
          )
          <input type="hidden" class="" name="{{$forms[$parts]['name']}}[]" value="{{$moviefile}}">
          <i class="{{$trashIcon}} small"></i>
        </li>
        @endforeach
      @else
        <li class="single-movie">
          @include($vendorPrefix.'layouts.parts.share.layout-parts-form.field-file-upload-view-tags',
            ['field' => 'movie', 'file' => $post[$forms[$parts]['name']], 'type' => $forms[$parts]['ufType']]
          )
          <input type="hidden" class="" name="{{$forms[$parts]['name']}}" value="{{$post[$forms[$parts]['name']]}}">
          <i class="{{$trashIcon}} small"></i>
        </li>
      @endif
      </ul>
    @else
      <div class="no-file-view"></div>
    @endif
    </div>
    <div class="btn">
      <label class="" style="display:inline;">
        <p class="button primary">ファイル選択</p>
        <input type="file" @if(isMultiple($forms[$parts]))multiple="multiple"@endif
            class="azuki-upload-field" name="" accept="video/*" style="display:none;">
      </label>
      @if(isMultiple($forms[$parts]))
      <button class="button secondary btn-all-movie-delete" type="button"
          data-target="{{$forms[$parts]['name']}}-movie">すべて削除</button>
      @else
      <button class="button secondary btn-single-movie-delete" type="button"
          data-target="{{$forms[$parts]['name']}}-movie">削除</button>
      @endif
    </div>
    <div class="upload-error-message"></div>
    <p class="help-text">{!!nl2br(e(getUploadFileInfomation($forms[$parts]['ufType'])))!!}</p>
  </div>
@endif
