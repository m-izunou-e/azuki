@if($formMode == FORM_MODE_VIEW)
  @if(isset($forms[$parts]))
    <p>{{getFormatView($post, $forms[$parts], $areaName)}}</p>
  @endif
@else
{{--
  <label>{{$set['label'] ?? ''}}</label>
  <p><small id="image_attention_{{$set['name']}}">{{displayImageAttention($set['kind'])}}</small></p>
--}}
  <div class="text-upload-field">
    <div class="preview"{{-- style="{{displayImageSizeStyle($set['kind'])}}" --}}>
    @if( isset($post[$forms[$parts]['name']]) && !empty($post[$forms[$parts]['name']]) )
      <img src="{{getUploadedFileUrl($post[$forms[$parts]['name']], $forms[$parts]['ufType'])}}" alt="preview" id="{{$forms[$parts]['name']}}-image">
    @else
      <img src="/admin/img/no_img.jpg" alt="preview" id="{{$forms[$parts]['name']}}-image">
    @endif
    </div>
    <div class="btn">
      <div class="btn-primary btn-xs upload_btn btn img_upload_btn">
        ファイル選択
        <input type="file" id="{{$forms[$parts]['name']}}" class="azuki-upload-field" name="" accept="*">
        <input type="hidden" id="{{$forms[$parts]['name']}}-image-name" name="{{$forms[$parts]['name']}}" value="{{$post[$forms[$parts]['name']] ?? ''}}">
      </div>
      <button class="btn btn-danger btn-xs btn-image-delete" type="button" data-target="{{$forms[$parts]['name']}}-image">削除</button>
    </div>
    <div class="upload-error-message"></div>
    <p class="help-text">{!!nl2br(e(getUploadFileInfomation($forms[$parts]['ufType'])))!!}</p>
  </div>
@endif
