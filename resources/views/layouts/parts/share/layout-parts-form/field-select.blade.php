@if($formMode == FORM_MODE_VIEW)
  @if(isset($forms[$parts]))
    @if(($val = getFormatView($post, $forms[$parts])) === '')
    <p>&nbsp;</p>
    @else
    <p>{{$val}}</p>
    @endif
  @else
    <p>&nbsp;</p>
  @endif
@else
    <select name="{{$forms[$parts]['name']}}" class="{{$addClass ?? ''}}" @if(requiredFieldType($forms[$parts], $flow) == IS_REQUIRED) required aria-required="true" @endif>
      <option value="">{{$forms[$parts]['placeholder'] ?? '---'}}</option>
    @foreach( getSelectList($forms[$parts]['select']) as $k => $val )
      <option label="{{$val['name']}}" value="{{$val['value'] ?? ""}}"@if (isset($post[$forms[$parts]['name']]) && $val["value"] == $post[$forms[$parts]['name']]) selected="selected" @endif>{{$val['name'] ?? ''}}</option>
    @endforeach
    </select>
@endif
