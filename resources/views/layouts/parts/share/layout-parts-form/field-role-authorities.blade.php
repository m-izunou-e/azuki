{{--
  formModeがVIEWだったら値の表示。それ以外はフォームを表示する
  →formModeがVIEWの時は、詳細や確認画面。
--}}
@if($formMode == FORM_MODE_VIEW)
  @if(isset($post[$forms[$parts]['name']]) && !empty($post[$forms[$parts]['name']]))
    @foreach($post[$forms[$parts]['name']] as $row)
  <div class="input-group">
    <div class="medium-6">
      <p>{{$row['url']}}</p>
    </div>
    <div class="medium-4">
      <p>{{getFormatView( $row, $forms[$parts]['parts']['authority'])}}</p>
    </div>
  </div>
    @endforeach
  @else
  <p>設定なし</p>
  @endif
@else
  <div id="role_authority_area">
  @if(isset($post[$forms[$parts]['name']]) && !empty($post[$forms[$parts]['name']]))
    @foreach($post[$forms[$parts]['name']] as $key => $row)
  <div class="input-group auth-group" data-key={{$key}}>
    @if(isset($row['id']))
    <input type="hidden" name="{{$forms[$parts]['name']}}[{{$key}}][id]" value="{{$row['id']}}">
    @endif
    <div class="medium-6">
      <input name="{{$forms[$parts]['name']}}[{{$key}}][url]" type="text" value="{{$row['url']}}" placeholder="対象URL">
    </div>
    <div class="medium-4">
      <select name="{{$forms[$parts]['name']}}[{{$key}}][authority]" class="{{$addClass ?? ''}}" >
        <option value="">{{$forms[$parts]['parts']['authority']['placeholder'] ?? '---'}}</option>
      @foreach( getSelectList($forms[$parts]['parts']['authority']['select']) as $k => $val )
        <option label="{{$val['name']}}" value="{{$val['value'] ?? ""}}"@if (isset($row['authority']) && $val["value"] == $row['authority']) selected="selected" @endif>{{$val['name'] ?? ''}}</option>
      @endforeach
      </select>
    </div>
    <div class="medium-2">
      <button type="button" class="button primary delete_authority">削除</button>
    </div>
  </div>
    @endforeach
  @endif
  </div>

  <div class="medium-12">
      <button type="button" id="add_role_authority" class="button primary">権限設定入力欄追加</button>
  </div>

  <div id="role_authority_template" class="hide">
  <div class="input-group auth-group">
    <div class="medium-6">
      <input name="__{{$forms[$parts]['name']}}[key][url]" type="text" value="" placeholder="対象URL">
    </div>
    <div class="medium-4">
      <select name="__{{$forms[$parts]['name']}}[key][authority]" class="{{$addClass ?? ''}}" >
        <option value="">{{$forms[$parts]['parts']['authority']['placeholder'] ?? '---'}}</option>
      @foreach( getSelectList($forms[$parts]['parts']['authority']['select']) as $k => $val )
        <option label="{{$val['name']}}" value="{{$val['value'] ?? ""}}">{{$val['name'] ?? ''}}</option>
      @endforeach
      </select>
    </div>
    <div class="medium-2">
      <button type="button" class="button primary delete_authority">削除</button>
    </div>
  </div>
  </div>
@endif
