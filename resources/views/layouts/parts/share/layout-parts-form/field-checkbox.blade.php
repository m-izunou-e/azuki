@if($formMode == FORM_MODE_VIEW)
  @if(isset($forms[$parts]))
    <p>{!!nl2br(space2nbsp(e(getFormatView($post, $forms[$parts]))))!!}</p>
  @else
    <p>&nbsp;</p>
  @endif
@else
  <div class="margin-bottom-1">
    @foreach( getSelectList($forms[$parts]['select']) as $k => $val )
    <label for="{{$forms[$parts]['name']}}_{{$val['value']}}"><input class="{{$addClass ?? ''}}" type="checkbox" name="{{$forms[$parts]['name']}}[]" value="{{$val['value']}}" id="{{$forms[$parts]['name']}}_{{$val['value']}}"@if(isset($post[$forms[$parts]['name']]) && is_array($post[$forms[$parts]['name']]) && in_array($val["value"], $post[$forms[$parts]['name']])) checked="checked" @endif>{{$val['name'] ?? ''}}</label>
    @endforeach
  </div>
@endif
