@if($formMode == FORM_MODE_VIEW)
    <p class="{{getSizeClassFormCell($forms[$parts])}} group-label">{{$forms[$parts]['label'] ?? ''}}</p>
@else
    <span class="{{getSizeClassFormCell($forms[$parts])}} input-group-label">{{$forms[$parts]['label'] ?? ''}}</span>
@endif
