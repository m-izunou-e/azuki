@if($formMode == FORM_MODE_VIEW)
  @if( ($val = getFormatView($post, $forms[$parts], $areaName)) == '')
    <p>&nbsp;</p>
  @else
    <p>{!!nl2br(e($val))!!}</p>
  @endif
@else
@endif
