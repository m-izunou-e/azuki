@if($formMode == FORM_MODE_VIEW)
  @if(isset($forms[$parts]))
    <p>{{getFormatView($post, $forms[$parts])}}</p>
  @else
    <p>&nbsp;</p>
  @endif
@else
    @foreach( getSelectList($forms[$parts]['select']) as $k => $val )
    <input class="{{$addClass ?? ''}}" type="radio" name="{{$forms[$parts]['name']}}" value="{{$val['value']}}" id="{{$forms[$parts]['name']}}_{{$val['value']}}"@if (isset($post[$forms[$parts]['name']]) && $val["value"] == $post[$forms[$parts]['name']]) checked="checked" @endif><label for="{{$forms[$parts]['name']}}_{{$val['value']}}">{{$val['name'] ?? ''}}</label>
    @endforeach
@endif
