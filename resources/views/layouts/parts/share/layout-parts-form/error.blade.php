    @if (isset($forms[$parts]['column']) && $errors->has($forms[$parts]['column']))
    <div class="form error">
        @foreach ( $errors->get($forms[$parts]['column']) as $msg )
      <p>{{trans($msg)}}</p>
        @endforeach
    </div>
    @endif
