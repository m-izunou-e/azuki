@if(isset($needDatetimePicker) && $needDatetimePicker)
<link href="//cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.min.css" rel="stylesheet">
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js"></script>
<script>
  jQuery(function($) {
    $.datetimepicker.setLocale('ja');
    $('.datetime-picker').datetimepicker({
      step: 10,
//      lang: 'ja',
    });
  });
</script>
@endif
