@if ($pager->total() == 0)
@else
  <div class="margin-top clearfix">
    <div class="float-left">
      <p>{{$pager->total()}} 件中 {{$pager->perPage()*($pager->currentPage()-1)+1}} - @if ($pager->perPage()*$pager->currentPage() < $pager->total()){{$pager->perPage()*$pager->currentPage()}}@else{{$pager->total()}}@endif件を表示</p>
    </div>
    <ul class="pagination float-right no-margin">

@if ($pager->currentPage() > 4)
    <li class="first"><a href="{{$story}}{{$controller}}/list{{$listAppendUrl ?? ''}}" title="最初へ">&lt;&lt;</a></li>
@endif
@if ($pager->currentPage() > 1)
    <li class="prev"><a href="{{$story}}{{$controller}}/list{{$listAppendUrl ?? ''}}?page={{$pager->currentPage()-1}}" title="前へ">&lt;</a></li>
@endif
@php
    $pagerStart = $pager->currentPage() - 3 >= 1 ? $pager->currentPage() - 3 : 1;
    $pagerLast  = $pager->currentPage() + 3 <= $pager->lastPage() ? $pager->currentPage() + 3 : $pager->lastPage();
    if( $pager->currentPage() <= 3 && $pager->lastPage() >= 7 ) {
        $pagerLast  = 7;
    }
    if( $pager->lastPage() - 3 < $pager->currentPage() && $pager->lastPage() >= 7 ) {
        $pagerStart = $pager->lastPage() - 6;
    }
@endphp
@if( $pagerLast > 1 ) 
    @for ($i = $pagerStart; $i <= $pagerLast; $i++)
        @if ($pager->currentPage() == $i)
      <li class="active"><a href="javascript:void(0);">{{$i}}</a></li>
        @else
      <li class="count"><a href="{{$story}}{{$controller}}/list{{$listAppendUrl ?? ''}}?page={{$i}}" tile="page {{$i}}">{{$i}}</a></li>    
        @endif
    @endfor
@endif
@if ($pager->lastPage() > $pager->currentPage())
      <li class="next"><a href="{{$story}}{{$controller}}/list{{$listAppendUrl ?? ''}}?page={{$pager->currentPage()+1}}" title="次へ">&gt;</a></li>
@endif
@if ($pager->lastPage() - 3 > $pager->currentPage())
      <li class="last"><a href="{{$story}}{{$controller}}/list{{$listAppendUrl ?? ''}}?page={{$pager->lastPage()}}" title="最後へ">&gt;&gt;</a></li>
@endif
    </ul>
  </div>
@endif
