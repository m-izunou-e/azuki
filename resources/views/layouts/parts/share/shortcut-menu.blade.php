    <div class="manage shortcut-menu">
        <ul class="vertical menu">
        @foreach( $shortcutMenu as $menu )
            <li>
                <a href="{{$story}}{{$menu['url']}}">
                    <i class="fi-{{$menu['icon']}}"></i>
                    <p>{{$menu['name']}}</p>
                </a>
            </li>
        @endforeach
        </ul>
    </div>
