<div class="reveal" id="globalMenu" data-reveal data-close-on-click="true" {{--data-animation-in="fade-in" data-animation-out="spin-out" --}}>
    <button class="close-button" data-close aria-label="Close reveal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
    <ul class="flex-area">
    @foreach( $globalMenu as $menu )
    <li class="cell">
        <a href="{{$story}}{{$menu['url']}}" class="clear">
            <i class="fi-{{$menu['icon']}} large"></i>
            <p>{{getMenuName($prefix, $menu)}}</p>
        </a>
    </li>
    @endforeach
    </ul>
</div>
