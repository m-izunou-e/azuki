@if(enableSnsLogin())
<div class="sns-login">
  <p>各種SNSでログインする</p>
  <ul>
    @foreach(snsProviderList() as $provider)
    <li><a href="{{ getSnsUrlFrom($provider) }}" class="btn">
      <img src="{{ getSnsImgSrc($provider) }}" class="{{ getSnsProviderName($provider) }}">
    </a></li>
    @endforeach
  </ul>
</div>
@endif
