@if($domain == 'common')
    <div class="center-box-login">
@else
    <div class="center-box">
@endif
        <div class="large-4 medium-6 small-10">
            <div class="logo">
                <h3 class="login-title">{{getLogoTitle($domain, 'login', 'ログイン')}}</h3>
                @if(isLogoImage($domain, 'login'))
                <img src="{{getLogoImagePath($domain, 'login')}}">
                @endif
            </div>
@if ($errors->any())
            <div class="alert callout" data-closable>
    @foreach ($errors->all() as $error)
                <p class="alert">{{$error}}</p>
    @endforeach
                <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
@endif
        <form method="post" action="{{$story}}login/auth">
            {{ csrf_field() }}
            <dl class="cell">
                @if(loginIs($domain) == 'email')
                <dt class="cell">Email</dt>
                <dd class="cell"><input type="text" name="email" value="{{old('email')}}" placeholder="メールアドレス"></dd>
                @else
                <dt class="cell">ログインID</dt>
                <dd class="cell"><input type="text" name="login_id" value="{{old('login_id')}}" placeholder="ログインID"></dd>
                @endif
                <dt class="cell">パスワード</dt>
                <dd class="cell"><input type="password" name="password" value="" placeholder="パスワードを入力"></dd>
            </dl>
            <div class="text-right">
                @if(enableResetPassword($domain))
                  @if($domain == 'common')
                <p><a href="{{ route('reset-password.send-reset-mail', [], false) }}">パスワード再設定</a></p>
                  @else
                <p><a href="{{ route($domain.'.reset-password.send-reset-mail', [], false) }}">パスワード再設定</a></p>
                  @endif
                @endif
                <input class="hollow button radius bordered shadow primary" type="submit" name="toLogin" value="ログイン">
            </div>
@if($domain == 'common')
  @include($vendorPrefix.'layouts.parts.sns_logins')
@endif
        </form>
        </div>
    </div>
