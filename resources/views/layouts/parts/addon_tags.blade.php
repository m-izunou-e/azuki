@foreach($analyticsTags as $key => $tag)
  @if(isset($tag['tag']) && !empty($tag['tag']))
    @if($key == 'gtmtag')
  <!-- Google Tag Manager -->
  <script>
    (function(w,d,s,l,i){
      w[l]=w[l]||[];
      w[l].push({
        'gtm.start': new Date().getTime(),
        event      : 'gtm.js'
      });
      var f=d.getElementsByTagName(s)[0], j=d.createElement(s), dl=l!='dataLayer'?'&l='+l:'';
      j.async=true;
      j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window, document, 'script', '{{$tag["dataLayer"] ?? "dataLayer"}}', 'GTM-{{$tag["tag"]}}');
  </script>
  <!-- End Google Tag Manager -->
    @endif
    
    @if($key == 'uatag')
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-{{$tag['tag']}}"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
  
    gtag('config', 'UA-{{$tag["tag"]}}');
  </script>
    @endif
    
    @if($key == 'ga4tag')
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-{{$tag['tag']}}"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
  
    gtag('config', 'G-{{$tag["tag"]}}');
  </script>
    @endif
  @endif
@endforeach
