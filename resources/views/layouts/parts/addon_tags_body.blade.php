@foreach($analyticsTags as $key => $tag)
  @if($key == 'gtmtag' && isset($tag['tag']) && !empty($tag['tag']))
  <!-- Google Tag Manager (noscript) -->
  <noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-{{$tag['tag']}}"
      height="0" width="0" style="display:none;visibility:hidden">
    </iframe>
  </noscript>
  <!-- End Google Tag Manager (noscript) -->
  @endif
@endforeach
