@extends($vendorPrefix.'layouts.simple')

@section('headTags')
@include($vendorPrefix.'layouts.parts.addon_tags')
@stop

@section('headScript')
@parent
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="//cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
@include($vendorPrefix.'layouts.parts.share.datetime-picker-script')
@stop

@section('bodyTags')
@include($vendorPrefix.'layouts.parts.addon_tags_body')
@stop

@section('contents')
    <div class="system head-line">
@if(disableHeadInfo($globalConfig))
  @if(enableLogout($globalConfig))
        <ul class="dropdown menu" data-dropdown-menu>
            <li class="left-open"><a href="javascript:void(0);"><i class="{{$userIcon}} medium"></i></a>
                <ul class="menu is-dropdown-submenu">
                    <li><a href="{{ route('system.logout', [], false) }}">ログアウト</a></li>
                </ul>
            </li>
        </ul>
  @endif
@else
        <p id="user-name">{{$user->name}}さん</p>
        <ul class="dropdown menu" data-dropdown-menu>
            <li class="left-open"><a href="javascript:void(0);"><i class="{{$userIcon}} medium"></i></a>
                <ul class="menu is-dropdown-submenu">
                    <li><a href="{{ route('system', [], false) }}">ダッシュボード</a></li>
                    <li><a href="{{ route('system.profile.detail', [], false) }}">プロフィール</a></li>
                    <li><a href="{{ route('system.logout', [], false) }}">ログアウト</a></li>
                </ul>
            </li>
        </ul>
@endif
{{--
        <p><button class="hollow button" data-toggle="modalHelp">
            <i class="{{$infoIcon}} medium"></i>
        </button></p>
--}}
    </div>
    <div class="system contents-body">
@section('contents-body')
@show
    </div>
{{--    @include($vendorPrefix.'layouts.parts.share.shortcut-menu');--}}
@if(!disableGlobalNavi($globalConfig))
    <p id="open-global-navi">
        <button class="hollow button radius shadow" data-toggle="globalMenu">
            <i class="{{$menuIcon}} pr5"></i><span>Menu</span>
        </button>
    </p>
@endif
@stop

@section('modalContents')
@include($vendorPrefix.'layouts.parts.share.global-menu')
@include($vendorPrefix.'layouts.parts.share.important-help')
@stop
