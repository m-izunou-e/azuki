@extends($vendorPrefix.'layouts.'.$baseLayout)

@section('headMeta')
@parent
<meta name="description" content="" />
<meta name="keywords" content="" />
@stop

@section('headLink')
@parent
@stop

@section('headCss')
@parent
@stop

@section('headScript')
@parent
@stop

@section('title')
<title>{{$pageTitle}}</title>
@stop

@section('contents-body')

      <div class="in-contents-body">
        <h3 class="header line-style">{{$pageName}}詳細情報</h3>
        
        @if( isset($displayResultOnDetail) && $displayResultOnDetail == true )
        @include ($vendorPrefix.'layouts.parts.share.process_result_on_list', ['name' => $pageName])
        @endif
        
        <section>
          @include($vendorPrefix.'layouts.parts.share.detail', ['controller' => $controller])
        </section>
      </div>
@stop

@section('modalContents')
@parent
@if(!isset($deleteModal) || $deleteModal != false)
  @include ($vendorPrefix."layouts.parts.share.delete-modal", ["controller" => $controller])
@endif
@stop


@section('footer-javascript')
@parent
@stop
