@extends($vendorPrefix.'layouts.'.$baseLayout)

@section('headMeta')
@parent
<meta name="description" content="" />
<meta name="keywords" content="" />
@stop

@section('headLink')
@parent
@stop

@section('headCss')
@parent
@stop

@section('headScript')
@parent
@stop

@section('title')
<title>{{$pageTitle}}</title>
@stop

@section('contents-body')
    @include($vendorPrefix.'layouts.parts.share.confirm', ['controller' => $controller])
@stop

@section('modalContents')
@parent
@stop


@section('footer-javascript')
@parent
<script type="text/javascript">
</script>
@stop
