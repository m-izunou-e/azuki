@extends($vendorPrefix.'layouts.simple')

@section('headTags')
@include($vendorPrefix.'layouts.parts.addon_tags')
@stop


@section('headScript')
@parent
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
@stop

@section('bodyTags')
@include($vendorPrefix.'layouts.parts.addon_tags_body')
@stop


@section('contents')
    <div class="common head-line" id="head-line">
      @if(Route::has('login'))
        @auth
        @if(isset($user))
        <p id="user-name">{{$user->name}}さん</p>
        @else
        <p id="user-name">{{app('request')->user()->name}}さん</p>
        @endif
        <ul class="dropdown menu" data-dropdown-menu>
            <li><a href="javascript:void(0);"><i class="{{$userIcon}} medium"></i></a>
                <ul class="menu is-dropdown-submenu">
                    <li><a href="{{ route('mypage', [], false) }}">マイページ</a></li>
                    <li><a href="{{ route('logout', [], false) }}">ログアウト</a></li>
                </ul>
            </li>
        </ul>
        @else
        <ul class="menu flex-right">
          @if(Route::has('index'))
          <li title="トップ"><a href="{{ route('index', [], false) }}"><i class="{{$homeIcon}} medium"></i></a></li>
          @endif
          @if(!Route::is('login'))
          <li title="ログイン"><a href="{{ route('login', [], false) }}"><i class="{{$userIcon}} medium"></i></a></li>
          @endif
          <li title="新規登録"><a href="{{ route('regist', [], false) }}"><i class="{{$userRegistIcon}} medium"></i></a></li>
        </ul>
        @endauth
      @else
      @endif
    </div>
    <div class="common contents-body">
@section('contents-body')
@show
    </div>
@stop

@section('modalContents')
@stop
