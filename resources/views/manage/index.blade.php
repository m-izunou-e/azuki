@extends($vendorPrefix.'layouts.manage_simple')

@section('headMeta')
@parent
<meta name="description" content="" />
<meta name="keywords" content="" />
@stop

@section('headLink')
@parent
@stop

@section('headCss')
@parent
<style>

#manage-main-dummy {
    height: calc(100vh - 3.9em);
/*    display: flex;*/
    justify-content: center;
    align-items: center;
    display: none;
}

#manage-main-dummy p {
    font-size: 8em;
    color: #636b6f;
    font-family: 'Raleway', sans-serif;
}

</style>
@stop

@section('headScript')
@parent
<script type="text/javascript">
$(function(){
    $('#manage-main-dummy').css('display', 'flex').hide().fadeIn(3000);
});
</script>
@stop

@section('title')
<title>{{$pageTitle}}</title>
@stop

@section('contents-body')
    <div id="manage-main-dummy">
        <p>Azuki</p>
    </div>
@stop
