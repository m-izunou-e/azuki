@extends($vendorPrefix.'layouts.simple')

@section('headMeta')
@parent
<meta name="robots" content="noindex">
<meta name="description" content="" />
<meta name="keywords" content="" />
@stop

@section('headLink')
@parent
@stop

@section('headCss')
@parent
@stop

@section('headScript')
@parent
@stop

@section('title')
<title>パスワード再設定－Azuki</title>
@stop

@section('contents')
<div class="grid-y grid-frame">
    <div class="grid-x align-right">
        <a class="box-padding"  href="{{ route('manage.login', [], false) }}">Login</a>
    </div>
    <div class="grid-x align-center align-middle large-12">
        <div class="large-4 margin-tb-40">
@if ($errors->any())
            <div class="alert callout" data-closable>
    @foreach ($errors->all() as $error)
                <p>{{$error}}</p>
    @endforeach
                <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
@endif
        <form method="post" action="{{ route('manage.reset-password.p-reset-password', [], false) }}">
            {{ csrf_field() }}
            <input type="hidden" name="token" value="{{$token}}">
            <dl class="cell">
                <dt class="cell">メールアドレス</dt>
                <dd class="cell">
                    <input type="text" name="email" value="{{old('email')}}" placeholder="メールアドレス">
{{--
                    @if($errors->has('email'))
                    <div class="form error margin-top-m1 margin-bottom-1">
                      @foreach ($errors->get('email') as $error)
                      <p>{{trans($error)}}</p>
                      @endforeach
                    </div>
                    @endif
--}}
                </dd>
                <dt class="cell">パスワード</dt>
                <dd class="cell">
                    <input type="password" name="password" value="">
{{--
                    @if($errors->has('password'))
                    <div class="form error margin-top-m1 margin-bottom-1">
                      @foreach ($errors->get('password') as $error)
                      <p>{{trans($error)}}</p>
                      @endforeach
                    </div>
                    @endif
--}}
                </dd>
                <dt class="cell">パスワード(確認)</dt>
                <dd class="cell"><input type="password" name="password_confirmation" value=""></dd>
            </dl>
            <div class="text-right">
                <input class="hollow button radius bordered shadow primary" type="submit" name="toResetPassword" value="パスワード再設定">
            </div>
        </form>
        </div>
    </div>
</div>
@stop
