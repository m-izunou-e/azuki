<?php

return [
    'system' => [
        'index' => [
            'title' => 'ダッシュボード｜システム管理',
        ],
        'login' => [
            'title'     => 'ログイン｜システム管理',
            'logoTitle' => 'システム管理ログイン',
        ],
        'directors' => [
            'menu'  => 'システム管理者',
            'title' => 'システム管理者管理',
            'name'  => 'システム管理者',
        ],
        'managers'  => [
            'menu'  => '運用スタッフ',
            'title' => '運用スタッフ管理',
            'name'  => '運用スタッフ',
        ],
        'users'  => [
            'menu'  => 'ユーザー',
            'title' => 'ユーザー管理',
            'name'  => 'ユーザー',
        ],
        'system-roles'  => [
            'menu'  => 'ロール',
            'title' => 'ロール管理',
            'name'  => 'ロール',
        ],
        'manage-roles'  => [
            'menu'  => 'ロール',
            'title' => 'ロール管理',
            'name'  => 'ロール',
        ],
        'common-roles'  => [
            'menu'  => 'ロール',
            'title' => 'ロール管理',
            'name'  => 'ロール',
        ],
        'organizations'  => [
            'menu'  => '組織',
            'title' => '組織管理',
            'name'  => '組織',
        ],
        'access-logs'  => [
            'menu'  => 'アクセスログ',
            'title' => 'アクセスログ管理',
            'name'  => 'アクセスログ',
        ],
        'login-logs'  => [
            'menu'  => 'ログインログ',
            'title' => 'ログインログ管理',
            'name'  => 'ログインログ',
        ],
        'operation-logs'  => [
            'menu'  => 'オペレーションログ',
            'title' => 'オペレーションログ管理',
            'name'  => 'オペレーションログ',
        ],
        'system-logs'  => [
            'menu'  => 'システムログ',
            'title' => 'システムログ管理',
            'name'  => 'システムログ',
        ],
        'execute-queue-status'  => [
            'menu'  => 'コマンド実行状況',
            'title' => 'コマンド実行処理状況確認',
            'name'  => 'コマンド実行処理状況',
        ],
        'environment'  => [
            'menu'  => '環境',
            'title' => '環境情報確認',
        ],
        'structure'  => [
            'menu'  => 'システム構成',
            'title' => 'システム構成確認・切り替え',
        ],
    ],
    'manage' => [
        'index' => [
            'title' => 'ダッシュボード｜運用管理',
        ],
        'login' => [
            'title'     => 'ログイン｜運用管理',
            'logoTitle' => '運用管理ログイン',
        ],
        'managers'  => [
            'menu'  => 'スタッフ',
            'title' => 'スタッフ管理',
            'name'  => 'スタッフ',
        ],
        'users'  => [
            'menu'  => 'ユーザー',
            'title' => 'ユーザー管理',
            'name'  => 'ユーザー',
        ],
        'execute-queue-status'  => [
            'menu'  => 'コマンド実行状況',
            'title' => 'コマンド実行処理状況確認',
            'name'  => 'コマンド実行処理状況',
        ],
    ],
    'common' => [
        'index' => [
            'title' => 'サイトトップ',
        ],
        'login' => [
            'title'     => 'ログイン｜ユーザー',
            'logoTitle' => 'ログイン',
        ],
        'mypage' => [
            'title' => 'マイページ｜ユーザー',
        ],
    ],
    'link' => [
        'roles' => [
            'system-roles' => 'システム管理者',
            'manage-roles' => '運用管理者',
            'common-roles' => 'ユーザー',
        ],
    ],
];
