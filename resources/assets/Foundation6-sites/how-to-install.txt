■foundation、nodeのインストール
　nvmを使用して特定nodeバージョンを選択するため、
　１．https://github.com/nvm-sh/nvm
　　を参考にnvmをインストールする
　　curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.2/install.sh | bash
　　→バージョン部分が最新状況で変わる

　２．パスを通す
　　source ~/.bashrc

　３．インストール状態を確認
　　nvm -v　などで確認する

　４．npmをインストールする
　　nvm install 10
　　を実行
　　→foundationで必要なnodeバージョンが10系なので上記コマンドでインストールする

　５．バージョン確認
　　npm -v
　　node -v
　　でそれぞれバージョン確認する

　６．foundationをインストール
　　npm install -g foundation-cli@2.2.6　//　←2.3.0系はちゃんと動作できないので2.2.6系を指定
　　resources/assets/Foundation6-sitesディレクトリにて
　　npm install
　　を実行

　７．動作確認
　　foundation watch　//　←-gオプションでfoundationをインストールしていなければパスが通らないのでフルパス指定が必要
　　を実行する


■参考URL
https://github.com/nvm-sh/nvm
https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-20-04-ja
