import $ from 'jquery';
import whatInput from 'what-input';

window.jQuery = $;
window.$ = $;

import Foundation from 'foundation-sites';
// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';

import azuki from './azuki/app.js';

$(function(){
   $(document).foundation();
   Azuki._init();
});
