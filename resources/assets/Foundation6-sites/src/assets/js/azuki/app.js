"use strict";

import $ from 'jquery';

var Azuki = {
    params: {
        uploading: false,
        uploadUrl: '/manage/file-upload',
        publicAddKeys: 0,
    },
    options: {
        deleteModalClass: 'trigger-delete-modal'
    },
    _init: function() {
        this.initModal();
        this.initDeleteModal();
        this.initListSortable();
        this.initSearchBox();
        this.initUploadField();
        this.initRoleAuthority();
        this.initDeleteDataFromDatabase();
        this.initSwitchSystem();
        this.initCronButton();
    },
    // 削除用のモーダルウィンドウの展開処理を紐づける
    initDeleteModal: function() {
        var dc = Azuki.options.deleteModalClass;
        $('.'+dc).on('click', function () {
            Azuki.openDeleteModel($(this));
        });
    },
    initModal : function() {
        $('.modal-trigger').on('click', function(){
            // モーダルを表示する
            var target = $(this).data('target');
            $(target).foundation('open');
        });
    },
    // 削除用のモーダルウィンドウの展開処理
    openDeleteModel: function($elm) {
        // 削除IDを取得する。取得に失敗したらログを残して終了。
        var delId = $elm.data('id');
//        console.log(delId);
        // 削除IDをセットする
        $('#modal-form-delete-id').val(delId);
        
        // モーダルを表示する
        var target = $elm.data('target');
        $(target).foundation('open');
    },
    // 一覧のソート機能を初期化
    initListSortable: function() {
        $('div.sortable-icons').on('click', 'a', function() {
            var column   = $(this).data('col');
            var sort     = $(this).data('sort');
            var type     = sort;
            var sortForm = $('#table_order_form');
            
            if( $(this).children('i.selected').length > 0 ) {
                type = 'none';
            }
            sortForm.append(
                $('<input>').attr('type', 'hidden').attr('name', 'order[click]').val(column)
            );
            var condition = 'order[condition]['+column+']';
            if(sortForm.children('input[name="'+condition+'"]').length > 0) {
                sortForm.children('input[name="'+condition+'"]').val(type);
            } else {
                sortForm.append(
                    $('<input>').attr('type', 'hidden').attr('name', condition).val(type)
                );
            }

            sortForm.submit();
        });
    },
    // 検索ボックスの機能を実装
    initSearchBox: function() {
        $('i#open-search-box').on('click', function(){
            $('form#list_search_box_short_form').addClass('hidden');
            $('form#list_search_box_form').removeClass('hidden');
        });
        $('i#close-search-box').on('click', function(){
            $('form#list_search_box_form').addClass('hidden');
            $('form#list_search_box_short_form').removeClass('hidden');
        });
        $('button#all_clear_btn').on('click', function(){
            Azuki.clearForm($('form#list_search_box_form'));
            Azuki.clearForm($('form#list_search_short_box_form'));
        });
    },
    clearForm: function(form) {
        form.find('input, select, textarea')
            .not(":button, :submit, :reset, :hidden, :checkbox, :radio")
            .val('')
            .prop('selected', false);
        form.find(':checkbox, :radio').prop('checked', false);
    },

    startUpload: function() {
        var ret = this.params.uploading ? false : true;
        if( ret ) {
            this.params.uploading = true;
            $.LoadingOverlay("show");
        }
        return ret;
    },
    endUpload: function(errors, area = null, reload = false) {
        var ret = this.params.uploading;
        if( ret ) {
            $.LoadingOverlay("hide");
            this.params.uploading = false;
            if(errors.length > 0) {
                alert('アップロードに失敗したファイルがあります。');
                this.displayUploadErrorMessage(errors, area);
            }
            if(reload) {
                location.reload();
            }
        }
        return ret;
    },
    displayUploadErrorMessage: function(errors, area) {
        var errorDisplay = area != null ? area.children('.upload-error-message') : null;
        if( typeof errorDisplay != 'undefined' ) {
            errorDisplay.addClass('form error');
        }
        errors.forEach(function(row) {
            if( typeof errorDisplay != 'undefined' ) {
                errorDisplay.append('<p>ファイル&nbsp;['+row.file+']&nbsp;は&nbsp;['+row.msg+']</p>');
            }
            console.log(row.file);
            console.log(row.msg);
            console.log(row.detail);
        });
    },
    clearUploadErrorMessage: function(area) {
        var errorDisplay = area != null ? area.children('.upload-error-message') : null;
        if( typeof errorDisplay != 'undefined' ) {
            errorDisplay.removeClass('form error');
            errorDisplay.empty();
        }
    },

    // ファイルアップロードに関する処理を記述
    initUploadField: function() {
        var Azuki = this;
    
        // 登録画像がない場合のサムネイル表示部分の初期化
        this.initNotFileField();
        
        var getMimeErrorMessage = function(file, acceptMime) {
            var msg = '選択されたファイル['+file.name+'-type:'+file.type+']は許可されていないファイルタイプです。'+"\n" + 
                '許可されるファイルタイプは'+"\n"+
                '['+acceptMime+']';
            return msg;
        };
        
        var checkMimeType = function(type, acceptMime) {
            var mimeList = acceptMime.split(',');
            return $.inArray(type, mimeList) >= 0;
        };

        var fileUpload = function(options, files, validate, createElement) {
            var uploadUrl     = options.uploadUrl;
            var throughParam  = options.through;
            var eventArea     = options.eventArea;
            var fieldName     = eventArea.data('fieldname');
            var contentsType  = eventArea.data('type');
            var previewArea   = eventArea.children('.preview');
            var completeNum   = 0;
            var isMulti       = eventArea.data('ismulti');
            fieldName         = isMulti == 1 ? fieldName+'[]' : fieldName;

            var errorObj = [];
            Azuki.clearUploadErrorMessage(eventArea);
            if( typeof files != 'undefined' && files.length > 0 && Azuki.startUpload() ) {
                // ここでアップロード中表示を開始する
                var num = isMulti == 1 ? files.length : 1;
                for( var i = 0; i < num; i++ ) {

                    var file = files[i];
                    if( !validate(file) ) {
                        completeNum++;
                        console.log('valid:number:'+completeNum);
                        errorObj.push({file:file.name,msg:'アップロードできないファイル',detail:'バリデーションエラー'});
                        continue;
                    }

                    var formData = new FormData();
                    formData.append('upload-file', file);
                    formData.append('type', contentsType);
                    formData.append('through', throughParam);

                    Azuki.execFileUpload(
                        formData,
                        uploadUrl,
                        function(response){
                            if(response.result) {
                                if( previewArea.children('.no-file-view').length ) {
                                    previewArea.children('.no-file-view').remove();
                                    var previewListUL = $('<ul>').addClass('preview-list');
                                    previewArea.append( previewListUL );
                                    Azuki.bindSortable(previewListUL);
                                }

                                if(isMulti != 1) {
                                    previewArea.children('ul.preview-list').empty();
                                }
                                var previewElm = createElement(response)
                                    .append( $('<i>').addClass('fi-trash small') )
                                    .append( $('<input>').attr('type', 'hidden').attr('name', fieldName).val(response.idName) );

                                previewArea.children('ul').append(previewElm);
                            } else {
                                var message = 'アップロードに失敗';
                                if(typeof response.message != 'undefined') {
                                    message = response.message;
                                }
                                errorObj.push({file:response.name,msg:message,detail:''});
                            }

                            // 全画像の処理が終わっていたらアップロード処理完了表示に
                            completeNum++;
                            if( num <= completeNum ) {
                                Azuki.endUpload(errorObj, eventArea);
                            }
                        },
                        function(xhr, error){
                            var fileName = '--';
                            var erCode = xhr.status;
                            if(typeof xhr.requestData == 'object' && xhr.requestData.toString().slice(8, -1) == 'FormData') {
                                if(xhr.requestData.get('upload-file') != null) {
                                    fileName = xhr.requestData.get('upload-file').name;
                                }
                            }

                            // 全画像の処理が終わっていたらアップロード処理完了表示に
                            errorObj.push({file:fileName,msg:erCode+'：アップロードに失敗',detail:error});
                            completeNum++;
                            if( num <= completeNum ) {
                                Azuki.endUpload(errorObj, eventArea);
                            }
                        }
                    );
                }
                if( num <= completeNum ) {
                    Azuki.endUpload(errorObj, eventArea);
                }
            }

        };

        var textUpload = function ( textUpload, files ) {
            var iconClass = 'fi-page';
            fileUpload(
                {
                    uploadUrl: Azuki.params.uploadUrl,
                    through:   '',
                    eventArea: textUpload,
                },
                files,
                function (file) {
                    var msg = '';
                    var fileName = file.name;
                    var fileSize = file.size;
                    var acceptMime = 'text/plain,application/pdf,application/msword,application/msexcel';
                    
                    if(!checkMimeType(file.type, acceptMime)) {
                        msg = getMimeErrorMessage(file, acceptMime);
                        console.log(msg);
//                        alert(msg);
                        return false;
                    }

                    if( file.type === 'application/pdf' ) {
                        iconClass = 'fi-pdf';
                    } else if( file.type === 'application/msword' ) {
                        iconClass = 'fi-doc';
                    } else if( file.type === 'application/msexcel' ) {
                        iconClass = 'fi-doc';
//                    } else if( file.type === 'application/zip' ) {
                    }
                    return true;
                },
                function (response) {
                    return $('<li>').addClass('text')
                        .append(
                            $('<a>').attr('src', response.url)
                                .append($('<i>').addClass(iconClass))
                                .html(response.name)
                        );
                }
            );
        }

        var imageUpload = function ( imageUploadField, files ) {
            var isMulti = imageUploadField.data('ismulti');
            var liClass = isMulti == 1 ? '' : 'single-image';
            fileUpload(
                {
                    uploadUrl: Azuki.params.uploadUrl,
                    through:   '',
                    eventArea: imageUploadField,
                },
                files,
                function (file) {
                    var msg = '';
                    var fileName = file.name;
                    var fileSize = file.size;
                    var acceptMime = 'image/jpeg,image/jpg,image/gif,image/png,image/apng,'+
                        'image/bmp,image/x-icon,image/svg+xml,image/tiff,image/webp,'+
                        'image/x-icon,image/vnd.microsoft.icon,image/psd,'+
                        'image/vnd.adobe.photoshop,image/photoshop,image/x-photoshop';
                    
                    if(!checkMimeType(file.type, acceptMime)) {
                        msg = getMimeErrorMessage(file, acceptMime);
                        console.log(msg);
//                        alert(msg);
                        return false;
                    }
                    return true;
                },
                function (response) {
                    var addClass = liClass;
                    addClass = addClass + ' ' + response.type.replace('/', '_').replace('+', '_');
                    if(typeof response.thumbnailUrl != 'undefined') {
                        return $('<li>').addClass(addClass)
                            .append(
                                $('<a>').addClass('pop-img-tag').attr('href', 'javascript:void(0);').data('org-src', response.url)
                                .append(
                                    $('<img>').attr('alt', 'preview').attr('src', response.thumbnailUrl)
                                )
                            );
                    } else {
                        return $('<li>').addClass(addClass)
                            .append(
                                $('<a>').addClass('pop-img-tag').attr('href', 'javascript:void(0);')
                                .append(
                                    $('<img>').attr('alt', 'preview').attr('src', response.url)
                                )
                            );
                    }
                }
            );
        };

        var movieUpload = function ( movieUpload, files ) {
            var isMulti = movieUpload.data('ismulti');
            var liClass = isMulti == 1 ? '' : 'single-movie';
            fileUpload(
                {
                    uploadUrl: Azuki.params.uploadUrl,
                    through:   '',
                    eventArea: movieUpload,
                },
                files,
                function (file) {
                    var msg = '';
                    var fileName = file.name;
                    var fileSize = file.size;
                    var acceptMime = 'video/mp4,video/mpeg,video/webm,video/ogg,video/quicktime,video/x-msvideo';
                    
                    if(!checkMimeType(file.type, acceptMime)) {
                        msg = getMimeErrorMessage(file, acceptMime);
                        console.log(msg);
//                        alert(msg);
                        return false;
                    }
                    return true;
                },
                function (response) {
                    var addClass = liClass;
                    addClass = addClass + ' ' + response.type.replace('/', '_').replace('+', '_');
                    if(typeof response.thumbnailUrl != 'undefined') {
                        return $('<li>').addClass(addClass)
                            .append(
                                $('<a>').addClass('pop-movie-tag').attr('href', 'javascript:void(0);').data('org-src', response.url)
                                .append(
                                    $('<img>').attr('alt', 'preview').attr('src', response.thumbnailUrl)
                                )
                            );
                    } else {
                        return $('<li>').addClass(addClass)
                            .append(
                                $('<video>').attr('alt', 'preview').attr('src', response.url)
                                    .attr('preload', 'metadata').prop('controls', true).prop('playsinline', true)
                            );
                    }
                }
            );
        }

        var csvUpload = function ( csvUploadField, files ) {
            var uploadUrl    = Azuki.params.uploadUrl;
            var contentsType = csvUploadField.parents('div').find('input[name="type"]').val();
            var contentsKind = csvUploadField.parents('div').find('input[name="kind"]').val();

            var errorObj = [];
            if( typeof files != 'undefined' && files.length > 0 && Azuki.startUpload() ) {
                var formData = new FormData();
                formData.append('upload-file', files[0]);
                formData.append('type', contentsType);
                formData.append('kind', contentsKind);
    
                Azuki.execFileUpload(
                    formData,
                    uploadUrl,
                    function(response){
                        var reload = false;
                        var msg = "CSVアップロードしました。\n";
                        if( response.executable ) {
                            if( response.sync ) {
                                reload = true;
                                if(response.result) {
                                    msg = msg + '登録・更新処理に成功しました。';
                                } else {
                                    msg = msg + '登録・更新処理に失敗したデータがあります。';
                                }
                            } else {
                                msg = msg + 'バックグランドで処理中です。';
                            }
                        }
                        alert(msg);
                        Azuki.endUpload(errorObj, null, reload);
                    },
                    function(xhr, error){
                        alert('CSVアップロードに失敗しました。');
                        Azuki.endUpload(errorObj);
                    }
                );
            }
        };
        $('#csv-upload_btn').on('click', function(){
            csvUpload($(this), $('#csv-select_file').prop('files'));
        });
        $('#csv-download_btn').on('click', function(){
            // 検索フィールドの条件をフォームに設定
            var csvDlForm = $('#csv-download_form');
            var searchBox = $('#list_search_box_form');
      
            csvDlForm.find('input[name^="search"]').remove();
            searchBox.find('input[type="text"],input[type="date"],input[type="radio"]:checked,input[type="checkbox"]:checked,select').each(
                function(idx, elm){
                    var elmName = $(elm).attr('name');
                    var elmVal  = $(elm).val();
        
                    if(elmName.indexOf('search') == 0) {
                        csvDlForm.append(
                            $('<input>').attr('type', 'hidden').attr('name', elmName).val(elmVal)
                        );
                    }
                }
            );
            
            csvDlForm.submit();
        });

        $('input[type="file"].azuki-upload-field').on('change', function(){
            // ajaxを使ったアップロード処理を行う
            var files       = $(this).prop('files');
            var imageUploadField = $(this).parents('.image-upload-field');
            var movieUploadField = $(this).parents('.movie-upload-field');
            var textUploadField  = $(this).parents('.text-upload-field');
            
            // 画像のアップロード処理
            if( imageUploadField.length ) {
                imageUpload(imageUploadField, files);
            }
            // 動画のアップロード処理
            if( movieUploadField.length ) {
                movieUpload(movieUploadField, files);
            }
            // ファイルのアップロード処理
            if( textUploadField.length ) {
                textUpload(textUploadField, files);
            }
            
            // test CSVファイルのアップロードのCSVファイル選択処理。アップロードは行わない。
            if( $(this).attr('id') == 'csv-select_file' ) {
                var csvFileName = files.length > 0 ? files[0].name : '選択して下さい';
                $('#upload-csv-name').html(csvFileName);
                return;
            }
            
            
            $(this).val('');
        });
        
        var deleteAllUploadedFile = function(targetId, msg, check = true) {
            // アップロードされているファイルをすべて削除する
            // 確認コンフォームの表示もする
            if( check && !confirm(msg) ) {
                return;
            }
            
            $('div#'+targetId).empty();
            if( $('div#'+targetId+' .no-file-view').length <= 0 ) {
                $('div#'+targetId).append( $('<div>').addClass('no-file-view') );
                Azuki.initNotFileField();
            }
        }
        $('.image-upload-field .btn-all-image-delete').on('click', function(){
            deleteAllUploadedFile($(this).data('target'), '画像をすべて削除します。');
        });
        $('.image-upload-field .btn-single-image-delete').on('click', function(){
            deleteAllUploadedFile($(this).data('target'), '画像をすべて削除します。', false);
        });
        $('.movie-upload-field .btn-all-movie-delete').on('click', function(){
            deleteAllUploadedFile($(this).data('target'), '動画をすべて削除します。');
        });
        $('.movie-upload-field .btn-single-movie-delete').on('click', function(){
            deleteAllUploadedFile($(this).data('target'), '動画をすべて削除します。', false);
        });

        // ドラッグ＆ドロップでの画像登録処理
        $("div.draggable")
        .on('dragenter', function(e){
            e.stopPropagation();
            e.preventDefault();
            return false;
        })
        .on('dragleave', function(e){
            e.stopPropagation();
            e.preventDefault();
            $(this).css("background-color","#FFFFFF");
            return false;
        })
        .on('dragover', function(e){
            e.stopPropagation();
            e.preventDefault();
            $(this).css("background-color","#CCFFFF");
            return false;
        })
        .on('drop', function(e){
            e.stopPropagation();
            e.preventDefault();
            $(this).css("background-color","#FFFFFF");

            var files = e.originalEvent.dataTransfer.files;
            var imageUploadField = $(this).parents('.image-upload-field');
            var movieUploadField = $(this).parents('.movie-upload-field');

            // 画像のアップロード処理
            if( imageUploadField.length ) {
                imageUpload(imageUploadField, files);
            }
            if( movieUploadField.length ) {
                movieUpload(movieUploadField, files);
            }

            return false;
        });

        // 登録された画像のソート処理
        var sortableImageList = $(".image-upload-field .preview ul.preview-list");
        var sortableMovieList = $(".movie-upload-field .preview ul.preview-list");
        Azuki.bindSortable(sortableImageList);
        Azuki.bindSortable(sortableMovieList);
        
        $('div.cell ul.preview-list').on('click', 'li a.pop-img-tag', function(e){
            var url = $(this).data('orgSrc');
            previewModalOpen('image', url);
        });
        $('div.preview').on('click', 'ul.preview-list li a.pop-img-tag', function(e){
            var url = $(this).data('orgSrc');
            previewModalOpen('image', url);
        });
        
        $('div.cell ul.preview-list').on('click', 'li a.pop-movie-tag', function(e){
            var url = $(this).data('orgSrc');
            previewModalOpen('movie', url);
        });
        $('div.preview').on('click', 'ul.preview-list li a.pop-movie-tag', function(e){
            var url = $(this).data('orgSrc');
            previewModalOpen('movie', url);
        });
        
        var previewModalOpen = function(type, url) {
            if($('div#media-preview-modal').length <= 0) {
                return;
            }
            $('img.preview-modal, video.preview-modal').each(function(idx, elm){
                $(elm).attr('src', '');
                $(elm).hide();
            });
            var target = $('#preview-modal-'+type);
            target.attr('src', url);
            target.show();
            $('div#media-preview-modal').foundation('open');
        };

    },
    execFileUpload: function(formData, uploadUrl, successCallback = null, failCallback = null, allways = null) {
        Azuki.execAjax({
                method: 'POST',
                url : uploadUrl,
                contentType: false,
                processData: false,
                data:formData,
                dataType:"json",
                beforeSend: function(jqXHR, settings) {
                    jqXHR.requestData = settings.data;
                },
            },
            successCallback,
            failCallback,
            allways
        );
    },
    execAjax: function(ajaxOption, success, fail, allways) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax(ajaxOption)
        .then(function(data, status, jqXHR) {
            success(data, status, jqXHR);
        }).catch(function(jqXHR, data, status){
            fail(jqXHR, data, status);
        }).then(function(){
            if(typeof allways == 'function') {
                allways();
            }
        });
    },
    bindSortable: function( target ) {
        if( target.length <= 0 ) {
            return ;
        }
        var dropOut = false;
        target.sortable({
            revert: false,
            cursor: "move",
            deactivate:function(e, obj) {
                if(dropOut) {
                    if( confirm("今範囲外まで移動したファイルを削除しますか？") ) {
                        $(obj.item[0]).remove(); // 削除方法は要検討
                    }
                }
            },
            start: function() {
                $(this).addClass('sortActive');
            },
            stop: function() {
                $(this).removeClass('sortActive');
            },
            out: function() {
//                dropOut = true;
                $(this).removeClass('sortActive');
            },
            over: function() {
                dropOut = false;
                $(this).addClass('sortActive');
            }
        }).on('click', '.fi-trash', function(){
            var previewArea = $(this).parents('.preview');
            var parantUL    = $(this).parents('ul');

            // 各ファイルの削除処理を行う
            $(this).parent('li').remove();
            // 全部削除された際にドラッグ＆ドラッグ状態に戻す処理
            var lists = parantUL.children('li');

            if( lists.length <= 0 ) {
                previewArea.empty();
                if( previewArea.children('.no-file-view').length <= 0 ) {
                    previewArea.append( $('<div>').addClass('no-file-view') );
                    Azuki.initNotFileField();
                }
            }
        });
    },
    initNoImageField: function( initFunc ) {
        var target  = $('.image-upload-field .no-file-view');
        var message = '画像をドラッグ＆ドロップで登録できます';
        initFunc( target, message );
    },
    initNoMovieField: function( initFunc ) {
        var target  = $('.movie-upload-field .no-file-view');
        var message = '動画をドラッグ＆ドロップで登録できます';
        initFunc( target, message );
    },
    initNotFileField: function() {
        var notFileField = function( target, message ) {
            if( target.length ) {
                target.each(function(idx, elm) {
                    if( $(elm).children('div').length <= 0 ) {
                        $(elm).append( $('<div>').append($('<p>').html(message)) );
                    }
                });
            }
        };
        this.initNoImageField(notFileField);
        this.initNoMovieField(notFileField);
    },
    // ロール権限設定フィールドの追加削除処理のイベント登録を行う
    initRoleAuthority() {
        // 対象のフィールドが存在していない場合は何もしない
        if( $('#role_authority_area').length <= 0 ) {
            return;
        }
        // フィールド追加時の初期番号を設定する
        Azuki.params.publicAddKeys = function() {
            var number = 0;
            $('#role_authority_area').children('div.auth-group').each(function(){
                var num = $(this).data('key');
                if( num > number ) {
                    number = num;
                }
            });
            return number + 1;
        }();
        
        // 追加・削除イベントを登録する
        $('#role_authority_area').on('click', 'button.delete_authority', function(){
            $(this).parents('.auth-group').remove();
        });
        $('#add_role_authority').click(function(){
            var elm = $('#role_authority_template').children().clone();
            var key = parseInt(Azuki.params.publicAddKeys);
            elm.find('input,select').each(function(idx, element){
                $(this).attr(
                    'name',
                    $(this).attr('name').replace(/^__/, '').replace('[key]', '['+key+']')
                );
            });
            elm.data('key', key);
            $('#role_authority_area').append(elm);
            
            Azuki.params.publicAddKeys = ++key;
        });
    },
    initDeleteDataFromDatabase : function() {
        $('div.delete-from-database').on('click', 'button[name="btn-delete"]', function() {
            var year = $(this).parents('div.delete-from-database').find('select[name="delete-target"]').val();
            if(year == '') {
                alert('年数が選択されていません。');
                return ;
            }
            if(confirm(year+'年以上前のデータをDBから完全に削除します。')) {
                var url = $(this).data('url');
                var formData = new FormData();
                formData.append('year', year);
                $.LoadingOverlay("show");
                Azuki.execAjax(
                    {
                        method      : 'POST',
                        url         : url,
                        contentType : false,
                        processData : false,
                        data        : formData,
                    },
                    (data, status, jqXHR) => {
                        if(data.result) {
                            let message = data.message;
                            alert(message);
                            location.reload();
                        } else {
                            let message = data.message;
                            alert(message);
                        }
                    },
                    (jqXHR, data, status) => {
                        alert('削除に失敗');
                    },
                    () => {
                        $.LoadingOverlay("hide");
                    }
                );
            }
        });
    },
    initSwitchSystem : function() {
        var url = $('#systemSwitchUrl').val();
        $('input.switch-input').on('change', function(e) {
            var formData = new FormData();
            formData.append('kind', $(this).data('kind'));
            if($(this).prop('checked')) {
                // onにする
                formData.append('status', 'on');
            } else {
                // offにする
                formData.append('status', 'off');
            }
            var option = {
                method      : 'POST',
                url         : url,
                contentType : false,
                processData : false,
                data        : formData,
            };

            $.LoadingOverlay("show");
            Azuki.execAjax(
                option,
                (data, status, jqXHR) => {
                    if(!data.result) {
                        // スイッチを元に戻す
                        $(this).prop('checked') ? $(this).prop('checked', false) : $(this).prop('checked', true);
                        let message = data.message;
                        alert(message);
                    }
                },
                (jqXHR, data, status) => {
                    // スイッチを元に戻す
                    $(this).prop('checked') ? $(this).prop('checked', false) : $(this).prop('checked', true);
                    alert('切替に失敗');
                },
                () => {
                    $.LoadingOverlay("hide");
                }
           );

        });
    },
    initCronButton : function() {
        $('button.regist-cron,button.remove-cron').on('click', function(){
            if($(this).hasClass('remove-cron') && !confirm('cron登録を削除します')) {
                return ;
            }
        
            var option = {
                method      : 'POST',
                url         : $(this).data('url'),
                contentType : false,
                processData : false,
            };

            $.LoadingOverlay("show");
            Azuki.execAjax(
                option,
                (data, status, jqXHR) => {
                    if(!data.result) {
                        let message = data.message;
                        alert(message);
                    } else {
                        location.reload();
                    }
                },
                (jqXHR, data, status) => {
                    let message = data.message;
                    alert(message);
                },
                () => {
                    $.LoadingOverlay("hide");
                }
           );
        });
    },
};

Azuki.params.uploadUrl = $('link[rel="index"]').attr('href') + 'file-upload';
window.Azuki = Azuki;
export {Azuki};
