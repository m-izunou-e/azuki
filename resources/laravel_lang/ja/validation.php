<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute に同意していただく必要があります。',
    'active_url'           => ':attribute は有効なURLではありません。',
    'after'                => ':attribute は :date より後にしてください。',
    'after_or_equal'       => ':attribute は :date 以降にしてください。',
    'alpha'                => ':attribute は英字のみとしてください。',
    'alpha_dash'           => ':attribute 英数字か「-」「_」のみとしてください。',
    'alpha_num'            => ':attribute は数字のみとしてください。',
    'array'                => ':attribute は配列にしてください。',
    'before'               => ':attribute は :date よりも前にしてください。',
    'before_or_equal'      => ':attribute は :date 以前にしてください。',
    'between'              => [
        'numeric' => ':attribute は :min から :max にしてください。',
        'file'    => ':attribute は :min キロバイトから :max キロバイトにしてください。',
        'string'  => ':attribute は :min 文字から :max 文字にしてください。',
        'array'   => ':attribute の要素数はは :min から :max にしてください。',
    ],
    'boolean'              => ':attribute は真偽値にしてください。',
    'confirmed'            => ':attribute が確認値と違っています。',
    'date'                 => ':attribute は不正な日付です。',
    'date_format'          => ':attribute が :format になっていません。',
    'different'            => ':attribute が :other と異なっています。',
    'digits'               => ':attribute は :digits 桁の数字にしてください。',
    'digits_between'       => ':attribute は :min 桁から :max 桁の整数にしてください。',
    'dimensions'           => ':attribute は許可されていないファイルです。',
    'distinct'             => ':attribute は重複しています。',
    'email'                => ':attribute は不正なメールアドレスです。',
    'exists'               => ':attribute が不正です。',
    'file'                 => ':attribute のアップロードに失敗しました。',
    'filled'               => ':attribute は必須です。',
    'image'                => ':attribute は（jpg,png,bmp,gif,svg）にしてください。',
    'in'                   => ':attribute が不正です。',
    'in_array'             => ':attribute は :other に存在する値にしてください。',
    'integer'              => ':attribute は整数値にしてください。',
    'ip'                   => ':attribute はIPアドレスとして正しくありません。',
    'ipv4'                 => ':attribute はIPv4アドレスとして正しくありません。',
    'ipv6'                 => ':attribute はIPv6アドレスとして正しくありません。',
    'json'                 => ':attribute はJSON形式として正しくありません。',
    'max'                  => [
        'numeric' => ':attribute は :max 以下にしてください。',
        'file'    => ':attribute は :max キロバイト以下にしてください。',
        'string'  => ':attribute は :max 文字以下にしてください。',
        'array'   => ':attribute は :max 個以下にしてください。',
    ],
    'mimes'                => ':attribute は :values のいずれかのMIMEタイプにしてください。',
    'mimetypes'            => ':attribute は :values のいずれかのMIMEタイプにしてください。',
    'min'                  => [
        'numeric' => ':attribute は :min 以上にしてください。',
        'file'    => ':attribute は :min キロバイト以上にしてください。',
        'string'  => ':attribute は :min 文字以上にしてください。',
        'array'   => ':attribute は :min 個以上にしてください。',
    ],
    'not_in'               => ':attribute は不正です。',
    'numeric'              => ':attribute は数値にしてください。',
    'present'              => ':attribute は必須です。',
    'regex'                => ':attribute は不正な形式です。',
    'required'             => ':attribute は必須です。',
    'required_if'          => ':other の値が :value であるとき、:attribute は必須です。',
    'required_unless'      => ':other の値が :values でないとき、:attribute は必須です。',
    'required_with'        => ':values が存在しているとき、:attribute は必須です。',
    'required_with_all'    => ':values が全て存在しているとき、:attribute は必須です。',
    'required_without'     => ':values がどれか１つでも存在していないとき、:attribute は必須です。',
    'required_without_all' => ':values が全て存在していないとき、:attribute は必須です。',
    'same'                 => ':attribute は :other と同じ値にしてください。',
    'size'                 => [
        'numeric' => ':attribute は :size にしてください。。',
        'file'    => ':attribute は :size キロバイトにしてください。',
        'string'  => ':attribute は :size 文字にしてください。',
        'array'   => ':attribute は :size 個にしてください。',
    ],
    'string'               => ':attribute は文字列にしてください。',
    'timezone'             => ':attribute はタイムゾーンとして不正です。',
    'unique'               => ':attribute がすでに登録されています。',
    'uploaded'             => ':attribute アップロードに失敗しました。',
    'url'                  => ':attribute は有効なURLではありません。',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    'alpha_num'        => ':attribute は半角英数字のみとしてください。',
    'alpha_num_symbol' => ':attribute は半角英数記号のみとしてください。',
    'kana'             => ':attribute はカタカナにしてください。',
    'kana_space'       => ':attribute はカタカナ,スペースにしてください。',
    'zip_code'         => ':attribute が無効な形式です。ハイフンなし、半角数字７桁にしてください。',
    'tel'              => ':attribute が無効な形式です。ハイフンあり、半角数字にしてください',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'name'     => 'お名前',
        'login_id' => 'ログインID',
        'email'    => 'メールアドレス',
        'password' => 'パスワード',
    ],

];
