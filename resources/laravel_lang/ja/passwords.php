<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'パスワードが条件を満たしていないか、確認用と一致しておりません。',
    'reset'    => 'パスワードはリセットされました。',
    'sent'     => 'パスワードリセット用URLを記載したメールを送信しました。',
    'token'    => 'パスワードリセットトークンが正しくありません。パスワードリセット用URLの有効期限が切れたか、途中で切れたりしていませんでしょうか。',
    'user'     => "ご入力のメールアドレスは登録されておりません。",

];
