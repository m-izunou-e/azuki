<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => '入力された内容が登録内容と一致しておりません。',
    'throttle' => '一定回数以上ログインに失敗したため、一時的にアカウントがロックされました。 :seconds秒後に再度お試しください。',

];
