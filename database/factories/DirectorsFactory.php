<?php

use Faker\Generator as Faker;

$factory->define(\Azuki\App\Models\Directors::class, function (Faker $faker) {
    $email    = $faker->unique()->safeEmail;
    $password = $faker->password;
    $role     = $faker->numberBetween(1, 2);
    $enable   = $faker->numberBetween(1, 2);
    return [
        'name'     => $faker->name,
        'email'    => $email,
//        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'password' => bcrypt($password),
        'role'     => $role,
        'enable'   => $enable,
        'remember_token' => str_random(10),
    ];
});

$factory->state(\Azuki\App\Models\Directors::class, 'roleIsAdmin', [
    'role' => '1',
]);

$factory->state(\Azuki\App\Models\Directors::class, 'roleIsCommon', [
    'role' => '2',
]);

$factory->state(\Azuki\App\Models\Directors::class, 'userEnable', [
    'enable' => '1',
]);

$factory->state(\Azuki\App\Models\Directors::class, 'userDisable', [
    'enable' => '2',
]);
