<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemSwitchTable extends Migration
{
    protected $table = 'azuki_system_switch';

    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';

            $table->bigIncrements('id')->unsigned()->comment('一意の識別子');
            $table->string('title','128')->comment('表示名');
            $table->string('kind','128')->unique()->comment('種類名');
            $table->smallInteger('status')->unsigned()->comment('ステータス：１：有効、２：無効');
            $table->timestampsTz();

        });

        DB::statement("ALTER TABLE `".$this->table."` COMMENT 'システムの切替項目・状況を管理する'");
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
