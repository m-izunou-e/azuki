<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessLogsTable extends Migration
{
    protected $table = 'azuki_access_logs';

    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';

            $table->increments('id')->unsigned()->comment('一意の識別子');
            $table->string('url')->comment('アクセスされたURL');
            $table->integer('user_id')->unsigned()->nullable()->index()->comment('アクセスしたユーザーのID');
            $table->integer('user_kind')->unsigned()->nullable()->index()->comment('アクセスしたユーザーの種類。１：director、２：manager、３：user');
            $table->text('user_info')->nullable()->comment('アクセスしたユーザーの情報');
            $table->text('access_info')->nullable()->comment('アクセス情報');
            $table->timestampsTz();
        });

        DB::statement("ALTER TABLE `".$this->table."` COMMENT 'データベースのデータアクセスに関するログを記録するテーブル'");
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
