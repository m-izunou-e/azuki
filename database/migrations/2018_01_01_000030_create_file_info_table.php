<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileInfoTable extends Migration
{
    protected $table = 'azuki_file_info';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';

            $table->increments('id')->unsigned()->comment('一意の識別子');
            $table->string('id_name', 255)->index()->unique()->comment('識別用の一意なファイル名');
            $table->string('name', 255)->comment('ファイル名');
            $table->string('path', 255)->comment('ファイルのパス。database保存の場合はとりあえず空');
            $table->string('mime_type', 64)->comment('mime-type');
            $table->string('type', 32)->comment('ファイルのタイプ。詳細はupload_file.phpで定義');
            $table->softDeletesTz();
            $table->timestampsTz();
            
        });
        DB::statement("ALTER TABLE `".$this->table."` COMMENT 'ファイル情報管理テーブル'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
