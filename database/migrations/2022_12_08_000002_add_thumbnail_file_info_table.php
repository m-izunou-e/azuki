<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddThumbnailFileInfoTable extends Migration
{
    protected $table = 'azuki_file_info';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->table, function (Blueprint $table) {
            $table->dropUnique('azuki_file_info_id_name_unique');
            $table->string('ext', 5)->after('id_name')->comment('拡張子');
            $table->smallInteger('category')
                ->unsigned()
                ->default(UPLOAD_DATA_CATEGORY_ORIGINAL)
                ->after('ext')->comment('データの分類');
            
            $table->unique(['id_name', 'category']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table, function (Blueprint $table) {
            $table->dropUnique('azuki_file_info_id_name_category_unique');
            $table->dropColumn('category');
            $table->dropColumn('ext');
            $table->unique('id_name');
        });
    }
}
