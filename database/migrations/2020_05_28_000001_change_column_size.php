<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnSize extends Migration
{
    private $foreignKeys = [
        'azuki_file_data'               => 'file_info',
        'azuki_execute_target_queue'    => 'file_info',
        'azuki_system_role_authorities' => 'system_roles',
    ];
    
    private $targetTables = [
        'azuki_directors',
        'azuki_managers',
        'azuki_users',
        'azuki_mail_logs',
        'azuki_file_info',
        'azuki_file_data',
        'azuki_execute_target_queue',
        'azuki_system_roles',
        'azuki_system_role_authorities',
        'azuki_organizations',
        'azuki_belongs_to_organizations',
        'azuki_operation_logs',
        'azuki_login_logs',
        'azuki_access_logs',
        'azuki_system_logs',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        $this->dropForeignKeys();
        foreach($this->targetTables as $tname) {
            Schema::table($tname, function (Blueprint $table) use($tname){
                $table->bigIncrements('id')->change();
                
                if(in_array( $tname, ['azuki_file_data', 'azuki_execute_target_queue'] )) {
                    $table->bigInteger('file_info_id')->unsigned()->change();
                }
                if(in_array( $tname, ['azuki_system_role_authorities'] )) {
                    $table->bigInteger('system_roles_id')->unsigned()->change();
                }
                if(in_array( $tname, ['azuki_belongs_to_organizations'] )) {
                    $table->bigInteger('target_data_id')->unsigned()->change();
                }
                if(in_array( $tname, ['azuki_operation_logs'] )) {
                    $table->bigInteger('target_id')->unsigned()->change();
                    $table->longText('dirty_data')->change();
                }
                if(in_array( $tname, ['azuki_operation_logs', 'azuki_login_logs', 'azuki_access_logs'] )) {
                    $table->bigInteger('user_id')->unsigned()->change();
                }
            });
        }
        $this->revoverForeignKeys();

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        $this->dropForeignKeys();
        foreach($this->targetTables as $tname) {
            Schema::table($tname, function (Blueprint $table) use($tname){
                $table->increments('id')->change();
                
                if(in_array( $tname, ['azuki_file_data', 'azuki_execute_target_queue'] )) {
                    $table->integer('file_info_id')->unsigned()->change();
                }
                if(in_array( $tname, ['azuki_system_role_authorities'] )) {
                    $table->integer('system_roles_id')->unsigned()->change();
                }
                if(in_array( $tname, ['azuki_belongs_to_organizations'] )) {
                    $table->integer('target_data_id')->unsigned()->change();
                }
                if(in_array( $tname, ['azuki_operation_logs'] )) {
                    $table->integer('target_id')->unsigned()->change();
                    $table->text('dirty_data')->change();
                }
                if(in_array( $tname, ['azuki_operation_logs', 'azuki_login_logs', 'azuki_access_logs'] )) {
                    $table->integer('user_id')->unsigned()->change();
                }
            });
        }
        $this->revoverForeignKeys();
        
        Schema::enableForeignKeyConstraints();
    }
    
    private function dropForeignKeys()
    {
        foreach($this->foreignKeys as $tname => $fKeys) {
            Schema::table($tname, function (Blueprint $table) use($tname, $fKeys){
                $table->dropForeign($tname.'_'.$fKeys.'_id_foreign');
            });
        }
    }
    
    private function revoverForeignKeys()
    {
        foreach($this->foreignKeys as $tname => $fKeys) {
            Schema::table($tname, function (Blueprint $table) use($tname, $fKeys){
                if($tname == 'azuki_system_role_authorities') {
                    $table->foreign($fKeys.'_id')
                        ->references('id')->on('azuki_'.$fKeys)
                        ->onDelete('cascade');
                } else {
                    $table->foreign($fKeys.'_id')
                        ->references('id')->on('azuki_'.$fKeys);
                }
            });
        }
    }
}
