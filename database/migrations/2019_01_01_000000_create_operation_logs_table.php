<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperationLogsTable extends Migration
{
    protected $table = 'azuki_operation_logs';

    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';

            $table->increments('id')->unsigned()->comment('一意の識別子');
            $table->string('table')->comment('対象のテーブル名');
            $table->integer('target_id')->unsigned()->nullable()->comment('操作対象データのID');
            $table->integer('user_id')->unsigned()->nullable()->index()->comment('操作したユーザーのID');
            $table->integer('user_kind')->unsigned()->nullable()->index()->comment('操作したユーザーの種類。１：director、２：manager、３：user');
            $table->text('user_info')->nullable()->comment('操作したユーザーの情報');
            $table->integer('op_mode')->unsigned()->index()->comment('操作の種類。１：新規作成。２：更新。３：削除');
            $table->text('original_data')->nullable()->comment('操作前のデータ内容');
            $table->text('dirty_data')->nullable()->comment('処理したデータ内容');
            $table->timestampsTz();
        });

        DB::statement("ALTER TABLE `".$this->table."` COMMENT 'データベースのデータアクセスに関するログを記録するテーブル'");
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
