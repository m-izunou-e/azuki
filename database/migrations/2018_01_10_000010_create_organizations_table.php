<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationsTable extends Migration
{
    protected $table = 'azuki_organizations';

    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';

            $table->increments('id')->unsigned()->comment('一意の識別子');
            $table->string('name', 255)->comment('組織・グループ・企業・店舗名');
            $table->string('name_id', 255)->comment('識別名称');
            $table->string('original_domain', 255)->nullable()->comment('個別ドメイン');
            $table->integer('value')->unsigned()->index()->comment('データ値');
            $table->integer('order')->unsigned()->comment('並び順');
            $table->softDeletesTz();
            $table->timestampsTz();
        });

        DB::statement("ALTER TABLE `".$this->table."` COMMENT '組織階層のグルーピングを管理するテーブル'");
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
