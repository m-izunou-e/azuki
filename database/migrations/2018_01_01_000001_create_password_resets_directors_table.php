<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePasswordResetsDirectorsTable extends Migration
{
    protected $table = 'azuki_password_resets_directors';

    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';

            $table->string('email','255')->index()->comment('パスワードリセットするユーザーのメールアドレス');
            $table->string('token','255')->comment('リセット用のトークン');
            $table->timestamp('created_at')->nullable()->comment('レコード生成日時');

            $table->foreign('email')
                ->references('email')->on('azuki_directors')
                ->onDelete('cascade');
        });

        DB::statement("ALTER TABLE `".$this->table."` COMMENT 'システム管理者ユーザー用のパスワードリセットテーブル'");
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
