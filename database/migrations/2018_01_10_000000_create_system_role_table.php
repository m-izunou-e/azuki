<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemRoleTable extends Migration
{
    protected $table = 'azuki_system_roles';

    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';

            $table->increments('id')->unsigned()->comment('一意の識別子');
            $table->string('name','36')->comment('マスターデータ名');
            $table->integer('value')->unsigned()->comment('データ値');
            $table->integer('order')->unsigned()->comment('並び順');
            $table->smallinteger('deletable')->unsigned()->default(2)->comment('削除可否：１削除不可、２削除可');
            $table->softDeletesTz();
            $table->timestampsTz();
        });

        DB::statement("ALTER TABLE `".$this->table."` COMMENT 'システム管理用のユーザー役割テーブル'");
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
