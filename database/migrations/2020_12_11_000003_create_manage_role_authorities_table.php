<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManageRoleAuthoritiesTable extends Migration
{
    protected $table = 'azuki_manage_role_authorities';

    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';

            $table->increments('id')->unsigned()->comment('一意の識別子');
            $table->integer('manage_roles_id')->unsigned()->comment('紐づくマネージャーロールのID');
            $table->string('url','255')->comment('対象URL。「*」を設定するとデフォルト値の設定として扱う');
            $table->integer('authority')->unsigned()->comment('認可：１許可。２拒否');
            $table->softDeletesTz();
            $table->timestampsTz();

            $table->foreign('manage_roles_id')
                ->references('id')->on('azuki_manage_roles')
                ->onDelete('cascade');
        });

        DB::statement("ALTER TABLE `".$this->table."` COMMENT 'サイト管理用のユーザー役割に対する認可テーブル'");
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
