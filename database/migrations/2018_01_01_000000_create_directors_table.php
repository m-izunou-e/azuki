<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDirectorsTable extends Migration
{
    protected $table = 'azuki_directors';

    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';

            $table->increments('id')->unsigned()->comment('一意の識別子');
            $table->string('name','36')->comment('名前');
            $table->string('email','255')->index()->comment('メールアドレス、ログイン一意の識別子');
            $table->string('password','200')->comment('パスワード');
            $table->smallInteger('role')->unsigned()->default(1)->comment('役割、権限');
            $table->smallInteger('enable')->unsigned()->index()->default(1)->comment('有効：1、無効：2');
            $table->softDeletesTz();
            $table->timestampsTz();
            $table->rememberToken();

        });


        DB::statement("ALTER TABLE `".$this->table."` COMMENT 'システム管理用のユーザーテーブル'");
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
