<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNumberFileDataTable extends Migration
{
    protected $table = 'azuki_file_data';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->table, function (Blueprint $table) {
            $table->smallInteger('number')->unsigned()->default(1)->after('data')->comment('データ順');
            
            $table->unique(['file_info_id', 'number']);
            $table->dropIndex('azuki_file_data_file_info_id_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table, function (Blueprint $table) {
            $table->index('file_info_id');
            $table->dropUnique('azuki_file_data_file_info_id_number_unique');
            $table->dropColumn('number');
        });
    }
}
