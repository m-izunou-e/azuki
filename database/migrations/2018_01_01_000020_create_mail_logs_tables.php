<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailLogsTables extends Migration
{
    protected $table = 'azuki_mail_logs';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';

            $table->increments('id')->unsigned()->comment('一意の識別子');
            $table->string('messageId', 512)->index()->comment('メールのメッセージID');
            $table->text('headers')->comment('ヘッダ');
            $table->string('date', 64)->index()->comment('作成された日時(キューに入れて配信した場合にDBに記録時点と相違があるかもしれないのでフィールドを設けておく)');
            $table->string('subject', 255)->index()->comment('件名');
            $table->string('from', 255)->index()->comment('送信元アドレス。');
            $table->string('return_path', 255)->index()->comment('returnpathのアドレス');
            $table->string('reply_to', 255)->comment('replytoのアドレス。');
            $table->string('to', 768)->index()->comment('宛先。カンマ区切り');
            $table->text('cc')->comment('cc。カンマ区切り');
            $table->text('bcc')->comment('bcc。カンマ区切り');
            $table->text('sender')->comment('センダー。カンマ区切り');
            $table->mediumtext('body')->comment('本文');
            $table->integer('result')->unsigned()->default('0')->comment('0:結果ログ未記録。1:ペンディング。16:成功。17:スプール。256:あやふや。4096:失敗。REF:Swift_Events_SendEventに16進にて定数定義あり');
            $table->text('failures')->nullable()->comment('失敗したメールアドレス');
            $table->softDeletesTz();
            $table->timestampsTz();
        });

        DB::statement("ALTER TABLE `".$this->table."` COMMENT '送信メールを記録するテーブル'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
