<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileDataTable extends Migration
{
    protected $table = 'azuki_file_data';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';

            $table->increments('id')->unsigned()->comment('一意の識別子');
            $table->integer('file_info_id')->unsigned()->index()->comment('紐づくfile_infoのID');
            $table->binary('data')->comment('ファイルのデータ。base64エンコードしたもの');
            $table->timestampsTz();

            $table->foreign('file_info_id')
                ->references('id')->on('azuki_file_info');
            
        });
        DB::statement("ALTER TABLE `".$this->table."` change column data data mediumblob");
        DB::statement("ALTER TABLE `".$this->table."` COMMENT 'ファイルデータ管理テーブル'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
