<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemLogsTable extends Migration
{
    protected $table = 'azuki_system_logs';

    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';

            $table->increments('id')->unsigned()->comment('一意の識別子');
            $table->smallInteger('kind')->unsigned()->index()->comment('ログの種類');
            $table->smallInteger('status')->unsigned()->index()->comment('ログの状態');
            $table->text('message')->comment('ログの内容');
            $table->timestampTz('check_at')->nullable()->comment('確認した日時');
            $table->timestampTz('restore_at')->nullable()->comment('復旧した日時');
            $table->timestampsTz();
        });

        DB::statement("ALTER TABLE `".$this->table."` COMMENT 'データベースのデータアクセスに関するログを記録するテーブル'");
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
