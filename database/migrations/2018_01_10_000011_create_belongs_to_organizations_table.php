<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBelongsToOrganizationsTable extends Migration
{
    protected $table = 'azuki_belongs_to_organizations';

    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';

            $table->increments('id')->unsigned()->comment('一意の識別子');
            $table->string('target_table', 64)->index()->comment('対象テーブル名');
            $table->integer('belong')->unsigned()->nullable()->comment('所属ID');
            $table->integer('target_data_id')->index()->unsigned()->comment('対象データID');
            $table->softDeletesTz();
            $table->timestampsTz();
            
            $table->foreign('belong')
                ->references('value')->on('azuki_organizations')
                ->onDelete('cascade');
        });

        DB::statement("ALTER TABLE `".$this->table."` COMMENT '各関連テーブルのデータとグルーピングの関係を管理するテーブル'");
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
