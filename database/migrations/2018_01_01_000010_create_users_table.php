<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('azuki_users', function (Blueprint $table) {
            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';

            $table->increments('id')->unsigned()->comment('一意の識別子');
            $table->string('name', 36)->index()->comment('ユーザー名。最大32文字');
            $table->string('login_id','255')->index()/*->unique()*/->comment('ログイン一意の識別子');
            $table->string('email', 255)->index()/*->unique()*/->comment('メールアドレス');
            $table->string('password', 200)->comment('パスワード。英数記号大文字小文字混在６文字以上32文字以内。bcryptでハッシュ化');
            $table->smallInteger('role')->unsigned()->index()->default(0)->comment('権限番号');
            $table->smallInteger('enable')->unsigned()->index()->default(1)->comment('有効・無効。有効：1．無効：2');
            $table->rememberToken();
            $table->softDeletesTz();
            $table->timestampsTz();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('azuki_users');
    }
}
