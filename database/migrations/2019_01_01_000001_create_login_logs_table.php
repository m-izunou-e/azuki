<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoginLogsTable extends Migration
{
    protected $table = 'azuki_login_logs';

    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';

            $table->increments('id')->unsigned()->comment('一意の識別子');
            $table->integer('user_id')->unsigned()->index()->comment('ログインしたユーザーのID');
            $table->integer('user_kind')->unsigned()->index()->comment('ログインしたユーザーの種類。１：director、２：manager、３：user');
            $table->text('user_info')->nullable()->comment('ログインしたユーザーの情報');
            $table->timestampsTz();
        });

        DB::statement("ALTER TABLE `".$this->table."` COMMENT 'ログインに関するログを記録するテーブル'");
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
