<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialIdentityTable extends Migration
{
    protected $table = 'azuki_social_identity';

    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';

            $table->increments('id')->unsigned()->comment('一意の識別子');
            $table->bigInteger('users_id')->index()->unsigned()->comment('紐づくuserテーブルのID');
            $table->string('social_id', 128)->comment('プロバイダー側でのID');
            $table->string('provider', 64)->comment('プロバイダー名');
            $table->smallInteger('provider_kind')->unsinged()->comment('プロバイダー種別番号');
            $table->softDeletesTz();
            $table->timestampsTz();
            
            $table->index(['social_id','provider_kind']);
            $table->unique(['users_id','provider_kind']);
            
            $table->foreign('users_id')
                ->references('id')->on('azuki_users')
                ->onDelete('cascade');
        });

        DB::statement("ALTER TABLE `".$this->table."` COMMENT 'ユーザーテーブルに紐づくSNSログインプロバイダ情報を管理するテーブル'");
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
