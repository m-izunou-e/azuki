<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExecuteTargetQueueTable extends Migration
{
    protected $table = 'azuki_execute_target_queue';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';

            $table->increments('id')->unsigned()->comment('一意の識別子');
            $table->integer('belong')->unsigned()->index()->nullable()->default(null)->comment('所属。デフォルトNULL');
            $table->integer('file_info_id')->unsigned()->nullable()->index()->comment('紐づくfile_infoのID');
            $table->string('kind', 32)->index()->comment('実行の設定を決める識別子');
            $table->integer('status')->index()->unsigned()->comment('実行状況。1:未実施。2:実行中。3:実行終了。4:実行時エラーあり。5:異常終了');
            $table->integer('total_line')->unsigned()->nullable()->comment('ファイルの行数');
            $table->integer('total_count')->unsigned()->nullable()->comment('対象データ数');
            $table->integer('success_count')->unsigned()->nullable()->comment('成功データ数');
            $table->integer('failure_count')->unsigned()->nullable()->comment('失敗データ数');
            $table->text('message')->nullable()->comment('実行時メッセージ（エラーメッセージなど）');
            $table->softDeletesTz();
            $table->timestampsTz();

            $table->foreign('file_info_id')
                ->references('id')->on('azuki_file_info');
            
        });
        DB::statement("ALTER TABLE `".$this->table."` COMMENT '実行対象を状況とともに管理するテーブル'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
