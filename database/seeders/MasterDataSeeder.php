<?php
namespace Database\Seeders;

use Azuki\App\Database\Seeders\MasterDataSeeder as Seeder;

class MasterDataSeeder extends Seeder
{
    /**
     * システム管理者の初期ロール設定
     *
     */
    protected $systemRole = [
        [
            'name'      => 'スーパーバイザー',
            'value'     => 1,
            'order'     => 1,
            'deletable' => 1,  // １は削除不可、２が削除可能
            'role_authorities' => [
                [
                    'url'             => '*',
                    'authority'       => 1,
                ],
            ],
        ],
        [
            'name'  => 'ユーザー管理者',
            'value' => '2',
            'order' => '2',
            'role_authorities' => [
                [
                    'url'             => '*',
                    'authority'       => 2,
                ],
                [
                    'url'             => '*/users',
                    'authority'       => 1,
                ],
                [
                    'url'             => '*/users/*',
                    'authority'       => 1,
                ],
            ],
        ],
    ];

    /**
     * 運営管理者の初期ロール設定
     *
     */
    protected $manageRole = [
        [
            'name'      => 'スーパーバイザー',
            'value'     => 1,
            'order'     => 1,
            'deletable' => 1,
            'role_authorities' => [
                [
                    'url'             => '*',
                    'authority'       => 1,
                ],
            ],
        ],
        [
            'name'  => '一般',
            'value' => '2',
            'order' => '2',
            'role_authorities' => [
                [
                    'url'             => '*',
                    'authority'       => 2,
                ],
                [
                    'url'             => '*/managers',
                    'authority'       => 1,
                ],
                [
                    'url'             => '*/managers/*',
                    'authority'       => 1,
                ],
            ],
        ],
    ];

    /**
     * 一般ユーザーの初期ロール設定
     *
     */
    protected $commonRole = [
        [
            'name'      => '一般ユーザー',
            'value'     => 1,
            'order'     => 1,
            'deletable' => 1,
            'role_authorities' => [
                [
                    'url'             => '*',
                    'authority'       => 1,
                ],
            ],
        ],
    ];

    /**
     * システム切り替えスイッチの初期登録データ
     *
     */
    protected $switch = [
        [
            'title'  => 'ログ自動削除',
            'kind'   => SWITCH_KIND_AUTO_DELETE_LOGS,
            'status' => SWITCH_STATUS_IS_STOP,
        ],
    ];
}
