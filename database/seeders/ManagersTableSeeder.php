<?php
namespace Database\Seeders;

use Azuki\App\Database\Seeders\ManagersTableSeeder as Seeder;

class ManagersTableSeeder extends Seeder
{
    /**
     * 運営管理者ユーザーの初期登録データ
     *
     */
    protected $managers = [
        [
            'name'           => 'マスター管理者',
            'login_id'       => 'master@example.com',
            'email'          => 'master@example.com',
            'password'       => 'secret1234',
            'role'           => 1,
            'enable'         => 1,
        ],
    ];
}
