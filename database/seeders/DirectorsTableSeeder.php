<?php
namespace Database\Seeders;

use Azuki\App\Database\Seeders\DirectorsTableSeeder as Seeder;

class DirectorsTableSeeder extends Seeder
{
    /**
     * システム管理者初期登録データ
     *
     */
    protected $directors = [
        [
            'id'       => 1,  // id => 1　は削除不可
            'name'     => 'システム管理者',
            'email'    => 'azuki@example.com',
            'password' => 'secret1234',
            'role'     => 1,
            'enable'   => 1,
        ],
    ];
}
